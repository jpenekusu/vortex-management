pipeline {
    agent {label 'maven'}

    options {
        buildDiscarder(
            logRotator(
                daysToKeepStr: '45',
                artifactDaysToKeepStr: '45',
                numToKeepStr: '30',
                artifactNumToKeepStr: '30'
            )
        )
    }

    environment {
        GIT_COMMIT_SHORT = "${env.GIT_COMMIT}".take(8)
        MAVEN_OPTS = "-Drevision=${env.GIT_COMMIT_SHORT}"
        SONAR_OPTS = "-Dsonar.branch.name=${BRANCH_NAME} ${env.CHANGE_TARGET != null ? '-Dsonar.branch.target='+env.CHANGE_TARGET : ''}"
    }

    stages {
        stage('Build') {
            steps {
                withMaven(mavenSettingsConfig: 'vortex-settings', publisherStrategy: 'EXPLICIT') {
                    sh './mvnw clean compile'
                }
            }
        }

        // stage('Tests') {
        //     steps {
        //         withMaven(mavenSettingsConfig: 'vortex-settings') {
        //             sh './mvnw test'
        //         }
        //     }
        // }

        // stage('Static Analysis') {
        //     steps {
        //         script{
        //             withSonarQubeEnv('sonarOmogen') {
        //                 withMaven(mavenSettingsConfig: 'vortex-settings', publisherStrategy: 'EXPLICIT') {
        //                     sh './mvnw sonar:sonar ${SONAR_OPTS}'
        //                 }
        //             }
        //             timeout(time: 30, unit: 'MINUTES') {
        //                 qualitygate = waitForQualityGate(
        //                     abortPipeline: false
        //                 )
        //             }
        //             /*
        //             if ( qualitygate.status != "OK" ){
        //                 emailext(
        //                     body: "Erreur lors de l'analyse sonar de ${JOB_NAME}",
        //                     subject: "[${JOB_NAME}] Rapport sonar en erreur",
        //                     to: "omogen.agile-equipe@ac-dijon.fr, cassis.agile@ac-dijon.fr"
        //                 )
        //             }
        //             */
        //         }
        //     }
        // }

        stage('Publish') {
            parallel {
                stage('doc') {
                    steps{
                        sh( label: "Upload sur le serveur d'intégration de la doc",
                            script: """
                                npm install -D redoc-cli
                                npx redoc-cli bundle src/doc/yaml/omogen-management.openapi.yaml
                                mv redoc-static.html omogen-management.html
                                ssh -i ~/.ssh/id_rsa_integration integration@integration.in.ac-dijon.fr "mkdir -p /var/www/integration/openapi/omogen/vortex-management/${BRANCH_NAME}"
                                scp -i ~/.ssh/id_rsa_integration  omogen-management.html integration@integration.in.ac-dijon.fr:/var/www/integration/openapi/omogen/vortex-management/${BRANCH_NAME}/
                            """

                        )
                        archiveArtifacts artifacts: "omogen-management.html", fingerprint:true
                    }
                }
                stage('dev') {
                    when {
                        not {
                            branch 'release/*'
                        }
                    }
                    steps {
                        withMaven(mavenSettingsConfig: 'vortex-settings', publisherStrategy: 'EXPLICIT') {
                            sh './mvnw deploy -Dmaven.test.skip=true -P dev'
                        }
                        archiveArtifacts artifacts: "target/*.jar", fingerprint:true
                    }
                }
                stage('release') {
                    when {
                        branch 'release/*'
                    }
                    steps {
                        withMaven(mavenSettingsConfig: 'vortex-settings', publisherStrategy: 'EXPLICIT') {
                            sh './mvnw deploy -Dmaven.test.skip=true -P release'
                        }
                        archiveArtifacts artifacts: "target/*.jar", fingerprint:true
                    }
                }
            }
        }

        // TODO: Dockerisation à revoir :
        //         - Ajouter launcher vertx pour executer les migrations au démarrage (https://flywaydb.org/getstarted/firststeps/api#integrating-flyway)
        //         - Baser les Dockerfile sur les images vertx ? (http://vertx.io/docs/vertx-docker/)
        //         - S'assurer que ca fonctionne avec une BD dockerisée (compose ou pas) ou non
        /*
        stage('Docker') {
            when {
                anyOf {
                    branch 'develop'
                    branch 'release/*'
                    branch 'feature/OM-1394' // Ajout du context maven pour la génération du docker (get version from pom.xml)
                }
            }
            steps {
                configFileProvider([
                    configFile(
                    fileId: 'vortex-settings',
                    variable: 'MAVEN_SETTINGS'
                    )
                    ]) {
                    sh "sudo bash -x ./docker_build.sh ${env.GIT_COMMIT_SHORT} ${MAVEN_SETTINGS}"
                }
            }
        }*/
    }
}
