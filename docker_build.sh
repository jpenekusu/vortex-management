#!/bin/bash

# TODO: Devraient être des variables d'environnement
REGISTRY_DOCKER=harbor.in.ac-dijon.fr
REGISTRY_PROJECT=omogen

# test des paramètres
[[ -z ${1:?Le numéro de révision est manquant} ]]
[[ -z ${2:?Le ficher de setting maven est manquant} ]]

# récupération de la version dans le pom
version=$(./mvnw org.apache.maven.plugins:maven-help-plugin:3.1.0:evaluate -Dexpression=project.version -q -DforceStdout -Drevision=${1} -s ${2})

docker build -f vortex.management.flyway.DockerFile -t "${REGISTRY_DOCKER}"/"${REGISTRY_PROJECT}"/vortex.management.flyway:$version -t "${REGISTRY_DOCKER}"/"${REGISTRY_PROJECT}"/vortex.management.flyway:latest .
[[ "$?" != "0" ]] && exit 5
docker push "${REGISTRY_DOCKER}"/"${REGISTRY_PROJECT}"/vortex.management.flyway
[[ "$?" != "0" ]] && exit 6

docker build -t "${REGISTRY_DOCKER}"/"${REGISTRY_PROJECT}"/vortex.management:$version -t "${REGISTRY_DOCKER}"/"${REGISTRY_PROJECT}"/vortex.management:latest .
[[ "$?" != "0" ]] && exit 5
docker push "${REGISTRY_DOCKER}"/"${REGISTRY_PROJECT}"/vortex.management
[[ "$?" != "0" ]] && exit 6

exit 0
