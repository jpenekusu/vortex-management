# vortex management

Expose un ensemble de services pour l'administration, supervision et l'exploitation.

## Pour commencer

### Prérequis

- vortex-common doit être build
- ng vortex-management-webapp : build --base-href /supervision/

### Installation


```
mvn install
```

- copier le repertoire dist (du build de vortex-management-webapp) à coté du jar.

### Configuration

Fichier de configuration par défaut : `vortex-common/src/main/resources/conf/default.conf`  
Surcharge possible par :
  - un fichier au format HOCON : `-Dconf=vortex-management.conf`
  - des variables d'environnement
  - des propriétés système : `-Dvortex-management.http.port=8090`
  - des options de déploiement : `new DeploymentOptions()...`

Exemple :
```json
vortex-management {
    database {
        ## the JDBC connection URL for the database
        url = "jdbc:postgresql://localhost:5432/postgres?currentSchema=management"

        ## the class of the JDBC driver
        driver_class = "org.postgresql.Driver"

        ## the username for the database
        user = "postgres"

        ## the password for the database
        password = ""
    }

    http {
        ## URL du serveur HTTP
        host = "0.0.0.0"

        ## port d'écoute du serveur HTTP, default = 8081
        port = 8081
    }
    
    ### Bloc permettant de configurer l'authentification par LDAP 
    ldap  {
        ## Url du ldap (url = ldaps://nom_du_serveur:port )
        url = "xxx_adresse_ldap"
        
        ## template du DN       
        dn_template = "ou=xxx,ou=aaa"
        
        ##Filtre de recherche
        filter_search="uid={0}"
        
        ## Uniquement si le ldap est securise (ces champs ne sont pas necessaires pour un ldap non securise)
        ## Utilisateur ldap pour effectuer la recherche
        #user_ldap_recherche = "cn=xxxx,dc=organisation"
        
        ## Mot de passe de l'utilisateur ldap pour effectuer la recherche
        #pwd_ldap_recherche = "xxx_xxx"
    }
}
```

### Exécution

Lancement en mode cluster

```
cd $repertoireProjet-vortex-management
java -Dfile.encoding=UTF-8 -Dvertx.hazelcast.config=<chemin vers cluster.xml> -jar ./target/vortex-management-1.0.0-SNAPSHOT-fat.jar run fr.edu.vortex.management.MainVerticle --cluster
```

Pour plus d'informations : [Hazelcast Cluster Manager](https://vertx.io/docs/vertx-hazelcast/java/#_using_this_cluster_manager)



#### Configurer les logs

La configuration des logs se fait en passant des propriétés système.

##### En utilisant JUL  

Sans passer de propriété système c'est JUL qui est utilisé et la configuration est celle du fichier `vertx-default-jul-logging.properties` contenu dans le JAR. Il est possible de surcharger le fichier de configuration en passant la propriété système `java.util.logging.config.file`.

**Exemple**

```
  java -Dfile.encoding=UTF-8 -Djava.util.logging.config.file=/tmp/logging.properties -Dvertx.hazelcast.config=../cluster.xml -jar target/vortex-management-1.0.0-SNAPSHOT-fat.jar run fr.edu.vortex.management.MainVerticle -cluster
```

##### En utilisant SL4J

Il est possible d'utiliser la framework SL4J avec logback pour ce faire il faut utiliser 
les propriétés système suivantes :

 - `vertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory` 
    - permet d'utiliser la framework  SLF4J
 - `hazelcast.logging.type=slf4j` 
    - pour que les logs de hazelcast utilise la framework SL4J
 - `logback.configurationFile=logback.xml` 
    - emplacement du fichier de configuration si cette propriété n'est pas définie alors le fichier de configuration contenu dans le jar est utilisé.
    

**Exemple**

```
	java -Dfile.encoding=UTF-8 -Dlogback.configurationFile=/tmp/config.xml -Dhazelcast.logging.type=slf4j -Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory -Dvertx.hazelcast.config=../cluster.xml -jar target/vortex-management-1.0.0-SNAPSHOT-fat.jar run fr.edu.vortex.management.MainVerticle -cluster
```

## Test unitaire local

Afin que les tests unitaires puissent s'exécuter, il faut une base de données postgresql joignable sur le port 5433.

Une image docker à été créé dans ce but.
Les tests attende la base sur l'adresse "postgres"
Ajouter la ligne suivante dans votre fichier hosts
`127.0.0.1 postgres`
Voici les commandes à éxécuter afin de pouvoir jouer les tests

Sous linux

```
OMOGEN_DB_PASSWORD=omogen
export MAVEN_REPO_URL=http://nexus3.dpn.tsuna.fr:8081/repository
docker pull registry.gitlab.com/k6-dijon/vortex-test-db:develop
docker run -d -p 5433:5432 -e POSTGRES_DB="omogen" -e POSTGRES_USER="omogen" -e POSTGRES_PASSWORD="${OMOGEN_DB_PASSWORD}" -e POSTGRES_HOST_AUTH_METHOD="trust" --name="vortex-pg" registry.gitlab.com/k6-dijon/vortex-test-db:develop
./mvnw -s .m2/settings.xml -Dflyway.user=omogen -Dflyway.password="$OMOGEN_DB_PASSWORD" -Dflyway.schemas=management -Dflyway.url=jdbc:postgresql://postgres:5433/management flyway:migrate
./mvnw -s .m2/settings.xml test
docker stop vortex-pg
docker rm vortex-pg
```

Sous Windows

```
SET OMOGEN_DB_PASSWORD=omogen
SET MAVEN_REPO_URL=http://nexus3.dpn.tsuna.fr:8081/repository
docker pull registry.gitlab.com/k6-dijon/vortex-test-db:develop
docker run -d -p 5433:5432 -e POSTGRES_DB="omogen" -e POSTGRES_USER="omogen" -e POSTGRES_PASSWORD="%OMOGEN_DB_PASSWORD%" -e POSTGRES_HOST_AUTH_METHOD="trust" --name="vortex-pg" registry.gitlab.com/k6-dijon/vortex-test-db:develop
.\mvnw.cmd -s .m2/settings.xml -Dflyway.user=omogen -Dflyway.password="%OMOGEN_DB_PASSWORD%" -Dflyway.schemas=management -Dflyway.url=jdbc:postgresql://postgres:5433/management flyway:migrate
.\mvnw.cmd -s .m2/settings.xml test
docker stop vortex-pg
docker rm vortex-pg
```

## Déploiement

Ajouter les informations pour déployer sur un autre type d'environnement.

## Pour contribuer

- Liens sur page confluence editorConfig
- Liens sur page confluence checkstyle
- Liens sur page d'aide sur pull request
- Format for commit messages

## Projets liés

liste des projets (hyperlien bitbucket) en lien avec ce projet


