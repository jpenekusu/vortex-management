//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.notification;

import java.util.HashMap;
import java.util.Map;

public enum EtatEmissionNotificationEnum {
    NON_EMIS(0), EMIS(1), EMISSION_EN_ERREUR(2);

    private final Integer value;
    private static Map<Integer, EtatEmissionNotificationEnum> map = new HashMap<>();

    EtatEmissionNotificationEnum(Integer value) {
        this.value = value;
    }

    static {
        for (EtatEmissionNotificationEnum etatEmission : EtatEmissionNotificationEnum.values()) {
            map.put(etatEmission.value, etatEmission);
        }
    }

    public static EtatEmissionNotificationEnum valueOf(Integer etatEmission) {
        return (EtatEmissionNotificationEnum) map.get(etatEmission);
    }

    public Integer getValue() {
        return value;
    }
}
