//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.notification;

import java.io.Serializable;

import fr.edu.vortex.management.persistence.PersistableAsJsonObject;
import io.vertx.core.json.JsonObject;

public class Notification implements PersistableAsJsonObject, Serializable {

    private static final long serialVersionUID = 7581226836698585948L;

    public static final String KEY_JSON_ID = "id";
    public static final String KEY_JSON_CORRELATION_ID = "correlation_id";
    public static final String KEY_JSON_TYPE_NOTIFICATION = "type";
    public static final String KEY_JSON_ETAT = "etat";
    public static final String KEY_JSON_ETAT_EMISSION = "etat_emission";

    public static final String KEY_DB_ID = "id";
    public static final String KEY_DB_CORRELATION_ID = "correlation_id";
    public static final String KEY_DB_TYPE_NOTIFICATION = "type";
    public static final String KEY_DB_ETAT = "etat";
    public static final String KEY_DB_ETAT_EMISSION = "etat_emission";

    private Integer id;
    private String correlationId;
    private TypeNotificationEnum type;
    private EtatNotificationEnum etat;
    private EtatEmissionNotificationEnum etatEmission;

    public Notification(String correlationId, TypeNotificationEnum type, EtatNotificationEnum etat,
            EtatEmissionNotificationEnum etatEmission) {
        this.setCorrelationId(correlationId);
        this.setType(type);
        this.setEtat(etat);
        this.setEtatEmission(etatEmission);
    }

    public Notification(Integer id, String correlationId, TypeNotificationEnum type, EtatNotificationEnum etat,
            EtatEmissionNotificationEnum etatEmission) {
        this.setId(id);
        this.setCorrelationId(correlationId);
        this.setType(type);
        this.setEtat(etat);
        this.setEtatEmission(etatEmission);
    }

    public Notification(JsonObject jsonObject) {
        if (jsonObject != null) {
            this.setId(jsonObject.getInteger(KEY_JSON_ID));
            this.setCorrelationId(jsonObject.getString(KEY_JSON_CORRELATION_ID));
            this.setType(TypeNotificationEnum.valueOf(jsonObject.getString(KEY_JSON_TYPE_NOTIFICATION)));
            this.setEtat(EtatNotificationEnum.valueOf(jsonObject.getString(KEY_JSON_ETAT)));
            this.setEtatEmission(EtatEmissionNotificationEnum.valueOf(jsonObject.getString(KEY_JSON_ETAT_EMISSION)));
        }
    }

    @Override
    public JsonObject toJsonObject() {
        JsonObject jsonToEncode = new JsonObject();

        jsonToEncode.put(KEY_JSON_ID, this.getId());
        jsonToEncode.put(KEY_JSON_CORRELATION_ID, this.getCorrelationId());
        jsonToEncode.put(KEY_JSON_TYPE_NOTIFICATION, this.getType().getValue());
        jsonToEncode.put(KEY_JSON_ETAT, this.getEtat().getValue());
        jsonToEncode.put(KEY_JSON_ETAT_EMISSION, this.getEtatEmission().getValue());

        return jsonToEncode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public TypeNotificationEnum getType() {
        return type;
    }

    public void setType(TypeNotificationEnum type) {
        this.type = type;
    }

    public EtatNotificationEnum getEtat() {
        return etat;
    }

    public void setEtat(EtatNotificationEnum etat) {
        this.etat = etat;
    }

    public EtatEmissionNotificationEnum getEtatEmission() {
        return etatEmission;
    }

    public void setEtatEmission(EtatEmissionNotificationEnum etatEmission) {
        this.etatEmission = etatEmission;
    }

}