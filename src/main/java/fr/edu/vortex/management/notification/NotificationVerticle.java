//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.notification;

import fr.edu.echanges.nat.vortex.common.adresses.notification.NotificationAdresses;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.persistence.dao.NotificationDAO;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class NotificationVerticle extends AbstractVerticle {

    private static final Logger logger = LoggerFactory.getLogger(NotificationVerticle.class);
    private NotificationDAO notificationDAO;

    @Override
    public void start() throws Exception {

        notificationDAO = (notificationDAO == null) ? MainVerticle.notificationDAO : notificationDAO;

        vertx.eventBus().consumer(NotificationAdresses.GET_NOTIFICATION_BY_CORRELATION_ID)
                .handler(this::getNotificationByCorrelationId);

        vertx.eventBus().consumer(NotificationAdresses.ACQUITTEMENT_MESSAGE_OK).handler(this::acquittementMessageOk);
        vertx.eventBus().consumer(NotificationAdresses.ACQUITTEMENT_MESSAGE_ERREUR)
                .handler(this::acquittementMessageErreur);
        vertx.eventBus().consumer(NotificationAdresses.ACQUITTEMENT_EMISSION_OK).handler(this::acquittementEmissionOk);
        vertx.eventBus().consumer(NotificationAdresses.ACQUITTEMENT_EMISSION_ERREUR)
                .handler(this::acquittementEmissionErreur);
    }

    protected void setNotificationDAO(NotificationDAO notificationDAO) {
        this.notificationDAO = notificationDAO;
    }

    private void acquittementMessageOk(Message<Object> message) {
        String action = "acquittementMessageOk(Message<Object> message)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        this.createNotification(message, TypeNotificationEnum.ACK, EtatNotificationEnum.OK,
                EtatEmissionNotificationEnum.NON_EMIS);
    }

    private void acquittementMessageErreur(Message<Object> message) {
        String action = "acquittementMessageErreur(Message<Object> message)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        this.createNotification(message, TypeNotificationEnum.ACK, EtatNotificationEnum.EN_ERREUR,
                EtatEmissionNotificationEnum.NON_EMIS);
    }

    private void acquittementEmissionOk(Message<Object> message) {
        String action = "acquittementEmissionOk(Message<Object> message)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        this.updateEtatEmissionNotification(message, TypeNotificationEnum.ACK, EtatEmissionNotificationEnum.EMIS);
    }

    private void acquittementEmissionErreur(Message<Object> message) {
        String action = "acquittementEmissionErreur(Message<Object> message)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        this.updateEtatEmissionNotification(message, TypeNotificationEnum.ACK,
                EtatEmissionNotificationEnum.EMISSION_EN_ERREUR);
    }

    private void getNotificationByCorrelationId(Message<Object> message) {
        String action = "getNotificationByCorrelationId(Message<Object> message)";
        String correlationId = null;
        try {
            correlationId = (String) message.body();
        } catch (ClassCastException e) {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
        }

        if (correlationId != null) {
            final String correlationIdFinal = correlationId;
            notificationDAO.getAllNotificationsByCorrelationId(correlationIdFinal).setHandler(getAllAr -> {
                if (getAllAr.succeeded()) {
                    logger.debug(Constants.LOG_MESSAGE, correlationIdFinal, action + " OK");
                    JsonArray jsonArray = new JsonArray();
                    for (Notification notif : getAllAr.result()) {
                        jsonArray.add(notif.toJsonObject());
                    }
                    if (jsonArray.size() > 0) {
                        message.reply(jsonArray);
                    } else {
                        message.fail(204, "Aucune notification trouvée en BD");
                    }
                } else {
                    logger.error(Constants.LOG_ERROR, correlationIdFinal, action, null,
                            getAllAr.cause());
                    message.fail(500, getAllAr.cause().getMessage());
                }
            });
        } else {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "Wrong parameters");
            message.fail(500, "Wrong parameters");
        }

    }

    private void createNotification(Message<Object> message, TypeNotificationEnum typeNotif,
            EtatNotificationEnum etatNotif, EtatEmissionNotificationEnum etatEmissionNotif) {
        String action = "createNotification";
        String correlationId = null;
        try {
            correlationId = (String) message.body();
        } catch (ClassCastException e) {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
        }

        if (correlationId != null) {
            final String correlationIdFinal = correlationId;
            Notification notif = new Notification(correlationIdFinal, typeNotif, etatNotif, etatEmissionNotif);
            notificationDAO.save(notif).setHandler(saveAr -> {
                if (saveAr.succeeded()) {
                    Notification newNotif = (Notification) saveAr.result();
                    logger.debug(Constants.LOG_MESSAGE, correlationIdFinal,
                            "Notification de type '" + typeNotif.getValue() + "' sauvegardée en BD avec l'état "
                                    + etatNotif.getValue());
                    message.reply(newNotif.toJsonObject());
                } else {
                    logger.error(Constants.LOG_ERROR, correlationIdFinal, action, null,
                            saveAr.cause());
                    message.fail(500, saveAr.cause().getMessage());
                }
            });
        } else {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "Wrong parameters");
        }
    }

    private void updateEtatEmissionNotification(Message<Object> message, TypeNotificationEnum typeNotif,
            EtatEmissionNotificationEnum etatEmissionNotif) {

        String action = "updateNotification";
        String correlationId = null;
        try {
            correlationId = (String) message.body();
        } catch (ClassCastException e) {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
        }

        if (correlationId != null) {
            final String correlationIdFinal = correlationId;
            notificationDAO
                    .getNotificationByCorrelationIdAndTypeNotification(correlationIdFinal, typeNotif)
                    .setHandler(getNotifAr -> {
                        if (getNotifAr.succeeded()) {
                            Notification notif = getNotifAr.result();
                            notificationDAO
                                    .updateEtatEmissionNotification(notif.getId(), etatEmissionNotif)
                                    .setHandler(updateNotifAr -> {
                                        if (updateNotifAr.succeeded()) {
                                            Notification updatedNotif = (Notification) updateNotifAr.result();
                                            logger.debug(Constants.LOG_MESSAGE, correlationIdFinal,
                                                    "Notification de type '" + typeNotif.getValue()
                                                            + "' maj en BD avec le statut d'émission "
                                                            + etatEmissionNotif.getValue());
                                            message.reply(updatedNotif.toJsonObject());
                                        } else {
                                            logger.error(Constants.LOG_ERROR, correlationIdFinal, action, null,
                                                    updateNotifAr.cause());
                                            message.fail(500, updateNotifAr.cause().getMessage());
                                        }
                                    });

                        } else {

                            logger.error(Constants.LOG_ERROR, correlationIdFinal, action, null,
                                    getNotifAr.cause());
                            message.fail(500, getNotifAr.cause().getMessage());
                        }
                    });

        } else {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "Wrong parameters");
        }

    }

}
