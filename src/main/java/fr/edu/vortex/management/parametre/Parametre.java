//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.parametre;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import fr.edu.vortex.management.persistence.PersistableAsJsonObject;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Parametre implements PersistableAsJsonObject, Serializable {

	private static final long serialVersionUID = 5176135790397182973L;
	
	public static final String CODE_AUTO_ENREGISTREMENT = "AUTO_ENREGISTREMENT";
	public static final String VALEUR_AUTO_ENREGISTREMENT_ACTIF = "1";

	public static final String KEY_JSON_CODE = "code";
	public static final String KEY_JSON_VALEUR = "valeur";

	public static final String KEY_DB_CODE = "code";
	public static final String KEY_DB_VALEUR = "valeur";

	private String code;

	private String valeur;

	public Parametre(String code, String valeur) {
		super();
		this.code = code;
		this.setValeur(valeur);
	}

	public Parametre(JsonObject jsonObject) {
		if (jsonObject != null) {
			this.setCode(jsonObject.getString(KEY_JSON_CODE));
			this.setValeur(jsonObject.getString(KEY_JSON_VALEUR));
		}
	}

	public static Parametre fromJson(JsonObject json) {
		return Json.mapper.convertValue(json.getMap(), Parametre.class);
	}

	@Override
	public JsonObject toJsonObject() {
		JsonObject jsonToEncode = new JsonObject();
		jsonToEncode.put(KEY_JSON_CODE, this.getCode());
		jsonToEncode.put(KEY_JSON_VALEUR, this.getValeur());
		return jsonToEncode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValeur() {
		return valeur;
	}

	public void setValeur(String valeur) {
		this.valeur = valeur;
	}

}
