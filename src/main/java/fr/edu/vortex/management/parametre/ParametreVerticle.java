//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.parametre;

import java.net.HttpURLConnection;
import java.util.Optional;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.persistence.dao.ParametreDAO;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ParametreVerticle extends AbstractVerticle {

	private static final Logger logger = LoggerFactory.getLogger(ParametreVerticle.class);

	public static final String LISTER_ROLE = "vortex.management.role.lister";

	private ParametreDAO parametreDAO;

	@Override
	public void start() throws Exception {

		parametreDAO = MainVerticle.parametreDAO;
		vertx.eventBus().consumer(ParametreAdresses.GET_PARAMETRE_BY_CODE).handler(this::getParametreByCode);

	}

	private void getParametreByCode(Message<Object> message) {
		String action = "Récupération d'un parametre par son code";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		JsonObject jsonMessage = (JsonObject) message.body();
		String code = jsonMessage.getString(Parametre.KEY_JSON_CODE);

		if (code == null || "".equals(code)) {
			message.fail(400, "Le code du paramêtre n'est pas renseigné");
		}

		parametreDAO.getParametreByCode(code).setHandler(getParamAr -> {
			if (getParamAr.succeeded()) {
				Optional<Parametre> param = getParamAr.result();
				if (param.isPresent()) {
					message.reply(param.get().toJsonObject());
				} else {
					message.fail(404, "Le paramêtre n'existe pas");
				}
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", getParamAr.cause());
				message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR,
						"Erreur lors de la récupération d'un paramêtre par son code");
			}
		});

	}

}