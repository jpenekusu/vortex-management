//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.auth;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.OptionalInt;
import java.util.Random;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class HashUtils {

    private static final int MIN_ITERATIONS = 5000;
    private static final int MAX_ITERATIONS = 20000;
    private static final String ALGORITHM = "PBKDF2WithHmacSHA1";

    public static HashInfo generateStorngPasswordHash(String password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        int iterations = getRandomNumberInRange();
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();

        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(ALGORITHM);
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return new HashInfo(toHex(hash), toHex(salt), iterations);
    }

    public static boolean validatePassword(String originalPassword, HashInfo storedPasswordInfo)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        int iterations = storedPasswordInfo.iterations;
        byte[] salt = fromHex(storedPasswordInfo.saltUsed);
        byte[] hash = fromHex(storedPasswordInfo.hashedPassword);

        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(ALGORITHM);
        byte[] testHash = skf.generateSecret(spec).getEncoded();

        int diff = hash.length ^ testHash.length;
        for (int i = 0; i < hash.length && i < testHash.length; i++) {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[32];
        sr.nextBytes(salt);
        return salt;
    }

    private static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    private static byte[] fromHex(String hex) {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    private static int getRandomNumberInRange() {

        Random r = new Random();
        OptionalInt rdmOpt = r.ints(MIN_ITERATIONS, (MAX_ITERATIONS + 1)).findFirst();
        if (rdmOpt.isPresent()) {
            return rdmOpt.getAsInt();
        } else {
            return 10666;
        }

    }

    public static class HashInfo {
        private String hashedPassword;
        private String saltUsed;
        private Integer iterations;

        public HashInfo(String hashedPassword, String saltUsed, Integer iterations) {
            this.hashedPassword = hashedPassword;
            this.saltUsed = saltUsed;
            this.iterations = iterations;
        }

        public String getHashedPassword() {
            return hashedPassword;
        }

        public String getSaltUsed() {
            return saltUsed;
        }

        public Integer getIterations() {
            return iterations;
        }

        public String toString() {
            return hashedPassword + ":" + saltUsed + ":" + iterations;
        }

    }
}
