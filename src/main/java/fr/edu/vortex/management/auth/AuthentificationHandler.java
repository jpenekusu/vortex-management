//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.auth;

import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureEvtTrace;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureTypeTrace;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.TraceVerticle;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.web.RoutingContext;

public class AuthentificationHandler implements Handler<RoutingContext> {

    private static final Logger log = LoggerFactory.getLogger(AuthentificationHandler.class);
    private static final String DEFAULT_USERNAME = "username";
    private static final String DEFAULT_PASSWORD = "password";

    private final VortexAuthProvider jwtProvider;
    private final LoginAuthProvider loginAuth;
    private final LDAPAuthProvider ldapAuth;

    private String usernameParam = DEFAULT_USERNAME;
    private String passwordParam = DEFAULT_PASSWORD;

    private Vertx vertx;

    public AuthentificationHandler setUsernameParam(String usernameParam) {
        this.usernameParam = usernameParam;
        return this;
    }

    public AuthentificationHandler setPasswordParam(String passwordParam) {
        this.passwordParam = passwordParam;
        return this;
    }

    public AuthentificationHandler(VortexAuthProvider jwtProvider, Vertx vertx) {
        this.jwtProvider = jwtProvider;
        this.vertx = vertx;

        // Création des providers de login et de ldap
        this.loginAuth = new LoginAuthProvider(vertx);
        this.ldapAuth = new LDAPAuthProvider(vertx);
    }

    public AuthentificationHandler(VortexAuthProvider jwtProvider, String usernameParam,
            String passwordParam, Vertx vertx) {
        this.jwtProvider = jwtProvider;
        this.usernameParam = usernameParam;
        this.passwordParam = passwordParam;
        this.vertx = vertx;
        // Création des providers de login et de ldap
        this.loginAuth = new LoginAuthProvider(vertx);
        this.ldapAuth = new LDAPAuthProvider(vertx);
    }

    public void handle(RoutingContext context) {

        HttpServerRequest req = context.request();
        if (req.method() != HttpMethod.POST) {
            context.fail(405); // Must be a POST
            return;
        }

        String username = "";
        String password = "";

        switch (req.getHeader("Content-Type")) {
        case "application/x-www-form-urlencoded; charset=utf-8": {
            if (!req.isExpectMultipart()) {
                log.warn("Form body not parsed - do you forget to include a BodyHandler?");
                // throw new IllegalStateException("Form body not parsed - do you forget to
                // include a BodyHandler?");
            }
            MultiMap params = req.formAttributes();
            username = params.get(usernameParam);
            password = params.get(passwordParam);

            break;
        }
        case "application/json": {
            JsonObject bodyAsJson = context.getBodyAsJson();
            username = bodyAsJson.getString(usernameParam);
            password = bodyAsJson.getString(passwordParam);
            break;
        }
        default: {
            context.response().setStatusMessage("form attributes or json expected");
            context.fail(400);
            return;
        }
        }

        if (username == null || password == null) {
            String message = usernameParam + " " + passwordParam + " expected";
            log.warn(message);
            context.response().setStatusMessage(message);
            context.fail(400);
            return;
        }

        JsonObject authInfo = new JsonObject().put("username", username).put("password", password);

        // Vérif si login exite dans la base du management et recuperation de son type
        // d'authentification
        // SI LDAP, on s'identifie LDAP
        // Sinon on s'identifie avec login/password de la BD management
        recuperationUtilisateurViaLogin(result -> {
            if (!result.succeeded()) {
                // TODO A tester pour recup message
                context.response().setStatusMessage(result.cause().getMessage());
                context.fail(403);
                String idIdentite = authInfo.getString("username");
                traceEvent(NomenclatureTypeTrace.Utilisateur.getValeur(), NomenclatureEvtTrace.Connexion_Refusee.getValeur(), authInfo.getString("username"),
                		null, null, Constants.TRACE_ORIGINE_CASSIS, idIdentite, null);
                return;
            }

            AuthProvider authProviderCourant = null;
            Utilisateur utilisateur = result.result();
            String typeAuthentification = utilisateur.getTypeAuthentification();
            switch (typeAuthentification) {
            case Utilisateur.AUTHENTIFICATION_LOCALE: {
                authProviderCourant = loginAuth;
                log.info(LOG_MESSAGE, LOG_NO_CORRELATION_ID, "Authentification via la BD");
                break;
            }
            case Utilisateur.AUTHENTIFICATION_LDAP: {
                authProviderCourant = ldapAuth;
                log.info(LOG_MESSAGE, LOG_NO_CORRELATION_ID, "Authentification via le LDAP");
                break;
            }

            default: {
                authProviderCourant = loginAuth;
                break;
            }
            }

            authProviderCourant.authenticate(authInfo, res -> {
                if (res.failed()) {
                    log.error(LOG_MESSAGE, LOG_NO_CORRELATION_ID, "Error d'Authentification ");
                    context.response().setStatusMessage(res.cause().getMessage());
                    context.fail(403); // Failed login unauthorized !
                    
                    String idIdentite = authInfo.getString("username");
                    traceEvent(NomenclatureTypeTrace.Utilisateur.getValeur(), NomenclatureEvtTrace.Connexion_Refusee.getValeur(), authInfo.getString("username"),
                    		null, null, Constants.TRACE_ORIGINE_CASSIS, idIdentite, String.valueOf(utilisateur.getId()));
                    return;
                }
                JsonObject claims = utilisateur.toJson();
                
                // generer JWT
                jwtProvider.generateToken(claims, new JWTOptions()).setHandler(arToken -> {
                	if (arToken.failed()) {
                		context.response().setStatusMessage("fail jwt generation");
                		context.fail(500);
                		return;                		
                	} else {              	
                		String token = arToken.result();
                		String idIdentite = utilisateur.getLogin() + " (" + utilisateur.getNom() + " " + utilisateur.getPrenom() + ")";
                		traceEvent(NomenclatureTypeTrace.Utilisateur.getValeur(), NomenclatureEvtTrace.Connexion_Acceptee.getValeur(), utilisateur.getLogin(),
                				utilisateur.getNom(), utilisateur.getPrenom(), Constants.TRACE_ORIGINE_CASSIS, idIdentite, String.valueOf(utilisateur.getId()));

                		context.response()
                		.setStatusCode(201)
                		.putHeader("Authorization", token)
                		.end();
                	}

                });
 
            });

        }, username);

    }

    private void recuperationUtilisateurViaLogin(Handler<AsyncResult<Utilisateur>> resultHandler,
            String username) {
        JsonObject filtres = new JsonObject();
        filtres.put(Utilisateur.KEY_JSON_LOGIN, "=" + username);
        filtres.put(FluxVerticle.KEY_JSON_PAGE, "1");
        filtres.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, "20");

        vertx.eventBus().send(UtilisateurVerticle.LISTER_UTILISATEUR, filtres, ar -> {
            if (ar.failed()) {
                resultHandler.handle(Future.failedFuture("Echec lors de l'authentification"));
                return;
            }

            JsonArray listeUtilisateurs = (JsonArray) ar.result().body();
            switch (listeUtilisateurs.size()) {
            case 0: {
                resultHandler
                        .handle(Future.failedFuture("Aucun utilisateur n'existe pour ce login dans l'application"));
                break;
            }
            case 1: {
                JsonObject utilisateurJsonObject = listeUtilisateurs.getJsonObject(0);
                Utilisateur utilisateur = Utilisateur.fromJson(utilisateurJsonObject);
                resultHandler.handle(Future.succeededFuture(utilisateur));
                break;
            }
            default: {
                resultHandler.handle(Future.failedFuture("Echec lors de l'authentification"));
                break;
            }
            }
        });
    }
    
	private void traceEvent(String typeElement, String event, String login, String nom, String prenom, String origine, String idEntite, String idElement) {
		JsonObject jsonTrace = new JsonObject();

		jsonTrace.put("typeElement", typeElement);
		jsonTrace.put("event", event);
		jsonTrace.put("origine", origine);
		jsonTrace.put("login", login);
		jsonTrace.put("nom", nom);
		jsonTrace.put("prenom", prenom);
		jsonTrace.put("idEntite", idEntite);
		if (idElement != null) {
			jsonTrace.put("idElement", idElement);
		}

		vertx.eventBus().send(TraceVerticle.CREER_TRACE_LOGIN_OU_INSCRIPTION, jsonTrace);
	}

}
