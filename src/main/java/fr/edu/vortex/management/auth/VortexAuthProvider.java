//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.auth;

import static fr.edu.vortex.management.Constants.LOG_ERROR_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;

import java.security.KeyPair;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import fr.edu.vortex.management.utilisateur.pojo.Permission;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.AsyncMap;
import io.vertx.ext.auth.User;
import io.vertx.ext.jwt.JWTOptions;

public class VortexAuthProvider {

	private static final Logger logger = LoggerFactory.getLogger(VortexAuthProvider.class);

	private static final String ISSUER = "VortexAuthProvider";
	// a modifier avec config pour une instance de vortex donnee example : OMOGEN
	// ...
	private static final String AUDIENCE = "VortexInstance";


	private Vertx vertx;

	private static final Duration DEFAULT_VALIDITY_PERIOD =  Duration.ofMinutes(120);
	private Duration validity_periode = DEFAULT_VALIDITY_PERIOD;

	public VortexAuthProvider(Vertx vertx, Duration validity_periode) {
		if(validity_periode==null || validity_periode.isNegative() || validity_periode.isZero()){
			logger.warn(" validity_period => defaulting to " + DEFAULT_VALIDITY_PERIOD.toString() )  ;
			validity_periode = DEFAULT_VALIDITY_PERIOD;
		}
		this.validity_periode = validity_periode;
		logger.warn(" validity_period => set to " + this.validity_periode.toString() )  ;
		this.vertx = vertx;
	}


	public Future<String> generateToken(JsonObject claims, JWTOptions options) {
		Future<String> future = Future.future();		
		getKeyPair().setHandler(arKey -> {
			if (arKey.failed()) {
				future.fail(arKey.cause());
			} else {

				KeyPair key = arKey.result();				
				Instant expiredAt = Instant.now().plus(validity_periode);
				Utilisateur utilisateur = Utilisateur.fromJson(claims);
				String jws = null;
				try {
					jws = Jwts.builder()
							.setHeaderParam(JwsHeader.KEY_ID, "VortexAutoKey")
							.setId(UUID.randomUUID().toString())
							.setIssuer(ISSUER)
							.setAudience(AUDIENCE)
							.setIssuedAt(Date.from(Instant.now()))
							.setExpiration(Date.from(expiredAt))
							.setSubject(utilisateur.getLogin())
							.addClaims(getClaims(utilisateur))
							.signWith(key.getPrivate())
							.compact();

				} catch (Exception ex) {
					ex.printStackTrace();
				}				
				future.complete(jws);
			}
		});
		return future;
	}

	public void authenticate(JsonObject authInfo, Handler<AsyncResult<User>> resultHandler) {

		getKeyPair().setHandler(arKey -> {
			if (arKey.failed()) {
				resultHandler.handle(Future.failedFuture("key not initialized !"));
			} else {

				KeyPair key = arKey.result();

				String jwtToken = authInfo.getString("jwt");
				
				if (jwtToken == null || jwtToken.isEmpty()) {
					logger.error(
							LOG_ERROR_WITH_DATA,
							LOG_NO_CORRELATION_ID,
							String.format("jeton non renseigné"),
							"ERR_MANAGEMENT_AUTHENFICATION_JWT");
					resultHandler.handle(Future.failedFuture("jeton non renseigné"));
					return;
				}


				Jws<Claims> jws;
				try {
					jws = Jwts.parser()
							.setAllowedClockSkewSeconds(180) // <---- authorise 3 min de decalage (entre machine )pour la
							// gestion des timestamp
							.setSigningKey(key.getPublic())
							.requireAudience(AUDIENCE)
							.requireIssuer(ISSUER)
							.parseClaimsJws(jwtToken);

					VortexUser user = new VortexUser(getUtilisateur(jws.getBody()));

					resultHandler.handle(Future.succeededFuture(user));
				} catch (JwtException ex) {

					resultHandler.handle(Future.failedFuture(ex));
				}
			}
		});

	}

	// TODO verify signature par un access https sur
	// https://YOUR_DOMAIN/.well-known/jwks.json

	private Map<String, Object> getClaims(Utilisateur utilisateur) {

		Map<String, Object> claims = new HashMap<>();

		claims.put("login", utilisateur.getLogin());
		claims.put("nom", utilisateur.getNom());
		claims.put("id", utilisateur.getId());
		claims.put("prenom", utilisateur.getPrenom());
		claims.put("email", utilisateur.getEmail());
		claims.put("type_authentification", utilisateur.getTypeAuthentification());
		if (utilisateur.getRole() != null) {
			claims.put("role", utilisateur.getRole().toJson());
		}
		return claims;
	}

	private Utilisateur getUtilisateur(Claims claims) {
		Utilisateur utilisateur = null;
		if (claims != null) {
			utilisateur = new Utilisateur();
			utilisateur.setLogin((String) claims.get("login"));
			utilisateur.setNom((String) claims.get("nom"));
			utilisateur.setId((Integer) claims.get("id"));
			utilisateur.setPrenom((String) claims.get("prenom"));
			utilisateur.setEmail((String) claims.get("email"));
			utilisateur.setTypeAuthentification((String) claims.get("type_authentification"));
			LinkedHashMap map = (LinkedHashMap)claims.get("role");
			if (map != null && map.get("map") != null){
				LinkedHashMap roleMap = (LinkedHashMap)map.get("map");
				Role role = new Role();
				role.setId((Integer)roleMap.get("id"));
				role.setSysteme((Boolean)roleMap.get("systeme"));
				role.setLibelle((String) roleMap.get("libelle") );
				role.setPermissions((List<Permission>) roleMap.get("permissions"));    			
				utilisateur.setRole(role); 			
			}

		}
		return utilisateur;
	}

	private Future<KeyPair> getKeyPair(){
		Future<KeyPair> future = Future.future();
		vertx.sharedData().<String, KeyPair>getAsyncMap(AuthManager.SHARED_VORTEX_AUTH_KEY, res -> {
			if (res.failed()) {
				future.fail("Impossible de recuperer l'asyncmap : " + AuthManager.SHARED_VORTEX_AUTH_KEY);
				return;
			}

			AsyncMap<String, KeyPair> map = res.result();
			map.get(AuthManager.MAIN_KEY, getMapRes -> {
				if (getMapRes.failed()){
					future.fail("Impossible de recuperer la cle "+ AuthManager.MAIN_KEY+ " de l'asyncmap " + AuthManager.SHARED_VORTEX_AUTH_KEY);
					return;
				}

				final KeyPair keyPair = getMapRes.result();
				if (keyPair == null) {
					future.fail("Probleme avec la recuperation de cle ");
				}
				future.complete(keyPair);
			});
		});
		return future;
	}
}
