//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.auth;

import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AbstractUser;
import io.vertx.ext.auth.AuthProvider;

public class VortexUser extends AbstractUser {

    private Utilisateur utilisateur;
    private AuthProvider provider;

    public VortexUser(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Override
    protected void doIsPermitted(String permission, Handler<AsyncResult<Boolean>> resultHandler) {
        resultHandler.handle(Future.succeededFuture(permission.equalsIgnoreCase("role")));
    }

    @Override
    public JsonObject principal() {
        return utilisateur.toJson();
    }

    @Override
    public void setAuthProvider(AuthProvider authProvider) {
       this.provider = authProvider;
    }
}
