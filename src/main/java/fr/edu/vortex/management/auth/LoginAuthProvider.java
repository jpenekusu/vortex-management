//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.auth;

import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;

public class LoginAuthProvider implements AuthProvider {

    public final String USERNAME = "username";
    public final String PASSWORD = "password";

    private Vertx vertx;

    public LoginAuthProvider(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void authenticate(JsonObject authInfo, Handler<AsyncResult<User>> resultHandler) {

        String username = authInfo.getString(USERNAME);
        if (username == null) {
            resultHandler.handle(Future.failedFuture("Le json doit contenir un username dans le champ :" + USERNAME));
            return;
        }
        String password = authInfo.getString(PASSWORD);
        if (password == null) {
            resultHandler.handle(Future.failedFuture("Le json doit contenir un password dans le champ :" + PASSWORD));
            return;
        }

        // Authentification via BD
        authenticateFromBD(resultHandler, username, password);

    }

    private void authenticateFromBD(Handler<AsyncResult<User>> resultHandler, String username, String password) {
        JsonObject filtres = new JsonObject();
        filtres.put(Utilisateur.KEY_JSON_LOGIN,  "=" + username);
        filtres.put(FluxVerticle.KEY_JSON_PAGE, "1");
        filtres.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, "20");

        vertx.eventBus().send(UtilisateurVerticle.LISTER_UTILISATEUR, filtres, ar -> {
            if (ar.failed()) {
                resultHandler.handle(Future.failedFuture("Echec dans l'authentification"));
                return;
            }

            JsonArray listeUtilisateurs = (JsonArray) ar.result().body();

            switch (listeUtilisateurs.size()) {
            case 0: {
                resultHandler.handle(Future.failedFuture("Utilisateur/mot de passe invalide"));
                break;
            }
            case 1: {
                JsonObject utilisateurJsonObject = listeUtilisateurs.getJsonObject(0);

                if (!validPassword(utilisateurJsonObject, password)) {
                    resultHandler.handle(Future.failedFuture("Utilisateur/mot de passe invalide"));
                    return;
                }

                Utilisateur utilisateur = Utilisateur.fromJson(utilisateurJsonObject);
                User user = new VortexUser(utilisateur);
                resultHandler.handle(Future.succeededFuture(user));

                break;
            }
            default: {
                resultHandler.handle(Future.failedFuture("Echec dans l'authentification"));
                break;
            }

            }
        });
    }

    private boolean validPassword(JsonObject utilisateurJsonObject, String password) {

        Utilisateur utilisateur = Utilisateur.fromJson(utilisateurJsonObject);

        HashUtils.HashInfo hashInfo = new HashUtils.HashInfo(
                utilisateur.getHash(),
                utilisateur.getSel(),
                utilisateur.getNombreIteration());
        try {
            return HashUtils.validatePassword(password, hashInfo);
        } catch (Exception e) {
            return false;
        }
    }
}
