//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.auth;

import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.time.Duration;
import java.util.Base64;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.AsyncMap;


public class AuthManager {

    private static final Logger logger = LoggerFactory.getLogger(AuthManager.class);

    private static final SignatureAlgorithm DEFAULT_SIGNATURE_ALGORITHM = SignatureAlgorithm.RS256; // 2048 bit
    public static final String SHARED_VORTEX_AUTH_KEY = "vortexAuthKey";
    public static final String MAIN_KEY = "mainKey";
    public static final String AUTH_PUBKEY_BASEURL = "/auth/pubkey";

    private Vertx vertxContext;
    private VortexAuthProvider provider;
    private KeyPair cachedKeyPair;
    private Duration validityPeriod;

    private AuthManager(Vertx vertxContext) {
        this.vertxContext = vertxContext;
    }


    public static void create(Vertx vertx, Config authConfig, Handler<AsyncResult<AuthManager>> resultHandler){
        AuthManager authMgr = new AuthManager(vertx);
        authMgr.getOrCreateKey(keypairRes->{
            if (keypairRes.failed()){
                resultHandler.handle(Future.failedFuture(keypairRes.cause()));
            }

            try {
                if (authConfig == null) {
                    logger.warn("la configuration de validity_period est manquante ou invalide");
                }
                else{
                	authMgr.validityPeriod = authConfig.getDuration("validity_period");
                }
            }
            catch(ConfigException ce){
                logger.warn("la configuration de validity_period est manquante ou invalide");
            }
            authMgr.provider = new VortexAuthProvider( authMgr.vertxContext, authMgr.validityPeriod);

            resultHandler.handle(Future.succeededFuture(authMgr));
        });
    }



    public VortexAuthProvider getProvider() {
        return provider;
    }
    
    public KeyPair getKeyPair() {
    	return cachedKeyPair;
    }
    
    public Duration getValidityPeriod() {
    	return validityPeriod;
    }
    
    private void getOrCreateKey(Handler<AsyncResult<KeyPair>> resultHandler){
        vertxContext.sharedData().<String, KeyPair>getAsyncMap(SHARED_VORTEX_AUTH_KEY, res -> {
            if (res.failed()) {
                resultHandler.handle(Future.failedFuture("Impossible de recuperer l'asyncmap : " + SHARED_VORTEX_AUTH_KEY));
                return;
            }

            AsyncMap<String, KeyPair> map = res.result();

            map.get(MAIN_KEY, getMapRes -> {
                if (getMapRes.failed()){
                    resultHandler.handle(Future.failedFuture("Impossible de recuperer la cle "+ MAIN_KEY+ " de l'asyncmap " + SHARED_VORTEX_AUTH_KEY));
                    return;
                }

                final KeyPair keyPair = getMapRes.result()!=null?getMapRes.result():Keys.keyPairFor(DEFAULT_SIGNATURE_ALGORITHM);
                if (keyPair == null) {
                    resultHandler.handle(Future.failedFuture("Probleme avec la recuperation/generation de cle "));
                }

                map.put(MAIN_KEY, keyPair, putRes -> {
                    if (putRes.failed()) {
                        resultHandler.handle(Future.failedFuture("Impossible d'enregistrer la cle "+ MAIN_KEY + " dans l'asyncmap " + SHARED_VORTEX_AUTH_KEY));
                    }

                        resultHandler.handle(Future.succeededFuture(keyPair));
                });

            });
        });
    }

    public JsonObject getJwks() {

        JsonObject jwks = new JsonObject();
        JsonArray keysArr = new JsonArray();
        keysArr.add(getJwk(cachedKeyPair.getPublic()));

        jwks.put("keys",keysArr);

        return jwks;

    }

    public static JsonObject getJwk(PublicKey pairPublic) {
        JsonObject jwk = new JsonObject();
        try {

            jwk.put("kid",SHARED_VORTEX_AUTH_KEY);
            jwk.put("alg",DEFAULT_SIGNATURE_ALGORITHM.getValue());
            jwk.put("kty",DEFAULT_SIGNATURE_ALGORITHM.getFamilyName());
            jwk.put("use","sig");

            String certValue = Base64.getEncoder().encodeToString(pairPublic.getEncoded());
            JsonArray chain = new JsonArray();
            chain.add(certValue);
            jwk.put("x5c", chain);

            jwk.put("n",((RSAPublicKey)pairPublic).getModulus());
            jwk.put("e",((RSAPublicKey)pairPublic).getPublicExponent());

            MessageDigest digest = null;
            digest = MessageDigest.getInstance("SHA-1");
            byte[] encodedhash = digest.digest(pairPublic.getEncoded());

            jwk.put("x5t",Base64.getEncoder().encodeToString(encodedhash));

            jwk.put("x5u", AUTH_PUBKEY_BASEURL + "/" + SHARED_VORTEX_AUTH_KEY);

        } catch (NoSuchAlgorithmException e) {
            logger.error("Probleme d'implementation d'alogo avec la jvm ? : " + e.getMessage());
        }

        return jwk;
    }

    public String getPublicKeyAsPEM(String kid) {

        if (kid==null || !SHARED_VORTEX_AUTH_KEY.equalsIgnoreCase(kid)){
            return "";
        }
        //use the publicKey.getEncoded() method to get the byte array, base64 encode the bytes,
        String encodePubKey = Base64.getEncoder().encodeToString(cachedKeyPair.getPublic().getEncoded());
        //put newlines after every 64 characters,
        String encodePubKeyBreak64 = encodePubKey.replaceAll("(.{64})", "$1\n");
        // then wrap it in the BEGIN and END lines.
        String keysAsPeM = "-----BEGIN PUBLIC KEY-----\n"+ encodePubKeyBreak64 +"\n-----END PUBLIC KEY----\n";

        return keysAsPeM;
    }



}
