//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.ldap;

import static java.net.HttpURLConnection.HTTP_CONFLICT;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.utilisateur.exception.ManagementException;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class LdapVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(LdapVerticle.class);

    public static final String LDAP_VERIF_LOGIN = "vortex.management.ldap.verif";
    public static final String LDAP_AUTHENTIFICATION = "vortex.management.ldap.authentification";

    private static final String LOGIN_NON_PRESENT_DANS_LDAP = "Le login est inexistant dans l'annuaire LDAP";

    private static final String ERROR_LDAP = "Erreur lors de la recherche dans le LDAP ";

    public final String USERNAME = "username";
    public final String PASSWORD = "password";
    public final String LOGIN = "login";

    private JsonObject configLdap;

    @Override
    public void start() throws Exception {

        this.configLdap = config();

        vertx.eventBus().consumer(LDAP_VERIF_LOGIN, this::verifierPresenceDansLDAP);
        vertx.eventBus().consumer(LDAP_AUTHENTIFICATION, this::authentification);
    }

    private void verifierPresenceDansLDAP(Message<Object> message) {
        String login = (String) message.body();
        vertx.executeBlocking(handleExecBlock -> {
            rechercheLDAP(login,null, handleExecBlock);
        }, retourHandler -> {
            if (retourHandler.succeeded()) {
                message.reply(true);
            } else {
                message.fail(404, retourHandler.cause().getMessage());
            }
        });
    }

    private void rechercheLDAP(String login, String password, Future<Object> handleExecBlock) {
	Hashtable<String, String> env = LdapConfiguration.parametrageLdap(configLdap, login, password, false,null);

	try {
	    LdapContext ctx = new InitialLdapContext(env, null);
	    ctx.setRequestControls(null);

	    String champARechercher = LdapConfiguration.filterSearch.replace("{0}", login);
	    NamingEnumeration<?> namingEnum = ctx.search(
	            LdapConfiguration.DN_template,
	            champARechercher,
	            getSimpleSearchControls());
	    
	    if (namingEnum.hasMore()) {
	        SearchResult result = (SearchResult) namingEnum.next ();    
	        String valeurDN = result.getNameInNamespace();

	        namingEnum.close();
	        handleExecBlock.handle(Future.succeededFuture(valeurDN));
	        return;
	    }
	    ManagementException err = new ManagementException(HTTP_CONFLICT,
	            String.format(LOGIN_NON_PRESENT_DANS_LDAP, login));
	    handleExecBlock.handle(Future.failedFuture(err.getMessage()));
	} catch (Exception e) {
	    log.error(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID,
                    ERROR_LDAP +e.getMessage());
	    ManagementException err = new ManagementException(HTTP_CONFLICT, ERROR_LDAP);
	    handleExecBlock.handle(Future.failedFuture(err.getMessage()));
	}
    }

    private void authentification(Message<Object> message) {
        JsonObject authInfo = (JsonObject) message.body();
        String username = authInfo.getString(USERNAME);
        String password = authInfo.getString(PASSWORD);
        
        vertx.executeBlocking(handleExecBlock -> {
            rechercheLDAP(username,null, handleExecBlock);
        }, retourHandler -> {
            if (retourHandler.succeeded()) {
        	 String retourDN = (String) retourHandler.result();
        	 vertx.executeBlocking(resultHandler -> {
        	          
        	            Hashtable<String, String> env = LdapConfiguration.parametrageLdap(configLdap, username, password, true,retourDN);

        	            try {
        	                LdapContext ctx = new InitialLdapContext(env, null);
        	                
        	                log.info("Authentification avec le DAP effectue pour le login " + username);
        	                resultHandler.handle(Future.succeededFuture(null));

        	            } catch (AuthenticationException e) {
        	        	String erreur = "Erreur lors de l'authentification LDAP avec le login "+ username;
        	                log.error(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID,
        	                        erreur, e.getMessage());
        	                resultHandler.handle(Future.failedFuture(erreur));

        	            } catch (NamingException e) {
        	        	String erreur = "Erreur lors de l'authentification LDAP avec le login "+ username;
        	                log.error(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID,
        	                        erreur, e.getMessage());
        	                resultHandler.handle(Future.failedFuture(erreur));
        	            }

        	        }, resultHandler -> {
        	            if (resultHandler.succeeded()) {
        	                message.reply(true);
        	            } else {
        	                message.fail(404, resultHandler.cause().getMessage());
        	            }
        	        });
        	     
                
            } else {
                message.fail(404, retourHandler.cause().getMessage());
            }
        });
       
    }

    private SearchControls getSimpleSearchControls() {
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setTimeLimit(30000);
        return searchControls;
    }

}
