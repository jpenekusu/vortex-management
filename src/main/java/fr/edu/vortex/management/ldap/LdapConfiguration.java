//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.ldap;

import java.util.Hashtable;

import javax.naming.Context;

import io.vertx.core.json.JsonObject;

public class LdapConfiguration {

    public static String urlLDAP;
    public static String DN_template;
    public static String filterSearch;

    public static String userAdminLDAP;
    public static String passwordAdminLDAP;

    private static final String CONFIG_URL = "url";
    private static final String CONFIG_DN_TEMPLATE = "dn_template";
    private static final String CONFIG_DN_FILTER_SEARCH = "filter_search";
    private static final String CONFIG_USER_LDAP_RECHERCHE = "user_ldap_recherche";
    private static final String CONFIG_PWD_LDAP_RECHERCHE = "pwd_ldap_recherche";

    private static void recuperationInfosConfLDAP(JsonObject configLdap, String login) {
        if (configLdap.getString(CONFIG_URL) != null) {
            urlLDAP = configLdap.getString(CONFIG_URL);
        }
        if (configLdap.getString(CONFIG_DN_FILTER_SEARCH) != null) {
            filterSearch = configLdap.getString(CONFIG_DN_FILTER_SEARCH);
        }
        if (configLdap.getString(CONFIG_DN_TEMPLATE) != null) {
            DN_template = configLdap.getString(CONFIG_DN_TEMPLATE);
        }
        if (configLdap.getString(CONFIG_USER_LDAP_RECHERCHE) != null) {
            userAdminLDAP = configLdap.getString(CONFIG_USER_LDAP_RECHERCHE);
        }
        if (configLdap.getString(CONFIG_PWD_LDAP_RECHERCHE) != null) {
            passwordAdminLDAP = configLdap.getString(CONFIG_PWD_LDAP_RECHERCHE);
        }
    }

    public static Hashtable<String, String> parametrageLdap(JsonObject configLdap, String login, String password,
            boolean authentificationLDAP, String authentification_DN) {
        Hashtable<String, String> env = new Hashtable<>();

        // TODO Si parametres de conf mal renseignés -> remonter erreur ? Ne pas
        // demarrer management ?....
        recuperationInfosConfLDAP(configLdap, login);

        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, LdapConfiguration.urlLDAP);

        // Cas ou on veut se logger via le LDAP donc avec son login/pwd
        if (authentificationLDAP) {
            env.put(Context.SECURITY_AUTHENTICATION, "simple");

            //on recupere le DN complet pour l'authentification
            env.put(Context.SECURITY_PRINCIPAL, authentification_DN);
            env.put(Context.SECURITY_CREDENTIALS, password);
        }
        // Cas où l'on fait une recherche via un LDAP non securise
        else if (!authentificationLDAP && userAdminLDAP == null && passwordAdminLDAP == null) {
            env.put(Context.SECURITY_AUTHENTICATION, "none");
        }
        // Cas où l'on fait une recherche via un LDAP securise (avec un login/pwd
        // d'admin a saisir dans le fichier de conf)
        else {
            env.put(Context.SECURITY_AUTHENTICATION, "simple");

            // on se connecte au LDAP securise pour effecteur une recherche avec un
            // adminLDAP
            env.put(Context.SECURITY_PRINCIPAL, userAdminLDAP);
            env.put(Context.SECURITY_CREDENTIALS, passwordAdminLDAP);
        }

        return env;
    }
}
