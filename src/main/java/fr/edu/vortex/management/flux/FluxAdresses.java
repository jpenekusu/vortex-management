//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.flux;

public class FluxAdresses {
    
	public static final String BASE = "vortex.management";
    public static final String FLUX_BASE = BASE + "." + "flux";
    public static final String LIST_FLUX = FLUX_BASE + "." + "listFlux";
    public static final String GET_FLUX_BY_CORRELATION_ID = FLUX_BASE + "." + "getFluxByCorrelationId";
    public static final String GET_FLUX_BY_CORRELATIONS_ID_FLUX_AND_CONSUMER = FLUX_BASE + "."
            + "getFluxByCorrelationsIdFluxAndConsumer";
    
	public static final String REMETTRE_A_DISPOSITION_MESSAGE = "vortex.core.message.remettreADispositionMessage";
    public static final String RESET_FLUX_TO_DELIVERED = FLUX_BASE + "." + "resetFluxToDelivered";
    public static final String FLUX_EXIST = FLUX_BASE + "." + "fluxExist";
    public static final String LIST_FLUX_NON_TROUVES = FLUX_BASE + "." + "listFluxNonTrouves";

}
