//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.flux;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import fr.edu.echanges.nat.vortex.MessageVortex;
import fr.edu.vortex.management.notification.Notification;
import fr.edu.vortex.management.persistence.PersistableAsJsonObject;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class Flux implements PersistableAsJsonObject, Serializable {

    private static final long serialVersionUID = -1930377604333153440L;
    public static final String KEY_JSON_ID = "id";
    public static final String KEY_JSON_CORRELATION_ID = "correlation_id";
    public static final String KEY_JSON_STATUS = "status";
    public static final String KEY_JSON_STATUS_ORDER = "status_order";
    public static final String KEY_JSON_DATE = "date";
    public static final String KEY_JSON_PRODUCER = "producer";
    public static final String KEY_JSON_EXCHANGE_NAME = "exchange_name";
    public static final String KEY_JSON_ROUTING_KEY = "routing_key";
    public static final String KEY_JSON_CONSUMER_CORRELATION_ID = "consumer_correlation_id";
    public static final String KEY_JSON_CONSUMER_BUSINESS_ID = "consumer_business_id";
    public static final String KEY_JSON_CONSUMER_QUEUE_NAME = "consumer_queue_name";
    public static final String KEY_JSON_FILENAME = "file_name";
    public static final String KEY_JSON_FILESIZE = "file_size";
    public static final String KEY_JSON_FILE_URL = "file_url";
    public static final String KEY_JSON_ERROR_MESSAGE = "error_message";
    public static final String KEY_JSON_STACKTRACE = "stacktrace";
    public static final String KEY_JSON_ERROR_CODE = "error_code";
    public static final String KEY_JSON_STATUS_HISTORY_LIST = "status_history";
    public static final String KEY_JSON_NOTIFICATION_LIST = "notifications";
    public static final String KEY_JSON_LIST_CORRELATION_ID = "list_correlation_id";
    public static final String KEY_JSON_LIST_FILENAME = "list_file_name";
  
    // CONSTANCE UTILISE POUR CONSTRUIRE UN FLUX EN FONCTION D'UN MESSAGE MODEL DU CORE
	public static final String KEY_JSON_HORODATAGE = "horodatage";
	public static final String KEY_JSON_IDENTIFIANT = "identifiant";
	public static final String KEY_JSON_TYPE_CONTENU = "type_contenu";
	public static final String KEY_JSON_CLE_ROUTAGE = "cle_routage";
	public static final String KEY_JSON_ENTETE = "entete";
	public static final String KEY_JSON_METADONNEES_CONTENU = "metadonnees_contenu";
	public static final String KEY_JSON_URL = "url";
	public static final String KEY_JSON_ID_PRODUCTEUR_MESSAGE = "id_producteur_message";
	public static final String KEY_JSON_NOM_ECHANGE = "nom_echange";
	public static final String KEY_JSON_QUEUE = "queue";
	public static final String KEY_JSON_CONTENU = "contenu";

    public static final String KEY_DB_ID = "id";
    public static final String KEY_DB_CORRELATION_ID = "correlation_id";
    public static final String KEY_DB_STATUS = "status";
    public static final String KEY_DB_STATUS_ORDER = "status_order";
    public static final String KEY_DB_DATE = "date";
    public static final String KEY_DB_PRODUCER = "producer";
    public static final String KEY_DB_EXCHANGE_NAME = "exchange_name";
    public static final String KEY_DB_ROUTING_KEY = "routing_key";
    public static final String KEY_DB_CONSUMER_CORRELATION_ID = "consumer_correlation_id";
    public static final String KEY_DB_CONSUMER_BUSINESS_ID = "consumer_business_id";
    public static final String KEY_DB_CONSUMER_QUEUE_NAME = "consumer_queue_name";
    public static final String KEY_DB_FILENAME = "file_name";
    public static final String KEY_DB_FILESIZE = "file_size";
    public static final String KEY_DB_FILE_URL = "file_url";
    public static final String KEY_DB_ERROR_MESSAGE = "error_message";
    public static final String KEY_DB_STACKTRACE = "stacktrace";
    public static final String KEY_DB_ERROR_CODE = "error_code";

    private Integer id;
    private String correlationId;
    private StatusEnum status;
    private Instant date;
    private String producer;
    private String exchangeName;
    private String routingKey;
    private String consumerCorrelationId;
    private String consumerBusinessId;
    private String consumerQueueName;
    private String filename;
    private Long filesize;
    private String fileUrl;
    private String errorMessage;
    private String stacktrace;
    private String errorCode;

    private List<StatusHistory> statusHistoryList;    
    private List<Notification> notificationsList;

    public Flux(Integer id, String correlationId, StatusEnum status, Instant date, String producer, String exchangeName,
            String routingKey, String consumerCorrelationId, String consumerBusinessId, String consumerQueueName,
            String filename, Long filesize, String fileUrl, String errorMessage, String stacktrace, String errorCode) {
        this.setId(id);
        this.setCorrelationId(correlationId);
        this.setStatus(status);
        this.setDate(date);
        this.setProducer(producer);
        this.setExchangeName(exchangeName);
        this.setRoutingKey(routingKey);
        this.setConsumerCorrelationId(consumerCorrelationId);
        this.setConsumerBusinessId(consumerBusinessId);
        this.setConsumerQueueName(consumerQueueName);
        this.setFilename(filename);
        this.setFilesize(filesize);
        this.setFileUrl(fileUrl);
        this.setErrorMessage(errorMessage);
        this.setStacktrace(stacktrace);
        this.setErrorCode(errorCode);
    }

    public Flux(String correlationId, StatusEnum status, Instant date, String producer, String exchangeName,
            String routingKey, String consumerCorrelationId, String consumerBusinessId, String consumerQueueName,
            String filename, Long filesize, String fileUrl) {
        this.setCorrelationId(correlationId);
        this.setStatus(status);
        this.setDate(date);
        this.setProducer(producer);
        this.setExchangeName(exchangeName);
        this.setRoutingKey(routingKey);
        this.setConsumerCorrelationId(consumerCorrelationId);
        this.setConsumerBusinessId(consumerBusinessId);
        this.setConsumerQueueName(consumerQueueName);
        this.setFilename(filename);
        this.setFilesize(filesize);
        this.setFileUrl(fileUrl);
    }

    public Flux(String correlationId, StatusEnum status, Instant date, String producer) {
        this.setCorrelationId(correlationId);
        this.setStatus(status);
        this.setDate(date);
        this.setProducer(producer);
    }

    public Flux(MessageVortex message, Instant date, StatusEnum status) {
        this.setCorrelationId(message.getIdentifiant());
        this.setStatus(status);
        this.setDate(date);
        this.setProducer(message.getId_producteur_message());
        this.setExchangeName(message.getNom_echange());
        this.setRoutingKey(message.getCle_routage());

        if (!message.getMetadonnees_contenu().isEmpty()) {
            if (message.getMetadonnees_contenuAsJson().getString("nomFichier") != null) {
                this.setFilename(message.getMetadonnees_contenuAsJson().getString("nomFichier"));
            }

            if (message.getMetadonnees_contenuAsJson().getString("tailleFichier") != null) {
                this.setFilesize(Long.parseLong(message.getMetadonnees_contenuAsJson().getString("tailleFichier")));
            }
        }
    }

    public Flux(JsonObject jsonObject) {
        if (jsonObject != null) {
            this.setId(jsonObject.getInteger(KEY_JSON_ID));
            this.setCorrelationId(jsonObject.getString(KEY_JSON_CORRELATION_ID));
            this.setStatus(StatusEnum.valueOf(jsonObject.getString(KEY_JSON_STATUS)));
            this.setDate(Instant.parse((jsonObject.getString(KEY_JSON_DATE))));
            this.setProducer(jsonObject.getString(KEY_JSON_PRODUCER));
            this.setExchangeName(jsonObject.getString(KEY_JSON_EXCHANGE_NAME));
            this.setRoutingKey(jsonObject.getString(KEY_JSON_ROUTING_KEY));
            this.setConsumerCorrelationId(jsonObject.getString(KEY_JSON_CONSUMER_CORRELATION_ID));
            this.setConsumerBusinessId(jsonObject.getString(KEY_JSON_CONSUMER_BUSINESS_ID));
            this.setConsumerQueueName(jsonObject.getString(KEY_JSON_CONSUMER_QUEUE_NAME));
            this.setFilename(jsonObject.getString(KEY_JSON_FILENAME));
            this.setFilesize(jsonObject.getLong(KEY_JSON_FILESIZE));
            this.setFileUrl(jsonObject.getString(KEY_JSON_FILE_URL));
            this.setErrorMessage(jsonObject.getString(KEY_JSON_ERROR_MESSAGE));
            this.setStacktrace(jsonObject.getString(KEY_JSON_STACKTRACE));
            this.setErrorCode(jsonObject.getString(KEY_JSON_ERROR_CODE));

            JsonArray statusHistoryArray = jsonObject.getJsonArray(KEY_JSON_STATUS_HISTORY_LIST);
            if (statusHistoryArray != null) {
                List<StatusHistory> statusHistoryList = new ArrayList<StatusHistory>();
                for (int i = 0; i < statusHistoryArray.size(); i++) {
                    statusHistoryList.add(new StatusHistory(statusHistoryArray.getJsonObject(i)));
                }
                this.setStatusHistoryList(statusHistoryList);
            }
            
            JsonArray notificationsArray = jsonObject.getJsonArray(KEY_JSON_NOTIFICATION_LIST);
            if (notificationsArray != null) {
                List<Notification> notificationList = new ArrayList<Notification>();
                for (int i = 0; i < notificationsArray.size(); i++) {
                	notificationList.add(new Notification(notificationsArray.getJsonObject(i)));
                }
                this.setNotificationsList(notificationList);
            }
        }
    }
    
	public Flux(JsonObject messageModelJsonObject, String correlationId, String consumerBusinessId, String queueName, StatusEnum status)  throws Exception  {
		try {
		
		if (messageModelJsonObject != null) {
			this.setCorrelationId(correlationId);
			this.setDate(Instant.parse((messageModelJsonObject.getString(KEY_JSON_HORODATAGE))));
			this.setProducer(messageModelJsonObject.getString(KEY_JSON_ID_PRODUCTEUR_MESSAGE));
			this.setExchangeName(messageModelJsonObject.getString(KEY_JSON_NOM_ECHANGE));
			this.setRoutingKey(messageModelJsonObject.getString(KEY_JSON_CLE_ROUTAGE));
			this.setConsumerCorrelationId(messageModelJsonObject.getString(KEY_JSON_IDENTIFIANT));
			this.setConsumerBusinessId(consumerBusinessId);
			this.setConsumerQueueName(queueName);
			this.setStatus(status);
			
			JsonObject metadonnees = new JsonObject(messageModelJsonObject.getString(KEY_JSON_METADONNEES_CONTENU));
					
			this.setFilename(metadonnees.getString("nomFichier"));
			this.setFilesize(Long.parseLong(metadonnees.getString("tailleFichier")));
			this.setFileUrl(messageModelJsonObject.getString(KEY_JSON_URL));
		}
		}
		catch(Exception e) {
			throw e;
		}

	}
	

    @Override
    public JsonObject toJsonObject() {
        JsonObject jsonToEncode = new JsonObject();

        jsonToEncode.put(KEY_JSON_ID, this.getId());
        jsonToEncode.put(KEY_JSON_CORRELATION_ID, this.getCorrelationId());
        jsonToEncode.put(KEY_JSON_STATUS, this.getStatus().getValue());
        jsonToEncode.put(KEY_JSON_STATUS_ORDER, this.getStatus().getOrder());
        jsonToEncode.put(KEY_JSON_DATE, this.getDate());
        jsonToEncode.put(KEY_JSON_PRODUCER, this.getProducer());
        jsonToEncode.put(KEY_JSON_EXCHANGE_NAME, this.getExchangeName());
        jsonToEncode.put(KEY_JSON_ROUTING_KEY, this.getRoutingKey());
        jsonToEncode.put(KEY_JSON_CONSUMER_CORRELATION_ID, this.getConsumerCorrelationId());
        jsonToEncode.put(KEY_JSON_CONSUMER_BUSINESS_ID, this.getConsumerBusinessId());
        jsonToEncode.put(KEY_JSON_CONSUMER_QUEUE_NAME, this.getConsumerQueueName());
        jsonToEncode.put(KEY_JSON_FILENAME, this.getFilename());
        jsonToEncode.put(KEY_JSON_FILESIZE, this.getFilesize());
        jsonToEncode.put(KEY_JSON_FILE_URL, this.getFileUrl());
        jsonToEncode.put(KEY_JSON_ERROR_MESSAGE, this.getErrorMessage());
        jsonToEncode.put(KEY_JSON_STACKTRACE, this.getStacktrace());
        jsonToEncode.put(KEY_JSON_ERROR_CODE, this.getErrorCode());

        if (this.getStatusHistoryList() != null) {
            JsonArray statusHistoryArray = new JsonArray();
            for (StatusHistory statusHistory : this.getStatusHistoryList()) {
                statusHistoryArray.add(statusHistory.toJsonObject());
            }
            jsonToEncode.put(KEY_JSON_STATUS_HISTORY_LIST, statusHistoryArray);
        }
        
        if (this.getNotificationsList() != null) {
            JsonArray notificationsArray = new JsonArray();
            for (Notification notification : this.getNotificationsList()) {
            	notificationsArray.add(notification.toJsonObject());
            }
            jsonToEncode.put(KEY_JSON_NOTIFICATION_LIST, notificationsArray);
        }

        return jsonToEncode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public Long getFilesize() {
        return filesize;
    }

    public void setFilesize(Long filesize) {
        this.filesize = filesize;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public List<StatusHistory> getStatusHistoryList() {
        return statusHistoryList;
    }

    public void setStatusHistoryList(List<StatusHistory> statusHistoryList) {
        this.statusHistoryList = statusHistoryList;
    }

    public String getConsumerCorrelationId() {
        return consumerCorrelationId;
    }

    public void setConsumerCorrelationId(String consumerCorrelationId) {
        this.consumerCorrelationId = consumerCorrelationId;
    }

    public String getConsumerBusinessId() {
        return consumerBusinessId;
    }

    public void setConsumerBusinessId(String consumerBusinessId) {
        this.consumerBusinessId = consumerBusinessId;
    }

    public String getConsumerQueueName() {
        return consumerQueueName;
    }

    public void setConsumerQueueName(String consumerQueueName) {
        this.consumerQueueName = consumerQueueName;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

	public List<Notification> getNotificationsList() {
		return notificationsList;
	}

	public void setNotificationsList(List<Notification> notificationsList) {
		this.notificationsList = notificationsList;
	}



}
