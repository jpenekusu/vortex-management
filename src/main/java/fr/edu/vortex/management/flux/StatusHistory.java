//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.flux;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.edu.echanges.nat.vortex.common.log.LogFormater;
import fr.edu.vortex.management.persistence.PersistableAsJsonObject;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class StatusHistory implements PersistableAsJsonObject, Serializable {

    private static final long serialVersionUID = 730077788293990364L;

    private static final Logger logger = LoggerFactory.getLogger(StatusHistory.class);

    public static final String KEY_JSON_STATUS = "status";
    public static final String KEY_JSON_STATUS_ORDER = "status_order";
    public static final String KEY_JSON_DATE = "date";

    public static final String KEY_DB_STATUS = "status";
    public static final String KEY_DB_STATUS_ORDER = "status_order";
    public static final String KEY_DB_DATE = "date";

    public final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    private StatusEnum status;
    private Date date;

    public StatusHistory(StatusEnum status, Date date) {
        this.setStatus(status);
        this.setDate(date);
    }

    public StatusHistory(JsonObject jsonObject) {
        if (jsonObject != null) {
            try {
                this.setStatus(StatusEnum.valueOf(jsonObject.getString(KEY_JSON_STATUS)));
                SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
                this.setDate(formatter.parse((jsonObject.getString(KEY_JSON_DATE))));
            } catch (ParseException e) {
                logger.error(new LogFormater(null, "Flux(JsonObject jsonObject)", null, e.getMessage()), e);
            }
        }
    }

    @Override
    public JsonObject toJsonObject() {
        JsonObject jsonToEncode = new JsonObject();
        jsonToEncode.put(KEY_JSON_STATUS, this.getStatus().getValue());
        jsonToEncode.put(KEY_JSON_STATUS_ORDER, this.getStatus().getOrder());
        if (this.getDate() != null) {
            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
            jsonToEncode.put(KEY_JSON_DATE, formatter.format(this.getDate()));
        }
        return jsonToEncode;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
