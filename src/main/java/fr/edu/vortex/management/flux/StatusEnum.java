//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.flux;

public enum StatusEnum {
    IMMATRICULE(10, "IMMATRICULE"), EN_RECEPTION(20, "EN_RECEPTION"), RECU(30, "RECU"), ROUTAGE(40,
            "ROUTAGE"), A_ENVOYER(50, "A_ENVOYER"), EN_EMISSION(60, "EN_EMISSION"), DELIVRE(70, "DELIVRE"), //
    OBSOLETE(120, "OBSOLETE"), //
    ERREUR(100, "ERREUR");

    private Integer order;
    private String value;

    StatusEnum(Integer order, String value) {
        this.order = order;
        this.value = value;
    }

    public Integer getOrder() {
        return order;
    }

    public String getValue() {
        return value;
    }
}
