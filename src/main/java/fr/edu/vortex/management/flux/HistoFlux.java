//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.flux;

import java.io.Serializable;
import java.time.Instant;

import fr.edu.vortex.management.persistence.PersistableAsJsonObject;
import io.vertx.core.json.JsonObject;

public class HistoFlux implements PersistableAsJsonObject, Serializable {

	private static final long serialVersionUID = -1930377604333153440L;
	public static final String KEY_JSON_ID = "id";
	public static final String KEY_JSON_DATE = "date";
	public static final String KEY_JSON_CORRELATION_ID = "correlation_id";
	public static final String KEY_JSON_CONSUMER_CORRELATION_ID = "consumer_correlation_id";
    public static final String KEY_JSON_CONSUMER_QUEUE_NAME = "consumer_queue_name";	
    public static final String KEY_JSON_STATUS = "status";
    public static final String KEY_JSON_STATUS_ORDER = "status_order";	
	public static final String KEY_JSON_ACTION = "action";

	public static final String KEY_DB_ID = "id";
	public static final String KEY_DB_DATE = "date";
	public static final String KEY_DB_CORRELATION_ID = "correlation_id";
	public static final String KEY_DB_CONSUMER_CORRELATION_ID = "consumer_correlation_id";
    public static final String KEY_DB_CONSUMER_QUEUE_NAME = "consumer_queue_name";	
    public static final String KEY_DB_STATUS = "status";
    public static final String KEY_DB_STATUS_ORDER = "status_order";	
	public static final String KEY_DB_ACTION = "action";

	private Integer id;
	private Instant date;
	private String correlationId;
	private String consumerCorrelationId;
    private String consumerQueueName;	
    private StatusEnum status;	
	private ActionEnum action;

	public HistoFlux(Integer id, Instant date, String correlationId, String consumerCorrelationId, String consumerQueueName, StatusEnum status, ActionEnum action) {
		this.setId(id);
		this.setDate(date);
		this.setCorrelationId(correlationId);
		this.setConsumerCorrelationId(consumerCorrelationId);
		this.setConsumerQueueName(consumerQueueName);
		this.setStatus(status);
		this.setAction(action);
	}

	public HistoFlux(Instant date, String correlationId, String consumerCorrelationId, String consumerQueueName, StatusEnum status, ActionEnum action) {
		this.setDate(date);
		this.setCorrelationId(correlationId);
		this.setConsumerCorrelationId(consumerCorrelationId);
		this.setConsumerQueueName(consumerQueueName);		
		this.setStatus(status);
		this.setAction(action);
	}

	public HistoFlux(JsonObject jsonObject) {
		if (jsonObject != null) {
			this.setId(jsonObject.getInteger(KEY_JSON_ID));
			this.setDate(Instant.parse((jsonObject.getString(KEY_JSON_DATE))));
			this.setCorrelationId(jsonObject.getString(KEY_JSON_CORRELATION_ID));
			this.setConsumerCorrelationId(jsonObject.getString(KEY_JSON_CONSUMER_CORRELATION_ID));
			this.setConsumerQueueName(jsonObject.getString(KEY_JSON_CONSUMER_QUEUE_NAME));
            this.setStatus(StatusEnum.valueOf(jsonObject.getString(KEY_JSON_STATUS)));			
			this.setAction(ActionEnum.valueOf(jsonObject.getString(KEY_JSON_ACTION)));
		}
	}

	@Override
	public JsonObject toJsonObject() {
		JsonObject jsonToEncode = new JsonObject();

		jsonToEncode.put(KEY_JSON_ID, this.getId());
		jsonToEncode.put(KEY_JSON_DATE, this.getDate());
		jsonToEncode.put(KEY_JSON_CORRELATION_ID, this.getCorrelationId());
		jsonToEncode.put(KEY_JSON_CONSUMER_CORRELATION_ID, this.getConsumerCorrelationId());
		jsonToEncode.put(KEY_JSON_CONSUMER_QUEUE_NAME, this.getConsumerQueueName());
        jsonToEncode.put(KEY_JSON_STATUS, this.getStatus().getValue());
        jsonToEncode.put(KEY_JSON_STATUS_ORDER, this.getStatus().getOrder());		
		jsonToEncode.put(KEY_JSON_ACTION, this.getAction().getValue());

		return jsonToEncode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getConsumerCorrelationId() {
		return consumerCorrelationId;
	}

	public void setConsumerCorrelationId(String consumerCorrelationId) {
		this.consumerCorrelationId = consumerCorrelationId;
	}

	public ActionEnum getAction() {
		return action;
	}

	public void setAction(ActionEnum action) {
		this.action = action;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public String getConsumerQueueName() {
		return consumerQueueName;
	}

	public void setConsumerQueueName(String consumerQueueName) {
		this.consumerQueueName = consumerQueueName;
	}

}
