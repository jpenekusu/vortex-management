//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.flux;

import static fr.edu.vortex.management.Constants.LOG_ERROR_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.persistence.dao.HistoFluxDAO;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class HistoFluxVerticle extends AbstractVerticle {

	private static final Logger logger = LoggerFactory.getLogger(HistoFluxVerticle.class);

	public static final String GET_BY_CORRELATION_ID_AND_CONSUMER_CORRELATION_ID = "vortex.management.histoFlux.getByCorrelationIdAndConsumerCorrelationId";

	private HistoFluxDAO histoFluxDAO;

	@Override
	public void start() throws Exception {

		histoFluxDAO = (histoFluxDAO == null) ? MainVerticle.histoFluxDAO : histoFluxDAO;

		vertx.eventBus().consumer(GET_BY_CORRELATION_ID_AND_CONSUMER_CORRELATION_ID)
				.handler(this::getHistoFluxByCorrelationIdAndConsumerCorrelationId);

	}

	private void getHistoFluxByCorrelationIdAndConsumerCorrelationId(Message<Object> message) {
		String action = "Récupérer l'historique des flux par le correlationId et le ConsommateurCorrelationId";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		JsonObject jsonMessage = (JsonObject) message.body();
		String correlationId = jsonMessage.getString(Flux.KEY_JSON_CORRELATION_ID);
		String consumerCorrelationId = jsonMessage.getString(Flux.KEY_JSON_CONSUMER_CORRELATION_ID);
		
		if (correlationId == null || correlationId.isEmpty()) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", "Le paramêtre correlation_id est manquant");
			message.fail(HTTP_BAD_REQUEST, "Le paramêtre correlation_id est manquant");
			return;
		}
		
		histoFluxDAO.getHistoFluxByCorrelationIdAndConsumerCorrelationId(correlationId, consumerCorrelationId)
				.setHandler(getHistoFlux -> {
					if (getHistoFlux.failed()) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
								getHistoFlux.cause());
						message.fail(500, getHistoFlux.cause().getMessage());
					} else {
						JsonArray jsonFluxArray = new JsonArray();
						for (HistoFlux histoFlux : getHistoFlux.result()) {
							jsonFluxArray.add(histoFlux.toJsonObject());
						}
						message.reply(jsonFluxArray);
					}
				});

	}

}