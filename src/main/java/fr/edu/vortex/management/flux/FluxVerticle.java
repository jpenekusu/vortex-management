//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.flux;

import java.net.HttpURLConnection;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import fr.edu.echanges.nat.vortex.MessageVortex;
import fr.edu.echanges.nat.vortex.common.adresses.message.MessageAdresses;
import fr.edu.echanges.nat.vortex.common.event.VortexEvent;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.api.Pagination;
import fr.edu.vortex.management.api.PaginationException;
import fr.edu.vortex.management.persistence.dao.FluxDAO;
import fr.edu.vortex.management.persistence.dao.HistoFluxDAO;
import fr.edu.vortex.management.persistence.dao.NotificationDAO;
import fr.edu.vortex.management.utils.PaginationUtils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

public class FluxVerticle extends AbstractVerticle {

	private static final String ERREUR_HISTORIQUE_STATUS_FLUX = "Erreur sur la récupération de l'historique des status du flux.";
	private static final String ERREUR_NOTIFICATION_FLUX = "Erreur sur la récupération des notification du flux.";
	private static final String ERREUR_GET_FLUX = "Erreur lors de la récupération du flux dans la persistence.";
	private static final String ERREUR_GET_FLUX_NON_TROUVES = "Erreur lors de la récupération des flux non trouvés.";
	private static final String ERREUR_GET_FLUX_NON_TROUVES_FORMAT = "Format incorrect.";
	private static final String ERREUR_COUNT_FLUX = "Erreur lors du comptage des flux dans la persistence.";
	public static final String ERREUR_PARAM_MANQUANT = "Le paramètre %s est manquant";

	public static final String MOVE_MESSAGE_TO_DLQ = "vortex.core.message.moveMessageDLQ";

	public static final String ERR_CORE_SUPP_ZR = "ERR_CORE_SUPP_ZR";
	public static final String ERR_CORE_SUPP_ZR_LIBELLE = "La zone de réception associée a été suprimée";

	public static final String DLQ_NAME = "vortex.zoneErreur";

	private static final Logger logger = LoggerFactory.getLogger(FluxVerticle.class);
	private FluxDAO fluxDAO;
	private HistoFluxDAO histoFluxDAO;
	private NotificationDAO notificationDAO;

	public static final String KEY_JSON_MESSAGE_MODEL = "message_model";
	public static final String KEY_JSON_CORRELATION_ID = "correlation_id";
	public static final String KEY_JSON_CONSUMER_CORRELATION_ID = "consumer_correlation_id";
	public static final String KEY_JSON_QUEUE_NAME = "queue_name";
	public static final String KEY_JSON_CONSUMER_BUSINESS_ID = "consumer_business_id";

	public static final String KEY_JSON_MESSAGE_VORTEX = "message_vortex";
	public static final String KEY_JSON_ERROR_MESSAGE = "error_message";
	public static final String KEY_JSON_ERROR_CODE = "error_code";
	public static final String KEY_JSON_STACKTRACE = "stacktrace";
	public static final String KEY_JSON_FILE_URL = "file_url";

	public static final String KEY_JSON_PAGE = "page";
	public static final String KEY_JSON_PAGE_LIMIT = "page_limit";
	public static final String KEY_JSON_SORT = "sort";
	public static final String KEY_JSON_SORT_ORDER = "sort_order";

	public static final String KEY_JSON_START_DATE_FILTER = "start_date";
	public static final String KEY_JSON_END_DATE_FILTER = "end_date";

	@Override
	public void start() throws Exception {

		fluxDAO = (fluxDAO == null) ? MainVerticle.fluxDAO : fluxDAO;
		histoFluxDAO = (histoFluxDAO == null) ? MainVerticle.histoFluxDAO : histoFluxDAO;
		notificationDAO = (notificationDAO == null) ? MainVerticle.notificationDAO : notificationDAO;

		vertx.eventBus().consumer(FluxAdresses.LIST_FLUX).handler(this::loadAllFlux);
		vertx.eventBus().consumer(FluxAdresses.LIST_FLUX_NON_TROUVES).handler(this::getFluxNonTrouves);
		vertx.eventBus().consumer(FluxAdresses.GET_FLUX_BY_CORRELATION_ID).handler(this::getFluxByCorrelationId);
		vertx.eventBus().consumer(FluxAdresses.GET_FLUX_BY_CORRELATIONS_ID_FLUX_AND_CONSUMER)
				.handler(this::getFluxByCorrelationIdAndConsumerCorrelationId);

		vertx.eventBus().consumer(MessageAdresses.REGISTERED_MESSAGE).handler(this::registedMessage);
		vertx.eventBus().consumer(MessageAdresses.RECEIVED_MESSAGE).handler(this::receivedMessage);
		vertx.eventBus().consumer(MessageAdresses.ROUTING_MESSAGE).handler(this::routingMessage);
		vertx.eventBus().consumer(MessageAdresses.TO_DELIVERED_MESSAGE).handler(this::toDeliveredMessage);
		vertx.eventBus().consumer(MessageAdresses.EMITTED_MESSAGE).handler(this::emittedMessage);
		vertx.eventBus().consumer(MessageAdresses.IN_EMISSION_MESSAGE).handler(this::inEmissionMessage);
		vertx.eventBus().consumer(MessageAdresses.ERROR_MESSAGE).handler(this::setFluxInError);
		vertx.eventBus().consumer(MessageAdresses.DELIVERED_ERROR_MESSAGE).handler(this::setFluxConsumerInError);
		vertx.eventBus().consumer(MessageAdresses.CORE_MESSAGE_OBSOLETED).handler(this::obsoleteFlux);

		vertx.eventBus().consumer(MOVE_MESSAGE_TO_DLQ).handler(this::setFluxToDeliveredToError);
		vertx.eventBus().consumer(FluxAdresses.RESET_FLUX_TO_DELIVERED).handler(this::resetFluxToDelivered);
		vertx.eventBus().consumer(FluxAdresses.FLUX_EXIST).handler(this::fluxExists);

	}

	protected void setFluxDAO(FluxDAO fluxDAO) {
		this.fluxDAO = fluxDAO;
	}

	private Future<Flux> saveFlux(Flux flux, ActionEnum action) {
		Future<Flux> future = Future.future();
		fluxDAO.save(flux).setHandler(saveAr -> {
			if (saveAr.succeeded()) {
				logger.debug(Constants.LOG_MESSAGE, flux.getCorrelationId(),
						"Flux sauvegardé en BD avec le statut " + saveAr.result().getStatus());

				this.historiserEtatFlux(flux, action).setHandler(histoFluxAr -> {
					if (histoFluxAr.succeeded()) {
						logger.debug(Constants.LOG_MESSAGE, flux.getCorrelationId(), "Flux Historisé en BD");
						future.complete(saveAr.result());
					} else {
						logger.error(Constants.LOG_ERROR, flux.getCorrelationId(), "Historisation d'un flux en base");
						future.fail(histoFluxAr.cause().getMessage());
					}
				});

			} else {
				logger.error(Constants.LOG_ERROR, flux.getCorrelationId(), "Sauvegarde d'un flux en base", null,
						saveAr.cause());
				future.fail(saveAr.cause().getMessage());
			}
		});

		return future;
	}

	private Future<HistoFlux> historiserEtatFlux(Flux flux, ActionEnum action) {
		Future<HistoFlux> future = Future.future();
		HistoFlux histoFlux = new HistoFlux(flux.getDate(), flux.getCorrelationId(), flux.getConsumerCorrelationId(),
				flux.getConsumerQueueName(), flux.getStatus(), action);
		histoFluxDAO.save(histoFlux).setHandler(saveAr -> {
			if (saveAr.succeeded()) {
				logger.debug(Constants.LOG_MESSAGE, flux.getCorrelationId(),
						"Flux historisé en BD avec le statut " + saveAr.result().getStatus());
				future.complete(saveAr.result());
			} else {
				logger.error(Constants.LOG_ERROR, flux.getCorrelationId(), "Historisation d'un flux en base", null,
						saveAr.cause());
				future.fail(saveAr.cause().getMessage());
			}
		});
		return future;
	}

	private void registedMessage(Message<Object> message) {

		String action = "Immatriculation d'un message";
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();
		String correlationId = null;
		String producer = null;
		Long fileSize = null;
		String fileUrl = null;
		try {
			correlationId = (String) properties.get(Flux.KEY_JSON_CORRELATION_ID);
			producer = (String) properties.get(Flux.KEY_JSON_PRODUCER);
			fileSize = Long.valueOf((String) properties.get(Flux.KEY_JSON_FILESIZE));
			fileUrl = (String) properties.get(Flux.KEY_JSON_FILE_URL);
		} catch (ClassCastException e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
		}

		if (correlationId != null && producer != null && event.getCreationtime() != null) {
			Flux flux = new Flux(correlationId, StatusEnum.IMMATRICULE, event.getCreationtime(), producer);
			flux.setFilesize(fileSize);
			flux.setFileUrl(fileUrl);
			this.saveFlux(flux, ActionEnum.SYSTEME);
		} else {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "Wrong parameters");
		}

	}

	private void receivedMessage(Message<Object> message) {
		String action = "Réception d'un message";
		saveFluxWithMessageVortex(message, action, StatusEnum.RECU);
	}

	private void routingMessage(Message<Object> message) {
		String action = "Routage d'un message";
		saveFluxWithMessageVortex(message, action, StatusEnum.ROUTAGE);
	}

	private void toDeliveredMessage(Message<Object> message) {

		String action = "Stockage d'un message dans une zone de réception";
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();
		Optional<MessageVortex> optMessageVortex = null;
		JsonObject messageModel = null;
		String queueName = null;
		String correlationId = null;
		String consumerCorrelationId = null;
		String consumerBusinessId = null;
		String fileUrl = null;
		try {
			if (properties.get(KEY_JSON_MESSAGE_VORTEX) instanceof JsonObject) {
				optMessageVortex = MessageVortex.fromJson((JsonObject) properties.get(KEY_JSON_MESSAGE_VORTEX));
			} else {
				optMessageVortex = Optional.empty();
			}
			messageModel = (JsonObject) properties.get(KEY_JSON_MESSAGE_MODEL);
			correlationId = (String) properties.get((Flux.KEY_JSON_CORRELATION_ID));
			queueName = (String) properties.get(KEY_JSON_QUEUE_NAME);
			consumerCorrelationId = (String) properties.get((Flux.KEY_JSON_CONSUMER_CORRELATION_ID));
			consumerBusinessId = (String) properties.get(Flux.KEY_JSON_CONSUMER_BUSINESS_ID);
			fileUrl = (String) properties.get(Flux.KEY_JSON_FILE_URL);
		} catch (ClassCastException e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
			message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, e.getMessage());
		}

		if (messageModel != null && correlationId != null && queueName != null && consumerBusinessId != null) {
			try {
				Flux flux = new Flux(messageModel, correlationId, consumerBusinessId, queueName, StatusEnum.A_ENVOYER);
				this.saveFlux(flux, ActionEnum.REMIS_A_DISPOSITION);
				message.reply(204);
			} catch (Exception e) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
				message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, e.getMessage());
			}

		} else if (optMessageVortex.isPresent()) {
			if (queueName != null && consumerCorrelationId != null && consumerBusinessId != null
					&& event.getCreationtime() != null) {
				MessageVortex messageVortex = optMessageVortex.get();
				Flux flux = new Flux(messageVortex, event.getCreationtime(), StatusEnum.A_ENVOYER);
				flux.setConsumerCorrelationId(consumerCorrelationId);
				flux.setConsumerBusinessId(consumerBusinessId);
				flux.setConsumerQueueName(queueName);
				flux.setFileUrl(fileUrl);
				message.reply(204);
				this.saveFlux(flux, ActionEnum.SYSTEME);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "Wrong parameters");
				message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, "Wrong parameters");
			}
		}

	}

	private void emittedMessage(Message<Object> message) {
		String action = "Emission d'un message";
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();
		String consumerCorrelationId = null;
		try {
			consumerCorrelationId = (String) properties.get((Flux.KEY_JSON_CONSUMER_CORRELATION_ID));
		} catch (ClassCastException e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
		}

		if (consumerCorrelationId != null && event.getCreationtime() != null) {
			fluxDAO.getFluxByConsumerCorrelationId(consumerCorrelationId).setHandler(getFluxAr -> {
				if (getFluxAr.succeeded()) {
					Flux flux = getFluxAr.result();
					flux.setDate(event.getCreationtime());
					flux.setStatus(StatusEnum.DELIVRE);
					this.saveFlux(flux, ActionEnum.SYSTEME);
				} else {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, getFluxAr.cause());
				}
			});

		} else {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "Wrong parameters");
		}
	}

	private void inEmissionMessage(Message<Object> message) {
		String action = "Message en emission";
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();

		JsonObject listMessageVortex = (JsonObject) properties.get("listMessageVortex");
		JsonArray jsonArrayMsgVortex = listMessageVortex.getJsonArray("messages_vortex");

		if (jsonArrayMsgVortex.size() > 0) {
			for (int i = 0; i < jsonArrayMsgVortex.size(); i++) {
				JsonObject messageVortexJson = jsonArrayMsgVortex.getJsonObject(i);
				Optional<MessageVortex> msgVortex = MessageVortex.fromJson(messageVortexJson);
				if (msgVortex.isPresent()) {
					MessageVortex messageVortex = msgVortex.get();
					String consumerCorrelationId = messageVortex.getIdentifiant();
					if (consumerCorrelationId != null && event.getCreationtime() != null) {
						fluxDAO.getFluxByConsumerCorrelationId(consumerCorrelationId).setHandler(getFluxAr -> {
							if (getFluxAr.succeeded()) {
								Flux flux = getFluxAr.result();
								flux.setDate(event.getCreationtime());
								flux.setStatus(StatusEnum.EN_EMISSION);
								this.saveFlux(flux, ActionEnum.SYSTEME);
							} else {
								logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null,
										getFluxAr.cause());
							}
						});

					} else {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "Wrong parameters");
					}
				}
			}
		}

	}

	private Future<Flux> setFluxToDeliveredToError(Message<Object> message) {
		Future<Flux> future = Future.future();
		String consumerCorrelationId = (String) message.body();

		fluxDAO.getFluxByConsumerCorrelationId(consumerCorrelationId).setHandler(ar -> {
			if (ar.succeeded()) {
				// On rajoute le flux en erreur
				Flux flux = ar.result();
				flux.setId(null);
				flux.setStatus(StatusEnum.ERREUR);
				flux.setErrorCode(ERR_CORE_SUPP_ZR);
				flux.setErrorMessage(ERR_CORE_SUPP_ZR_LIBELLE);
				flux.setDate(Instant.now());
				flux.setConsumerQueueName(DLQ_NAME);
				this.saveFlux(flux, ActionEnum.DEPLACEMENT_ZE).setHandler(arSave -> {
					if (arSave.succeeded()) {
						logger.debug(Constants.LOG_MESSAGE, flux.getCorrelationId(),
								"Flux sauvegardé en BD avec le statut " + flux.getStatus());
						future.complete(arSave.result());
					} else {
						logger.error(Constants.LOG_ERROR, consumerCorrelationId, "Flux non sauvegardé", null,
								ar.cause());
						future.fail(arSave.cause());
					}
				});

			} else {
				logger.error(Constants.LOG_ERROR, consumerCorrelationId, "Flux non trouvé", null, ar.cause());
				future.fail(ar.cause());
			}
		});
		return future;
	}

	private void setFluxInError(Message<Object> message) {
		String action = "Mis en erreur d'un message";
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();

		if (properties.get(KEY_JSON_MESSAGE_VORTEX) != null) {
			saveFluxWithMessageVortexInError(message, action);
		} else {
			saveFluxWithoutMessageVortexInError(message, action);
		}
	}

	private void saveFluxWithoutMessageVortexInError(Message<Object> message, String action) {
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();
		String correlationId = null;
		String producer = null;
		Long fileSize = null;
		String fileUrl = null;
		String errorCode = null;
		String errorMessage = null;
		String stacktrace = null;
		String queueName = null;
		try {
			correlationId = (String) properties.get(Flux.KEY_JSON_CORRELATION_ID);
			producer = (String) properties.get(Flux.KEY_JSON_PRODUCER);
			if (properties.get(Flux.KEY_JSON_FILESIZE) != null) {
				fileSize = Long.valueOf((String) properties.get(Flux.KEY_JSON_FILESIZE));
			}
			fileUrl = (String) properties.get(Flux.KEY_JSON_FILE_URL);
			queueName = (String) properties.get(KEY_JSON_QUEUE_NAME);
			errorCode = (String) properties.get(KEY_JSON_ERROR_CODE);
			errorMessage = (String) properties.get(KEY_JSON_ERROR_MESSAGE);
			stacktrace = (String) properties.get(KEY_JSON_STACKTRACE);
		} catch (ClassCastException e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
		}

		if (correlationId != null && errorCode != null && errorMessage != null && event.getCreationtime() != null) {
			Flux flux = new Flux(correlationId, StatusEnum.ERREUR, event.getCreationtime(), producer);
			flux.setFilesize(fileSize);
			flux.setFileUrl(fileUrl);
			flux.setErrorCode(errorCode);
			flux.setErrorMessage(errorMessage);
			flux.setStacktrace(stacktrace);
			if (queueName != null) {
				flux.setConsumerQueueName(queueName);
			}
			this.saveFlux(flux, ActionEnum.SYSTEME);
		} else {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action,
					"Wrong parameters in saveFluxWithoutMessageVortexInError");
		}
	}

	private void saveFluxWithMessageVortexInError(Message<Object> message, String action) {
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();

		Optional<MessageVortex> optMessageVortex = null;
		String errorCode = null;
		String errorMessage = null;
		String stacktrace = null;
		String queueName = null;
		String fileUrl = null;
		try {
			optMessageVortex = MessageVortex
					.fromJson(event.toJson().getJsonObject("properties").getJsonObject("message_vortex"));
			queueName = (String) properties.get(KEY_JSON_QUEUE_NAME);
			errorCode = (String) properties.get(KEY_JSON_ERROR_CODE);
			errorMessage = (String) properties.get(KEY_JSON_ERROR_MESSAGE);
			stacktrace = (String) properties.get(KEY_JSON_STACKTRACE);
			fileUrl = (String) properties.get(KEY_JSON_FILE_URL);
		} catch (ClassCastException e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
		}

		if (optMessageVortex.isPresent()) {
			if (errorCode != null && errorMessage != null && event.getCreationtime() != null) {
				MessageVortex messageVortex = optMessageVortex.get();
				Flux flux = new Flux(messageVortex, event.getCreationtime(), StatusEnum.ERREUR);
				flux.setErrorCode(errorCode);
				flux.setErrorMessage(errorMessage);
				flux.setStacktrace(stacktrace);
				flux.setFileUrl(fileUrl);
				if (queueName != null) {
					flux.setConsumerQueueName(queueName);
					flux.setConsumerCorrelationId(messageVortex.getIdentifiant());
				}
				this.saveFlux(flux, ActionEnum.SYSTEME);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action,
						"Wrong parameters in saveFluxWithMessageVortexInError");
			}
		}
	}

	private void setFluxConsumerInError(Message<Object> message) {
		String action = "Mis en erreur d'un message en cours d'emmission";
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();
		String consumerCorrelationId = null;
		String errorCode = null;
		String errorMessage = null;
		String stacktrace = null;
		String fileUrl = null;
		try {
			consumerCorrelationId = (String) properties.get((Flux.KEY_JSON_CONSUMER_CORRELATION_ID));
			errorCode = (String) properties.get(KEY_JSON_ERROR_CODE);
			errorMessage = (String) properties.get(KEY_JSON_ERROR_MESSAGE);
			stacktrace = (String) properties.get(KEY_JSON_STACKTRACE);
			fileUrl = (String) properties.get(KEY_JSON_FILE_URL);
		} catch (ClassCastException e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
		}

		if (consumerCorrelationId != null && errorCode != null && errorMessage != null
				&& event.getCreationtime() != null) {
			// TODO Tout pourri Merci L'asynchrone !
			String errorCode2 = errorCode;
			String errorMessage2 = errorMessage;
			String stacktrace2 = stacktrace;
			String fileUrl2 = fileUrl;
			fluxDAO.getFluxByConsumerCorrelationId(consumerCorrelationId).setHandler(getFluxAr -> {
				if (getFluxAr.succeeded()) {
					Flux flux = getFluxAr.result();
					flux.setDate(event.getCreationtime());
					flux.setStatus(StatusEnum.ERREUR);
					flux.setErrorCode(errorCode2);
					flux.setErrorMessage(errorMessage2);
					flux.setStacktrace(stacktrace2);
					flux.setFileUrl(fileUrl2);
					this.saveFlux(flux, ActionEnum.SYSTEME);
				} else {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, getFluxAr.cause());
				}
			});

		} else {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action,
					"Wrong parameters in setFluxConsumerInError");
		}
	}

	private void saveFluxWithMessageVortex(Message<Object> message, String action, StatusEnum statusEnum) {
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();
		Optional<MessageVortex> optMessageVortex = null;
		Object objectProp = properties.get(KEY_JSON_MESSAGE_VORTEX);
		String fileUrl = (String) properties.get(Flux.KEY_JSON_FILE_URL);
		if (objectProp != null && objectProp instanceof JsonObject) {
			JsonObject jsonMV = ((JsonObject) properties.get(KEY_JSON_MESSAGE_VORTEX));
			optMessageVortex = MessageVortex.fromJson(jsonMV);
		} else {
			optMessageVortex = Optional.empty();
		}

		if (optMessageVortex.isPresent()) {
			MessageVortex messageVortex = optMessageVortex.get();
			if (event.getCreationtime() != null) {
				Flux flux = new Flux(messageVortex, event.getCreationtime(), statusEnum);
				flux.setFileUrl(fileUrl);
				this.saveFlux(flux, ActionEnum.SYSTEME);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action,
						"Wrong parameters in saveFluxWithMessageVortex");
			}
		} else {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, "Le messageVortex est absent");
		}
	}

	private void loadAllFlux(Message<Object> message) {
		String action = "Chargement des flux";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		JsonObject json = (JsonObject) message.body();
		String page = json.getString(FluxVerticle.KEY_JSON_PAGE);
		String pageLimit = json.getString(FluxVerticle.KEY_JSON_PAGE_LIMIT);
		String sort = json.getString(FluxVerticle.KEY_JSON_SORT);
		String sortOrder = json.getString(FluxVerticle.KEY_JSON_SORT_ORDER);
		String date = json.getString(Flux.KEY_JSON_DATE);

		Map<String, Object> filters = PaginationUtils.genererFiltresPourPagination(message,Flux.KEY_JSON_DATE,date);

		filters.put(Flux.KEY_DB_CORRELATION_ID, json.getString(Flux.KEY_JSON_CORRELATION_ID));
		filters.put(Flux.KEY_DB_STATUS, json.getString(Flux.KEY_JSON_STATUS));
		filters.put(Flux.KEY_DB_PRODUCER, json.getString(Flux.KEY_JSON_PRODUCER));
		filters.put(Flux.KEY_DB_EXCHANGE_NAME, json.getString(Flux.KEY_JSON_EXCHANGE_NAME));
		filters.put(Flux.KEY_DB_ROUTING_KEY, json.getString(Flux.KEY_JSON_ROUTING_KEY));
		filters.put(Flux.KEY_DB_CONSUMER_CORRELATION_ID, json.getString(Flux.KEY_JSON_CONSUMER_CORRELATION_ID));
		filters.put(Flux.KEY_DB_CONSUMER_BUSINESS_ID, json.getString(Flux.KEY_JSON_CONSUMER_BUSINESS_ID));
		filters.put(Flux.KEY_DB_CONSUMER_QUEUE_NAME, json.getString(Flux.KEY_JSON_CONSUMER_QUEUE_NAME));
		filters.put(Flux.KEY_DB_ERROR_CODE, json.getString(Flux.KEY_JSON_ERROR_CODE));
		filters.put(Flux.KEY_DB_FILENAME, json.getString(Flux.KEY_JSON_FILENAME));
		filters.put(Flux.KEY_JSON_LIST_CORRELATION_ID, json.getString(Flux.KEY_JSON_LIST_CORRELATION_ID));
		filters.put(Flux.KEY_JSON_LIST_FILENAME, json.getString(Flux.KEY_JSON_LIST_FILENAME));

		fluxDAO.countFlux(filters).setHandler(countFluxAr -> {
			if (countFluxAr.succeeded()) {
				Integer borneMoins = null;
				Integer bornePlus = null;
				Pagination<JsonObject> paginateur = null;
				Integer nbFlux = countFluxAr.result();
				if (page != null && pageLimit != null) {
					try {
						paginateur = new Pagination<>(page, pageLimit);
						borneMoins = paginateur.getBorneMoins() - 1;
						bornePlus = paginateur.getBornePlus() - 1;
					} catch (PaginationException e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						message.fail(400,
								"Les paramêtres " + KEY_JSON_PAGE + " et " + KEY_JSON_PAGE_LIMIT + " sont erronés");
					}

				} else {
					paginateur = new Pagination<>(0, nbFlux);
					borneMoins = paginateur.getBorneMoins();
					bornePlus = paginateur.getBornePlus();
				}

				fluxDAO.loadFlux(borneMoins, bornePlus, filters, sort, sortOrder).setHandler(loadFluxAr -> {
					if (loadFluxAr.succeeded()) {
						List<Future> futLoop = new ArrayList<>();
						// On utilise la hashmap pour ne pas perdre le tri...
						HashMap<Integer, Flux> mapFlux = new HashMap<Integer, Flux>();
						int i = 0;
						for (Flux flux : loadFluxAr.result()) {
							futLoop.add(addHistoryAndNotif(flux, mapFlux, i));
							i++;
						}

						CompositeFuture.all(futLoop).setHandler(arLoop -> {
							if (arLoop.succeeded()) {
								JsonArray jsonFluxArray = new JsonArray();
								for (Entry<Integer, Flux> entry : mapFlux.entrySet()) {
									jsonFluxArray.add(entry.getValue().toJsonObject());
								}
								if (jsonFluxArray.size() != nbFlux && jsonFluxArray.size() != 0) {
									DeliveryOptions opts = new DeliveryOptions();
									opts.addHeader("count", nbFlux.toString());
									message.reply(jsonFluxArray, opts);
								} else {
									message.reply(jsonFluxArray);
								}
							} else {
								message.reply(arLoop.cause().getMessage());
							}
						});

					} else {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
								loadFluxAr.cause());
						message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERREUR_GET_FLUX);
					}
				});

			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", countFluxAr.cause());
				message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERREUR_COUNT_FLUX);
			}
		});

	}

	private void getFluxNonTrouves(Message<Object> message) {
		String action = "getFluxNonTrouves(Message<Object> message)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		JsonObject json = (JsonObject) message.body();

		String liste_correlation_id = json.getString(Flux.KEY_JSON_LIST_CORRELATION_ID);
		String liste_file_name = json.getString(Flux.KEY_JSON_LIST_FILENAME);

		if ((liste_correlation_id == null || liste_correlation_id.trim().isEmpty())
				&& (liste_file_name == null || liste_file_name.trim().isEmpty())) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
					ERREUR_GET_FLUX_NON_TROUVES_FORMAT);
			message.fail(HttpURLConnection.HTTP_BAD_REQUEST, ERREUR_GET_FLUX_NON_TROUVES_FORMAT);
			return;
		}

		Map<String, Object> filters = new HashMap<>();
		filters.put(Flux.KEY_JSON_LIST_CORRELATION_ID, liste_correlation_id);
		filters.put(Flux.KEY_JSON_LIST_FILENAME, liste_file_name);

		fluxDAO.loadFluxLot(filters).setHandler(loadFluxAr -> {
			if (loadFluxAr.succeeded()) {
				List<String> listeFluxTrouves = loadFluxAr.result();

				List<String> listeFluxATrouver = new ArrayList<String>();
				// correlation_id
				if (liste_correlation_id != null) {
					String[] data = StringUtils.split((String) liste_correlation_id, ",");
					if (data != null) {
						for (int i = 0; i < data.length; i++) {
							listeFluxATrouver.add(data[i]);
						}
					}
				}

				// file_name
				if (liste_file_name != null) {
					String[] data = StringUtils.split((String) liste_file_name, ",");
					if (data != null) {
						for (int i = 0; i < data.length; i++) {
							listeFluxATrouver.add(data[i]);
						}
					}
				}

				// suppression des flux trouvés dans la liste
				for (Iterator<String> iterator = listeFluxATrouver.iterator(); iterator.hasNext();) {
					String fluxIdATrouver = iterator.next();
					if (listeFluxTrouves.contains(fluxIdATrouver)) {
						iterator.remove();
					}
				}
				JsonArray jsonFluxArray = new JsonArray();
				for (Iterator<String> iterator = listeFluxATrouver.iterator(); iterator.hasNext();) {
					String fluxIdATrouver = iterator.next();
					jsonFluxArray.add(fluxIdATrouver);
				}
				message.reply(jsonFluxArray);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", loadFluxAr.cause());
				message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERREUR_GET_FLUX_NON_TROUVES);
			}
		});
	}

	private void getFluxByCorrelationId(Message<Object> message) {
		String action = "Récupération d'un flux par son correlation_id";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		String correlationId = (String) message.body();

		fluxDAO.getFluxByCorrelationId(correlationId).setHandler(getFluxAr -> {
			if (getFluxAr.succeeded()) {
				List<Future> futLoop = new ArrayList<>();
				// On utilise la hashmap pour ne pas perdre le tri...
				HashMap<Integer, Flux> mapFlux = new HashMap<Integer, Flux>();
				int i = 0;
				for (Flux flux : getFluxAr.result()) {
					futLoop.add(addHistoryAndNotif(flux, mapFlux, i));
					i++;
				}
				CompositeFuture.all(futLoop).setHandler(arLoop -> {
					if (arLoop.succeeded()) {
						JsonArray jsonFluxArray = new JsonArray();
						for (Entry<Integer, Flux> entry : mapFlux.entrySet()) {
							jsonFluxArray.add(entry.getValue().toJsonObject());
						}
						message.reply(jsonFluxArray);
					} else {
						message.reply(arLoop.cause());
					}
				});

			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", getFluxAr.cause());
				message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERREUR_GET_FLUX);
			}
		});

	}

	private void getFluxByCorrelationIdAndConsumerCorrelationId(Message<Object> message) {
		String action = "Récupération d'un flux par son correlation_id et son consumer_correlation_id";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		JsonObject jsonMessage = (JsonObject) message.body();
		String correlationId = jsonMessage.getString(Flux.KEY_JSON_CORRELATION_ID);
		String consumerCorrelationId = jsonMessage.getString(Flux.KEY_JSON_CONSUMER_CORRELATION_ID);

		fluxDAO.getFluxByCorrelationIdAndConsumerCorrelationId(correlationId, consumerCorrelationId)
				.setHandler(getFluxAr -> {
					if (getFluxAr.succeeded()) {
						List<Future> futLoop = new ArrayList<>();
						// On utilise la hashmap pour ne pas perdre le tri...
						HashMap<Integer, Flux> mapFlux = new HashMap<Integer, Flux>();
						int i = 0;
						for (Flux flux : getFluxAr.result()) {
							futLoop.add(addHistoryAndNotif(flux, mapFlux, i));
							i++;
						}
						CompositeFuture.all(futLoop).setHandler(arLoop -> {
							if (arLoop.succeeded()) {
								JsonArray jsonFluxArray = new JsonArray();
								for (Entry<Integer, Flux> entry : mapFlux.entrySet()) {
									jsonFluxArray.add(entry.getValue().toJsonObject());
								}
								message.reply(jsonFluxArray);
							} else {
								message.reply(arLoop.cause());
							}
						});

					} else {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
								getFluxAr.cause());
						message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERREUR_GET_FLUX);
					}
				});

	}

	private Future<Void> addHistoryAndNotif(Flux flux, HashMap<Integer, Flux> mapFlux, Integer keyMap) {
		String action = "addHistoryAndNotif";
		Future<Void> futCompleterFlux = Future.future();

		fluxDAO.loadAllStatusHistoryFluxByCorrelationsId(flux.getCorrelationId(), flux.getConsumerCorrelationId())
				.setHandler(loadStatusAr -> {
					if (loadStatusAr.failed()) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
								loadStatusAr.cause());
						futCompleterFlux.fail(ERREUR_HISTORIQUE_STATUS_FLUX);
					} else {
						flux.setStatusHistoryList(loadStatusAr.result());
						if (flux.getStatus().getOrder() > StatusEnum.A_ENVOYER.getOrder() && flux.getConsumerBusinessId()!=null) {
							notificationDAO.getAllNotificationsByCorrelationId(flux.getConsumerCorrelationId())
									.setHandler(arGetAllNotif -> {
										if (arGetAllNotif.failed()) {
											logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action,
													"", arGetAllNotif.cause());
											futCompleterFlux.fail(ERREUR_NOTIFICATION_FLUX);
										} else {
											flux.setNotificationsList(arGetAllNotif.result());
											mapFlux.put(keyMap, flux);
											futCompleterFlux.complete();
										}
									});
						} else {
							mapFlux.put(keyMap, flux);
							futCompleterFlux.complete();
						}
					}
				});

		return futCompleterFlux;
	}

	private void obsoleteFlux(Message<Object> message) {
		String action = "Obsoletisation d'un flux";
		String consumerCorrelationId = (String) message.body();

		if (consumerCorrelationId != null) {
			fluxDAO.getFluxByConsumerCorrelationId(consumerCorrelationId).setHandler(getFluxAr -> {
				if (getFluxAr.succeeded()) {
					Flux flux = getFluxAr.result();
					flux.setDate(Instant.now());
					flux.setStatus(StatusEnum.OBSOLETE);
					this.saveFlux(flux, ActionEnum.PURGE);
				} else {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, getFluxAr.cause());
				}
			});

		} else {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "Wrong parameters");
		}
	}

	private void resetFluxToDelivered(Message<Object> message) {

		String action = "Remise à l'état à envoyer d'un flux";
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();

		final String consumerCorrelationId = (String) properties.get((KEY_JSON_CONSUMER_CORRELATION_ID));

		if (consumerCorrelationId != null) {
			try {
				this.fluxDAO.deleteByConsumerCorrelationIdAndStatusGreaterThanOrEqual(consumerCorrelationId,
						StatusEnum.A_ENVOYER).setHandler(arDelete -> {
							if (arDelete.succeeded()) {
								this.notificationDAO.deleteNotification(consumerCorrelationId);
								message.reply(204);

							} else {
								logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null,
										arDelete.cause());
								message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, arDelete.cause().getMessage());
							}
						});

			} catch (Exception e) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, e);
				message.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, e.getMessage());
			}
		} else {
			message.fail(400, "Un des paramêtres de la méthode est null");
		}
	}

	private void fluxExists(Message<Object> message) {

		String action = "Test si un flux existe";
		VortexEvent event = (VortexEvent) message.body();
		Map<String, Object> properties = event.getProperties();

		final String consumerCorrelationId = (String) properties.get((KEY_JSON_CONSUMER_CORRELATION_ID));

		if (consumerCorrelationId != null) {

			this.fluxDAO.getFluxByConsumerCorrelationId(consumerCorrelationId).setHandler(arGet -> {
				if (arGet.succeeded()) {
					if (arGet.result() != null) {
						message.reply(true);
					} else {
						message.reply(false);
					}

				} else {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, null, arGet.cause());
					message.fail(500, arGet.cause().getMessage());
				}
			});

		} else {
			message.fail(400, "Un des paramêtres de la méthode est null");
		}
	}

}