//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.flux;

import fr.edu.vortex.management.utilisateur.pojo.NomenclatureEvtTrace;

public enum ActionEnum {
	DEPLACEMENT_ZE(NomenclatureEvtTrace.Deplacement_ZE.getValeur()),
	PURGE(NomenclatureEvtTrace.Purge.getValeur()), 
	REMIS_A_DISPOSITION(NomenclatureEvtTrace.Remis_A_Disposition.getValeur()),
	SYSTEME("SYSTEME");

	private String value;

	ActionEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
