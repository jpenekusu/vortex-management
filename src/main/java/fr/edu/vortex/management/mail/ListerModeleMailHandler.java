package fr.edu.vortex.management.mail;

import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.api.Pagination;
import fr.edu.vortex.management.api.PaginationException;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.persistence.dao.ModeleMailDAO;
import fr.edu.vortex.management.persistence.dao.impl.ModeleMailDAOImpl;
import fr.edu.vortex.management.utils.PaginationUtils;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ListerModeleMailHandler extends ModeleMailHandler implements Handler<Message<Object>> {

	private static final Logger logger = LoggerFactory.getLogger(ListerModeleMailHandler.class);

	private static final String LOG_START = "Acteur permettant de lister les modèles de mail créés.";

	Vertx vertx;

	ModeleMailDAO modeleMailDAO;

	public static ListerModeleMailHandler create(Vertx vertx) {
		return new ListerModeleMailHandler(vertx);
	}

	private ListerModeleMailHandler(Vertx vertx) {
		this.vertx = vertx;
		modeleMailDAO = new ModeleMailDAOImpl();
		modeleMailDAO.init(vertx);
		logger.info(LOG_MESSAGE, LOG_NO_CORRELATION_ID, LOG_START);
	}

	@Override
	public void handle(Message<Object> event) {
		String action = "getModeleMails";

		if (!(event.body() instanceof JsonObject)) {
			event.fail(400, "mal formaté");
			return;
		}

		JsonObject criteres = (JsonObject) event.body();
		String page = criteres.getString(FluxVerticle.KEY_JSON_PAGE);
		String pageLimit = criteres.getString(FluxVerticle.KEY_JSON_PAGE_LIMIT);
		String sort = criteres.getString(FluxVerticle.KEY_JSON_SORT);
		String sortOrder = criteres.getString(FluxVerticle.KEY_JSON_SORT_ORDER);
		
		PaginationUtils.genererFiltresPourPagination(event,null,null);

      	JsonObject filters = new JsonObject();
		filters.put(ModeleMail.KEY_JSON_NOM, criteres.getString(ModeleMail.KEY_JSON_NOM));
		filters.put(ModeleMail.KEY_JSON_DESTINATAIRE, criteres.getString(ModeleMail.KEY_JSON_DESTINATAIRE));
		filters.put(ModeleMail.KEY_JSON_SYSTEME, criteres.getBoolean(ModeleMail.KEY_JSON_SYSTEME));
		filters.put(ModeleMail.KEY_JSON_PERMISSIONS, criteres.getString(ModeleMail.KEY_JSON_PERMISSIONS));

		modeleMailDAO.countModeles(filters).setHandler(countAr -> {
			if (countAr.succeeded()) {
				Integer borneMoins = null;
				Integer bornePlus = null;
				Pagination<JsonObject> paginateur = null;
				Integer nbModeles = countAr.result();
				if (page != null && pageLimit != null) {
					try {
						paginateur = new Pagination<>(page, pageLimit);
						borneMoins = paginateur.getBorneMoins() - 1;
						bornePlus = paginateur.getBornePlus() - 1;
					} catch (PaginationException e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						event.fail(400,
								"Les paramêtres " + FluxVerticle.KEY_JSON_PAGE + " et " + FluxVerticle.KEY_JSON_PAGE_LIMIT + " sont erronés");
					}

				} else {
					paginateur = new Pagination<>(0, nbModeles);
					borneMoins = paginateur.getBorneMoins();
					bornePlus = paginateur.getBornePlus();
				} 

				modeleMailDAO.listerModeles(borneMoins, bornePlus, filters, sort, sortOrder).setHandler(ar -> {
					if (ar.succeeded()) {
						JsonArray result = new JsonArray();
						for (ModeleMail item : ar.result()) {
							result.add(item.toJson());
						}
						if (result.size() != nbModeles && result.size() != 0) {
							DeliveryOptions opts = new DeliveryOptions();
							opts.addHeader("count", nbModeles.toString());
							event.reply(result, opts);
						} else {
							event.reply(result);
						}

					} else {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
								ar.cause());
						event.fail(500, ERR_MANAGEMENT_LISTER_MODELE_TECH);
					}
				});

			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", countAr.cause());
				event.fail(500, ERR_MANAGEMENT_COUNT_MODELE_TECH);
			}
		});

	}
}
