package fr.edu.vortex.management.mail;

import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.dao.ModeleMailDAO;
import fr.edu.vortex.management.persistence.dao.ModeleMailPermissionDAO;
import fr.edu.vortex.management.persistence.dao.PermissionDAO;
import fr.edu.vortex.management.persistence.dao.impl.ModeleMailDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.ModeleMailPermissionDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.PermissionDAOImp;
import fr.edu.vortex.management.utilisateur.exception.Failure;
import fr.edu.vortex.management.utilisateur.exception.ManagementException;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class CreerModeleMailHandler extends ModeleMailHandler implements Handler<Message<JsonObject>> {

	private static final Logger logger = LoggerFactory.getLogger(CreerModeleMailHandler.class);

	private static final String ERR_NOM_EXISTE_DEJA = "Impossible de créer le modèle car il existe déjà un modèle avec ce nom";

    private static final String LOG_START = "Acteur permettant de créer un modèle de mail";

	ModeleMailDAO modeleMailDAO;
	ModeleMailPermissionDAO modeleMailPermissionDAO;
	PermissionDAO permissionDao;

	public static CreerModeleMailHandler create(Vertx vertx) {
		return new CreerModeleMailHandler(vertx);
	}

	private CreerModeleMailHandler(Vertx vertx) {
		this.vertx = vertx;

		modeleMailDAO = new ModeleMailDAOImpl();
		modeleMailDAO.init(vertx);

		modeleMailPermissionDAO = new ModeleMailPermissionDAOImpl();
		modeleMailPermissionDAO.init(vertx);

		permissionDao = new PermissionDAOImp();
		permissionDao.init(vertx);

		logger.info(LOG_MESSAGE, LOG_NO_CORRELATION_ID, LOG_START);

	}

	@Override
	public void handle(Message<JsonObject> event) {

		if (!(event.body() instanceof JsonObject)) {
			event.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		JsonObject newJsonModeleMail = (JsonObject) event.body();

		if (!checkParams(event, newJsonModeleMail)) {
			return;
		}

		logger.trace(
				LOG_MESSAGE_WITH_DATA,
				LOG_NO_CORRELATION_ID,
				"Demande de creation de modele de mail entrante",
				newJsonModeleMail);

		ModeleMail modele = ModeleMail.fromJson(newJsonModeleMail);

		JsonArray permissions = newJsonModeleMail.getJsonArray(ModeleMail.KEY_JSON_PERMISSIONS);

		if (permissions != null && !permissionsSontValidesMail(permissions)) {
			event.fail(HTTP_BAD_REQUEST, ERR_MAL_FORMATE_PERMISSIONS);
			return;
		}

		verifierNomValide(modele.getNom())
		.compose(v -> verifierPermissionsConnues(permissions,permissionDao, ERR_ROLE_PERMISSION_NON_EXISTANTE))
		.compose(v -> modeleMailDAO.creer(modele))
		.compose(modeleCree -> creerLiensPermissions(modeleCree, permissions))
		.setHandler(ar -> {
			if (ar.succeeded()) {
				event.reply(ar.result().toJson());
			} else {
				logger.error(
						Constants.LOG_ERROR,
						LOG_NO_CORRELATION_ID,
						String.format(ERR_MANAGEMENT_CREATION_MODELE_TECH, ar.cause().getMessage()),
						ERR_MANAGEMENT_MODELE_MAIL_TECH,
						ar.cause());

				Optional<Failure> fail = Failure.create(ar.cause());
				if (fail.isPresent()) {
					event.fail(fail.get().getCode(), fail.get().getMessage());
				} else {
					event.fail(HTTP_INTERNAL_ERROR, ERR_MANAGEMENT_CREATION_MODELE_TECH);
				}
			}
		});

	}




	/**
	 * Vérifie si le libelle du rôle à créer est déjà utilisé par un autre rôle
	 * 
	 * @param nom
	 *            libelle à vérifier
	 * @return
	 */
	private Future<Void> verifierNomValide(String nom) {
		Future<Void> fut = Future.future();
		modeleMailDAO.chercherModeleMailParNom(nom).setHandler(ar -> {
			if (ar.succeeded()) {
				List<ModeleMail> result = ar.result();
				if (result.isEmpty()) {
					fut.complete();
				} else {
					ManagementException errMetier = new ManagementException(HTTP_CONFLICT, ERR_NOM_EXISTE_DEJA);
					fut.fail(errMetier);
				}
			} else {
				fut.fail(ar.cause());
			}
		});
		return fut;
	}

	/**
	 * Crée un liens entre le modele et la permission pour chaque permissions
	 * 
	 * @param modele
	 *            le modele cible
	 * @param permissions
	 *            l'ensemble des permissions visées par le traitement
	 * @return Future contenant le role
	 */
	private Future<ModeleMail> creerLiensPermissions(ModeleMail modele, JsonArray permissions) {

		if (permissions == null || permissions.isEmpty()) {
			return Future.succeededFuture(modele);
		}

		Future<ModeleMail> fut = Future.future();

		List<Future> creationPerms = permissions.stream()
				.map(o -> (JsonObject) o)
				.map(jsonPerm -> modeleMailPermissionDAO.create(modele.getId(), jsonPerm.getString(Permission.KEY_JSON_CODE)))
				.collect(Collectors.toList());

		CompositeFuture.all(creationPerms).setHandler(ar -> {
			if (ar.succeeded()) {
				fut.complete(modele);
			} else {
				fut.fail(ar.cause());
			}
		});
		return fut;
	}


}
