package fr.edu.vortex.management.mail;

import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CONFLICT;

import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.dao.ModeleMailDAO;
import fr.edu.vortex.management.persistence.dao.ModeleMailPermissionDAO;
import fr.edu.vortex.management.persistence.dao.PermissionDAO;
import fr.edu.vortex.management.persistence.dao.impl.ModeleMailDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.ModeleMailPermissionDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.PermissionDAOImp;
import fr.edu.vortex.management.utilisateur.exception.Failure;
import fr.edu.vortex.management.utilisateur.exception.ManagementException;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ModifierModeleMailHandler extends ModeleMailHandler implements Handler<Message<JsonObject>> {

	private static final Logger logger = LoggerFactory.getLogger(ModifierModeleMailHandler.class);

    private static final String LOG_START = "Acteur permettant de modifier un modèle de mail";

	private static final String ERR_NOM_EXISTE_DEJA = "Impossible de modifier le modèle car il existe déjà un autre modèle avec ce nom";

	private static final String MESSAGE_ENTRANT = "Demande de modification de modele de mail entrante";
	
	ModeleMailDAO modeleMailDAO;
	ModeleMailPermissionDAO modeleMailPermissionDAO;
	PermissionDAO permissionDao;

	public static ModifierModeleMailHandler create(Vertx vertx) {
		return new ModifierModeleMailHandler(vertx);
	}

	private ModifierModeleMailHandler(Vertx vertx) {
		this.vertx = vertx;

		modeleMailDAO = new ModeleMailDAOImpl();
		modeleMailDAO.init(vertx);

		modeleMailPermissionDAO = new ModeleMailPermissionDAOImpl();
		modeleMailPermissionDAO.init(vertx);

		permissionDao = new PermissionDAOImp();
		permissionDao.init(vertx);

		logger.info(LOG_MESSAGE, LOG_NO_CORRELATION_ID, LOG_START);

	}

	@Override
	public void handle(Message<JsonObject> event) {

		if (!(event.body() instanceof JsonObject)) {
			event.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		JsonObject body = event.body();

		logger.trace(LOG_MESSAGE_WITH_DATA, LOG_NO_CORRELATION_ID, MESSAGE_ENTRANT, body.encode());

		if (!body.containsKey(ModeleMail.KEY_JSON_ID)) {
			event.fail(HTTP_BAD_REQUEST, ERR_IDENTIFIANT_MODELE_MAIL);
			return;
		}

		if (!checkParams(event, body)) {
			return;
		}


		ModeleMail updateModeleMail = new ModeleMail(body);

		JsonArray permissions = body.getJsonArray(ModeleMail.KEY_JSON_PERMISSIONS);

		if (permissions != null && !permissionsSontValidesMail(permissions)) {
			event.fail(HTTP_BAD_REQUEST, ERR_MAL_FORMATE_PERMISSIONS);
			return;
		}

		verifierNomValide(updateModeleMail.getId(), updateModeleMail.getNom())
		.compose(v -> verifierPermissionsConnues(permissions,permissionDao, ERR_MODELE_PERMISSION_NON_EXISTANTE))
		.compose(v -> modeleMailDAO.modifier(updateModeleMail))
		.compose(modeleMailCree -> modifierLiensPermissions(modeleMailCree, permissions))
		.setHandler(ar -> {
			if (ar.succeeded()) {  			
				event.reply(ar.result().toJson());
			} else {
				logger.error(
						Constants.LOG_ERROR,
						LOG_NO_CORRELATION_ID,
						String.format(ERR_MANAGEMENT_MODIFICATION_MODELE_TECH, ar.cause().getMessage()),
						ERR_MANAGEMENT_MODELE_MAIL_TECH,
						ar.cause());

				Optional<Failure> fail = Failure.create(ar.cause());
				if (fail.isPresent()) {
					event.fail(fail.get().getCode(), fail.get().getMessage());
				} else {
					event.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERR_MANAGEMENT_MODIFICATION_MODELE_TECH);
				}
			}
		});

	}

	/**
	 * Vérifie si le nom du modèle à créer est déjà utilisé par un autre modèle
	 * 
	 * @param nom
	 *            libelle à vérifier
	 * @return
	 */
	private Future<Void> verifierNomValide(int id, String nom) {
		Future<Void> fut = Future.future();
		modeleMailDAO.chercherModeleMailParNom(nom).setHandler(ar -> {
			if (ar.succeeded()) {
				List<ModeleMail> result = ar.result();
				if (result.isEmpty()) {
					fut.complete();
				} else {
					boolean libelleExiste = false;
					for (Iterator<ModeleMail> iterator = result.iterator(); iterator.hasNext();) {
						ModeleMail modeleMail = iterator.next();
						if (modeleMail.getId() != id) {
							libelleExiste = true;
						}
					}
					if (libelleExiste) {
						ManagementException errMetier = new ManagementException(HTTP_CONFLICT, ERR_NOM_EXISTE_DEJA);
						fut.fail(errMetier);
					} else {
						fut.complete();
					}
				}
			} else {
				fut.fail(ar.cause());
			}
		});
		return fut;
	}

	/**
	 * Crée un liens en le modeleMail et pour chaque permissions
	 * 
	 * @param modeleMail
	 *            le modeleMail cible
	 * @param permissions
	 *            l'ensemble des permissions visées par le traitement
	 * @return Future contenant le modeleMail
	 */
	private Future<ModeleMail> modifierLiensPermissions(ModeleMail modeleMail, JsonArray permissions) {

		if (permissions == null || permissions.isEmpty()) {
			return Future.succeededFuture(modeleMail);
		}

		Future<ModeleMail> fut = Future.future();

		//suppression des permissions
		modeleMailPermissionDAO.delete(modeleMail.getId()).setHandler(ars -> {
			if (ars.succeeded()) {
				//creation des permissions
				List<Future> creationPerms = permissions.stream()
						.map(o -> (JsonObject) o)
						.map(jsonPerm -> modeleMailPermissionDAO.create(modeleMail.getId(), jsonPerm.getString(Permission.KEY_JSON_CODE)))
						.collect(Collectors.toList());

				CompositeFuture.all(creationPerms).setHandler(ar -> {
					if (ar.succeeded()) {
						fut.complete(modeleMail);
					} else {
						fut.fail(ar.cause());
					}
				});            
			} else {
				fut.fail(ars.cause());
			}
		});
		return fut;
	}
}