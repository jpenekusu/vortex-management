package fr.edu.vortex.management.mail;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.utilisateur.handler.PermissionHandler;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ModeleMailHandler extends PermissionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ModeleMailHandler.class);

	public static final String MAL_FORMATE = "Format du message modèle mail incorrect";

	protected static final String ERR_MANAGEMENT_MODELE_MAIL_TECH = "ERR_MANAGEMENT_MODELE_MAIL_TECH";

	protected static final String ERR_MANAGEMENT_CREATION_MODELE_TECH = "Une erreur est survenue lors de la création du modèle de mail";
	protected static final String ERR_MANAGEMENT_MODIFICATION_MODELE_TECH = "Une erreur est survenue lors de la modification du modèle de mail";
	protected static final String ERR_MANAGEMENT_LISTER_MODELE_TECH = "Une erreur est survenue lors de la recherche de modèles de mail";
	protected static final String ERR_MANAGEMENT_COUNT_MODELE_TECH = "Une erreur est survenue lors du comptage de modèles de mail";
	protected static final String ERR_MANAGEMENT_SUPPRESSION_MODELE_TECH = "Une erreur est survenue lors de la suppression du modèle de mail";

	public static final String ERR_NOM_MODELE_MAIL = "Le nom du modèle de mail n'est pas renseigné";
	public static final String ERR_NOM_MODELE_MAIL_INVALIDE = "Le nom du modèle de mail est invalide";
	public static final String ERR_EMAIL_MODELE_MAIL = "L'email du modèle de mail n'est pas renseigné";
	public static final String ERR_EMAIL_MODELE_MAIL_INVALIDE = "L'email du modèle de mail est invalide";
	public static final String ERR_DESTINATAIRE_MODELE_MAIL = "Le destinataire du modèle de mail n'est pas renseigné";
	public static final String ERR_DESTINATAIRE_MODELE_MAIL_INVALIDE = "Le destinataire du modèle de mail est invalide";
	public static final String ERR_CONTENU_MODELE_MAIL = "Le modèle de mail n'est pas renseigné";
	public static final String ERR_CONTENU_MODELE_MAIL_INVALIDE = "Le modèle de mail est invalide";
	public static final String ERR_IDENTIFIANT_MODELE_MAIL_INVALIDE = "L'identifiant du modèle de mail est invalide";
	public static final String ERR_IDENTIFIANT_MODELE_MAIL = "L'identifiant du modèle de mail n'est pas renseigné";
	public static final String ERR_PERMISSION_MODELE_MAIL = "Les permissions ne sont pas renseignées";
	public static final String ERR_PERMISSION_MODELE_MAIL_INVALIDE = "Les permissions ne sont invalides";

	public static final String ERR_CREA_LIBELLE_EXISTE_DEJA = "Impossible de créer le modèle de mail car il existe déjà un modèle de mail avec ce libellé";
	public static final String ERR_MODIF_LIBELLE_EXISTE_DEJA = "Impossible de renommer le modèle de mail car il existe déjà un modèle de mail avec ce libellé";

	public static final String ERR_MAL_FORMATE_PERMISSIONS = "Demande de création/modification d'un modèle de mail avec des permissions mal formatée";

	
	
	protected boolean checkParams(Message<JsonObject> message, JsonObject json) {

		//nom
		try {   		
			String nom = json.getString(ModeleMail.KEY_JSON_NOM);
			if ( nom == null || !nom.matches(".{1,100}$")) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_NOM_MODELE_MAIL, ERR_MANAGEMENT_MODELE_MAIL_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_NOM_MODELE_MAIL);
				return false;   			
			}
		} catch (Exception e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_NOM_MODELE_MAIL_INVALIDE, ERR_MANAGEMENT_MODELE_MAIL_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_NOM_MODELE_MAIL_INVALIDE);
			return false;
		}   	

		//email
		try {   		
			String email = json.getString(ModeleMail.KEY_JSON_EMAIL);
			if ( email == null || email != null && !email.trim().isEmpty() && (!email.matches("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$") || email.length() > 250)) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_EMAIL_MODELE_MAIL, ERR_MANAGEMENT_MODELE_MAIL_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_EMAIL_MODELE_MAIL);
				return false;   			
			}
		} catch (Exception e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_EMAIL_MODELE_MAIL_INVALIDE, ERR_MANAGEMENT_MODELE_MAIL_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_EMAIL_MODELE_MAIL_INVALIDE);
			return false;
		}   	

		//destinataire
		try {   		
			String destinataire = json.getString(ModeleMail.KEY_JSON_DESTINATAIRE);
			if ( destinataire == null || !destinataire.matches(".{1,100}$")) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_DESTINATAIRE_MODELE_MAIL, ERR_MANAGEMENT_MODELE_MAIL_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_DESTINATAIRE_MODELE_MAIL);
				return false;   			
			}
		} catch (Exception e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_DESTINATAIRE_MODELE_MAIL_INVALIDE, ERR_MANAGEMENT_MODELE_MAIL_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_DESTINATAIRE_MODELE_MAIL_INVALIDE);
			return false;
		}   	

		//contenu
		try {   		   		
			JsonObject contenu = json.getJsonObject(ModeleMail.KEY_JSON_CONTENU);
			if (contenu == null) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_CONTENU_MODELE_MAIL, ERR_MANAGEMENT_MODELE_MAIL_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_CONTENU_MODELE_MAIL);
				return false;   			
			}
		} catch (Exception e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_CONTENU_MODELE_MAIL_INVALIDE, ERR_MANAGEMENT_MODELE_MAIL_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_CONTENU_MODELE_MAIL_INVALIDE);
			return false;   			
		}

		//permission
		try {   		   		
			JsonArray permissions = json.getJsonArray(ModeleMail.KEY_JSON_PERMISSIONS);
			if (permissions == null) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_PERMISSION_MODELE_MAIL, ERR_MANAGEMENT_MODELE_MAIL_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_PERMISSION_MODELE_MAIL);
				return false;   			
			}
		} catch (Exception e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_PERMISSION_MODELE_MAIL_INVALIDE, ERR_MANAGEMENT_MODELE_MAIL_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_PERMISSION_MODELE_MAIL_INVALIDE);
			return false;   			
		}

		return true;
	}
	
	  /**
     * Vérifie si les permissions envoyé dans la requête sont correctes
     * 
     * @param permissions
     * @return
     */
    protected boolean permissionsSontValidesMail(JsonArray permissions) {
        long nombrePermissionsSansCode = permissions.stream()
                .map(o -> (JsonObject) o)
                .filter(it -> !it.containsKey(Permission.KEY_JSON_CODE))
                .count();

        return nombrePermissionsSansCode == 0;
    }
}
