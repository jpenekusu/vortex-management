package fr.edu.vortex.management.mail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import fr.edu.vortex.management.utilisateur.pojo.Permission;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ModeleMail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1157581894097373401L;

	/**
	 * 
	 */	
	public static final String KEY_JSON_ID = "id";
	public static final String KEY_JSON_NOM = "nom";
	public static final String KEY_JSON_EMAIL = "email";
	public static final String KEY_JSON_DESTINATAIRE = "destinataire";
	public static final String KEY_JSON_CONTENU = "contenu";
	public static final String KEY_JSON_SYSTEME = "systeme";
	public static final String KEY_JSON_PERMISSIONS = "permissions";

	private Integer id;

	private String nom;

	private String email;

	private String destinataire;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private JsonObject contenu;

	private Boolean systeme;

	@JsonInclude(Include.NON_EMPTY)
	private List<Permission> permissions;

	public ModeleMail() {
		super();
	}

	public ModeleMail(Integer id, String nom, String email, String destinataire, JsonObject contenu,
			boolean systeme, List<Permission> permissions) {
		super();
		this.id = id;
		this.nom = nom;
		this.email = email;
		this.destinataire = destinataire;
		this.contenu = contenu;
		this.systeme = systeme;
		this.permissions = permissions;
	}

	@Override
	public String toString() {
		return "ModeleMail [id=" + id + ", nom="+ nom +", " + ", email="+ email +", "
				+ "destinataire=" + destinataire + ", contenu=" + contenu + ", systeme=" + systeme + ", permissions=" + permissions
				+ "]";
	}

	public static ModeleMail fromJson(JsonObject json) {
		return new ModeleMail(json);
	}   

	public ModeleMail(JsonObject jsonObject) {
		if (jsonObject != null) {
			this.setId(jsonObject.getInteger(KEY_JSON_ID));
			this.setNom(jsonObject.getString(KEY_JSON_NOM));
			this.setEmail(jsonObject.getString(KEY_JSON_EMAIL));
			this.setDestinataire(jsonObject.getString(KEY_JSON_DESTINATAIRE));
			Object contenu =  jsonObject.getValue(KEY_JSON_CONTENU);
			if (contenu instanceof String) {
				this.setContenu(new JsonObject(jsonObject.getString(KEY_JSON_CONTENU)));	
			} else {
				this.setContenu(jsonObject.getJsonObject(KEY_JSON_CONTENU));	
			}
			this.setSysteme(jsonObject.getBoolean(KEY_JSON_SYSTEME));
			ArrayList<Permission> perm = new ArrayList<>();
			if (jsonObject.getJsonArray(KEY_JSON_PERMISSIONS) != null){
				for (int i = 0; i < jsonObject.getJsonArray(KEY_JSON_PERMISSIONS).size(); i++) {
					JsonObject permission = jsonObject.getJsonArray(KEY_JSON_PERMISSIONS).getJsonObject(i);
					perm.add(Permission.fromJson(permission));
				}
			}
			this.setPermissions(perm);
		}
	}

	public JsonObject toJson() {
		JsonObject jsonToEncode = new JsonObject();
		jsonToEncode.put(KEY_JSON_ID, this.getId());
		jsonToEncode.put(KEY_JSON_NOM, this.getNom());
		jsonToEncode.put(KEY_JSON_EMAIL, this.getEmail());
		jsonToEncode.put(KEY_JSON_DESTINATAIRE, this.getDestinataire());
		jsonToEncode.put(KEY_JSON_CONTENU, this.getContenu());
		jsonToEncode.put(KEY_JSON_SYSTEME, this.getSysteme());
		JsonArray perm = new JsonArray();
		if (this.getPermissions() != null){
			for (Iterator<Permission> iterator = this.getPermissions().iterator(); iterator.hasNext();) {
				Permission permission = iterator.next();
				perm.add(permission.toJson());
			}
		}
		jsonToEncode.put(KEY_JSON_PERMISSIONS, perm);

		return jsonToEncode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDestinataire() {
		return destinataire;
	}

	public void setDestinataire(String destinataire) {
		this.destinataire = destinataire;
	}

	public JsonObject getContenu() {
		return contenu;
	}

	public void setContenu(JsonObject contenu) {
		this.contenu = contenu;
	}

	public Boolean getSysteme() {
		return systeme;
	}

	public void setSysteme(Boolean systeme) {
		this.systeme = systeme;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

}
