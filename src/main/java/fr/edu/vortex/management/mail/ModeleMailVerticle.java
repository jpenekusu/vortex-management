package fr.edu.vortex.management.mail;

import io.vertx.core.AbstractVerticle;

public class ModeleMailVerticle extends AbstractVerticle {

	public static final String LISTER_MODELE_MAIL = "vortex.management.utilisateur.modele_mail.lister";
	public static final String CREER_MODELE_MAIL = "vortex.management.utilisateur.modele_mail.creer";
	public static final String MODIFIER_MODELE_MAIL = "vortex.management.utilisateur.modele_mail.modifier";
	public static final String SUPPRIMER_MODELE_MAIL = "vortex.management.utilisateur.modele_mail.supprimer";

	@Override
	public void start() throws Exception {
		vertx.eventBus().consumer(LISTER_MODELE_MAIL, ListerModeleMailHandler.create(vertx));
		vertx.eventBus().consumer(CREER_MODELE_MAIL, CreerModeleMailHandler.create(vertx));
		vertx.eventBus().consumer(MODIFIER_MODELE_MAIL, ModifierModeleMailHandler.create(vertx));
		vertx.eventBus().consumer(SUPPRIMER_MODELE_MAIL, SupprimerModeleMailHandler.create(vertx));
	}
}
