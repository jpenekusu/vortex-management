package fr.edu.vortex.management.mail;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.util.Optional;

import fr.edu.vortex.management.persistence.dao.ModeleMailDAO;
import fr.edu.vortex.management.persistence.dao.ModeleMailPermissionDAO;
import fr.edu.vortex.management.persistence.dao.impl.ModeleMailDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.ModeleMailPermissionDAOImpl;
import fr.edu.vortex.management.utilisateur.exception.Failure;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class SupprimerModeleMailHandler extends ModeleMailHandler implements Handler<Message<JsonObject>> {

	private static final Logger logger = LoggerFactory.getLogger(SupprimerModeleMailHandler.class);

	private static final String LOG_START = "Acteur permettant de supprimer un modèle de mail";

	ModeleMailDAO modeleMailDAO;
	ModeleMailPermissionDAO modeleMailPermissionDAO;

	public static SupprimerModeleMailHandler create(Vertx vertx) {
		return new SupprimerModeleMailHandler(vertx);
	}

	private SupprimerModeleMailHandler(Vertx vertx) {
		this.vertx = vertx;

		modeleMailDAO = new ModeleMailDAOImpl();
		modeleMailDAO.init(vertx);

		modeleMailPermissionDAO = new ModeleMailPermissionDAOImpl();
		modeleMailPermissionDAO.init(vertx);

		logger.info(LOG_MESSAGE, LOG_NO_CORRELATION_ID, LOG_START);
	}

	@Override
	public void handle(Message<JsonObject> event) {

		if (!(event.body() instanceof JsonObject)) {
			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, MAL_FORMATE, ERR_MANAGEMENT_MODELE_MAIL_TECH);
			event.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		JsonObject body = event.body();

		if (!body.containsKey("id")) {
			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, ERR_IDENTIFIANT_MODELE_MAIL, ERR_MANAGEMENT_MODELE_MAIL_TECH);
			event.fail(HTTP_BAD_REQUEST, ERR_IDENTIFIANT_MODELE_MAIL);
			return;
		}

		int id;
		try {
			id = body.getInteger("id");
		} catch (Exception e) {
			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, ERR_IDENTIFIANT_MODELE_MAIL_INVALIDE, ERR_MANAGEMENT_MODELE_MAIL_TECH);
			event.fail(HTTP_BAD_REQUEST, ERR_IDENTIFIANT_MODELE_MAIL_INVALIDE);
			return;
		}

		// on va chercher le modele
		recupereModeleMail(id).setHandler(arRecup -> {
			if (arRecup.succeeded()) {
				// supprime toutes les liaisons modele - permission lié à ce modele
				modeleMailPermissionDAO.delete(id)
				// supprime le modele
				.compose(nbLiensDeleted -> modeleMailDAO.delete(id))
				.setHandler(ar -> {
					if (ar.succeeded()) {                   	
						event.reply(null);
					} else {
						Optional<Failure> failure = Failure.create(ar.cause());
						if (failure.isPresent()) {
							event.fail(failure.get().getCode(), failure.get().getMessage());
						} else {
							event.fail(HTTP_INTERNAL_ERROR, ERR_MANAGEMENT_SUPPRESSION_MODELE_TECH);
						}
					}
				});
			} else {
				Optional<Failure> failure = Failure.create(arRecup.cause());
				if (failure.isPresent()) {
					event.fail(failure.get().getCode(), failure.get().getMessage());
				} else {
					event.fail(HTTP_INTERNAL_ERROR, ERR_MANAGEMENT_SUPPRESSION_MODELE_TECH);
				}

			}

		});

	}

	/**
	 * Cherche le modele et vérifie qu'il n'ai pas système.
	 * 
	 * @param modeleId
	 *            identifiant du modele cible
	 * @return Future contenant le modele récupéré, échoue si le modele est de type
	 *         système
	 */
	private Future<ModeleMail> recupereModeleMail(int modeleId) {
		Future<ModeleMail> fut = Future.future();
		modeleMailDAO.chercherModeleMailByID(modeleId).setHandler(
				ar -> {
					if (ar.succeeded()) {
						Optional<ModeleMail> modele = ar.result();
						if (modele.isPresent()) {
							fut.complete(modele.get());
						} else {
							fut.fail(ar.cause());
						}
					} else {
						fut.fail(ar.cause());
					}
				});
		return fut;
	}

}
