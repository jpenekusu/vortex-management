//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management;

import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;

public class HebergementWebApp {

    private static final Logger logger = LoggerFactory.getLogger(HebergementWebApp.class);

    private static final String URI = "/supervision/*";

    Vertx vertx;

    public Router getRouterWebApp(Vertx vertx, String repDistWebApp) throws URISyntaxException, FileNotFoundException {
        this.vertx = vertx;

        Router router = Router.router(vertx);

        Path distDirectory = getPathWebApp(repDistWebApp);

        if (distDirectory.toFile().exists()) {
            StaticHandler webApp = createStaticHandlerWebApp(distDirectory);
            router.route(URI).handler(webApp);
        } else {
            throw new FileNotFoundException();
        }

        return router;
    }

    /**
     * Création du StaticHandler pour la webApp.
     * 
     * @param distDirectory
     *            répertoire où se trouve la webApp.
     * @return
     */
    private StaticHandler createStaticHandlerWebApp(Path distDirectory) {
        StaticHandler webApp = StaticHandler.create().setCachingEnabled(false).setAllowRootFileSystemAccess(true)
                .setWebRoot(distDirectory.toString());
        String action = "Appel à la méthode createStaticHandlerWebApp(Path distDirectory)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        return webApp;
    }

    /**
     * Donne le répertoire où se trouve la webApp.
     * 
     * @return Path ou se trouve l'index.html de webApp
     * @throws URISyntaxException
     */
    private Path getPathWebApp(String repDistWebApp) throws URISyntaxException {
        String action = "Appel à la méthode getPathWebApp()";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        URI location = MainVerticle.class.getProtectionDomain().getCodeSource().getLocation().toURI();
        Path jarDirectory = Paths.get(location).getParent();
        return Paths.get(jarDirectory.toString(), repDistWebApp);
    }

}
