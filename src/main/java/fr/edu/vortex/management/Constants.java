//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management;

public class Constants {

    public static final String LOG_NO_CORRELATION_ID = "";

    public static final String LOG_MESSAGE = "[correlationId:'{}'] {}";
    public static final String LOG_MESSAGE_WITH_DATA = "[correlationId:'{}'] {} [donnees:'{}']";
    public static final String LOG_ERROR = "[correlationId:'{}'] [code:{}] {}";
    public static final String LOG_ERROR_WITH_DATA = "[correlationId:'{}'] [code:'{}'] {} [donnees:'{}'] ";

    public static final String LOG_DEPLOY_PENDING = "[correlationId:'{}'] Déploiment du verticle '{}' en cours";
    public static final String LOG_DEPLOY_SUCCEEDED = "[correlationId:'{}'] Verticle '{}' déployé";
    public static final String LOG_DEPLOY_FAILED = "[correlationId:'{}']Échec du déploiement du verticle '{}'";

    public static final String LOG_SQL_EXEC_INCOMING = "[correlationId:'{}'] Exécution d'une requête SQL. sql='{}' params='{}'";
    public static final String LOG_SQL_EXEC_RESULT = "[correlationId:'{}'] Résultat de la requête SQL. rows='{}'";
    
    
    public static final String TRACE_ORIGINE_CASSIS = "CASSIS";
    public static final String TYPE_RAPPORT_IMPORT = "IMPORT";
    public static final String TYPE_RAPPORT_IMPORT_UTILISATEUR = "IMPORT UTILISATEURS";
    public static final String RAPPORT_ORIGINE_CASSIS = "CASSIS";
   

}