package fr.edu.vortex.management.utils;

import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.edu.vortex.management.flux.FluxVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

public class PaginationUtils {
    
    public PaginationUtils() {
	
    }
    
    public static Map<String, Object> genererFiltresPourPagination(Message<Object> event,String champDate,String valeurDate) {
	JsonObject criteres = (JsonObject) event.body();

	String page = criteres.getString(FluxVerticle.KEY_JSON_PAGE);
	String pageLimit = criteres.getString(FluxVerticle.KEY_JSON_PAGE_LIMIT);
	String sortOrder = criteres.getString(FluxVerticle.KEY_JSON_SORT_ORDER);

	Map<String, Object> filters = new HashMap<>();

	if (page != null && pageLimit == null) {
		event.fail(400, String.format(FluxVerticle.ERREUR_PARAM_MANQUANT, FluxVerticle.KEY_JSON_PAGE_LIMIT));
	} else if (page == null && pageLimit != null) {
		event.fail(400, String.format(FluxVerticle.ERREUR_PARAM_MANQUANT, FluxVerticle.KEY_JSON_PAGE));
	} else if (sortOrder != null && !(("ASC").equals(sortOrder) || ("DESC").equals(sortOrder))) {
		event.fail(400, "Le paramètre " + FluxVerticle.KEY_JSON_SORT_ORDER + " est erroné");
	}
	// vérification du format de la date
	else if (valeurDate != null) {
		List<String> dates = Arrays.asList(valeurDate.split("_"));
		if (dates.size() == 2) {
			try {
				filters.put(FluxVerticle.KEY_JSON_START_DATE_FILTER, Instant.parse(dates.get(0)));
				filters.put(FluxVerticle.KEY_JSON_END_DATE_FILTER, Instant.parse(dates.get(1)));
			} catch (DateTimeParseException e) {
				event.fail(400, "Le paramètre " + champDate + " est erroné");
			}
		} else {
			event.fail(400, "Le paramètre " + champDate + " est erroné");
		}
	}
	return filters;
    }


}
