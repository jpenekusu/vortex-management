//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_ERROR_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.util.Optional;

import fr.edu.vortex.management.persistence.dao.UtilisateurDAO;
import fr.edu.vortex.management.persistence.dao.impl.UtilisateurDAOImpl;
import fr.edu.vortex.management.utilisateur.exception.Failure;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * Classe recevant une demande de création d'un utilisateur qui va faire les
 * premières vérifications et orienter la demande vers le bonne acteur
 * 
 */
public class CreerUtilisateurLocalHandler extends UtilisateurHandler implements Handler<Message<Object>> {

    private static final String ERR_INATTENDUE = "Une erreur inattendue est arrivee pour la création d'un utilisateur message = %s";

    private static final String ERREUR_CREATION_UTILISATEUR = "erreur lors de la création de l'utilisateur";

    private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_CREER_UTILISATEUR_LOCALE_TECH";
    
    private static final Logger logger = LoggerFactory.getLogger(CreerUtilisateurLocalHandler.class);

    Vertx vertx;

    UtilisateurDAO utilisateurDao;

    public static CreerUtilisateurLocalHandler create(Vertx vertx) {
        return new CreerUtilisateurLocalHandler(vertx);
    }

    private CreerUtilisateurLocalHandler(Vertx vertx) {
        this.vertx = vertx;

        utilisateurDao = new UtilisateurDAOImpl();
        utilisateurDao.init(vertx);
    }

    @Override
    public void handle(Message<Object> event) {

        if (!(event.body() instanceof JsonObject)) {
            logger.error(
                    LOG_ERROR,
                    LOG_NO_CORRELATION_ID,
                    String.format(NOT_JSON, event.body().toString()),
                    ERR_MANAGEMENT_TECH);
            event.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
            return;
        }

        JsonObject body = (JsonObject) event.body();

        Utilisateur utilisateur;
        try {
            utilisateur = Utilisateur.fromJson(body);
        } catch (IllegalArgumentException e) {
            logger.error(
                    LOG_ERROR_WITH_DATA,
                    LOG_NO_CORRELATION_ID,
                    String.format(MAL_FORMATE),
                    body,
                    ERR_MANAGEMENT_TECH);
            event.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
            return;
        }

        logger.trace(
                LOG_MESSAGE_WITH_DATA,
                LOG_NO_CORRELATION_ID,
                "Demande de creation d'utilisateur entrante",
                body);

        if (utilisateur.getNom() == null || utilisateur.getNom().isEmpty()) {
            event.fail(HTTP_BAD_REQUEST, NOM_MANQUANT);
            return;
        }

        if (utilisateur.getPrenom() == null || utilisateur.getPrenom().isEmpty()) {
            event.fail(HTTP_BAD_REQUEST, PRENOM_MANQUANT);
            return;
        }

        if (utilisateur.getLogin() == null || utilisateur.getLogin().isEmpty()) {
            event.fail(HTTP_BAD_REQUEST, LOGIN_MANQUANT);
            return;
        }
        
        if (utilisateur.getEmail() == null || utilisateur.getEmail().isEmpty()) {
            event.fail(HTTP_BAD_REQUEST, EMAIL_MANQUANT);
            return;
        }

        if (utilisateur.getMotDePasse() == null || utilisateur.getMotDePasse().isEmpty()) {
            event.fail(HTTP_BAD_REQUEST, MOT_DE_PASSE_MANQUANT);
            return;
        }

        if (!Utilisateur.AUTHENTIFICATION_LOCALE.equals(utilisateur.getTypeAuthentification())) {
            event.fail(HTTP_INTERNAL_ERROR, ERR_AUTH);
            return;
        }

        Future<Void> verifierUniciteLogin = verifierUniciteLogin(utilisateur.getLogin(), vertx);
        Future<Void> verifierRoleExiste = verifierRoleExiste(utilisateur.getRole(), vertx);
        Future<Void> verifierSecuritePassword = verifierSecuritePassword(utilisateur.getMotDePasse());

        Future<CompositeFuture> futVerifications = Future.future();
        CompositeFuture
                .all(verifierSecuritePassword,verifierUniciteLogin, verifierRoleExiste)
                .setHandler(futVerifications::handle);

        futVerifications
                .compose(compositeFuture -> creerHashSiPresent(utilisateur))
                .compose(utilisateurDao::creer)
                .compose(compositeFuture -> masquerDonneesSensibles(utilisateur))
                .setHandler(ar -> {
                    if (ar.succeeded()) {
                        event.reply(ar.result());
                    } else {
                        Optional<Failure> fail = Failure.create(ar.cause());
                        if (fail.isPresent()) {

                            logger.error(
                                    LOG_ERROR,
                                    LOG_NO_CORRELATION_ID,
                                    fail.get().getMessage(),
                                    ERR_MANAGEMENT_TECH);

                            event.fail(fail.get().getCode(), fail.get().getMessage());
                        } else {

                            logger.error(
                                    LOG_ERROR,
                                    LOG_NO_CORRELATION_ID,
                                    String.format(ERR_INATTENDUE, ar.cause().getMessage()),
                                    ERR_MANAGEMENT_TECH,
                                    ar.cause());

                            event.fail(HTTP_INTERNAL_ERROR, ERREUR_CREATION_UTILISATEUR);
                        }
                    }
                });
    }



}
