package fr.edu.vortex.management.utilisateur.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.vertx.core.json.JsonObject;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Filtre implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -360142865546940187L;
	
	/**
	 * 
	 */	
	public static final String KEY_JSON_ID = "id";
	public static final String KEY_JSON_UTILISATEUR_ID = "utilisateur_id";
	public static final String KEY_JSON_NOM = "nom";
	public static final String KEY_JSON_DESCRIPTION = "description";
	public static final String KEY_JSON_TYPE_FILTRE = "type_filtre";
	public static final String KEY_JSON_CONTENU = "contenu";

	private Integer id;

	private Integer utilisateur_id;
	
	private String nom;

	private String description;

	private String type_filtre;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private JsonObject contenu;

	
	
	public Filtre() {
		super();
	}

	public Filtre(Integer id, Integer utilisateur_id, String nom, String description, String type_filtre, JsonObject contenu) {
		super();
		this.id = id;
		this.utilisateur_id = utilisateur_id;
		this.nom = nom;
		this.description = description;
		this.type_filtre = type_filtre;
		this.contenu = contenu;
	}
	

	@Override
	public String toString() {
		return "Filtre [id=" + id + ", utilisateur_id=" + utilisateur_id + ", nom="+ nom +", " + ", description="+ description +", "
				+ "type_filtre=" + type_filtre + ", contenu=" + contenu+"]";
	}

	public static Filtre fromJson(JsonObject json) {
		return new Filtre(json);
	}   

	public Filtre(JsonObject jsonObject) {
		if (jsonObject != null) {
			this.setId(jsonObject.getInteger(KEY_JSON_ID));
			this.setUtilisateur_id(jsonObject.getInteger(KEY_JSON_UTILISATEUR_ID));
			this.setNom(jsonObject.getString(KEY_JSON_NOM));
			this.setDescription(jsonObject.getString(KEY_JSON_DESCRIPTION));
			this.setType_filtre(jsonObject.getString(KEY_JSON_TYPE_FILTRE));
			Object contenu =  jsonObject.getValue(KEY_JSON_CONTENU);
			if (contenu instanceof String) {
				this.setContenu(new JsonObject(jsonObject.getString(KEY_JSON_CONTENU)));	
			} else {
				this.setContenu(jsonObject.getJsonObject(KEY_JSON_CONTENU));	
			}
			
		}
	}

	public JsonObject toJsonObject() {
		JsonObject jsonToEncode = new JsonObject();
		jsonToEncode.put(KEY_JSON_ID, this.getId());
		jsonToEncode.put(KEY_JSON_UTILISATEUR_ID, this.getUtilisateur_id());
		jsonToEncode.put(KEY_JSON_NOM, this.getNom());
		jsonToEncode.put(KEY_JSON_DESCRIPTION, this.getDescription());
		jsonToEncode.put(KEY_JSON_TYPE_FILTRE, this.getType_filtre());
		jsonToEncode.put(KEY_JSON_CONTENU, this.getContenu());

		return jsonToEncode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUtilisateur_id() {
		return utilisateur_id;
	}

	public void setUtilisateur_id(Integer utilisateur_id) {
		this.utilisateur_id = utilisateur_id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getType_filtre() {
		return type_filtre;
	}

	public void setType_filtre(String type_filtre) {
		this.type_filtre = type_filtre;
	}

	public JsonObject getContenu() {
		return contenu;
	}

	public void setContenu(JsonObject contenu) {
		this.contenu = contenu;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
