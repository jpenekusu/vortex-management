//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.edu.vortex.management.persistence.dao.UtilisateurDAO;
import fr.edu.vortex.management.persistence.dao.impl.UtilisateurDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.util.GenerationPassword;
import fr.edu.vortex.management.utilisateur.verticle.RoleVerticle;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ImporterUtilisateursHandler implements Handler<Message<Object>> {



	private static final String KEY_MOT_DE_PASSE = "mot_de_passe";

	private static final Logger logger = LoggerFactory.getLogger(ImporterUtilisateursHandler.class);

	private static final String KEY_JSON_UTILISATEURS = "utilisateurs";

	private static final String KEY_JSON_LOGIN = "login";
	
	private static final String KEY_JSON_NOM = "nom";
	
	private static final String KEY_JSON_PRENOM = "prenom";
	
	private static final String KEY_JSON_EMAIL = "email";
	
	private static final String KEY_JSON_TYPE_AUTH = "type_authentification";
	
	private static final String KEY_JSON_ID = "id";
	
	private static final String KEY_JSON_ERREUR = "erreur";

	private static final String KEY_HEADER_ORIGINE = "origine";
	
	private static final String KEY_HEADER = "header";

	
	private static final String ERREUR_LOGIN_EXISTANT = "Le login est déjà présent dans le fichier";
	
	private static final String ERREUR_CHAMPS_OBLIGATOIRE = "Le champ %s de l'utilisateur n'est pas renseigné";								
	
	private static final String ERREUR_MAIL_MAL_FORMATE = "Le champ email de l'utilisateur est invalide";
	
	private static final String ERREUR_TYPE_AUTH_MAL_FORMATE = "Le champ type_authentification de l'utilisateur est invalide";

	private static final String EMAIL_PATTERN = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";
	

	Vertx vertx;

	UtilisateurDAO utilisateurDao;

	public static ImporterUtilisateursHandler create(Vertx vertx) {
	    return new ImporterUtilisateursHandler(vertx);
	}

        private ImporterUtilisateursHandler(Vertx vertx) {
            this.vertx = vertx;
            utilisateurDao = new UtilisateurDAOImpl();
            utilisateurDao.init(vertx);
        }
    
        @Override
        public void handle(Message<Object> event) {
        	//Recuperation de la liste d'utilisateurs
        	JsonObject listeUtilisateursJson = (JsonObject) event.body();
        	
        	List<Future> listUtilisateurs = new ArrayList<Future>();
        	JsonArray utilisateurs = listeUtilisateursJson.getJsonArray(KEY_JSON_UTILISATEURS);
  
	    	String header = listeUtilisateursJson.getString(KEY_HEADER);
	    	String origine = listeUtilisateursJson.getString(KEY_HEADER_ORIGINE);

        	//gestion des doublons dans le json d'import
		Set<String> setUtilisateurs = new HashSet<>();
		for (int i = 0; i < utilisateurs.size(); i++) {
			JsonObject user = utilisateurs.getJsonObject(i);
			String login =  user.getString(KEY_JSON_LOGIN);
			if (setUtilisateurs.contains(login)) {
			    user.put(KEY_JSON_ERREUR, ERREUR_LOGIN_EXISTANT);
			} else {
			    setUtilisateurs.add(login);
			}
		}
        	
        	
    		if (utilisateurs != null) {
    			utilisateurs.stream().map(obj -> (JsonObject) obj).forEach(utilisateur -> {				
    			    Future<JsonObject> futcreationUtilisateur = Future.future();    			    
    			    
    			    //Vérification à effectuer sur le json d'un utilisateur
    			    //Cas ou le login est deja présent dans le json
    			    if (utilisateur.getString(KEY_JSON_ERREUR) != null) {
    				listUtilisateurs.add(futcreationUtilisateur);
    				futcreationUtilisateur.complete(utilisateur);
    				return;
    			    }
    			    
    			    //Champs obligatoires
    			   if(!utilisateur.containsKey(KEY_JSON_NOM) || utilisateur.getString(KEY_JSON_NOM)==null) {
    			        utilisateur.put(KEY_JSON_ERREUR, String.format(ERREUR_CHAMPS_OBLIGATOIRE, KEY_JSON_NOM));
    			        listUtilisateurs.add(futcreationUtilisateur);
    			        futcreationUtilisateur.complete(utilisateur);
    			        return;
    			   }
    			   
    			   if(!utilisateur.containsKey(KEY_JSON_PRENOM) || utilisateur.getString(KEY_JSON_PRENOM)==null) {
    			        utilisateur.put(KEY_JSON_ERREUR, String.format(ERREUR_CHAMPS_OBLIGATOIRE, KEY_JSON_PRENOM));
    			        listUtilisateurs.add(futcreationUtilisateur);
    			        futcreationUtilisateur.complete(utilisateur);
    			        return;
    			   }
    			   
    			   if(!utilisateur.containsKey(KEY_JSON_LOGIN) || utilisateur.getString(KEY_JSON_LOGIN)==null) {
    			        utilisateur.put(KEY_JSON_ERREUR, String.format(ERREUR_CHAMPS_OBLIGATOIRE, KEY_JSON_LOGIN));
    			        listUtilisateurs.add(futcreationUtilisateur);
    			        futcreationUtilisateur.complete(utilisateur);
    			        return;
    			   }
    			   
    			   if(!utilisateur.containsKey(KEY_JSON_EMAIL) || utilisateur.getString(KEY_JSON_EMAIL)==null) {
    			        utilisateur.put(KEY_JSON_ERREUR, String.format(ERREUR_CHAMPS_OBLIGATOIRE, KEY_JSON_EMAIL));
    			        listUtilisateurs.add(futcreationUtilisateur);
    			        futcreationUtilisateur.complete(utilisateur); 
    			        return;
    			   }
    			   
    			   if(!utilisateur.containsKey(KEY_JSON_TYPE_AUTH) || utilisateur.getString(KEY_JSON_TYPE_AUTH)==null) {
    			        utilisateur.put(KEY_JSON_ERREUR, String.format(ERREUR_CHAMPS_OBLIGATOIRE, KEY_JSON_TYPE_AUTH));
    			        listUtilisateurs.add(futcreationUtilisateur);
    			        futcreationUtilisateur.complete(utilisateur);
    			        return;
    			   }
    			   if(!utilisateur.getString(KEY_JSON_TYPE_AUTH).equals(Utilisateur.AUTHENTIFICATION_LOCALE)
    				   && !utilisateur.getString(KEY_JSON_TYPE_AUTH).equals(Utilisateur.AUTHENTIFICATION_LDAP)) {
    			        utilisateur.put(KEY_JSON_ERREUR, ERREUR_TYPE_AUTH_MAL_FORMATE);
			        listUtilisateurs.add(futcreationUtilisateur);
			        futcreationUtilisateur.complete(utilisateur);
			        return;
    			   }
    
    			    //Vérif email 
    			    if(utilisateur.getString(KEY_JSON_EMAIL)!=null) {
    				String mail = utilisateur.getString(KEY_JSON_EMAIL);
    				Pattern p = Pattern.compile(EMAIL_PATTERN);
    				Matcher matcher = p.matcher(mail);
    				boolean mailValid = matcher.matches();
    				if(!mailValid) {
    				    utilisateur.put(KEY_JSON_ERREUR, ERREUR_MAIL_MAL_FORMATE);
    				    listUtilisateurs.add(futcreationUtilisateur);
    				    futcreationUtilisateur.complete(utilisateur); 
    				    return;
    				}
    			    }
    			    			    
    			    Utilisateur userPojo = Utilisateur.fromJson(utilisateur);			    
    			    //Ajout du role superviseur par défaut
    			    Role roleSuperviseur = new Role();
    			    roleSuperviseur.setId(2);
    			    roleSuperviseur.setLibelle("superviseur·euse");
    			    userPojo.setRole(roleSuperviseur);
    		    
    			    //si authentification locale génération d'un PWD
    			    if(userPojo.getTypeAuthentification().equals(Utilisateur.AUTHENTIFICATION_LOCALE)) {
    				String generationMotDePasse = GenerationPassword.generationMotDePasse();
    				userPojo.setMotDePasse(generationMotDePasse);
    			    }
    			  
    		    	
    		    JsonObject input = new JsonObject().put("id", 2);
    		    vertx.eventBus().<JsonObject>send(RoleVerticle.CHERCHER_ROLE_PAR_ID, input, arGetRole -> {
    			if (arGetRole.succeeded()) {
				Role role = Role.fromJson(arGetRole.result().body());
				userPojo.setRole(role);
				
				  JsonObject utilisateurComplet = userPojo.toJson();
	    			   	
				  utilisateurComplet.put(KEY_HEADER, header);
				  utilisateurComplet.put(KEY_HEADER_ORIGINE, origine);
				  
				 //Creation de l'utilisateur en base
				  vertx.eventBus().send(UtilisateurVerticle.CREER_UTILISATEUR, utilisateurComplet, 
					  ar -> this.toResult(ar,utilisateurComplet,futcreationUtilisateur));
			}
    		    });
    		    listUtilisateurs.add(futcreationUtilisateur);	    				

    			    
    			});
    
    		}
    		
    		CompositeFuture.join(listUtilisateurs).setHandler(ar -> {
			JsonArray arrayResults = new JsonArray();
			for (Iterator<Future> iterator = listUtilisateurs.iterator(); iterator.hasNext();) {
				Future future = iterator.next();
				arrayResults.add(future.result());
			}
			JsonObject resultJson = new JsonObject().put("utilisateurs", arrayResults);
			event.reply(resultJson);
		});
        }
    
    
        private void toResult(AsyncResult<Message<Object>> ar, JsonObject utilisateur,
    	    Future<JsonObject> futCreateUtilisateur) {
            if(ar.failed()) {						
    		String erreur = ar.cause().getMessage();
    		utilisateur.put(KEY_JSON_ERREUR, erreur);
    		utilisateur.remove(KEY_MOT_DE_PASSE);
    	    }else {
    		JsonObject userCree = (JsonObject) ar.result().body();
    		utilisateur.put(KEY_JSON_ID, userCree.getInteger(KEY_JSON_ID));
    		//TODO Mot de passe à supprimer du json 
    		//En attendant l'envoi d'un email on le conserve
    		
    	    }
            utilisateur.remove(KEY_HEADER);
            utilisateur.remove(KEY_HEADER_ORIGINE);
    	    futCreateUtilisateur.complete(utilisateur);
        }
    
}