//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

/**
 * 
 */
package fr.edu.vortex.management.utilisateur.handler;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.dao.TraceDAO;
import fr.edu.vortex.management.persistence.dao.impl.TraceDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Trace;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * @author arhannaoui
 *
 */
public class HistoriqueTraceHandler implements Handler<Message<JsonObject>>{

	public static final String ERREUR_LISTER_TRACE = "Erreur lors de la récupération des traces dans la persistence.";    

	private static final Logger logger = LoggerFactory.getLogger(HistoriqueTraceHandler.class);

	Vertx vertx;

	TraceDAO traceDao;

	public static HistoriqueTraceHandler create(Vertx vertx) {
		return new HistoriqueTraceHandler(vertx);
	}

	private HistoriqueTraceHandler(Vertx vertx) {
		this.vertx = vertx;
		traceDao = new TraceDAOImpl();
		traceDao.init(vertx);
	}

	@Override
	public void handle(Message<JsonObject> event) {

		String action = "loadAllFlux(Message<Object> message)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		if (!(event.body() instanceof JsonObject)) {
			event.fail(HttpURLConnection.HTTP_BAD_REQUEST, "mal formaté");
			return;
		}
		
		JsonObject criteres = (JsonObject) event.body();
		
		String id_element = null;
		String type_element = null;
		try {
			id_element = criteres.getString(Trace.KEY_JSON_ID_ELEMENT);
			type_element = criteres.getString(Trace.KEY_JSON_TYPE_ELEMENT);
			if ((id_element == null || id_element.trim().isEmpty()) ||
					(type_element == null || type_element.trim().isEmpty())	) {
				event.fail(HttpURLConnection.HTTP_BAD_REQUEST, "mal formaté");
				return;				
			}		
		} catch (Exception e) {
			event.fail(HttpURLConnection.HTTP_BAD_REQUEST, "mal formaté");
			return;
		}

		Map<String, Object> filters = new HashMap<>();
		filters.put(Trace.KEY_JSON_ID_ELEMENT, id_element);
		filters.put(Trace.KEY_JSON_TYPE_ELEMENT, type_element);

		traceDao.listerHistoriqueTrace(filters).setHandler(ar -> {
			if (ar.succeeded()) {
				JsonArray result = new JsonArray();
				for (Trace item : ar.result()) {
					result.add(item.toJsonObject());
				}
				event.reply(result);

			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
						ar.cause());
				event.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERREUR_LISTER_TRACE);
			}
		});



	}

}
