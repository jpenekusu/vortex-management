//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

/**
 * 
 */
package fr.edu.vortex.management.utilisateur.verticle;


import fr.edu.vortex.management.utilisateur.handler.ListerTraceHandler;
import fr.edu.vortex.management.utilisateur.handler.CreerTraceHandler;
import fr.edu.vortex.management.utilisateur.handler.CreerTraceLoginOuInscriptionHandler;
import fr.edu.vortex.management.utilisateur.handler.HistoriqueTraceHandler;
import io.vertx.core.AbstractVerticle;

/**
 * @author arhannaoui
 *
 */
public class TraceVerticle extends AbstractVerticle {
	
	public static final String CHERCHER_TRACE = "vortex.management.trace.chercher";
	public static final String HISTORIQUE_TRACE = "vortex.management.trace.historique";
	public static final String CREER_TRACE = "vortex.management.trace.creer";
	public static final String CREER_TRACE_LOGIN_OU_INSCRIPTION = "vortex.management.trace.creer.loginOuInscription";
 
    @Override
    public void start() throws Exception {
        vertx.eventBus().consumer(CHERCHER_TRACE, ListerTraceHandler.create(vertx));
        vertx.eventBus().consumer(HISTORIQUE_TRACE, HistoriqueTraceHandler.create(vertx));
        vertx.eventBus().consumer(CREER_TRACE, CreerTraceHandler.create(vertx));
        vertx.eventBus().consumer(CREER_TRACE_LOGIN_OU_INSCRIPTION, CreerTraceLoginOuInscriptionHandler.create(vertx));
       
    }
}
