//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static fr.edu.vortex.management.utilisateur.pojo.Role.KEY_JSON_LIBELLE;
import static fr.edu.vortex.management.utilisateur.pojo.Role.KEY_JSON_PERMISSIONS;
import static fr.edu.vortex.management.utilisateur.pojo.Role.KEY_JSON_SYSTEME;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CONFLICT;

import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.dao.PermissionDAO;
import fr.edu.vortex.management.persistence.dao.RoleDAO;
import fr.edu.vortex.management.persistence.dao.RolePermissionDAO;
import fr.edu.vortex.management.persistence.dao.impl.PermissionDAOImp;
import fr.edu.vortex.management.persistence.dao.impl.RoleDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.RolePermissionDAOImpl;
import fr.edu.vortex.management.utilisateur.exception.Failure;
import fr.edu.vortex.management.utilisateur.exception.ManagementException;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureEvtTrace;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureTypeTrace;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ModifierRoleHandler extends PermissionHandler implements Handler<Message<JsonObject>> {

	private static final String MESSAGE_ENTRANT = "demande de modification d'un rôle";

	private static final String MESSAGE_MAL_FORMATE = "mal formaté";

	private static final String ERR_MAL_FORMATE_PERMISSIONS = "demande de modification d'un rôle avec des permissions mal formatée";

	private static final String ERR_MODIFICATION_ROLE = "une erreur est survenue à la modification d'un rôle (message d'erreur : %s)";

	private static final String ERR_LIBELLE_EXISTE_DEJA = "Impossible de modifier le libellé du rôle car il existe déjà un rôle avec ce libellé";

	private static final Logger logger = LoggerFactory.getLogger(ModifierRoleHandler.class);

	private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_MODIFIER_ROLE_TECH";

	private static final String LOG_START = "Acteur permettant de modifier un rôle créé";

	private static final String ERREUR_MODIFICATION_ROLE = "Erreur lors de la modification du rôle.";

	private static final String ERR_IMPOSSIBLE_MODIFIER_ROLE_SYSTEME = "Il est impossible de modifier un rôle systeme";

	public static final String MSG_CLE_MANQUANTE = "la clé '%s' est manquante";

	RoleDAO roleDao;
	RolePermissionDAO rolePermDao;
	PermissionDAO permissionDao;

	public static ModifierRoleHandler create(Vertx vertx) {
		return new ModifierRoleHandler(vertx);
	}

	private ModifierRoleHandler(Vertx vertx) {
		this.vertx = vertx;

		roleDao = new RoleDAOImpl();
		roleDao.init(vertx);

		rolePermDao = new RolePermissionDAOImpl();
		rolePermDao.init(vertx);

		permissionDao = new PermissionDAOImp();
		permissionDao.init(vertx);

		logger.info(LOG_MESSAGE, LOG_NO_CORRELATION_ID, LOG_START);

	}

	@Override
	public void handle(Message<JsonObject> event) {

		if (!(event.body() instanceof JsonObject)) {
			event.fail(HTTP_BAD_REQUEST, MESSAGE_MAL_FORMATE);
			return;
		}

		JsonObject body = event.body();

		logger.trace(LOG_MESSAGE_WITH_DATA, LOG_NO_CORRELATION_ID, MESSAGE_ENTRANT, body.encode());

		if (!body.containsKey(Role.KEY_JSON_ID)) {
			event.fail(HTTP_BAD_REQUEST, String.format(MSG_CLE_MANQUANTE, Role.KEY_JSON_ID));
			return;
		}

		if (!body.containsKey(Role.KEY_JSON_LIBELLE)) {
			event.fail(HTTP_BAD_REQUEST, String.format(MSG_CLE_MANQUANTE, Role.KEY_JSON_LIBELLE));
			return;
		}

		if (!body.containsKey(KEY_JSON_SYSTEME)) {
			event.fail(HTTP_BAD_REQUEST, String.format(MSG_CLE_MANQUANTE, Role.KEY_JSON_SYSTEME));
			return;
		}

		if (body.getBoolean(KEY_JSON_SYSTEME)) {
			event.fail(HTTP_BAD_REQUEST, ERR_IMPOSSIBLE_MODIFIER_ROLE_SYSTEME);
			return;
		}

		Role updateRole = new Role();
		updateRole.setLibelle(body.getString(Role.KEY_JSON_LIBELLE));
		updateRole.setId(body.getInteger(Role.KEY_JSON_ID));
		if (updateRole.getLibelle() == null || updateRole.getLibelle().isEmpty()) {
			event.fail(HTTP_BAD_REQUEST, String.format(MSG_CLE_MANQUANTE, KEY_JSON_LIBELLE));
			return;
		}
		updateRole.setSysteme(body.getBoolean(KEY_JSON_SYSTEME));

		if (body.containsKey(KEY_JSON_PERMISSIONS) && !(estJsonArray(body, KEY_JSON_PERMISSIONS))) {

			logger.warn(
					LOG_MESSAGE_WITH_DATA,
					LOG_NO_CORRELATION_ID,
					ERR_MAL_FORMATE_PERMISSIONS,
					body.encode());

			event.fail(HTTP_BAD_REQUEST, ERR_MAL_FORMATE_PERMISSIONS);
			return;
		}

		JsonArray permissions = body.getJsonArray(KEY_JSON_PERMISSIONS);

		if (permissions != null && !permissionsEstValides(permissions)) {
			event.fail(HTTP_BAD_REQUEST, ERR_MAL_FORMATE_PERMISSIONS);
			return;
		}

		verifierLibelleValide(updateRole.getId(), updateRole.getLibelle())
		.compose(v -> verifierPermissionsConnues(permissions,permissionDao, ERR_ROLE_PERMISSION_NON_EXISTANTE))
		.compose(v -> roleDao.modifier(updateRole))
		.compose(roleCree -> modifierLiensPermissions(roleCree, permissions))
		.setHandler(ar -> {
			if (ar.succeeded()) {
    			String header = body.getString("header");
    			body.remove("header");
    			String origine = body.getString("origine");
    			body.remove("origine");
    			JsonObject contenu = new JsonObject();
    			contenu.put("id", ar.result().toJson().getInteger(Role.KEY_JSON_ID));
    			contenu.put(Role.KEY_JSON_LIBELLE, body.getString(Role.KEY_JSON_LIBELLE));
    			contenu.put(Role.KEY_JSON_SYSTEME, body.getBoolean(Role.KEY_JSON_SYSTEME));
    			contenu.put(Role.KEY_JSON_PERMISSIONS, body.getJsonArray(Role.KEY_JSON_PERMISSIONS));   			
    			String idElement = String.valueOf(ar.result().toJson().getInteger(Role.KEY_JSON_ID));
    			String idEntite = body.getString(Role.KEY_JSON_LIBELLE);
    			
    			traceEvent(NomenclatureTypeTrace.Role.getValeur(), NomenclatureEvtTrace.Modification.getValeur(), header, contenu, origine, idElement, idEntite);
    			
				event.reply(ar.result().toJson());
			} else {

				logger.error(
						Constants.LOG_ERROR,
						LOG_NO_CORRELATION_ID,
						String.format(ERR_MODIFICATION_ROLE, ar.cause().getMessage()),
						ERR_MANAGEMENT_TECH,
						ar.cause());

				Optional<Failure> fail = Failure.create(ar.cause());
				if (fail.isPresent()) {
					event.fail(fail.get().getCode(), fail.get().getMessage());
				} else {
					event.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERREUR_MODIFICATION_ROLE);
				}
			}
		});

	}

	/**
	 * Vérifie si le libelle du rôle à créer est déjà utilisé par un autre rôle
	 * 
	 * @param libelle
	 *            libelle à vérifier
	 * @return
	 */
	private Future<Void> verifierLibelleValide(int id, String libelle) {
		Future<Void> fut = Future.future();
		roleDao.chercherRoleParLibelle(libelle).setHandler(ar -> {
			if (ar.succeeded()) {
				List<Role> result = ar.result();
				if (result.isEmpty()) {
					fut.complete();
				} else {
					boolean libelleExiste = false;
					for (Iterator<Role> iterator = result.iterator(); iterator.hasNext();) {
						Role role = iterator.next();
						if (role.getId() != id) {
							libelleExiste = true;
						}
					}
					if (libelleExiste) {
						ManagementException errMetier = new ManagementException(HTTP_CONFLICT, ERR_LIBELLE_EXISTE_DEJA);
						fut.fail(errMetier);
					} else {
						fut.complete();
					}
				}
			} else {
				fut.fail(ar.cause());
			}
		});
		return fut;
	}

	/**
	 * Crée un liens en le role et pour chaque permissions
	 * 
	 * @param role
	 *            le role cible
	 * @param permissions
	 *            l'ensemble des permissions visées par le traitement
	 * @return Future contenant le role
	 */
	private Future<Role> modifierLiensPermissions(Role role, JsonArray permissions) {

		if (permissions == null || permissions.isEmpty()) {
			return Future.succeededFuture(role);
		}

		Future<Role> fut = Future.future();

		//suppression des permissions
		rolePermDao.delete(role.getId()).setHandler(ars -> {
			if (ars.succeeded()) {
				//creation des permissions
				List<Future> creationPerms = permissions.stream()
						.map(o -> (JsonObject) o)
						.map(jsonPerm -> rolePermDao.create(role.getId(), jsonPerm.getString(Permission.KEY_JSON_CODE),jsonPerm.getString(Permission.KEY_JSON_ACTIONS)))
						.collect(Collectors.toList());

				CompositeFuture.all(creationPerms).setHandler(ar -> {
					if (ar.succeeded()) {
						fut.complete(role);
					} else {
						fut.fail(ar.cause());
					}
				});            
			} else {
				fut.fail(ars.cause());
			}
		});
		return fut;
	}

}
