//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

/**
 * 
 */
package fr.edu.vortex.management.utilisateur.verticle;

import fr.edu.vortex.management.utilisateur.handler.ChercherRapportHandler;
import fr.edu.vortex.management.utilisateur.handler.CreerRapportHandler;
import fr.edu.vortex.management.utilisateur.handler.ListerRapportHandler;
import io.vertx.core.AbstractVerticle;

/**
 * @author arhannaoui
 *
 */
public class RapportVerticle extends AbstractVerticle {
	

	public static final String CREER_RAPPORT = "vortex.management.rapport.creer";
	public static final String LISTER_RAPPORT = "vortex.management.rapport.lister";
	public static final String GET_RAPPORT = "vortex.management.rapport.get";
    
    @Override
    public void start() throws Exception {

        vertx.eventBus().consumer(CREER_RAPPORT, CreerRapportHandler.create(vertx));
        vertx.eventBus().consumer(LISTER_RAPPORT, ListerRapportHandler.create(vertx));
        vertx.eventBus().consumer(GET_RAPPORT, ChercherRapportHandler.create(vertx));
       
    }
}
