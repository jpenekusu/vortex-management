package fr.edu.vortex.management.utilisateur.verticle;


import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.dao.FiltreDAO;
import fr.edu.vortex.management.persistence.dao.impl.FiltreDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Filtre;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class FiltreUtilisateurVerticle extends AbstractVerticle {

	public static final String LISTER_FILTRE_UTILISATEUR = "vortex.management.utilisateur.filtre.lister";
	public static final String CREER_FILTRE_UTILISATEUR = "vortex.management.utilisateur.filtre.creer";
	public static final String MODIFIER_FILTRE_UTILISATEUR = "vortex.management.utilisateur.filtre.modifier";
	public static final String SUPPRIMER_FILTRE_UTILISATEUR = "vortex.management.utilisateur.filtre.supprimer";
	public static final String SUPPRIMER_FILTRES_UTILISATEUR = "vortex.management.utilisateur.filtres.supprimer";

	private static final Logger logger = LoggerFactory.getLogger(FiltreUtilisateurVerticle.class);

	public static final String MAL_FORMATE = "Format du message filtre utilisateur incorrect";

	private static final String ERR_MANAGEMENT_FILTRE_TECH = "ERR_MANAGEMENT_FILTRE_TECH";

	private static final String ERR_MANAGEMENT_CREATION_FILTRE_TECH = "Une erreur est survenue lors de la création du filtre";
	private static final String ERR_MANAGEMENT_MODIFICATION_FILTRE_TECH = "Une erreur est survenue lors de la modification du filtre";

	public static final String ERR_NOM_FILTRE = "Le nom du filtre n'est pas renseigné";
	public static final String ERR_NOM_FILTRE_INVALIDE = "Le nom du filtre est invalide";
	public static final String ERR_DESCRIPTION_FILTRE_INVALIDE = "La description du filtre est invalide";
	public static final String ERR_UTILISATEUR_FILTRE = "L'identifiant de l'utilisateur du filtre n'est pas renseigné";
	public static final String ERR_UTILISATEUR_FILTRE_INVALIDE = "L'identifiant de l'utilisateur du filtre est invalide";
	public static final String ERR_TYPE_FILTRE = "Le type du filtre n'est pas renseigné";
	public static final String ERR_TYPE_FILTRE_INVALIDE = "Le type du filtre est invalide";
	public static final String ERR_UTILISATEUR_CRITERES = "Les critères du filtre ne sont pas renseignés";
	public static final String ERR_UTILISATEUR_CRITERES_INVALIDES = "Les critères du filtre sont invalides";
	public static final String ERR_IDENTIFIANT_FILTRE_INVALIDE = "L'identifiant du filtre est invalide";
	public static final String ERR_IDENTIFIANT_FILTRE = "L'identifiant du filtre n'est pas renseigné";

	public static final String ERR_CREA_LIBELLE_EXISTE_DEJA = "Impossible de créer le filtre car il existe déjà un filtre avec ce libellé";
	public static final String ERR_MODIF_LIBELLE_EXISTE_DEJA = "Impossible de renommer le filtre car il existe déjà un filtre avec ce libellé";

	FiltreDAO filtreDAO;

	@Override
	public void start() throws Exception {
		vertx.eventBus().consumer(LISTER_FILTRE_UTILISATEUR, this::getFiltres);
		vertx.eventBus().consumer(CREER_FILTRE_UTILISATEUR, this::createFiltre);
		vertx.eventBus().consumer(MODIFIER_FILTRE_UTILISATEUR, this::updateFiltre);
		vertx.eventBus().consumer(SUPPRIMER_FILTRE_UTILISATEUR, this::deleteFiltre);
		vertx.eventBus().consumer(SUPPRIMER_FILTRES_UTILISATEUR, this::deleteFiltresParUtilisateur);

		filtreDAO = new FiltreDAOImpl();
		filtreDAO.init(vertx);
	}


	/**
	 * 
	 * @param message
	 */
	private void createFiltre(Message<Object> message) {

		if (!(message.body() instanceof JsonObject)) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, MAL_FORMATE, ERR_MANAGEMENT_FILTRE_TECH);
			message.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		JsonObject newJsonFiltre = (JsonObject) message.body();

		if (!checkParams(message, newJsonFiltre)) {
			return;
		}

		logger.trace(
				LOG_MESSAGE_WITH_DATA,
				LOG_NO_CORRELATION_ID,
				"Demande de creation de filtre entrante",
				newJsonFiltre);

		Map<String, Object> eqFilters = new HashMap<>();
		eqFilters.put(Filtre.KEY_JSON_NOM, newJsonFiltre.getString(Filtre.KEY_JSON_NOM).trim());
		eqFilters.put(Filtre.KEY_JSON_UTILISATEUR_ID, newJsonFiltre.getInteger(Filtre.KEY_JSON_UTILISATEUR_ID));
		eqFilters.put(Filtre.KEY_JSON_TYPE_FILTRE, newJsonFiltre.getString(Filtre.KEY_JSON_TYPE_FILTRE));		


		filtreDAO.lister(eqFilters).setHandler(ar -> {
			if (ar.succeeded()) {
				if (ar.result().size() > 0){
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_CREA_LIBELLE_EXISTE_DEJA, ERR_MANAGEMENT_FILTRE_TECH);
					message.fail(HttpURLConnection.HTTP_BAD_REQUEST, ERR_CREA_LIBELLE_EXISTE_DEJA);
					return;
				}

				Filtre newFiltre = new Filtre(newJsonFiltre);
				filtreDAO.creer(newFiltre)
				.setHandler(arFiltre -> {
					if (arFiltre.succeeded()) {
						Filtre result = arFiltre.result();

						logger.trace(
								LOG_MESSAGE_WITH_DATA,
								LOG_NO_CORRELATION_ID,
								result);

						message.reply(result.toJsonObject());
					} else {
						logger.error(LOG_ERROR,
								LOG_NO_CORRELATION_ID,
								ERR_MANAGEMENT_CREATION_FILTRE_TECH,
								ERR_MANAGEMENT_FILTRE_TECH,
								arFiltre.cause());
						message.fail(HTTP_INTERNAL_ERROR, ERR_MANAGEMENT_CREATION_FILTRE_TECH);
					}
				});
			} else {
				message.fail(HTTP_INTERNAL_ERROR, ERR_MANAGEMENT_CREATION_FILTRE_TECH);
			}
		});

	}


	/**
	 * 
	 * @param message
	 */
	private void updateFiltre(Message<Object> message) {

		if (!(message.body() instanceof JsonObject)) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, MAL_FORMATE, ERR_MANAGEMENT_FILTRE_TECH);
			message.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		JsonObject newJsonFiltre = (JsonObject) message.body();

		if (!checkParamsModif(message, newJsonFiltre)) {
			return;
		}

		if (newJsonFiltre.getInteger(Filtre.KEY_JSON_ID) == null){
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_IDENTIFIANT_FILTRE, ERR_MANAGEMENT_FILTRE_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_IDENTIFIANT_FILTRE);
			return;			
		}

		logger.trace(
				LOG_MESSAGE_WITH_DATA,
				LOG_NO_CORRELATION_ID,
				"Demande de modification de filtre entrante",
				newJsonFiltre);

		Map<String, Object> eqFilters = new HashMap<>();
		eqFilters.put(Filtre.KEY_JSON_NOM, newJsonFiltre.getString(Filtre.KEY_JSON_NOM).trim());
		eqFilters.put(Filtre.KEY_JSON_UTILISATEUR_ID, newJsonFiltre.getInteger(Filtre.KEY_JSON_UTILISATEUR_ID));
		eqFilters.put(Filtre.KEY_JSON_TYPE_FILTRE, newJsonFiltre.getString(Filtre.KEY_JSON_TYPE_FILTRE));		


		filtreDAO.lister(eqFilters).setHandler(ar -> {
			if (ar.succeeded()) {
				for (Filtre item : ar.result()) {
					if (!item.getId().equals(newJsonFiltre.getInteger(Filtre.KEY_JSON_ID))){
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_MODIF_LIBELLE_EXISTE_DEJA, ERR_MANAGEMENT_FILTRE_TECH);
						message.fail(HttpURLConnection.HTTP_BAD_REQUEST, ERR_MODIF_LIBELLE_EXISTE_DEJA);
						return;
					}
				}
				Filtre newFiltre = new Filtre(newJsonFiltre);
				filtreDAO.modifier(newFiltre)
				.setHandler(arModif -> {
					if (arModif.succeeded()) {
						Filtre result = arModif.result();
						message.reply(result.toJsonObject());
					} else {
						message.fail(HTTP_INTERNAL_ERROR, ERR_MANAGEMENT_MODIFICATION_FILTRE_TECH);
					}
				});
			} else {
				message.fail(HTTP_INTERNAL_ERROR, ERR_MANAGEMENT_MODIFICATION_FILTRE_TECH);
			}
		});



	}

	/**
	 * 
	 * @param message
	 */
	private void deleteFiltre(Message<Object> message) {

		if (!(message.body() instanceof JsonObject)) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, MAL_FORMATE, ERR_MANAGEMENT_FILTRE_TECH);
			message.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		JsonObject body = (JsonObject) message.body();

		if (!body.containsKey(Filtre.KEY_JSON_ID)) {
			message.fail(HTTP_BAD_REQUEST, ERR_IDENTIFIANT_FILTRE);
			return;
		}

		int id;
		String idAChercher = body.getString(Filtre.KEY_JSON_ID);
		try {
			id = new Integer(idAChercher).intValue();
		} catch (Exception e) {
			message.fail(HTTP_BAD_REQUEST, ERR_IDENTIFIANT_FILTRE_INVALIDE);
			return;
		}


		logger.trace(
				LOG_MESSAGE_WITH_DATA,
				LOG_NO_CORRELATION_ID,
				"Demande de suppression de filtre entrante");

		filtreDAO.delete(id)
		.setHandler(ar -> {
			if (ar.succeeded()) {
				Integer nombreLigneSupp = ar.result();
				if (nombreLigneSupp <= 0) {
					message.fail(HTTP_NOT_FOUND, "La ressource n'existe pas.");
				} else {
					message.reply(null);
				}
			} else {
				message.fail(HTTP_INTERNAL_ERROR, "une erreur est survenue lors de la suppression du filtre");
			}

		});
	}

	/**
	 * 
	 * @param message
	 */
	private void deleteFiltresParUtilisateur(Message<Object> message) {

		if (!(message.body() instanceof JsonObject)) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, MAL_FORMATE, ERR_MANAGEMENT_FILTRE_TECH);
			message.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		JsonObject body = (JsonObject) message.body();

		if (!body.containsKey(Filtre.KEY_JSON_UTILISATEUR_ID)) {
			message.fail(HTTP_BAD_REQUEST, ERR_UTILISATEUR_FILTRE);
			return;
		}

		int idUtilisateur;
		String idAChercher = body.getString(Filtre.KEY_JSON_UTILISATEUR_ID);
		try {
			idUtilisateur = new Integer(idAChercher).intValue();
		} catch (Exception e) {
			message.fail(HTTP_BAD_REQUEST, ERR_UTILISATEUR_FILTRE_INVALIDE);
			return;
		}


		logger.trace(
				LOG_MESSAGE_WITH_DATA,
				LOG_NO_CORRELATION_ID,
				"Demande de suppression de filtres entrante");

		filtreDAO.supprimerParUtilisateur(idUtilisateur)
		.setHandler(ar -> {
			if (ar.succeeded()) {
				message.reply(null);
			} else {
				message.fail(HTTP_INTERNAL_ERROR, "une erreur est survenue lors de la suppression des filtres");
			}

		});
	}
	
	/**
	 * 
	 * @param message
	 */
	private void getFiltres(Message<Object> message) {
		String action = "getFiltres";

		JsonObject criteres = (JsonObject) message.body();

		Map<String, Object> eqFilters = new HashMap<>();
		if (criteres != null){
			eqFilters.put(Filtre.KEY_JSON_ID, criteres.getString(Filtre.KEY_JSON_ID));
			if (criteres.getString(Filtre.KEY_JSON_NOM) != null){
				eqFilters.put(Filtre.KEY_JSON_NOM, criteres.getString(Filtre.KEY_JSON_NOM).trim());
			} else {
				eqFilters.put(Filtre.KEY_JSON_NOM, criteres.getString(Filtre.KEY_JSON_NOM));	
			}
			eqFilters.put(Filtre.KEY_JSON_UTILISATEUR_ID, criteres.getString(Filtre.KEY_JSON_UTILISATEUR_ID));
			eqFilters.put(Filtre.KEY_JSON_TYPE_FILTRE, criteres.getString(Filtre.KEY_JSON_TYPE_FILTRE));		
		}

		filtreDAO.lister(eqFilters).setHandler(ar -> {
			if (ar.succeeded()) {
				JsonArray result = new JsonArray();
				for (Filtre item : ar.result()) {
					result.add(item.toJsonObject());
				}
				message.reply(result);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
						ar.cause());
				message.fail(HTTP_INTERNAL_ERROR, "Une erreur est survenue lors de la récupération des filtres");
			}
		});
	}


	private boolean checkParams(Message<Object> message, JsonObject json) {

		if (!checkParamsModif(message, json)){
			return false;
		}

		//utilisateur_id
		try {   		   		
			Integer utilisateur_id = json.getInteger(Filtre.KEY_JSON_UTILISATEUR_ID);
			if (utilisateur_id == null) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_UTILISATEUR_FILTRE, ERR_MANAGEMENT_FILTRE_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_UTILISATEUR_FILTRE);
				return false;   			
			}
		} catch (Exception e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_UTILISATEUR_FILTRE_INVALIDE, ERR_MANAGEMENT_FILTRE_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_UTILISATEUR_FILTRE_INVALIDE);
			return false;   			
		}

		//type_filtre
		try {   		
			String type_filtre = json.getString(Filtre.KEY_JSON_TYPE_FILTRE);
			if ( type_filtre == null || !type_filtre.matches(".{1,250}$")) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_TYPE_FILTRE, ERR_MANAGEMENT_FILTRE_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_TYPE_FILTRE);
				return false;   			
			}
		} catch (Exception e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_TYPE_FILTRE_INVALIDE, ERR_MANAGEMENT_FILTRE_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_TYPE_FILTRE_INVALIDE);
			return false;
		}   	

		//contenu
		try {   		   		
			JsonObject contenu = json.getJsonObject(Filtre.KEY_JSON_CONTENU);
			if (contenu == null) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_UTILISATEUR_CRITERES, ERR_MANAGEMENT_FILTRE_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_UTILISATEUR_CRITERES);
				return false;   			
			}
		} catch (Exception e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_UTILISATEUR_CRITERES_INVALIDES, ERR_MANAGEMENT_FILTRE_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_UTILISATEUR_CRITERES_INVALIDES);
			return false;   			
		}

		return true;
	}


	private boolean checkParamsModif(Message<Object> message, JsonObject json) {

		//nom
		try {   		
			String nom = json.getString(Filtre.KEY_JSON_NOM);
			if ( nom == null) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_NOM_FILTRE, ERR_MANAGEMENT_FILTRE_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_NOM_FILTRE);
				return false;   			
			} else if (!nom.matches(".{1,100}$")) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_NOM_FILTRE_INVALIDE, ERR_MANAGEMENT_FILTRE_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_NOM_FILTRE_INVALIDE);
				return false;   			
			}
		} catch (Exception e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_NOM_FILTRE_INVALIDE, ERR_MANAGEMENT_FILTRE_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_NOM_FILTRE_INVALIDE);
			return false;
		}

		//description
		try {   		
			String description = json.getString(Filtre.KEY_JSON_DESCRIPTION);
			if (description != null && !description.trim().isEmpty() && !description.matches(".{1,250}$")) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_DESCRIPTION_FILTRE_INVALIDE, ERR_MANAGEMENT_FILTRE_TECH);
				message.fail(HTTP_BAD_REQUEST, ERR_DESCRIPTION_FILTRE_INVALIDE);
				return false;   			
			}
		} catch (Exception e) {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, ERR_DESCRIPTION_FILTRE_INVALIDE, ERR_MANAGEMENT_FILTRE_TECH);
			message.fail(HTTP_BAD_REQUEST, ERR_DESCRIPTION_FILTRE_INVALIDE);
			return false;
		}

		return true;
	}



}
