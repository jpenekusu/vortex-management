//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.pojo;


import java.io.Serializable;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.vertx.core.json.JsonObject;


@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Trace implements Serializable{

	private static final long serialVersionUID = -652831199414434071L;

	public static final String KEY_JSON_ID_ELEMENT = "id_element";
	public static final String KEY_JSON_NOM_ELEMENT = "nom_element";
	public static final String KEY_JSON_TYPE_ELEMENT = "type_element";
	public static final String KEY_JSON_ID = "id";
	public static final String KEY_JSON_CONTENU = "contenu";
	public static final String KEY_JSON_EVENT = "event";
	public static final String KEY_JSON_ORIGINE = "origine";
	public static final String KEY_JSON_NOM = "nom";
	public static final String KEY_JSON_PRENOM = "prenom";
	public static final String KEY_JSON_LOGIN = "login";
	public static final String KEY_JSON_DATE_TRACE = "date_trace";

	private Integer id;

	private String nom;

	private String prenom;

	private String login;

	private String origine;

	private String event;

	private String id_element;
	
	private String nom_element;
	
	private String type_element;

	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private JsonObject contenu;



	private Instant date_trace;



	public Trace() {
		super();
	}

	public Trace (Integer id, Instant date_trace , String login, String nom, String prenom, String origine, String event, String id_element, String nom_element, String type_element, JsonObject contenu
			) {
		super();
		this.setId(id);
		this.setDateTrace(date_trace);
		this.setLogin(login);
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setOrigine(origine);
		this.setEvent(event);
		this.setId_element(id_element);
		this.setNom_Element(nom_element);
		this.setType_Element(type_element);
		this.setContenu(contenu);
	}

	@Override
	public String toString() {
		return "Trace [id=" + id + ", date_trace=" + date_trace + ",  login=" + login +", nom="+ nom +", "
				+ "prenom=" + prenom + ", origine=" + origine+", event=" + event+", id_element=" + id_element+", nom_element=" + nom_element+", "
						+ "type_element=" + type_element+", contenu=" + contenu+"]";
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public static Trace fromJson(JsonObject json) {
		return new Trace(json);
	}   

	public Trace(JsonObject jsonObject) {
		if (jsonObject != null) {
			this.setId(jsonObject.getInteger(KEY_JSON_ID));

			this.setEvent(jsonObject.getString(KEY_JSON_EVENT));
			this.setId_element(jsonObject.getString(KEY_JSON_ID_ELEMENT));
			this.setNom_Element(jsonObject.getString(KEY_JSON_NOM_ELEMENT));
			this.setType_Element(jsonObject.getString(KEY_JSON_TYPE_ELEMENT));
			this.setLogin(jsonObject.getString(KEY_JSON_LOGIN));
			this.setNom(jsonObject.getString(KEY_JSON_NOM));
			this.setPrenom(jsonObject.getString(KEY_JSON_PRENOM));
			this.setOrigine(jsonObject.getString(KEY_JSON_ORIGINE));
			this.setDateTrace(jsonObject.getInstant(KEY_JSON_DATE_TRACE));
			Object contenu =  jsonObject.getValue(KEY_JSON_CONTENU);
			if (contenu instanceof String) {
				this.setContenu(new JsonObject(jsonObject.getString(KEY_JSON_CONTENU)));	
			} else {
				this.setContenu(jsonObject.getJsonObject(KEY_JSON_CONTENU));	
			}
			
		}
	}

	public JsonObject toJsonObject() {
		JsonObject jsonToEncode = new JsonObject();

		jsonToEncode.put(KEY_JSON_ID_ELEMENT, this.getId_element());
		jsonToEncode.put(KEY_JSON_NOM_ELEMENT, this.getNom_Element());
		jsonToEncode.put(KEY_JSON_TYPE_ELEMENT, this.getType_Element());
		jsonToEncode.put(KEY_JSON_ID, this.getId());
		jsonToEncode.put(KEY_JSON_CONTENU, this.getContenu());
		jsonToEncode.put(KEY_JSON_DATE_TRACE, this.getDateTrace());
		jsonToEncode.put(KEY_JSON_LOGIN, this.getLogin());
		jsonToEncode.put(KEY_JSON_NOM, this.getNom());
		jsonToEncode.put(KEY_JSON_PRENOM, this.getPrenom());
		jsonToEncode.put(KEY_JSON_ORIGINE, this.getOrigine());
		jsonToEncode.put(KEY_JSON_EVENT, this.getEvent());

		return jsonToEncode;
	}

	public void setContenu(JsonObject jsonObject) {
		// TODO Auto-generated method stub
		this.contenu= jsonObject;
	}

	/*public Json getContenu() {
		// TODO Auto-generated method stub
		return contenu;
	}
	 */
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the origine
	 */
	public String getOrigine() {
		return origine;
	}

	/**
	 * @param origine the origine to set
	 */
	public void setOrigine(String origine) {
		this.origine = origine;
	}

	/**
	 * @return the event
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(String event) {
		this.event = event;
	}

	/**
	 * @return the id_element
	 */
	public String getId_element() {
		return id_element;
	}

	/**
	 * @return the type_element
	 */
	public String getType_Element() {
		return type_element;
	}
	/**
	 * @param id_element2 the id_element to set
	 */
	public void setId_element(String id_element) {
		this.id_element = id_element;
	}
	/**
	 * @param type_element the type_element to set
	 */
	public void setType_Element(String type_element) {
		this.type_element = type_element;
	}
	/**
	 * @return the setDate
	 */
	public Instant getDateTrace() {
		return date_trace;
	}

	/**
	 * @param setDate the setDate to set
	 */
	public void setDateTrace(Instant instant) {
		this.date_trace = instant;
	}

	public JsonObject getContenu() {
		return contenu;
	}

	public String getNom_Element() {
		return nom_element;
	}

	public void setNom_Element(String nom_element) {
		this.nom_element = nom_element;
	}

}
