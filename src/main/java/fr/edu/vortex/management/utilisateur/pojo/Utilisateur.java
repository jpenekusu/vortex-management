//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Utilisateur implements Serializable {

    public static final String AUTHENTIFICATION_LOCALE = "LOCALE";
    public static final String AUTHENTIFICATION_LDAP = "LDAP";
    
    public static final String KEY_JSON_SYSTEME = "systeme";
    public static final String KEY_JSON_NOM = "nom";
    public static final String KEY_JSON_PRENOM = "prenom";
    public static final String KEY_JSON_LOGIN = "login";
    public static final String KEY_JSON_TYPE = "type_authentification";
    public static final String KEY_JSON_EMAIL = "email";

    public static final String KEY_DB_ID = "id";

    /**
     * 
     */
    private static final long serialVersionUID = -652831199414434071L;

    private int id;

    private String nom;

    private String prenom;

    private String login;

    private String hash;

    private String sel;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int nombreIteration;

    private String email;

    private String typeAuthentification;

    private boolean systeme;

    private Role role;

    private String motDePasse;

    public Utilisateur(int id, String nom, String prenom, String login, String hash, String sel, int nombreIteration,
            String email,
            String typeAuthentification, boolean systeme, Role role) {
        super();
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.hash = hash;
        this.sel = sel;
        this.nombreIteration = nombreIteration;
        this.email = email;
        this.typeAuthentification = typeAuthentification;
        this.systeme = systeme;
        this.role = role;
    }

    public Utilisateur() {
        super();
    }

    public static Utilisateur fromJson(JsonObject json) {
        return Json.mapper.convertValue(json.getMap(), Utilisateur.class);
    }

    public JsonObject toJson() {
        return new JsonObject(Json.encode(this));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTypeAuthentification() {
        return typeAuthentification;
    }

    public void setTypeAuthentification(String typeAuthentification) {
        this.typeAuthentification = typeAuthentification;
    }

    public boolean isSysteme() {
        return systeme;
    }

    public void setSysteme(boolean systeme) {
        this.systeme = systeme;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getSel() {
        return sel;
    }

    public void setSel(String sel) {
        this.sel = sel;
    }

    public int getNombreIteration() {
        return nombreIteration;
    }

    public void setNombreIteration(int nombreIteration) {
        this.nombreIteration = nombreIteration;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

}
