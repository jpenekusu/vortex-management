//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.verticle;

import fr.edu.vortex.management.utilisateur.handler.CreerUtilisateurLDAPHandler;
import fr.edu.vortex.management.utilisateur.handler.CreerUtilisateurLocalHandler;
import fr.edu.vortex.management.utilisateur.handler.CreerUtilisateurRepartiteurHandler;
import fr.edu.vortex.management.utilisateur.handler.ImporterUtilisateursHandler;
import fr.edu.vortex.management.utilisateur.handler.InscriptionUtilisateurRepartiteurHandler;
import fr.edu.vortex.management.utilisateur.handler.ListerUtilisateurHandler;
import fr.edu.vortex.management.utilisateur.handler.ModifierMDPUtilisateurLocalHandler;
import fr.edu.vortex.management.utilisateur.handler.ModifierUtilisateurLDAPHandler;
import fr.edu.vortex.management.utilisateur.handler.ModifierUtilisateurLocalHandler;
import fr.edu.vortex.management.utilisateur.handler.ModifierUtilisateurRepartiteurHandler;
import fr.edu.vortex.management.utilisateur.handler.SupprimerUtilisateurHandler;
import io.vertx.core.AbstractVerticle;

public class UtilisateurVerticle extends AbstractVerticle {
	
    public static final String KEY_JSON_ROLE_ID = "role_id";
    public static final String KEY_JSON_ROLE = "role";

    public static final String LISTER_UTILISATEUR = "vortex.management.utilisateur.lister";
    public static final String CREER_UTILISATEUR = "vortex.management.utilisateur.creer";
    public static final String INSCRIRE_UTILISATEUR = "vortex.management.utilisateur.inscrire";
    public static final String MODIFIER_UTILISATEUR = "vortex.management.utilisateur.modifier";
    public static final String MODIFIER_MDP_UTILISATEUR = "vortex.management.utilisateur.modifier.mdp";
    public static final String SUPPRIMER_UTILISATEUR = "vortex.management.utilisateur.supprimer";
    
    public static final String IMPORTER_UTILISATEURS = "vortex.management.utilisateur.importer";

    public static final String CREER_UTILISATEUR_LOCALE = "vortex.management.utilisateur.creer.locale";
    public static final String CREER_UTILISATEUR_LDAP = "vortex.management.utilisateur.creer.ldap";

    public static final String MODIFIER_UTILISATEUR_LOCALE = "vortex.management.utilisateur.modifier.locale";
    public static final String MODIFIER_UTILISATEUR_LDAP = "vortex.management.utilisateur.modifier.ldap";

    @Override
    public void start() throws Exception {
        vertx.eventBus().consumer(LISTER_UTILISATEUR, ListerUtilisateurHandler.create(vertx));
        vertx.eventBus().consumer(CREER_UTILISATEUR, CreerUtilisateurRepartiteurHandler.create(vertx));
        vertx.eventBus().consumer(INSCRIRE_UTILISATEUR, InscriptionUtilisateurRepartiteurHandler.create(vertx));
        vertx.eventBus().consumer(MODIFIER_UTILISATEUR, ModifierUtilisateurRepartiteurHandler.create(vertx));
        vertx.eventBus().consumer(MODIFIER_MDP_UTILISATEUR, ModifierMDPUtilisateurLocalHandler.create(vertx));
        vertx.eventBus().consumer(SUPPRIMER_UTILISATEUR, SupprimerUtilisateurHandler.create(vertx));
        vertx.eventBus().consumer(CREER_UTILISATEUR_LOCALE, CreerUtilisateurLocalHandler.create(vertx));
        vertx.eventBus().consumer(CREER_UTILISATEUR_LDAP, CreerUtilisateurLDAPHandler.create(vertx));
        vertx.eventBus().consumer(MODIFIER_UTILISATEUR_LOCALE, ModifierUtilisateurLocalHandler.create(vertx));
        vertx.eventBus().consumer(MODIFIER_UTILISATEUR_LDAP, ModifierUtilisateurLDAPHandler.create(vertx));
        vertx.eventBus().consumer(IMPORTER_UTILISATEURS, ImporterUtilisateursHandler.create(vertx));
    }

}
