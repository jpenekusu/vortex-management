//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.util.Optional;

import fr.edu.vortex.management.utilisateur.exception.Failure;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureEvtTrace;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureTypeTrace;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ModifierUtilisateurRepartiteurHandler extends UtilisateurHandler implements Handler<Message<Object>> {

    private static final String ERREUR_MODIFICATION_UTILISATEUR = "erreur lors de la modification d'un utilisateur";

    private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_MODIFIER_UTILISATEUR_TECH";

    private static final Logger logger = LoggerFactory.getLogger(CreerUtilisateurRepartiteurHandler.class);

    Vertx vertx;

    public static ModifierUtilisateurRepartiteurHandler create(Vertx vertx) {
        return new ModifierUtilisateurRepartiteurHandler(vertx);
    }

    private ModifierUtilisateurRepartiteurHandler(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void handle(Message<Object> event) {

        if (!(event.body() instanceof JsonObject)) {

            logger.error(
                    LOG_ERROR,
                    LOG_NO_CORRELATION_ID,
                    String.format(NOT_JSON, event.body().toString()),
                    ERR_MANAGEMENT_TECH);

            event.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
            return;
        }

        JsonObject body = (JsonObject) event.body();

		String header = body.getString("header");
		body.remove("header");
		String origine = body.getString("origine");
		body.remove("origine");

        if (!checkParams(event, body)) {
            return;
        }

        Utilisateur utilisateur;
        try {
            utilisateur = Utilisateur.fromJson(body);
        } catch (IllegalArgumentException e) {
            event.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
            return;
        }

        String adresse;

        if (Utilisateur.AUTHENTIFICATION_LOCALE.equals(utilisateur.getTypeAuthentification())) {
            adresse = UtilisateurVerticle.MODIFIER_UTILISATEUR_LOCALE;
        } else if (Utilisateur.AUTHENTIFICATION_LDAP.equals(utilisateur.getTypeAuthentification())) {
            adresse = UtilisateurVerticle.MODIFIER_UTILISATEUR_LDAP;
        } else {
            event.fail(HTTP_BAD_REQUEST, ERR_TYPE_AUTHENTIFICATION);
            return;
        }

        vertx.eventBus().<JsonObject>send(adresse, utilisateur.toJson(), ar -> {
            if (ar.succeeded()) {
    			JsonObject contenu = (JsonObject) ar.result().body();
    			String idEntite = contenu.getString("login");
    			String idElement = String.valueOf(contenu.getInteger("id"));
    			
    			traceEvent(NomenclatureTypeTrace.Utilisateur.getValeur(), NomenclatureEvtTrace.Modification.getValeur(), header, contenu, origine, idElement, idEntite, vertx);

                event.reply(ar.result().body());
            } else {
                Optional<Failure> failure = Failure.create(ar.cause());
                if (failure.isPresent()) {
                    event.fail(failure.get().getCode(), failure.get().getMessage());
                } else {
                    event.fail(HTTP_INTERNAL_ERROR, ERREUR_MODIFICATION_UTILISATEUR);
                }
            }
        });
    }

}
