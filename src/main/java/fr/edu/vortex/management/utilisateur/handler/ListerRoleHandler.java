//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.api.Pagination;
import fr.edu.vortex.management.api.PaginationException;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.persistence.dao.RoleDAO;
import fr.edu.vortex.management.persistence.dao.impl.RoleDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utils.PaginationUtils;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ListerRoleHandler implements Handler<Message<Object>> {

    private static final String DEMANDE_ENTRANTE = "Demande de la liste des rôles entrante";

    private static final String LOG_START = "Acteur permettant de lister les rôles créés.";

    private static final String ERREUR_LISTER_ROLES = "une erreur est survenue lors de la récupération des rôles";

    private static final String ERREUR_COUNT_ROLES = "une erreur est survenue lors du comptage des rôles";

    private static final Logger logger = LoggerFactory.getLogger(ListerRoleHandler.class);

    Vertx vertx;

    RoleDAO roleDao;

    public static ListerRoleHandler create(Vertx vertx) {
        return new ListerRoleHandler(vertx);
    }

    private ListerRoleHandler(Vertx vertx) {
        this.vertx = vertx;
        roleDao = new RoleDAOImpl();
        roleDao.init(vertx);
        logger.info(LOG_MESSAGE, LOG_NO_CORRELATION_ID, LOG_START);
    }

    @Override
    public void handle(Message<Object> event) {
        logger.trace(LOG_MESSAGE, LOG_NO_CORRELATION_ID, DEMANDE_ENTRANTE);
        
        String action = "listerRoles";
    	
		if (!(event.body() instanceof JsonObject)) {
			event.fail(HTTP_BAD_REQUEST, "mal formaté");
			return;
		}

		JsonObject criteres = (JsonObject) event.body();
		String page = criteres.getString(FluxVerticle.KEY_JSON_PAGE);
		String pageLimit = criteres.getString(FluxVerticle.KEY_JSON_PAGE_LIMIT);
		String sort = criteres.getString(FluxVerticle.KEY_JSON_SORT);
		String sortOrder = criteres.getString(FluxVerticle.KEY_JSON_SORT_ORDER);
		
		PaginationUtils.genererFiltresPourPagination(event,null,null);

      	JsonObject filters = new JsonObject();
      	filters.put(Role.KEY_JSON_LIBELLE, criteres.getString(Role.KEY_JSON_LIBELLE));
      	filters.put(Role.KEY_JSON_PERMISSIONS, criteres.getString(Role.KEY_JSON_PERMISSIONS));
		filters.put(Role.KEY_JSON_SYSTEME, criteres.getBoolean(Role.KEY_JSON_SYSTEME));

		roleDao.countRoles(filters).setHandler(countAr -> {
			if (countAr.succeeded()) {
				Integer borneMoins = null;
				Integer bornePlus = null;
				Pagination<JsonObject> paginateur = null;
				Integer nbUsers = countAr.result();
				if (page != null && pageLimit != null) {
					try {
						paginateur = new Pagination<>(page, pageLimit);
						borneMoins = paginateur.getBorneMoins() - 1;
						bornePlus = paginateur.getBornePlus() - 1;
					} catch (PaginationException e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						event.fail(HTTP_BAD_REQUEST,
								"Les paramêtres " + FluxVerticle.KEY_JSON_PAGE + " et " + FluxVerticle.KEY_JSON_PAGE_LIMIT + " sont erronés");
					}

				} else {
					paginateur = new Pagination<>(0, nbUsers);
					borneMoins = paginateur.getBorneMoins();
					bornePlus = paginateur.getBornePlus();
				} 

				roleDao.listerRoles(borneMoins, bornePlus, filters, sort, sortOrder).setHandler(ar -> {
					if (ar.succeeded()) {
						JsonArray result = new JsonArray();
						for (Role item : ar.result()) {
							result.add(item.toJson());
						}
						if (result.size() != nbUsers && result.size() != 0) {
							DeliveryOptions opts = new DeliveryOptions();
							opts.addHeader("count", nbUsers.toString());
							event.reply(result, opts);
						} else {
							event.reply(result);
						}

					} else {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
								ar.cause());
						event.fail(HTTP_INTERNAL_ERROR, ERREUR_LISTER_ROLES);
					}
				});

			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", countAr.cause());
				event.fail(HTTP_INTERNAL_ERROR, ERREUR_COUNT_ROLES);
			}
		});

    }

}
