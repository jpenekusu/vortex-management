//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.pojo;

import java.util.ArrayList;
import java.util.List;

public enum NomenclatureTypeTrace {

	//Objets directement construits
	Depot ("DEPOT"),
	Message ("MESSAGE"),
	Partenaire ("PARTENAIRE"),
	Role ("ROLE"),
	Route ("ROUTE"),
	Utilisateur ("UTILISATEUR"),
	ZoneReception ("ZR");

	private String TypeTrace = "";

	//Constructeur
	private NomenclatureTypeTrace(String TypeTrace){
		this.TypeTrace = TypeTrace;

	}

	public String toString(){
		return TypeTrace;
	}

	public String getValeur() {
		return this.TypeTrace;
	}

	public static List<String> getListeValeurs() {
		List<String> valeurs = new ArrayList<String>();
		
		NomenclatureTypeTrace[] values = values();
		for (int i = 0; i < values.length; i++) {
			valeurs.add(values[i].getValeur());
		}
		
		return valeurs;
	}

}
