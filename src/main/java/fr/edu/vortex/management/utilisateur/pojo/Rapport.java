//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.pojo;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.io.Serializable;

import io.vertx.core.json.JsonObject;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)

public class Rapport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7398080075748725142L;
	
	public static final String KEY_JSON_ID = "id";
	public static final String KEY_JSON_CONTENU = "contenu";
	public static final String KEY_JSON_LOGIN = "login";
	public static final String KEY_JSON_NOM = "nom";
	public static final String KEY_JSON_PRENOM = "prenom";
	public static final String KEY_JSON_TYPE_RAPPORT = "type_rapport";
	public static final String KEY_JSON_DATE_RAPPORT = "date_rapport";

	private Integer id;

	private String login;
	
	private String nom;
	
	private String prenom;
	
	private String type_rapport;

	private Instant date_rapport;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private JsonObject contenu;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	

	
	@Override
	public String toString() {
		return "Rapport [id=" + id + ", login=" + login + ", nom=" + nom + ", prenom=" + prenom + ", type_rapport="
				+ type_rapport + ", date_rapport=" + date_rapport + ", contenu=" + contenu + "]";
	}

	public Rapport() {
		super();
		// TODO Auto-generated constructor stub
	}

		
	public JsonObject getContenu() {
		return contenu;
	}

	public void setContenu(JsonObject contenu) {
		this.contenu = contenu;
	}

	public Rapport(Integer id, String login, String nom, String prenom,String type_rapport, Instant date_rapport, JsonObject contenu) {
		super();
		this.setId(id);
		this.setDate_rapport(date_rapport);
		this.setLogin(login);
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setType_rapport(type_rapport);
		this.setContenu(contenu);
	}
	
	
	public Rapport(JsonObject jsonObject) {
		if (jsonObject != null) {
			this.setId(jsonObject.getInteger(KEY_JSON_ID));

			this.setLogin(jsonObject.getString(KEY_JSON_LOGIN));
			this.setNom(jsonObject.getString(KEY_JSON_NOM));
			this.setPrenom(jsonObject.getString(KEY_JSON_PRENOM));
			this.setType_rapport(jsonObject.getString(KEY_JSON_TYPE_RAPPORT));
			this.setDate_rapport(jsonObject.getInstant(KEY_JSON_DATE_RAPPORT));
			Object contenu =  jsonObject.getValue(KEY_JSON_CONTENU);
			if (contenu instanceof String) {
				this.setContenu(new JsonObject(jsonObject.getString(KEY_JSON_CONTENU)));	
			} else {
				this.setContenu(jsonObject.getJsonObject(KEY_JSON_CONTENU));	
			}
			
		}
	}

	public JsonObject toJsonObject() {
		JsonObject jsonToEncode = new JsonObject();

		jsonToEncode.put(KEY_JSON_ID, this.getId());
		jsonToEncode.put(KEY_JSON_CONTENU, this.getContenu());
		jsonToEncode.put(KEY_JSON_DATE_RAPPORT, this.getDate_rapport());
		jsonToEncode.put(KEY_JSON_LOGIN, this.getLogin());
		jsonToEncode.put(KEY_JSON_NOM, this.getNom());
		jsonToEncode.put(KEY_JSON_PRENOM, this.getPrenom());
		jsonToEncode.put(KEY_JSON_TYPE_RAPPORT, this.getType_rapport());

		return jsonToEncode;
	}

	public static Rapport fromJson(JsonObject json) {
		return new Rapport(json);
	}   

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the type_rapport
	 */
	public String getType_rapport() {
		return type_rapport;
	}

	/**
	 * @param type_rapport the type_rapport to set
	 */
	public void setType_rapport(String type_rapport) {
		this.type_rapport = type_rapport;
	}

	/**
	 * @return the date_rapport
	 */
	public Instant getDate_rapport() {
		return date_rapport;
	}

	/**
	 * @param date_rapport the date_rapport to set
	 */
	public void setDate_rapport(Instant date_rapport) {
		this.date_rapport = date_rapport;
	}
	
	
}
