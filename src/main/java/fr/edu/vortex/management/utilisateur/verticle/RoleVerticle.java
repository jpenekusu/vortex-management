//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.verticle;

import fr.edu.vortex.management.utilisateur.handler.ChercherRoleHandler;
import fr.edu.vortex.management.utilisateur.handler.CreerRoleHandler;
import fr.edu.vortex.management.utilisateur.handler.ListerRoleHandler;
import fr.edu.vortex.management.utilisateur.handler.ModifierRoleHandler;
import fr.edu.vortex.management.utilisateur.handler.SupprimerRoleHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;

public class RoleVerticle extends AbstractVerticle {

    public static final String LISTER_ROLE = "vortex.management.role.lister";
    public static final String CREER_ROLE = "vortex.management.role.creer";
    public static final String MODIFIER_ROLE = "vortex.management.role.modifier";
    public static final String SUPPRIMER_ROLE = "vortex.management.role.supprimer";
    public static final String CHERCHER_ROLE_PAR_ID = "vortex.management.role.chercher_par_id";

    @Override
    public void start() throws Exception {
        vertx.eventBus().consumer(LISTER_ROLE, ListerRoleHandler.create(vertx));
        vertx.eventBus().<JsonObject>consumer(CREER_ROLE, CreerRoleHandler.create(vertx));
        vertx.eventBus().<JsonObject>consumer(MODIFIER_ROLE, ModifierRoleHandler.create(vertx));
        vertx.eventBus().<JsonObject>consumer(SUPPRIMER_ROLE, SupprimerRoleHandler.create(vertx));
        vertx.eventBus().<JsonObject>consumer(CHERCHER_ROLE_PAR_ID, ChercherRoleHandler.create(vertx));
    }

}
