//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import java.util.Optional;

import fr.edu.vortex.management.persistence.dao.RoleDAO;
import fr.edu.vortex.management.persistence.dao.impl.RoleDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

public class ChercherRoleHandler implements Handler<Message<JsonObject>> {

    Vertx vertx;

    RoleDAO roleDao;

    public static ChercherRoleHandler create(Vertx vertx) {
        return new ChercherRoleHandler(vertx);
    }

    private ChercherRoleHandler(Vertx vertx) {
        this.vertx = vertx;
        roleDao = new RoleDAOImpl();
        roleDao.init(vertx);
    }

    @Override
    public void handle(Message<JsonObject> event) {

        if (!(event.body() instanceof JsonObject)) {
            event.fail(400, "mal formaté");
            return;
        }

        JsonObject body = event.body();

        if (!body.containsKey("id")) {
            event.fail(400, "mal formaté");
            return;
        }

        Integer id = null;
        try {
            id = body.getInteger("id");
        } catch (Exception e) {
            event.fail(400, "mal formaté");
        }

        roleDao.chercherRoleByID(id).setHandler(ar -> {

            if (!ar.succeeded()) {
                event.fail(500, ar.cause().getMessage());
            } else {
                Optional<Role> result = ar.result();
                if (result.isPresent()) {
                    event.reply(result.get().toJson());
                } else {
                    event.fail(404, "La ressource n'existe pas.");
                }
            }
        });

    }

}
