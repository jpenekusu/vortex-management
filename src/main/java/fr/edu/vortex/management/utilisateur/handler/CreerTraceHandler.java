//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

/**
 * 
 */
package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_ERROR_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import fr.edu.vortex.management.persistence.dao.TraceDAO;
import fr.edu.vortex.management.persistence.dao.impl.TraceDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Trace;
import fr.edu.vortex.management.utilisateur.verticle.TokenVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;


/**
 * @author arhannaoui
 *
 */
public class CreerTraceHandler implements Handler<Message<Object>>{

	public static final String ORIGINE_MANQUANT = "origine manquant";

	public static final String EVENT_MANQUANT = "event manquant";

	public static final String ID_ELEMENT_MANQUANT = "id_element manquant";

	public static final String NOM_ELEMENT_MANQUANT = "nom_element manquant";

	public static final String TYPE_ELEMENT_MANQUANT = "type_element manquant";

	public static final String CONTENU_MANQUANT = "contenu manquant";

	public static final String DATE_MANQUANTE = "date manquante";

	public static final String LOGIN_MANQUANT = "login manquant";

	public static final String PRENOM_MANQUANT = "prénom manquant";

	public static final String NOM_MANQUANT = "nom manquant";

	public static final String ERR_AUTH = "acteur ne pouvant pas traiter la demande";

	public static final String MESSAGE_CONFLICT = "%s existe déjà";

	public static final String NOT_JSON = "le paramètre entrant n'est pas un json (input = %s)";

	public static final String MAL_FORMATE = "mal formaté";

	private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_CREER_TRACE_TECH";

	private static final String DEMANDE_ENTRANTE = "Demande de création de trace entrante";

	private static final String ERR_CREER_TRACE = "une erreur est survenue lors de la création de la trace";

	private static final Logger logger = LoggerFactory.getLogger(CreerTraceHandler.class);

	Vertx vertx;

	TraceDAO traceDao;

	public static CreerTraceHandler create(Vertx vertx) {
		return new CreerTraceHandler(vertx);
	}

	private CreerTraceHandler(Vertx vertx) {
		this.vertx = vertx;

		traceDao = new TraceDAOImpl();
		traceDao.init(vertx);
	}

	@Override
	public void handle(Message<Object> msg) {

		if (!(msg.body() instanceof JsonObject)) {
			logger.error(
					LOG_ERROR,
					LOG_NO_CORRELATION_ID,
					String.format(NOT_JSON, msg.body().toString()),
					ERR_MANAGEMENT_TECH);
			msg.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		JsonObject body = (JsonObject) msg.body();

		JsonObject contenuDesordonne = body.getJsonObject("contenu");
		//ordonner json
		JsonObject contenu = getJsonOrdonne(contenuDesordonne);		

		String header = body.getString("header");
		String typeElement = body.getString("typeElement");
		String idElement = body.getString("idElement");
		String idEntite = body.getString("idEntite");
		String origine = body.getString("origine");
		String event = body.getString("event");


		JsonObject jsonHeader = new JsonObject();
		jsonHeader.put("jwt", header);
		vertx.eventBus().send(TokenVerticle.TOKEN_UTILISATEUR, jsonHeader,
				ar -> {
					if (ar.succeeded()) {
						JsonObject user = (JsonObject) ar.result().body();

						JsonObject jsonTrace = new JsonObject();

						jsonTrace.put(Trace.KEY_JSON_ID_ELEMENT, idElement);
						jsonTrace.put(Trace.KEY_JSON_NOM_ELEMENT, idEntite);
						jsonTrace.put(Trace.KEY_JSON_TYPE_ELEMENT, typeElement);
						jsonTrace.put(Trace.KEY_JSON_CONTENU, contenu);
						jsonTrace.put(Trace.KEY_JSON_DATE_TRACE, Instant.now());

						jsonTrace.put(Trace.KEY_JSON_LOGIN, user.getString("login"));
						jsonTrace.put(Trace.KEY_JSON_NOM, user.getString("nom"));
						jsonTrace.put(Trace.KEY_JSON_PRENOM, user.getString("prenom"));

						jsonTrace.put(Trace.KEY_JSON_ORIGINE, origine);
						jsonTrace.put(Trace.KEY_JSON_EVENT, event);

						Trace trace;
						try {
							trace = Trace.fromJson(jsonTrace);
						} catch (IllegalArgumentException e) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(MAL_FORMATE),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
							return;
						}

						logger.trace(
								LOG_MESSAGE_WITH_DATA,
								LOG_NO_CORRELATION_ID,
								"Demande de creation de trace entrante",
								body);

						if (trace.getNom() == null || trace.getNom().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(NOM_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, NOM_MANQUANT);
							return;
						}

						if (trace.getPrenom() == null || trace.getPrenom().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(PRENOM_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, PRENOM_MANQUANT);
							return;
						}

						if (trace.getLogin() == null || trace.getLogin().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(LOGIN_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, LOGIN_MANQUANT);
							return;
						}

						if (trace.getOrigine() == null || trace.getOrigine().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(ORIGINE_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, ORIGINE_MANQUANT);
							return;
						}

						if (trace.getEvent() == null || trace.getEvent().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(EVENT_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, EVENT_MANQUANT);
							return;
						}

						if (trace.getId_element() == null || trace.getId_element().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(ID_ELEMENT_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, ID_ELEMENT_MANQUANT);
							return;
						}

						if (trace.getType_Element() == null || trace.getType_Element().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(TYPE_ELEMENT_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, TYPE_ELEMENT_MANQUANT);
							return;
						}

						if (trace.getContenu()== null || ((JsonObject) trace.getContenu()).isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(CONTENU_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, CONTENU_MANQUANT);
							return;
						}

						if (trace.getDateTrace()== null || trace.getDateTrace().toString().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(DATE_MANQUANTE),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, DATE_MANQUANTE);
							return;
						}

						logger.trace(LOG_MESSAGE, LOG_NO_CORRELATION_ID, DEMANDE_ENTRANTE);
						traceDao.creer(trace)
						.setHandler(arTrace -> {
							if (arTrace.succeeded()) {
								Trace result = arTrace.result();

								logger.trace(
										LOG_MESSAGE_WITH_DATA,
										LOG_NO_CORRELATION_ID,
										result);

								msg.reply(result.toJsonObject());
							} else {
								logger.error(LOG_ERROR,
										LOG_NO_CORRELATION_ID,
										ERR_CREER_TRACE,
										ERR_MANAGEMENT_TECH,
										arTrace.cause());
								msg.fail(HTTP_INTERNAL_ERROR, ERR_CREER_TRACE);
							}
						});

					} else {
						//utilisateur non récupéré
						logger.error(LOG_ERROR,
								LOG_NO_CORRELATION_ID,
								ERR_CREER_TRACE,
								ERR_MANAGEMENT_TECH,
								ar.cause());
						msg.fail(HTTP_INTERNAL_ERROR, ERR_CREER_TRACE);               		
					}
				});     
	}

	private JsonObject getJsonOrdonne(JsonObject contenuDesordonne) {

		Set<String> fieldNames = contenuDesordonne.fieldNames();
		List<String> liste = new ArrayList<String>();
		liste.addAll(fieldNames);
		//		Collections.sort(liste);

		JsonObject contenu = new JsonObject();
		contenu.put("id", contenuDesordonne.getValue("id"));

		if (liste.contains("pattern_binding")) {
			contenu.put("id", contenuDesordonne.getValue("id"));
			contenu.put("pattern_binding", contenuDesordonne.getValue("pattern_binding"));
			if (contenuDesordonne.getJsonObject("exchange") != null) {
				contenu.put("exchange", getJsonOrdonne(contenuDesordonne.getJsonObject("exchange")));
			}
			if (contenuDesordonne.getJsonObject("queue") != null) {
				contenu.put("queue", getJsonOrdonne(contenuDesordonne.getJsonObject("queue")));
			}
		} else {	
			for (Iterator<String> iterator = liste.iterator(); iterator.hasNext();) {
				String champ = iterator.next();
				if (!"id".equals(champ)) {
					if (contenuDesordonne.getValue(champ) instanceof JsonObject) {
						contenu.put(champ, getJsonOrdonne(contenuDesordonne.getJsonObject(champ)));					
					} else {
						contenu.put(champ, contenuDesordonne.getValue(champ));	
					}
				}
			}
		}
		return contenu;
	}


}

