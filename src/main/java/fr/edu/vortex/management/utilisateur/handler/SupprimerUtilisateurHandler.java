//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import fr.edu.vortex.management.persistence.dao.UtilisateurDAO;
import fr.edu.vortex.management.persistence.dao.impl.UtilisateurDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Filtre;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureEvtTrace;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureTypeTrace;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.FiltreUtilisateurVerticle;
import fr.edu.vortex.management.utilisateur.verticle.TraceVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

public class SupprimerUtilisateurHandler implements Handler<Message<Object>> {

    Vertx vertx;

    UtilisateurDAO utilisateurDao;

    public static SupprimerUtilisateurHandler create(Vertx vertx) {
        return new SupprimerUtilisateurHandler(vertx);
    }

    private SupprimerUtilisateurHandler(Vertx vertx) {
        this.vertx = vertx;
        utilisateurDao = new UtilisateurDAOImpl();
        utilisateurDao.init(vertx);
    }

    @Override
    public void handle(Message<Object> event) {

        if (!(event.body() instanceof JsonObject)) {
            event.fail(400, "mal formaté");
            return;
        }

        JsonObject body = (JsonObject) event.body();

        if (!body.containsKey("id")) {
            event.fail(400, "mal formaté");
            return;
        }

        int id;
        String idAChercher = body.getString("id");
        try {
            id = new Integer(idAChercher).intValue();
        } catch (Exception e) {
            event.fail(400, "identifiant passé n'est pas un entier");
            return;
        }
        
        utilisateurDao.getById(id).setHandler(arUtilisateur -> {
        	if (arUtilisateur.succeeded()) {
        		Utilisateur user = arUtilisateur.result();
        		if (user == null) {
        			event.fail(404, "La ressource n'existe pas.");
        		} else {
        			JsonObject idUtilisateur = new JsonObject();
        			idUtilisateur.put(Filtre.KEY_JSON_UTILISATEUR_ID, idAChercher);
        			vertx.eventBus().send(FiltreUtilisateurVerticle.SUPPRIMER_FILTRES_UTILISATEUR, idUtilisateur,
        					arSuppFiltres -> {
        						if (arSuppFiltres.succeeded()) {
        							utilisateurDao.delete(id)
        							.setHandler(ar -> {
        								if (ar.succeeded()) {
        									Integer nombreLigneSupp = ar.result();
        									if (nombreLigneSupp <= 0) {
        										event.fail(404, "La ressource n'existe pas.");
        									} else {
        										String header = body.getString("header");
        										body.remove("header");
        										String origine = body.getString("origine");
        										body.remove("origine");

        										user.setHash(null);
        										user.setSel(null);
        										user.setMotDePasse(null);

        										JsonObject contenu = user.toJson();
        										contenu.remove("nombre_iteration");

        										String idElement = String.valueOf(user.getId());
        										String idEntite = user.getLogin();

        										traceEvent(NomenclatureTypeTrace.Utilisateur.getValeur(), NomenclatureEvtTrace.Suppression.getValeur(), header, contenu, origine, idElement, idEntite);

        										event.reply(null);
        									}
        								} else {
        									event.fail(500, "une erreur est survenue lors de la suppression de l'utilisateur");
        								}
        							});
        						}else{
        							event.fail(500, "une erreur est survenue lors de la suppression des filtres de l'utilisateur");          						
        						}
        					});
        		}
        	} else {
        		event.fail(500, "une erreur est survenue lors de la suppression de l'utilisateur");
        	}      	
        });
    }

    private void traceEvent(String typeElement, String event, String header, JsonObject contenu, String origine,
			String idElement, String idEntite) {
		JsonObject jsonTrace = new JsonObject();

		jsonTrace.put("header", header);

		jsonTrace.put("typeElement", typeElement);
		jsonTrace.put("event", event);
		jsonTrace.put("origine", origine);

		jsonTrace.put("contenu", contenu);

		jsonTrace.put("idElement", idElement);
		jsonTrace.put("idEntite", idEntite);

		vertx.eventBus().send(TraceVerticle.CREER_TRACE, jsonTrace);
	}
}
