//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

/**
 * 
 */
package fr.edu.vortex.management.utilisateur.handler;

import java.util.Map;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.api.Pagination;
import fr.edu.vortex.management.api.PaginationException;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.persistence.dao.TraceDAO;
import fr.edu.vortex.management.persistence.dao.impl.TraceDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Trace;
import fr.edu.vortex.management.utils.PaginationUtils;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * @author arhannaoui
 *
 */
public class ListerTraceHandler implements Handler<Message<Object>>{

	public static final String ERREUR_LISTER_TRACE = "Erreur lors de la récupération des traces dans la persistence.";    
	public static final String ERREUR_COUNT_TRACE = "Erreur lors du comptage des traces dans la persistence.";

	private static final Logger logger = LoggerFactory.getLogger(ListerTraceHandler.class);

	Vertx vertx;

	TraceDAO traceDao;

	public static ListerTraceHandler create(Vertx vertx) {
		return new ListerTraceHandler(vertx);
	}

	private ListerTraceHandler(Vertx vertx) {
		this.vertx = vertx;
		traceDao = new TraceDAOImpl();
		traceDao.init(vertx);
	}

	@Override
	public void handle(Message<Object> event) {

		String action = "loadAllFlux(Message<Object> message)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		if (!(event.body() instanceof JsonObject)) {
			event.fail(400, "mal formaté");
			return;
		}

		JsonObject criteres = (JsonObject) event.body();
		String page = criteres.getString(FluxVerticle.KEY_JSON_PAGE);
		String pageLimit = criteres.getString(FluxVerticle.KEY_JSON_PAGE_LIMIT);
		String sort = criteres.getString(FluxVerticle.KEY_JSON_SORT);
		String sortOrder = criteres.getString(FluxVerticle.KEY_JSON_SORT_ORDER);

		String dateTrace = criteres.getString(Trace.KEY_JSON_DATE_TRACE);
		
		Map<String, Object> filters = PaginationUtils.genererFiltresPourPagination(event,Trace.KEY_JSON_DATE_TRACE,dateTrace);

		filters.put(Trace.KEY_JSON_ID_ELEMENT, criteres.getString(Trace.KEY_JSON_ID_ELEMENT));
		filters.put(Trace.KEY_JSON_NOM_ELEMENT, criteres.getString(Trace.KEY_JSON_NOM_ELEMENT));
		filters.put(Trace.KEY_JSON_TYPE_ELEMENT, criteres.getString(Trace.KEY_JSON_TYPE_ELEMENT));
		filters.put(Trace.KEY_JSON_EVENT, criteres.getString(Trace.KEY_JSON_EVENT));
		filters.put(Trace.KEY_JSON_ORIGINE, criteres.getString(Trace.KEY_JSON_ORIGINE));
		filters.put(Trace.KEY_JSON_NOM, criteres.getString(Trace.KEY_JSON_NOM));
		filters.put(Trace.KEY_JSON_PRENOM, criteres.getString(Trace.KEY_JSON_PRENOM));
		filters.put(Trace.KEY_JSON_LOGIN, criteres.getString(Trace.KEY_JSON_LOGIN));

		traceDao.countTrace(filters).setHandler(countTraceAr -> {
			if (countTraceAr.succeeded()) {
				Integer borneMoins = null;
				Integer bornePlus = null;
				Pagination<JsonObject> paginateur = null;
				Integer nbTrace = countTraceAr.result();
				if (page != null && pageLimit != null) {
					try {
						paginateur = new Pagination<>(page, pageLimit);
						borneMoins = paginateur.getBorneMoins() - 1;
						bornePlus = paginateur.getBornePlus() - 1;
					} catch (PaginationException e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						event.fail(400,
								"Les paramêtres " + FluxVerticle.KEY_JSON_PAGE + " et " + FluxVerticle.KEY_JSON_PAGE_LIMIT + " sont erronés");
					}

				} else {
					paginateur = new Pagination<>(0, nbTrace);
					borneMoins = paginateur.getBorneMoins();
					bornePlus = paginateur.getBornePlus();
				} 

				traceDao.listerTrace(borneMoins, bornePlus, filters, sort, sortOrder).setHandler(ar -> {
					if (ar.succeeded()) {
						JsonArray result = new JsonArray();
						for (Trace item : ar.result()) {
							result.add(item.toJsonObject());
						}
						if (result.size() != nbTrace && result.size() != 0) {
							DeliveryOptions opts = new DeliveryOptions();
							opts.addHeader("count", nbTrace.toString());
							event.reply(result, opts);
						} else {
							event.reply(result);
						}

					} else {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
								ar.cause());
						event.fail(500, ERREUR_LISTER_TRACE);
					}
				});

			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", countTraceAr.cause());
				event.fail(500, ERREUR_COUNT_TRACE);
			}
		});

	}

}
