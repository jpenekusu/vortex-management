//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_ERROR_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.time.Instant;

import fr.edu.vortex.management.persistence.dao.RapportDAO;
import fr.edu.vortex.management.persistence.dao.impl.RapportDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Rapport;
import fr.edu.vortex.management.utilisateur.verticle.TokenVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class CreerRapportHandler implements Handler<Message<Object>>{


	public static final String ID_MANQUANT = "id manquant";

	public static final String TYPE_RAPPORT_MANQUANT = "type rapport manquant";

	public static final String CONTENU_MANQUANT = "contenu manquant";

	public static final String DATE_RAPPORT_MANQUANTE = "date rapport manquante";

	public static final String LOGIN_MANQUANT = "login manquant";
	
	public static final String NOM_MANQUANT = "nom manquant";
	
	public static final String PRENOM_MANQUANT = "prenom manquant";

	public static final String ERR_AUTH = "acteur ne pouvant pas traiter la demande";

	public static final String MESSAGE_CONFLICT = "%s existe déjà";

	public static final String NOT_JSON = "le paramètre entrant n'est pas un json (input = %s)";

	public static final String MAL_FORMATE = "mal formaté";

	private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_CREER_RAPPORT_TECH";

	private static final String ERR_CREER_RAPPORT = "une erreur est survenue lors de la création du rapport";

	private static final Logger logger = LoggerFactory.getLogger(CreerRapportHandler.class);

	Vertx vertx;

	RapportDAO rapportDAO;

	public static CreerRapportHandler create(Vertx vertx) {
		return new CreerRapportHandler(vertx);
	}

	private CreerRapportHandler(Vertx vertx) {
		this.vertx = vertx;

		rapportDAO = new RapportDAOImpl();
		rapportDAO.init(vertx);
	}

	@Override
	public void handle(Message<Object> msg) {

		if (!(msg.body() instanceof JsonObject)) {
			logger.error(
					LOG_ERROR,
					LOG_NO_CORRELATION_ID,
					String.format(NOT_JSON, msg.body().toString()),
					ERR_MANAGEMENT_TECH);
			msg.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		JsonObject body = (JsonObject) msg.body();

		

		String header = body.getString("header");
		String type_rapport = body.getString("type_rapport");
		

		JsonObject jsonHeader = new JsonObject();
		jsonHeader.put("jwt", header);
		vertx.eventBus().send(TokenVerticle.TOKEN_UTILISATEUR, jsonHeader,
				ar -> {
					if (ar.succeeded()) {
						JsonObject user = (JsonObject) ar.result().body();

						JsonObject jsonRapport = new JsonObject();
						
						body.remove("header");
						body.remove("type_rapport");
						body.remove("origine");

						jsonRapport.put(Rapport.KEY_JSON_TYPE_RAPPORT, type_rapport);
						jsonRapport.put(Rapport.KEY_JSON_CONTENU, body);
						jsonRapport.put(Rapport.KEY_JSON_DATE_RAPPORT, Instant.now());
						jsonRapport.put(Rapport.KEY_JSON_LOGIN, user.getString("login"));
						jsonRapport.put(Rapport.KEY_JSON_NOM, user.getString("nom"));
						jsonRapport.put(Rapport.KEY_JSON_PRENOM, user.getString("prenom"));

						Rapport rapport;
						try {
							rapport = Rapport.fromJson(jsonRapport);
						} catch (IllegalArgumentException e) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(MAL_FORMATE),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
							return;
						}
						
			
						if (rapport.getLogin() == null || rapport.getLogin().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(LOGIN_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, LOGIN_MANQUANT);
							return;
						}
						
						if (rapport.getNom() == null || rapport.getNom().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(NOM_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, NOM_MANQUANT);
							return;
						}
						
						if (rapport.getPrenom() == null || rapport.getPrenom().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(PRENOM_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, PRENOM_MANQUANT);
							return;
						}

						if (rapport.getType_rapport() == null || rapport.getType_rapport().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(TYPE_RAPPORT_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, TYPE_RAPPORT_MANQUANT);
							return;
						}

						if (rapport.getContenu()== null || ((JsonObject) rapport.getContenu()).isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(CONTENU_MANQUANT),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, CONTENU_MANQUANT);
							return;
						}

						if (rapport.getDate_rapport()== null || rapport.getDate_rapport().toString().isEmpty()) {
							logger.error(
									LOG_ERROR_WITH_DATA,
									LOG_NO_CORRELATION_ID,
									String.format(DATE_RAPPORT_MANQUANTE),
									body,
									ERR_MANAGEMENT_TECH);
							msg.fail(HTTP_BAD_REQUEST, DATE_RAPPORT_MANQUANTE);
							return;
						}

						rapportDAO.creer(rapport)
						.setHandler(arRapport -> {
							if (arRapport.succeeded()) {
								Rapport result = arRapport.result();
								
								msg.reply(result.toJsonObject());
								
							} else {
								logger.error(LOG_ERROR,
										LOG_NO_CORRELATION_ID,
										ERR_CREER_RAPPORT,
										ERR_MANAGEMENT_TECH,
										arRapport.cause());
								msg.fail(HTTP_INTERNAL_ERROR, ERR_CREER_RAPPORT);
							}
						});

					} else {
						//utilisateur non récupéré
						logger.error(LOG_ERROR,
								LOG_NO_CORRELATION_ID,
								ERR_CREER_RAPPORT,
								ERR_MANAGEMENT_TECH,
								ar.cause());
						msg.fail(HTTP_INTERNAL_ERROR, ERR_CREER_RAPPORT);               		
					}
				}); 
				    
	}

}
