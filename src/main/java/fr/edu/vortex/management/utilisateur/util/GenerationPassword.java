package fr.edu.vortex.management.utilisateur.util;

import java.util.ArrayList;
import java.util.List;

import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordGenerator;
import org.passay.PasswordValidator;
import org.passay.Rule;
import org.passay.RuleResult;

public class GenerationPassword {
    
    private static final int TAILLE_PWD = 12;
    
    public static String generationMotDePasse() {
    	
    	PasswordGenerator passwordGenerator = new PasswordGenerator();
    
    	List<CharacterRule> listeRules = listeReglesARespecter();
    	
    	String generatePassword = passwordGenerator.generatePassword(TAILLE_PWD, listeRules);
    	return generatePassword;
    	
        }
    
    /**
     * Règles à repecter
     * Les mots de passe doivent comporter 12 caractères minimum (pas de lettre accentuée et pas d’espace). 
     * Il sera constitué d’au moins 1 majuscule, 1 minuscule, 1 chiffre et 
     * 1 caractère spécial (&"'-_=*:!+%?./#{[|\@]}<>)
     * 
     * @return
     */
    public static List<CharacterRule> listeReglesARespecter() {
            CharacterData madata = new CharacterData() {
                String special="&\"'-_=*:!+%?./#{[|\\@]";
            
                @Override
                public String getErrorCode() {
            	return "erreur génération mot de passe";
                }
                
                @Override
                public String getCharacters() {
            	return special;
                }
            };
        
            CharacterRule minusculesRules = new CharacterRule(EnglishCharacterData.LowerCase,1);
            CharacterRule majusculesRules = new CharacterRule(EnglishCharacterData.UpperCase,1);
            CharacterRule chiffresRules = new CharacterRule(EnglishCharacterData.Digit,1);
            CharacterRule specialRules = new CharacterRule(madata,1);
            
            List<CharacterRule> listeRules = new ArrayList<CharacterRule>();
            listeRules.add(minusculesRules);
            listeRules.add(majusculesRules);
            listeRules.add(chiffresRules);
            listeRules.add(specialRules);
            return listeRules;
        }
    
    /**
     * Méthode permettant de valider un mot de passe
     * 
     * @param motPasse
     * @return
     */
    public static boolean validationMotDePasse(String motPasse) {
	PasswordData passwordData = new PasswordData(motPasse);	
	List<CharacterRule> carateresValides = GenerationPassword.listeReglesARespecter();
       	List<Rule> listeArespecter = new ArrayList<Rule>();
       	listeArespecter.add(new LengthRule(12));
       	listeArespecter.addAll(carateresValides);
       	PasswordValidator p = new PasswordValidator(listeArespecter);	
       	RuleResult validate = p.validate(passwordData);
       	return validate.isValid();
   }
}
