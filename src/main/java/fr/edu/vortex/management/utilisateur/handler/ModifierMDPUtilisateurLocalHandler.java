//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.util.Optional;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.dao.UtilisateurDAO;
import fr.edu.vortex.management.persistence.dao.impl.UtilisateurDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureEvtTrace;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureTypeTrace;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ModifierMDPUtilisateurLocalHandler extends UtilisateurHandler implements Handler<Message<Object>> {

    private static final String ERREUR_MODIFICATION_UTILISATEUR = "erreur lors de la modification d'un utilisateur";

    private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_CREER_UTILISATEUR_TECH";

    private static final Logger logger = LoggerFactory.getLogger(ModifierMDPUtilisateurLocalHandler.class);

    Vertx vertx;

    UtilisateurDAO utilisateurDao;

    public static ModifierMDPUtilisateurLocalHandler create(Vertx vertx) {
        return new ModifierMDPUtilisateurLocalHandler(vertx);
    }

    private ModifierMDPUtilisateurLocalHandler(Vertx vertx) {
        this.vertx = vertx;
        utilisateurDao = new UtilisateurDAOImpl();
        utilisateurDao.init(vertx);
    }

    @Override
    public void handle(Message<Object> event) {

        if (!(event.body() instanceof JsonObject)) {

            logger.error(
                    LOG_ERROR,
                    LOG_NO_CORRELATION_ID,
                    String.format(NOT_JSON, event.body().toString()),
                    ERR_MANAGEMENT_TECH);

            event.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
            return;
        }

        JsonObject utilisateurModif = (JsonObject) event.body();

		String header = utilisateurModif.getString("header");
		utilisateurModif.remove("header");
		String origine = utilisateurModif.getString("origine");
		utilisateurModif.remove("origine");

        if (utilisateurModif.getString("login") == null || utilisateurModif.getString("login").isEmpty()) {
            event.fail(HTTP_BAD_REQUEST, LOGIN_MANQUANT);
            return;
        }

        if (utilisateurModif.getString(KEY_PARAM_MDP) == null || utilisateurModif.getString(KEY_PARAM_MDP).isEmpty()) {
            event.fail(HTTP_BAD_REQUEST, MOT_DE_PASSE_MANQUANT);
            return;
        }

        if (utilisateurModif.getInteger("id") == null) {
            event.fail(HTTP_BAD_REQUEST, ID_MANQUANT);
            return;
        }
        
        JsonObject criteres = new JsonObject();
        criteres.put(Utilisateur.KEY_DB_ID, utilisateurModif.getInteger("id"));
        
        Future<Void> verifierSecuritePassword = verifierSecuritePassword(utilisateurModif.getString(KEY_PARAM_MDP));
        
        verifierSecuritePassword
        .compose( v->utilisateurDao.listerUtilisateurs(0, 20, criteres, null, null))
        .compose(utilisateurs -> {
			Future<JsonObject> fut = Future.future();
			Optional<Utilisateur> findFirst = utilisateurs.stream()
					.filter(it -> (utilisateurModif.getInteger("id").intValue() == it.getId()))
					.findFirst();
			if (findFirst.isPresent()) {
				fut.complete(utilisateurModif);
			} else {
				event.fail(204, "L'utilisateur n'existe pas");
			}
			return fut;
		})
        .compose(this::creerHash)
        .compose(utilisateurDao::modifierMotDePasse)
        .compose(this::masquerDonneesSensibles)
        .setHandler(ar -> {
        	if (ar.succeeded()) {
    			JsonObject contenu = (JsonObject) ar.result();
    			String idEntite = contenu.getString("login");
    			String idElement = String.valueOf(contenu.getInteger("id"));
    			contenu.put("action", "Modification du mot de passe");
    			
    			traceEvent(NomenclatureTypeTrace.Utilisateur.getValeur(), NomenclatureEvtTrace.Modification.getValeur(), header, contenu, origine, idElement, idEntite, vertx);      		
        		
                event.reply(ar.result());
            } else {
                event.fail(HTTP_INTERNAL_ERROR, ERREUR_MODIFICATION_UTILISATEUR);
            }
        });
    }
    

}
