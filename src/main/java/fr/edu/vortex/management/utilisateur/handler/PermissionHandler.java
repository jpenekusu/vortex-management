//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;

import java.util.List;
import java.util.Optional;

import fr.edu.vortex.management.persistence.dao.PermissionDAO;
import fr.edu.vortex.management.utilisateur.exception.ManagementException;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import fr.edu.vortex.management.utilisateur.verticle.TraceVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class PermissionHandler {

    protected static final String ERR_ROLE_PERMISSION_NON_EXISTANTE = "Impossible de créer le rôle car une ou plusieurs permissions n'existe pas";

    protected static final String ERR_MODELE_PERMISSION_NON_EXISTANTE = "Impossible de créer le modèle car une ou plusieurs permissions n'existe pas";

    protected Vertx vertx;

    /**
     * Vérifie si les permissions envoyé dans la requête sont correctes
     * 
     * @param permissions
     * @return
     */
    protected boolean permissionsEstValides(JsonArray permissions) {
        long nombrePermissionsSansCode = permissions.stream()
                .map(o -> (JsonObject) o)
                .filter(it -> !it.containsKey(Permission.KEY_JSON_CODE))
                .filter(it -> !it.containsKey(Permission.KEY_JSON_ACTIONS))
                .count();

        return nombrePermissionsSansCode == 0;
    }
    
	protected boolean estJsonArray(JsonObject json, String key) {
		try {
			json.getJsonArray(key);
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}
	
    /**
     * Vérifie que les permissions passées lors de la demande de création du rôle
     * existent dans la persistence.
     * 
     * @param permissions
     *            les permissions passées lors de la demande création du rôle
     * @return
     */
    protected Future<Void> verifierPermissionsConnues(JsonArray permissions, PermissionDAO permissionDao, String errCode) {
        Future<Void> fut = Future.future();

        // pas besoin de faire la vérfication si pas de permissions dans la demande
        if (permissions == null || permissions.isEmpty()) {
            return Future.succeededFuture();
        }

        // on va chercher les permissions dans la persistence
        permissionDao.lister().setHandler(ar -> {
            if (ar.succeeded()) {
                List<Permission> permissionsConnues = ar.result();
                for (Object permissionCourante : permissions) {

                    String codeCourant = ((JsonObject) permissionCourante).getString(Permission.KEY_JSON_CODE);

                    // dans les permissions connues, on cherche si il en existe une avec le
                    // codeCourant
                    Optional<String> findAny = permissionsConnues.stream()
                            .map(Permission::getCode)
                            .filter(p -> p.equals(codeCourant))
                            .findAny();

                    if (!findAny.isPresent()) {
                        ManagementException errMetier = new ManagementException(
                                HTTP_BAD_REQUEST,
                                errCode);
                        fut.fail(errMetier);
                        return;
                    }
                }
                fut.complete();

            } else {
                fut.fail(ar.cause());
            }
        });
        return fut;
    }


	protected void traceEvent(String typeElement, String event, String header, JsonObject contenu, String origine,
			String idElement, String idEntite) {
		JsonObject jsonTrace = new JsonObject();

		jsonTrace.put("header", header);

		jsonTrace.put("typeElement", typeElement);
		jsonTrace.put("event", event);
		jsonTrace.put("origine", origine);

		jsonTrace.put("contenu", contenu);

		jsonTrace.put("idElement", idElement);
		jsonTrace.put("idEntite", idEntite);

		vertx.eventBus().send(TraceVerticle.CREER_TRACE, jsonTrace);
	}
}
