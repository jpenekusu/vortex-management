//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.pojo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Permission implements Serializable {

    private static final long serialVersionUID = -358049968867310022L;

    public static final String KEY_JSON_CODE = "code";
    public static final String KEY_JSON_LIBELLE = "libelle";
    public static final String KEY_JSON_SCOPE = "scope";
    public static final String KEY_JSON_RESSOURCE = "ressource";
    public static final String KEY_JSON_CONTEXTE = "contexte";
    public static final String KEY_JSON_ACTIONS = "actions";

    private String code;
    private String libelle;
    private String scope;
    private String ressource;
    private String contexte;
    private String actions;

    public Permission() {
        super();
    }

    public Permission(String code, String libelle, String scope, String ressource, String contexte, String actions) {
        super();
        this.code = code;
        this.libelle = libelle;
        this.scope = scope;
        this.ressource = ressource;
        this.contexte = contexte;
        this.actions = actions;
    }

    @Override
    public String toString() {
        return "Permission [code=" + code + ", libelle=" + libelle + ", scope=" + scope + ", ressource=" + ressource
                + ", contexte=" + contexte + ", actions=" + actions + "]";
    }

    public JsonObject toJson() {
        return new JsonObject(Json.encode(this));
    }

    public static Permission fromJson(JsonObject json) {
        return Json.mapper.convertValue(json.getMap(), Permission.class);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {

        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getRessource() {
        return ressource;
    }

    public void setRessource(String ressource) {
        this.ressource = ressource;
    }

    public String getContexte() {
        return contexte;
    }

    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String action) {
        this.actions = action;
    }

}
