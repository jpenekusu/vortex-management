package fr.edu.vortex.management.utilisateur.handler;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

import java.util.Optional;

import fr.edu.vortex.management.persistence.dao.RapportDAO;
import fr.edu.vortex.management.persistence.dao.impl.RapportDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Rapport;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

public class ChercherRapportHandler implements Handler<Message<JsonObject>> {

    Vertx vertx;

    RapportDAO rapportDAO;

    public static ChercherRapportHandler create(Vertx vertx) {
        return new ChercherRapportHandler(vertx);
    }

    private ChercherRapportHandler(Vertx vertx) {
        this.vertx = vertx;
        rapportDAO = new RapportDAOImpl();
        rapportDAO.init(vertx);
    }

    @Override
    public void handle(Message<JsonObject> event) {

        if (!(event.body() instanceof JsonObject)) {
            event.fail(HTTP_BAD_REQUEST, "mal formaté");
            return;
        }

        JsonObject body = event.body();

        if (!body.containsKey("rapportId")) {
            event.fail(HTTP_BAD_REQUEST, "mal formaté");
            return;
        }

        Integer rapportId = null;
        try {
        	rapportId = body.getInteger("rapportId");
        } catch (Exception e) {
            event.fail(400, "mal formaté");
        }

        rapportDAO.chercherRapportByID(rapportId).setHandler(ar -> {

            if (!ar.succeeded()) {
                event.fail(HTTP_INTERNAL_ERROR, ar.cause().getMessage());
            } else {
                Optional<Rapport> result = ar.result();
                if (result.isPresent()) {
                    event.reply(result.get().toJsonObject());
                } else {
                    event.fail(HTTP_NOT_FOUND, "La ressource n'existe pas.");
                }
            }
        });

    }
}
