//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.verticle;

import fr.edu.vortex.management.auth.AuthManager;
import fr.edu.vortex.management.auth.VortexAuthProvider;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

public class TokenVerticle extends AbstractVerticle {

    public static final String TOKEN_UTILISATEUR = "vortex.management.utilisateur.token";
    
    VortexAuthProvider provider;    
    
    public TokenVerticle(Vertx vertxContext, AuthManager authManager) {
		super();
		this.provider = new VortexAuthProvider(vertxContext, authManager.getValidityPeriod());
	}
    
	@Override
    public void start() throws Exception {
        vertx.eventBus().consumer(TOKEN_UTILISATEUR, this::getUserFromToken);
    }

	
	private void getUserFromToken(Message<Object> event) {

		if (!(event.body() instanceof JsonObject)) {
            event.fail(400, "mal formaté");
			return;
		}

		JsonObject jwt = (JsonObject) event.body();

		this.provider.authenticate(jwt, res -> {
            if (res.failed()) {
                event.fail(400, res.cause().getMessage());
                return;
            }
            event.reply(res.result().principal());

        });

	}
	
}
