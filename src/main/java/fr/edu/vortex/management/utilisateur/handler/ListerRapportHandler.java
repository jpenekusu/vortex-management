//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.util.List;
import java.util.Map;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.api.Pagination;
import fr.edu.vortex.management.api.PaginationException;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.persistence.dao.RapportDAO;
import fr.edu.vortex.management.persistence.dao.impl.RapportDAOImpl;
import fr.edu.vortex.management.utilisateur.pojo.Rapport;
import fr.edu.vortex.management.utils.PaginationUtils;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ListerRapportHandler implements Handler<Message<Object>> {

    private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_TECH";

    private static final String ERR_LISTER_RAPPORTS = "une erreur est survenue lors de la récupération des rapports";

    private static final Logger logger = LoggerFactory.getLogger(ListerRapportHandler.class);

    Vertx vertx;

    RapportDAO rapportDao;

    public static ListerRapportHandler create(Vertx vertx) {
        return new ListerRapportHandler(vertx);
    }

    private ListerRapportHandler(Vertx vertx) {
        this.vertx = vertx;
        rapportDao = new RapportDAOImpl();
        rapportDao.init(vertx);
    }

    @Override
    public void handle(Message<Object> event) {
	
	String action = "loadRapport(Message<Object> message)";
	
	JsonObject criteres = (JsonObject) event.body();
	String page = criteres.getString(FluxVerticle.KEY_JSON_PAGE);
	String pageLimit = criteres.getString(FluxVerticle.KEY_JSON_PAGE_LIMIT);
	String sort = criteres.getString(FluxVerticle.KEY_JSON_SORT);
	String sortOrder = criteres.getString(FluxVerticle.KEY_JSON_SORT_ORDER);
	
	String dateRapport = criteres.getString(Rapport.KEY_JSON_DATE_RAPPORT);
	Map<String, Object> filtres = PaginationUtils.genererFiltresPourPagination(event,Rapport.KEY_JSON_DATE_RAPPORT,dateRapport);

	filtres.put(Rapport.KEY_JSON_LOGIN, criteres.getString(Rapport.KEY_JSON_LOGIN));
	filtres.put(Rapport.KEY_JSON_NOM, criteres.getString(Rapport.KEY_JSON_NOM));
	filtres.put(Rapport.KEY_JSON_PRENOM, criteres.getString(Rapport.KEY_JSON_PRENOM));
	filtres.put(Rapport.KEY_JSON_TYPE_RAPPORT, criteres.getString(Rapport.KEY_JSON_TYPE_RAPPORT));
	
	rapportDao.countRapport(filtres).setHandler(arCount ->{
	    if (arCount.succeeded()) {
		Integer borneMoins = null;
		Integer bornePlus = null;
		Pagination<JsonObject> paginateur = null;
		Integer nbRapports = arCount.result();
		if (page != null && pageLimit != null) {
			try {
				paginateur = new Pagination<>(page, pageLimit);
				borneMoins = paginateur.getBorneMoins() - 1;
				bornePlus = paginateur.getBornePlus() - 1;
			} catch (PaginationException e) {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
				event.fail(400,
						"Les paramêtres " + FluxVerticle.KEY_JSON_PAGE + " et " + FluxVerticle.KEY_JSON_PAGE_LIMIT + " sont erronés");
			}

		} else {
			paginateur = new Pagination<>(0, nbRapports);
			borneMoins = paginateur.getBorneMoins();
			bornePlus = paginateur.getBornePlus();
		} 
		
		 rapportDao.listerRapport(borneMoins, bornePlus, filtres, sort, sortOrder)
	                .setHandler(ar -> {
	                    if (ar.succeeded()) {
	                        List<Rapport> result = ar.result();

	                        JsonArray response = new JsonArray();

	                        result.stream()
	                                .map(Rapport::toJsonObject)
	                                .forEach(response::add);
	                        
	                        if (result.size() != nbRapports && result.size() != 0) {
					DeliveryOptions opts = new DeliveryOptions();
					opts.addHeader("count", nbRapports.toString());
					event.reply(response, opts);
				} else {
					event.reply(response);
				}

	                    } else {
	                        logger.error(LOG_ERROR,
	                                LOG_NO_CORRELATION_ID,
	                                ERR_LISTER_RAPPORTS,
	                                ERR_MANAGEMENT_TECH,
	                                ar.cause());
	                        event.fail(HTTP_INTERNAL_ERROR, ERR_LISTER_RAPPORTS);
	                    }
	                });
		 
	    }else {
//		logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", countTraceAr.cause());
//		event.fail(500, ERREUR_COUNT_TRACE);
	}

	});
       
       
    }

}
