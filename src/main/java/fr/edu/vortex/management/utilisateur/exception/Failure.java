//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.exception;

import java.util.Optional;

import io.vertx.core.eventbus.ReplyException;

/**
 * Classe utilitaire permettant à partir d'une erreur d'en tirer un code est un
 * message
 * 
 */
public class Failure {

    private int code;
    private String message;

    public static Optional<Failure> create(Throwable cause) {
        if (cause instanceof ManagementException) {
            ManagementException err = (ManagementException) cause;
            return Optional.ofNullable(new Failure(err.getStatusCode(), err.getMessage()));
        } else if (cause instanceof ReplyException) {
            ReplyException err = (ReplyException) cause;
            return Optional.ofNullable(new Failure(err.failureCode(), err.getMessage()));
        } else {
            return Optional.empty();
        }
    }

    private Failure(int code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
