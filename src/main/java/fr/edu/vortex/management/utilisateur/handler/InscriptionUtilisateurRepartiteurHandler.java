//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;

import java.util.Optional;

import com.hazelcast.util.StringUtil;

import fr.edu.vortex.management.parametre.Parametre;
import fr.edu.vortex.management.parametre.ParametreAdresses;
import fr.edu.vortex.management.utilisateur.exception.Failure;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureEvtTrace;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureTypeTrace;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.RoleVerticle;
import fr.edu.vortex.management.utilisateur.verticle.TraceVerticle;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * Classe recevant une demande de création d'un utilisateur qui va faire les
 * premières vérifications et orienter la demande vers le bonne acteur
 * 
 */
public class InscriptionUtilisateurRepartiteurHandler extends UtilisateurHandler implements Handler<Message<Object>> {

	private static final String ERREUR_CREATION_UTILISATEUR = "erreur lors de l'inscription d'un utilisateur";

	private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_INSCRIPTION_UTILISATEUR_TECH";

	private static final Integer ID_ROLE_SUPERVISEUR = 2;

	private static final Logger logger = LoggerFactory.getLogger(InscriptionUtilisateurRepartiteurHandler.class);

	Vertx vertx;

	public static InscriptionUtilisateurRepartiteurHandler create(Vertx vertx) {
		return new InscriptionUtilisateurRepartiteurHandler(vertx);
	}

	private InscriptionUtilisateurRepartiteurHandler(Vertx vertx) {
		this.vertx = vertx;

	}

	@Override
	public void handle(Message<Object> event) {

		if (!(event.body() instanceof JsonObject)) {

			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(NOT_JSON, event.body().toString()),
					ERR_MANAGEMENT_TECH);

			event.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		JsonObject body = (JsonObject) event.body();

		Utilisateur utilisateur;
		try {
			utilisateur = Utilisateur.fromJson(body);
		} catch (IllegalArgumentException e) {
			event.fail(HTTP_BAD_REQUEST, MAL_FORMATE);
			return;
		}

		this.verifAutoEnregistrementActif()
			.compose(Void -> this.affecterRoleSuperviseur(utilisateur))
			.compose(utilisateurWithRole -> this.inscrireUtilisateur(event, utilisateurWithRole))
			.setHandler(ar -> {
				if (ar.succeeded()) {
					event.reply(ar.result());
				} else {
					event.fail(500, ar.cause().getMessage());
				}
			});

	}

	// On vérifie que l'auto enregistrement est actif
	private Future<Object> verifAutoEnregistrementActif() {
		Future<Object> futVerif = Future.future();
		JsonObject json = new JsonObject();
		json.put(Parametre.KEY_JSON_CODE, Parametre.CODE_AUTO_ENREGISTREMENT);
		vertx.eventBus().<JsonObject>send(ParametreAdresses.GET_PARAMETRE_BY_CODE, json, ar -> {
			if (ar.succeeded()) {
				Parametre parametre = new Parametre(ar.result().body());
				if (parametre.getValeur().equals(Parametre.VALEUR_AUTO_ENREGISTREMENT_ACTIF)) {
					futVerif.complete();
				} else {
					futVerif.fail("L'auto enregistrement est désactivé");
				}
			} else {
				futVerif.fail("Impossible de vérifier le paramêtre d'auto enregistrement");
			}
		});
		return futVerif;
	}

	// On récupére le rôle superviseur et on l'affecte à l'utilisateur
	private Future<Utilisateur> affecterRoleSuperviseur(Utilisateur utilisateur) {
		Future<Utilisateur> futAffecterRole = Future.future();

		JsonObject input = new JsonObject().put("id", ID_ROLE_SUPERVISEUR);
		vertx.eventBus().<JsonObject>send(RoleVerticle.CHERCHER_ROLE_PAR_ID, input, arGetRole -> {
			if (arGetRole.succeeded()) {
				Role role = Role.fromJson(arGetRole.result().body());
				utilisateur.setRole(role);
				futAffecterRole.complete(utilisateur);

			} else {
				futAffecterRole.fail("Impossible de récupérer le rôle superviseur");
			}
		});
		return futAffecterRole;
	}

	// On enregistre l'utilisateur en base de données
	private Future<JsonObject> inscrireUtilisateur(Message<Object> event, Utilisateur utilisateur) {
		Future<JsonObject> futInscrire = Future.future();

		String adresse;

		if (StringUtil.isNullOrEmptyAfterTrim(utilisateur.getTypeAuthentification())) {
			utilisateur.setTypeAuthentification(Utilisateur.AUTHENTIFICATION_LOCALE);
		}

		if (Utilisateur.AUTHENTIFICATION_LOCALE.equals(utilisateur.getTypeAuthentification())) {
			adresse = UtilisateurVerticle.CREER_UTILISATEUR_LOCALE;
		} else if (Utilisateur.AUTHENTIFICATION_LDAP.equals(utilisateur.getTypeAuthentification())) {
			adresse = UtilisateurVerticle.CREER_UTILISATEUR_LDAP;
		} else {
			event.fail(400, ERR_TYPE_AUTHENTIFICATION);
			
			futInscrire.fail(ERR_TYPE_AUTHENTIFICATION);
			return futInscrire;			
		}

		vertx.eventBus().<JsonObject>send(adresse, utilisateur.toJson(), ArInscrire -> {
			if (ArInscrire.succeeded()) {
				JsonObject utilisateurJson = ArInscrire.result().body();
				traceEvent(Utilisateur.fromJson(utilisateurJson));
				futInscrire.complete(utilisateurJson);
			} else {
				Optional<Failure> failure = Failure.create(ArInscrire.cause());
				if (failure.isPresent()) {
					event.fail(failure.get().getCode(), failure.get().getMessage());
					futInscrire.fail(failure.get().getMessage());
				} else {
					futInscrire.fail(ERREUR_CREATION_UTILISATEUR);
				}
			}
		});

		return futInscrire;
	}

	private void traceEvent(Utilisateur utilisateur) {
		JsonObject jsonTrace = new JsonObject();

		jsonTrace.put("typeElement", NomenclatureTypeTrace.Utilisateur.getValeur());
		jsonTrace.put("event", NomenclatureEvtTrace.Creation.getValeur());
		jsonTrace.put("origine", "CASSIS");
		jsonTrace.put("login", utilisateur.getLogin());
		jsonTrace.put("nom", utilisateur.getNom());
		jsonTrace.put("prenom", utilisateur.getPrenom());
		jsonTrace.put("idEntite", utilisateur.getLogin());
		jsonTrace.put("idElement", String.valueOf(utilisateur.getId()));
		jsonTrace.put("contenu", utilisateur.toJson());
		
		vertx.eventBus().send(TraceVerticle.CREER_TRACE_LOGIN_OU_INSCRIPTION, jsonTrace);

	}

}
