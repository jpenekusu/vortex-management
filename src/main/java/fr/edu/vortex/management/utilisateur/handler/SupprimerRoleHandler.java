//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.util.Optional;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.dao.RoleDAO;
import fr.edu.vortex.management.persistence.dao.RolePermissionDAO;
import fr.edu.vortex.management.persistence.dao.impl.RoleDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.RolePermissionDAOImpl;
import fr.edu.vortex.management.utilisateur.exception.Failure;
import fr.edu.vortex.management.utilisateur.exception.ManagementException;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureEvtTrace;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureTypeTrace;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import fr.edu.vortex.management.utilisateur.verticle.RoleVerticle;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * Acteur permettant de supprimer un role dans le système de persistence.
 * 
 */
public class SupprimerRoleHandler extends PermissionHandler implements Handler<Message<JsonObject>> {

    private static final String ROLE_LIE_UTILISATEUR = "Impossible de supprimer le rôle car il est lié à des utilisateurs.";

    private static final String IMPOSSIBLE_DE_SUPPRIMER_UN_ROLE_SYSTEME = "Impossible de supprimer un rôle systeme.";

    private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_TECH";

    private static final String ERR_INPUT_PAS_JSON = "le paramètre entrant de l'acteur supprimant un rôle n'est pas un json";

    private static final String ERR_PAS_CLE_ID = "la cle id est manquante dans le paramètre entrant pour la suppression d'un rôle";

    private static final String MSG_ROLE_UTILISE = "Le rôle est utilisé pour des utilisateurs";

    private static final String ERR_SUPP_ROLE = "une erreur est survenue lors de la suppression des rôles";

    private static final String ERR_ID_NOT_INTEGER = "identifiant passé n'est pas un entier";

    private static final String ERR_MAL_FORMATE = "mal formaté";

    private static final String LOG_START = "Acteur permettant de supprimer un rôle créé";

    private static final Logger logger = LoggerFactory.getLogger(SupprimerRoleHandler.class);

    RoleDAO roleDao;
    RolePermissionDAO rolePermDao;

    public static SupprimerRoleHandler create(Vertx vertx) {
        return new SupprimerRoleHandler(vertx);
    }

    private SupprimerRoleHandler(Vertx vertx) {
        this.vertx = vertx;

        roleDao = new RoleDAOImpl();
        roleDao.init(vertx);

        rolePermDao = new RolePermissionDAOImpl();
        rolePermDao.init(vertx);

        logger.info(LOG_MESSAGE, LOG_NO_CORRELATION_ID, LOG_START);
    }

    @Override
    public void handle(Message<JsonObject> event) {

        if (!(event.body() instanceof JsonObject)) {
            logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, ERR_INPUT_PAS_JSON, ERR_MANAGEMENT_TECH);
            event.fail(HTTP_BAD_REQUEST, ERR_MAL_FORMATE);
            return;
        }

        JsonObject body = event.body();

        if (!body.containsKey("id")) {
            logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, ERR_PAS_CLE_ID, ERR_MANAGEMENT_TECH);
            event.fail(HTTP_BAD_REQUEST, ERR_MAL_FORMATE);
            return;
        }

        int id;
        try {
            id = body.getInteger("id");
        } catch (Exception e) {
            event.fail(HTTP_BAD_REQUEST, ERR_ID_NOT_INTEGER);
            return;
        }

        // on va chercher le role
        recupereRole(id).setHandler(arRecup -> {
            if (arRecup.succeeded()) {
    			String idEntite = arRecup.result().getLibelle();
            	// vérifie si on peut le supprimer
                verifierLiaisonUtilisateur(id)
                // supprime toutes les liaisons role - permission lié à ce role
                .compose(empty -> rolePermDao.delete(id))
                // supprime le role
                .compose(nbLiensDeleted -> roleDao.delete(id))
                .setHandler(ar -> {
                    if (ar.succeeded()) {                   	
	        			String header = body.getString("header");
	        			body.remove("header");
	        			String origine = body.getString("origine");
	        			body.remove("origine");
	        			JsonObject contenu = body;
	        			String idElement = String.valueOf(id);
	        			
	        			traceEvent(NomenclatureTypeTrace.Role.getValeur(), NomenclatureEvtTrace.Suppression.getValeur(), header, contenu, origine, idElement, idEntite);

                        event.reply(null);
                    } else {
                        Optional<Failure> failure = Failure.create(ar.cause());
                        if (failure.isPresent()) {
                            event.fail(failure.get().getCode(), failure.get().getMessage());
                        } else {
                            event.fail(HTTP_INTERNAL_ERROR, ERR_SUPP_ROLE);
                        }
                    }
                });
            } else {
                Optional<Failure> failure = Failure.create(arRecup.cause());
                if (failure.isPresent()) {
                    event.fail(failure.get().getCode(), failure.get().getMessage());
                } else {
                    event.fail(HTTP_INTERNAL_ERROR, ERR_SUPP_ROLE);
                }
            	
            }
       	
        });

    }

    /**
     * Cherche le role et vérifie qu'il n'ai pas système.
     * 
     * @param roleId
     *            identifiant du role cible
     * @return Future contenant le role récupéré, échoue si le role est de type
     *         système
     */
    private Future<Role> recupereRole(int roleId) {
        Future<Role> fut = Future.future();
        JsonObject input = new JsonObject().put("id", roleId);
        vertx.eventBus().<JsonObject>send(
                RoleVerticle.CHERCHER_ROLE_PAR_ID,
                input,
                ar -> {
                    if (ar.succeeded()) {
                        Role role = Role.fromJson(ar.result().body());
                        if (role.isSysteme()) {
                            fut.fail(new ManagementException(HTTP_FORBIDDEN, IMPOSSIBLE_DE_SUPPRIMER_UN_ROLE_SYSTEME));
                        } else {
                            fut.complete(role);
                        }
                    } else {
                        fut.fail(ar.cause());
                    }
                });
        return fut;
    }

    /**
     * Cherche les utilisateurs qui ont le role roleId si il existe des utilisateur
     * avec ce role alors le Future ce termine en erreur ManagementException 409.
     * 
     * @param roleId
     *            l'identifiant du role cible
     * @return Future permettant de savoir si le role est utilise par des
     *         utilisateur
     */
    private Future<Void> verifierLiaisonUtilisateur(int roleId) {
        Future<Void> fut = Future.future();
        JsonObject criteresSelection = new JsonObject().put(UtilisateurVerticle.KEY_JSON_ROLE_ID, roleId);
        vertx.eventBus()
                .send(UtilisateurVerticle.LISTER_UTILISATEUR, criteresSelection,
                        ar -> {
                            if (ar.succeeded()) {
                                JsonArray utilisateursAvecLeRole = (JsonArray) ar.result().body();
                                if (utilisateursAvecLeRole.isEmpty()) {
                                    fut.complete();
                                } else {
                                    logger.warn(LOG_MESSAGE, LOG_NO_CORRELATION_ID, ROLE_LIE_UTILISATEUR);
                                    fut.fail(new ManagementException(HTTP_CONFLICT, MSG_ROLE_UTILISE));
                                }
                            } else {
                                fut.fail(ar.cause());
                            }
                        });
        return fut;
    }

}
