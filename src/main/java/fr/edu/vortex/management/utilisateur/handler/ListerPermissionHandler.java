//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_MESSAGE_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.util.List;

import fr.edu.vortex.management.persistence.dao.PermissionDAO;
import fr.edu.vortex.management.persistence.dao.impl.PermissionDAOImp;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ListerPermissionHandler implements Handler<Message<Object>> {

    private static final String MESSAGE_LIST_PERMS = "liste de permissions récupérée";

    private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_TECH";

    private static final String DEMANDE_ENTRANTE = "Demande de la liste des permissions entrante";

    private static final String LOG_START = "Acteur permettant de lister les permissions créées.";

    private static final String ERR_LISTER_PERMISSIONS = "une erreur est survenue lors de la récupération des permissions";

    private static final Logger logger = LoggerFactory.getLogger(ListerPermissionHandler.class);

    Vertx vertx;

    PermissionDAO permissionDao;

    public static ListerPermissionHandler create(Vertx vertx) {
        return new ListerPermissionHandler(vertx);
    }

    private ListerPermissionHandler(Vertx vertx) {
        this.vertx = vertx;
        permissionDao = new PermissionDAOImp();
        permissionDao.init(vertx);
        logger.info(LOG_MESSAGE, LOG_NO_CORRELATION_ID, LOG_START);
    }

    @Override
    public void handle(Message<Object> event) {
        logger.trace(LOG_MESSAGE, LOG_NO_CORRELATION_ID, DEMANDE_ENTRANTE);
        permissionDao.lister()
                .setHandler(ar -> {
                    if (ar.succeeded()) {
                        List<Permission> result = ar.result();

                        logger.trace(
                                LOG_MESSAGE_WITH_DATA,
                                LOG_NO_CORRELATION_ID,
                                MESSAGE_LIST_PERMS,
                                result);

                        JsonArray response = new JsonArray();

                        result.stream()
                                .map(Permission::toJson)
                                .forEach(response::add);

                        event.reply(response);
                    } else {
                        logger.error(LOG_ERROR,
                                LOG_NO_CORRELATION_ID,
                                ERR_LISTER_PERMISSIONS,
                                ERR_MANAGEMENT_TECH,
                                ar.cause());
                        event.fail(HTTP_INTERNAL_ERROR, ERR_LISTER_PERMISSIONS);
                    }
                });

    }

}
