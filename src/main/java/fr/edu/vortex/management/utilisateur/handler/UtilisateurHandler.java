//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static fr.edu.vortex.management.Constants.LOG_ERROR;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CONFLICT;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import fr.edu.vortex.management.auth.HashUtils;
import fr.edu.vortex.management.auth.HashUtils.HashInfo;
import fr.edu.vortex.management.ldap.LdapVerticle;
import fr.edu.vortex.management.utilisateur.exception.ManagementException;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.RoleVerticle;
import fr.edu.vortex.management.utilisateur.verticle.TraceVerticle;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class UtilisateurHandler {

    private static final Logger logger = LoggerFactory.getLogger(UtilisateurHandler.class);

    protected static final String KEY_PARAM_MDP = "mot_de_passe";

    // messages d'erreur
    protected static final String MESSAGE_CONFLICT = "Le %s existe déjà";

    protected static final String LOGIN_MANQUANT = "login manquant";

    protected static final String EMAIL_MANQUANT = "email manquant";

    protected static final String PRENOM_MANQUANT = "prénom manquant";

    protected static final String NOM_MANQUANT = "nom manquant";

    protected static final String MAL_FORMATE = "mal formaté";

    protected static final String NOT_JSON = "le paramètre entrant n'est pas un json (input = %s)";

    protected static final String ERR_AUTH = "acteur ne pouvant pas traiter la demande";

    protected static final String MOT_DE_PASSE_MANQUANT = "mot de passe manquant";

    protected static final String ID_MANQUANT = "identifiant manquant";

    protected static final String ERR_TYPE_AUTHENTIFICATION = "le type d'authentification n'est pas valide";

    private static final String REGEXP_PASSWORD = "^(?=.*([A-Z]){1,})(?=.*[\\'\"&!@#\\\\+=.$&*\\-_()\\[\\]{}\\\\\\\\/|<>,;:%\\\\?]{1,})(?=.*[0-9]{1,})(?=.*[a-z]{1,})(?!.*[\\u00C0-\\u017F ]{1,}).{12,250}";

    private static final String ERREUR_PASSWORD_SECURITE = "Le mot de passe saisi n'est pas assez sécurisé ou contient des caractères interdits";

    private static final String REGEXP_NOM_PRENOM = "(?! )[a-zA-Z0-9\u00C0-\u017F-' ]{1,250}";
    private static final String REGEXP_LOGIN = "^[a-zA-Z0-9-_.]{1,250}$";
    private static final String REGEXP_EMAIL = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";

    public static final String ERR_CHAMPS_UTILISATEUR = "Le champ %s de l'utilisateur n'est pas renseigné";
    public static final String ERR_CHAMPS_UTILISATEUR_INVALIDE = "Le champ %s de l'utilisateur est invalide";

    /**
     * Retire les données sensible pour le retour de l'appel
     * 
     * @param utilisateur
     * @return
     */
    public Future<JsonObject> masquerDonneesSensibles(Utilisateur utilisateur) {
	utilisateur.setHash(null);
	utilisateur.setSel(null);
	utilisateur.setMotDePasse(null);

	JsonObject json = utilisateur.toJson();
	json.remove("nombre_iteration");

	return Future.succeededFuture(json);
    }

    /**
     * Modifie l'utilisateur pour l'enrichir du mot de passe hashé
     * 
     * @param utilisateur
     * @return
     */
    public Future<Utilisateur> creerHashSiPresent(Utilisateur utilisateur) {
	Future<Utilisateur> fut = Future.future();
	try {
	    if (utilisateur.getMotDePasse() != null) {
		HashInfo hashInfo;
		hashInfo = HashUtils.generateStorngPasswordHash(utilisateur.getMotDePasse());
		utilisateur.setHash(hashInfo.getHashedPassword());
		utilisateur.setSel(hashInfo.getSaltUsed());
		utilisateur.setNombreIteration(hashInfo.getIterations());
	    }
	    fut.complete(utilisateur);
	} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
	    fut.fail(e);
	}
	return fut;
    }

    /**
     * Modifie l'objet pour l'enrichir du mot de passe hashé
     * 
     * @param utilisateur
     * @return
     */
    public Future<JsonObject> creerHash(JsonObject utilisateur) {
	Future<JsonObject> fut = Future.future();
	try {
	    HashInfo hashInfo;
	    hashInfo = HashUtils.generateStorngPasswordHash(
		    utilisateur.getString(ModifierMDPUtilisateurLocalHandler.KEY_PARAM_MDP));
	    utilisateur.put("hash", hashInfo.getHashedPassword());
	    utilisateur.put("sel", hashInfo.getSaltUsed());
	    utilisateur.put("nbIterations", hashInfo.getIterations());
	    fut.complete(utilisateur);
	} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
	    fut.fail(e);
	}
	return fut;
    }

    /**
     * Verifie l'existence du role de la système.
     * 
     * @param role
     *            role recherché
     * @return Future completé si existe , échoué si non présent
     */
    public Future<Void> verifierRoleExiste(Role role, Vertx vertx) {
	Future<Void> fut = Future.future();
	if (role == null) {
	    return Future.succeededFuture();
	}

	vertx.eventBus().<JsonObject>send(RoleVerticle.CHERCHER_ROLE_PAR_ID, role.toJson(), ar -> {
	    if (ar.succeeded()) {
		fut.complete();
	    } else {
		ReplyException cause = (ReplyException) ar.cause();
		if (cause.failureCode() == 404) {
		    String message = "le role (id = %s) n'existe pas ";
		    ManagementException err = new ManagementException(404, String.format(message, role.getId()));
		    fut.fail(err);
		} else {
		    fut.fail(ar.cause());
		}
	    }
	});
	return fut;
    }

    /**
     * Vérifie que le login n'est pas déjà utilisé dans le système.
     * 
     * @param login
     * @return
     */
    public Future<Void> verifierUniciteLogin(String login, Vertx vertx) {
	Future<Void> fut = Future.future();
	String elementVerifie = "login";
	verifierUnicite(elementVerifie, new JsonObject().put(elementVerifie, "=" + login), vertx)
		.setHandler(fut::handle);
	return fut;
    }

    private Future<Void> verifierUnicite(String elementVerifie, JsonObject data, Vertx vertx) {
	Future<Void> fut = Future.future();
	vertx.eventBus().send(UtilisateurVerticle.LISTER_UTILISATEUR, data, ar -> {
	    if (ar.succeeded()) {
		JsonArray body = (JsonArray) ar.result().body();
		if (body.size() > 0) {
		    ManagementException err = new ManagementException(
			    HTTP_CONFLICT, String.format(MESSAGE_CONFLICT, elementVerifie));
		    fut.fail(err);
		} else {
		    fut.complete();
		}
	    } else {
		fut.fail(ar.cause());
	    }
	});
	return fut;
    }

    /**
     * Vérifie que le login n'est pas déjà utilisé dans le système.
     * 
     * @param login
     * @return
     */
    public Future<Void> verifierUniciteLoginAvecIdExistant(String login, int id, Vertx vertx) {
	Future<Void> fut = Future.future();
	String elementVerifie = "login";
	verifierUniciteAvecIdExistant(elementVerifie, new JsonObject().put(elementVerifie, login), id, vertx)
		.setHandler(fut::handle);
	return fut;
    }

    private Future<Void> verifierUniciteAvecIdExistant(String elementVerifie, JsonObject data, int id, Vertx vertx) {
	Future<Void> fut = Future.future();
	vertx.eventBus().send(UtilisateurVerticle.LISTER_UTILISATEUR, data, ar -> {
	    if (ar.succeeded()) {
		JsonArray body = (JsonArray) ar.result().body();
		if (body.size() > 0 && body.getJsonObject(0).getInteger("id") != id) {
		    ManagementException err = new ManagementException(
			    HTTP_CONFLICT, String.format(MESSAGE_CONFLICT, elementVerifie));
		    fut.fail(err);
		} else {
		    fut.complete();
		}
	    } else {
		fut.fail(ar.cause());
	    }
	});
	return fut;
    }

    /**
     * Vérifie la présence du login dans le LDAP
     * 
     * @param login
     * @return
     */
    public Future<Void> verifierPresenceDansLDAP(String login, Vertx vertx) {
	Future<Void> fut = Future.future();
	vertx.eventBus().send(LdapVerticle.LDAP_VERIF_LOGIN, login, result -> {
	    if (result.succeeded()) {
		fut.complete();
	    } else {
		fut.fail(result.cause());
	    }
	});
	return fut;

    }

    /**
     * Vérifie sécurité mot de passe
     * 
     * @param motDePasse
     * @return
     */
    public Future<Void> verifierSecuritePassword(String motDePasse) {
	Future<Void> fut = Future.future();
	if (motDePasse.matches(REGEXP_PASSWORD)) {
	    fut.complete();
	} else {
	    ManagementException err = new ManagementException(
		    404, ERREUR_PASSWORD_SECURITE);
	    fut.fail(err);
	}
	return fut;
    }

    public void traceEvent(String typeElement, String event, String header, JsonObject contenu, String origine,
	    String idElement, String idEntite, Vertx vertx) {
	JsonObject jsonTrace = new JsonObject();

	jsonTrace.put("header", header);

	jsonTrace.put("typeElement", typeElement);
	jsonTrace.put("event", event);
	jsonTrace.put("origine", origine);

	jsonTrace.put("contenu", contenu);

	jsonTrace.put("idElement", idElement);
	jsonTrace.put("idEntite", idEntite);

	vertx.eventBus().send(TraceVerticle.CREER_TRACE, jsonTrace);
    }
    
    protected boolean checkParams(Message<Object> message, JsonObject json) {
    	boolean paramOK = true;

    	try {   		
    		String nom = json.getString(Utilisateur.KEY_JSON_NOM);
    		if ( nom== null) {
    			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR, Utilisateur.KEY_JSON_NOM));
    			message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR, Utilisateur.KEY_JSON_NOM));
    			paramOK = false;   			
    		} else if (!nom.matches(REGEXP_NOM_PRENOM)) {
    			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_NOM));
    			message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_NOM));
    			paramOK = false;   			
    		}
    	} catch (Exception e) {
    		logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_NOM));
    		message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_NOM));
    		paramOK = false;
    	}

    	try {   		
    		String prenom = json.getString(Utilisateur.KEY_JSON_PRENOM);
    		if ( prenom== null) {
    			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR, Utilisateur.KEY_JSON_PRENOM));
    			message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR, Utilisateur.KEY_JSON_PRENOM));
    			paramOK = false;   			
    		} else if (!prenom.matches(REGEXP_NOM_PRENOM)) {
    			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_PRENOM));
    			message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_PRENOM));
    			paramOK = false;   			
    		}
    	} catch (Exception e) {
    		logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_PRENOM));
    		message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_PRENOM));
    		paramOK = false;
    	}

    	try {   		
    		String login = json.getString(Utilisateur.KEY_JSON_LOGIN);
    		if ( login== null) {
    			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR, Utilisateur.KEY_JSON_LOGIN));
    			message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR, Utilisateur.KEY_JSON_LOGIN));
    			paramOK = false;   			
    		} else if (!login.matches(REGEXP_LOGIN)) {
    			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_LOGIN));
    			message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_LOGIN));
    			paramOK = false;   			
    		}
    	} catch (Exception e) {
    		logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_LOGIN));
    		message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_LOGIN));
    		paramOK = false;
    	}

    	try {   		
    		String typeAuth = json.getString(Utilisateur.KEY_JSON_TYPE);
    		if ( typeAuth== null) {
    			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR, Utilisateur.KEY_JSON_TYPE));
    			message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR, Utilisateur.KEY_JSON_TYPE));
    			paramOK = false;   			
    		} else if (typeAuth.length()>50) {
    			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_TYPE));
    			message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_TYPE));
    			paramOK = false;   			
    		}
    	} catch (Exception e) {
    		logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_TYPE));
    		message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_TYPE));
    		paramOK = false;
    	}

    	try {   		
    		String email = json.getString(Utilisateur.KEY_JSON_EMAIL);
    		if (email!= null && !email.isEmpty() && (email.length()>250 || !email.matches(REGEXP_EMAIL))) {
    			logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_EMAIL));
    			message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_EMAIL));
    			paramOK = false;   			
    		}
    	} catch (Exception e) {
    		logger.error(LOG_ERROR, LOG_NO_CORRELATION_ID, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_EMAIL));
    		message.fail(HTTP_BAD_REQUEST, String.format(ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_EMAIL));
    		paramOK = false;
    	}

    	return paramOK;
    }
}
