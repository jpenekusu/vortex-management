//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.pojo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Role implements Serializable {

    public static final String KEY_JSON_ID = "id";
    public static final String KEY_JSON_LIBELLE = "libelle";
    public static final String KEY_JSON_SYSTEME = "systeme";
    public static final String KEY_JSON_PERMISSIONS = "permissions";

    /**
     * 
     */
    private static final long serialVersionUID = -512030321877150146L;

    private int id;
    private String libelle;
    private boolean systeme;

    @JsonInclude(Include.NON_EMPTY)
    private List<Permission> permissions;

    public Role() {
        super();
    }

    public Role(int id, String libelle, boolean systeme, List<Permission> permissions) {
        super();
        this.id = id;
        this.libelle = libelle;
        this.systeme = systeme;
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "Role [id=" + id + ", libelle=" + libelle + ", systeme=" + systeme + ", permissions=" + permissions
                + "]";
    }

    public static Role fromJson(JsonObject json) {
        return Json.mapper.convertValue(json.getMap(), Role.class);
    }

    public JsonObject toJson() {
        return new JsonObject(Json.encode(this));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public boolean isSysteme() {
        return systeme;
    }

    public void setSysteme(boolean systeme) {
        this.systeme = systeme;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

}
