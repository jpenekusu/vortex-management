//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.health;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

import fr.edu.echanges.nat.vortex.common.adresses.management.health.ManagementHealthAdresses;
import fr.edu.vortex.management.Constants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.healthchecks.HealthChecks;
import io.vertx.ext.healthchecks.Status;

public class HealthMangementVerticle extends AbstractVerticle {

    private static final String PROCEDURE_MAIN = "main";
    private static final String KEY_HOST = "host";
    private static final String KEY_NAME = "name";
    private static final String NAME = "vortex-management";

    private static final Logger logger = LoggerFactory.getLogger(HealthMangementVerticle.class);

    @Override
    public void start() throws Exception {

        String hostName = InetAddress.getLocalHost().getHostName();

        HealthChecks healthChecks = HealthChecks.create(vertx);

        long timeout = TimeUnit.SECONDS.toMillis(5);
        healthChecks.register(PROCEDURE_MAIN, timeout, future -> {
            JsonObject name = new JsonObject();
            name.put(KEY_NAME, NAME);
            name.put(KEY_HOST, hostName);
            future.complete(Status.OK(name));
        });

        vertx.eventBus().consumer(ManagementHealthAdresses.GET_HEALTH, message -> healthChecks.invoke(message::reply));

        // String action = " deploiement " + this.getClass().getName() + " OK";

        logger.info(Constants.LOG_DEPLOY_SUCCEEDED, Constants.LOG_NO_CORRELATION_ID, this.getClass().getName());
    }

}
