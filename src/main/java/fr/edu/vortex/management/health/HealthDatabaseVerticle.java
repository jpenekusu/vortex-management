//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.health;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

import fr.edu.echanges.nat.vortex.common.adresses.management.health.ManagementHealthAdresses;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.healthchecks.HealthChecks;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.jdbc.JDBCClient;

public class HealthDatabaseVerticle extends AbstractVerticle {

    private static final int TIMEOUT_SEC = 5;
    private static final String KEY_HOST = "host";
    private static final String KEY_NAME = "name";
    private static final String NAME = "vortex-management-db";
    private JDBCClient pgClient;

    private static final Logger logger = LoggerFactory.getLogger(HealthDatabaseVerticle.class);

    @Override
    public void start() throws Exception {

        String hostName = InetAddress.getLocalHost().getHostName();

        HealthChecks healthChecks = HealthChecks.create(vertx);

        long timeout = TimeUnit.SECONDS.toMillis(TIMEOUT_SEC);

        JsonObject config = config().getJsonObject("vortex-management").getJsonObject("database");

        pgClient = JDBCClient.createShared(vertx, config);

        // TODO à remplacer par La connexion utilisée dans DatabaseVerticle
        healthChecks.register("main", timeout, future -> pgClient.getConnection(connection -> {
            JsonObject name = new JsonObject();
            name.put(KEY_NAME, NAME);
            name.put(KEY_HOST, hostName);

            if (connection.failed()) {
                future.fail(connection.cause());
            } else {
                connection.result().close();
                future.complete(Status.OK(name));
            }
        }));

        vertx.eventBus().consumer(ManagementHealthAdresses.GET_DATABASE_HEALTH,
                message -> healthChecks.invoke(message::reply));
    }

}
