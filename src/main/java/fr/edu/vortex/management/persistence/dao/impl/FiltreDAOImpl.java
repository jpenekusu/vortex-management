package fr.edu.vortex.management.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.edu.echanges.nat.vortex.common.adresses.persistence.PersistenceAdressesManagement;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.FiltreDAO;
import fr.edu.vortex.management.utilisateur.pojo.Filtre;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class FiltreDAOImpl extends GenericDAOImpl implements FiltreDAO{

	private static final String INSERT = "INSERT INTO filtre "
			+ "(nom, description, utilisateur_id, type_filtre, contenu) "
			+ "VALUES (?, ?, ?, ?, ?::json) returning id";

	private static final String DELETE_BY_ID = "DELETE FROM filtre WHERE id = ? returning *";

	private static final String DELETE_BY_ID_UTILISATEUR = "DELETE FROM filtre WHERE utilisateur_id = ? returning *";

	private static final String SELECT_FILTRES = "" +
			"SELECT " +
			"    f." + Filtre.KEY_JSON_ID + ","+
			"    f." + Filtre.KEY_JSON_UTILISATEUR_ID + ","+
			"    f." + Filtre.KEY_JSON_NOM + ","+
			"    f." + Filtre.KEY_JSON_DESCRIPTION + ","+
			"    f." + Filtre.KEY_JSON_TYPE_FILTRE + ","+
			"    f." + Filtre.KEY_JSON_CONTENU +
			" FROM " +
			"    management.filtre f WHERE 1=1 ";

	private static final String MAIN_TABLE = "filtre";

	private static final Logger logger = LoggerFactory.getLogger(FiltreDAOImpl.class);

	@Override
	public Future<Filtre> creer(Filtre filtre) {
		String action = "creer(Filtre)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		Future<Filtre> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, INSERT);

		JsonArray params = new JsonArray();
		params.add(filtre.getNom().trim());
		if (filtre.getDescription() != null) {
			params.add(filtre.getDescription());
		} else {
			params.add("");			
		}
		params.add(filtre.getUtilisateur_id());
		params.add(filtre.getType_filtre());
		if (filtre.getContenu() != null) {
			params.add(filtre.getContenu().toString());
		} else {
			params.add("''");			
		}
		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {

				if (!ar.result().containsKey(MAIN_TABLE)) {
					future.fail("résultat mal formaté");
					return;
				}

				JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
				if (rows.size() == 0) {
					future.fail("erreur lors de la création du filtre");
					return;
				}

				filtre.setId(rows.getJsonObject(0).getInteger("id"));

				future.complete(filtre);

			} else {
				future.fail(ar.cause());
			}
		});

		return future;
	}

	@Override
	public Future<Filtre> modifier(Filtre filtre) {
		Future<Filtre> future = Future.future();

		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append("UPDATE " + MAIN_TABLE + " SET ");
		sqlFilters.append(Filtre.KEY_JSON_NOM + " = '").append(getStringSQLCorrect(filtre.getNom().trim())).append("', ");
		sqlFilters.append(Filtre.KEY_JSON_DESCRIPTION + " = '").append(getStringSQLCorrect(filtre.getDescription())).append("'");
		sqlFilters.append(" WHERE id = ").append(Integer.toString(filtre.getId()));

		JsonObject jsonRequete = new JsonObject();
		jsonRequete.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());

		envoiRequeteAuVerticleBD(PersistenceAdressesManagement.UPDATE, jsonRequete).setHandler(ar -> {
			if (ar.succeeded()) {
				JsonArray keys = ((JsonObject) ar.result()).getJsonArray("keys");

				Filtre updatedFiltre = new Filtre(
						keys.getInteger(0),
						keys.getInteger(1),
						keys.getString(2), 
						keys.getString(3), 
						keys.getString(4), 
						new JsonObject(keys.getString(5)));
				future.complete(updatedFiltre);
			} else {
				future.fail(ar.cause().getMessage());
			}
		});

		return future;
	}

	@Override
	public Future<List<Filtre>> lister(Map<String, Object> eqFilters) {

		String action = "lister";

		Future<List<Filtre>> future = Future.future();

		// Construction de la requête
		String sqlFilters = buildEqualFilters(eqFilters);
		String order = buildOrder(null, null);

		StringBuffer requete = new StringBuffer();
		requete.append(String.format(SELECT_FILTRES + " %s", sqlFilters));
		requete.append(String.format(" %s ", order));

		List<Filtre> listeFiltres = new ArrayList<Filtre>();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", "filtres");
		jsonObjectSelect.put("requete", requete);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {
				try {
					JsonObject allTracesJson = ar.result();
					JsonArray rows = allTracesJson.getJsonArray("filtres");

					for (int i = 0; i < rows.size(); i++) {
						JsonObject filtre = rows.getJsonObject(i);
						listeFiltres.add(Filtre.fromJson(filtre));
					}
				} catch (Exception e) {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
					future.fail(e.getMessage());
				}
				future.complete(listeFiltres);

			} else {
				future.fail(ar.cause());
			}
		});
		
		return future;
	}

	@Override
	public Future<Integer> delete(int id) {
		Future<Integer> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, DELETE_BY_ID);

		JsonArray params = new JsonArray();
		params.add(id);
		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {
				JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
				future.complete(rows.size());
			} else {
				future.fail(ar.cause());
			}
		});

		return future;
	}
	
	

	private String buildEqualFilters(Map<String, Object> filters) {
		StringBuffer sqlFilters = new StringBuffer();
		if (filters != null) {
			filters.forEach((property, value) -> {
				if (value != null) {
					if (property.equals(Filtre.KEY_JSON_ID) ||
							property.equals(Filtre.KEY_JSON_UTILISATEUR_ID)) {
						sqlFilters.append(" AND " + "f." + property + " = " + value);
					} else {
						sqlFilters.append(" AND upper(" + "f." + property + ") = upper('" + getStringSQLCorrect(value.toString()) + "')");
					}

				}
			});
		}
		return sqlFilters.toString();
	}

	private String buildOrder(String sort, String sortOrder) {
		StringBuffer order = new StringBuffer();
		if (sort != null) {
			order.append(" ORDER BY ");
			order.append(sort);
			if (sortOrder != null) {
				order.append(" " + sortOrder);
			}
		} else {
			order.append(" ORDER BY " + Filtre.KEY_JSON_NOM + " ASC");
		}
		return order.toString();
	}

	@Override
	public Future<Integer> supprimerParUtilisateur(int utilisateurId) {
		Future<Integer> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, DELETE_BY_ID_UTILISATEUR);

		JsonArray params = new JsonArray();
		params.add(utilisateurId);
		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {
				JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
				future.complete(rows.size());
			} else {
				future.fail(ar.cause());
			}
		});

		return future;
	}
}
