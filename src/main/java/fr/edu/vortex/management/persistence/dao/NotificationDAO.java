//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao;

import java.util.List;

import fr.edu.vortex.management.notification.EtatEmissionNotificationEnum;
import fr.edu.vortex.management.notification.Notification;
import fr.edu.vortex.management.notification.TypeNotificationEnum;
import io.vertx.core.Future;

public interface NotificationDAO extends GenericDAO {

    Future<Notification> save(Notification notif);

    Future<Notification> updateEtatEmissionNotification(Integer idNotification,
            EtatEmissionNotificationEnum etatEmission);

    Future<Notification> getNotificationByCorrelationIdAndTypeNotification(String correlationId,
            TypeNotificationEnum type);

    Future<List<Notification>> getAllNotificationsByCorrelationId(String correlationId);

	Future<Void> deleteNotification(String correlation_id);

}
