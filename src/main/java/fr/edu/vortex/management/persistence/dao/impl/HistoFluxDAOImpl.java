//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import fr.edu.vortex.management.flux.ActionEnum;
import fr.edu.vortex.management.flux.HistoFlux;
import fr.edu.vortex.management.flux.StatusEnum;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.HistoFluxDAO;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Une implementation NOP( No operation) du service de persistence.
 *
 */

public class HistoFluxDAOImpl extends GenericDAOImpl implements HistoFluxDAO {

	static final String MAIN_TABLE = "histo_flux";

	private static final String REQ_INSERT = "INSERT INTO histo_flux "
			+ "(date, correlation_id, consumer_correlation_id, consumer_queue_name, status, status_order, action) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?) returning id";

	private static final String REQ_SELECT = "SELECT "
			+ "id, date, correlation_id, consumer_correlation_id, consumer_queue_name, status, status_order, action "
			+ "FROM histo_flux where 1=1 ";

	private static final String REQ_SELECT_BY_CORRELATION_ID = REQ_SELECT
			+ " AND correlation_id=? "
			+ " order by date ";
	
	private static final String REQ_SELECT_BY_CORRELATION_ID_AND_CONSUMER_CORRELATION_ID = REQ_SELECT
			+ " AND "
			+ " 	correlation_id=?"
			+ " AND  "
			+ " 	( "
			+ "  		(correlation_id=? AND status = 'ERREUR') "
			+ " 	OR "
			+ " 		consumer_correlation_id is null  "
			+ " 	OR "			
			+ " 		consumer_correlation_id=?  "
			+ " 	) "
			+ " order by date ";

	@Override
	public Future<HistoFlux> save(HistoFlux histoFlux) {
		Future<HistoFlux> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, REQ_INSERT);

		BiFunction<Object, JsonArray, JsonArray> addParams = (data, jsonArray) -> {
			return (data == null) ? jsonArray.addNull() : jsonArray.add(data);
		};

		JsonArray params = new JsonArray();
		params = addParams.apply(histoFlux.getDate(), params);
		params = addParams.apply(histoFlux.getCorrelationId(), params);
		params = addParams.apply(histoFlux.getConsumerCorrelationId(), params);
		params = addParams.apply(histoFlux.getConsumerQueueName(), params);
		params = addParams.apply(histoFlux.getStatus().getValue(), params);
		params = addParams.apply(histoFlux.getStatus().getOrder(), params);
		params = addParams.apply(histoFlux.getAction().getValue(), params);

		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
			if (ar.succeeded()) {

				if (!ar.result().containsKey(MAIN_TABLE)) {
					future.fail("résultat mal formaté");
					return;
				}

				JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
				if (rows.size() == 0) {
					future.fail("Erreur lors de la création de l'enregistrement dans histo_flux");
					return;
				}

				histoFlux.setId(rows.getJsonObject(0).getInteger("id"));

				future.complete(histoFlux);

			} else {
				future.fail(ar.cause());
			}
		});

		return future;
	}

	@Override
	public Future<List<HistoFlux>> getHistoFluxByCorrelationIdAndConsumerCorrelationId(String correlationId,
			String consumerCorrelationId) {

		Future<List<HistoFlux>> future = Future.future();

		if (correlationId != null && !correlationId.isEmpty()) {

			String requete = REQ_SELECT_BY_CORRELATION_ID;			
			JsonArray params = new JsonArray();
			params.add(correlationId);	

			if(consumerCorrelationId != null && !consumerCorrelationId.isEmpty()) {	
				params.add(correlationId);
				params.add(consumerCorrelationId);
				requete = REQ_SELECT_BY_CORRELATION_ID_AND_CONSUMER_CORRELATION_ID;
			}

			JsonObject jsonObjectSelect = new JsonObject();
			jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);		
			jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, requete);
			jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

			envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
				if (ar.succeeded()) {
					if (!ar.result().containsKey(MAIN_TABLE)) {
						future.fail("résultat mal formaté");
						return;
					}

					JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
					if (rows.size() == 0) {
						future.complete(new ArrayList<>());
						return;
					}
					List<HistoFlux> histoFlux = rowsSqlEnHistoFlux(rows);
					future.complete(histoFlux);
				} else {
					future.fail(ar.cause().getMessage());
				}
			});
		} else {
			future.fail("La valeur renseignée du correlation_id du flux est invalide");
		}

		return future;

	}

	/**
	 * Transforme plusieurs ligne sql en un ensemble d'histo flux.
	 * 
	 * @param rows
	 * @return
	 */
	private List<HistoFlux> rowsSqlEnHistoFlux(JsonArray rows) {
		List<HistoFlux> histo = new ArrayList<>();
		rows.stream().map(o -> (JsonObject) o).map(this::transform).forEach(currentHisto -> {
			histo.add(currentHisto);
		});
		return histo;
	}

	/**
	 * Transforme une ligne d'un select en un object histoFlux
	 * 
	 * @param row
	 * @return
	 */
	private HistoFlux transform(JsonObject row) {
		return new HistoFlux(
				row.getInteger("id"), 
				row.getInstant("date"), 
				row.getString("correlation_id"),
				row.getString("consumer_correlation_id"), 
				row.getString("consumer_queue_name"),
				StatusEnum.valueOf(row.getString("status")), 
				ActionEnum.valueOf(row.getString("action")));
	}
}
