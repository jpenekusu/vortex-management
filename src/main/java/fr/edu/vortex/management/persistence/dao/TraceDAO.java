//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao;

import java.util.List;
import java.util.Map;

import fr.edu.vortex.management.utilisateur.pojo.Trace;
import io.vertx.core.Future;

public interface TraceDAO extends GenericDAO {
	
	    Future<List<Trace>> listerTrace(int borneDebut, int borneFin, Map<String, Object> likeFilters, String sort,
	            String sortOrder);

		Future<Trace> creer(Trace trace);

		Future<Trace> creerTraceConnexion(Trace trace);
		
		Future<Integer> countTrace(Map<String, Object> likeFilters);
		
	    Future<List<Trace>> listerHistoriqueTrace(Map<String, Object> equalFilters);

}
