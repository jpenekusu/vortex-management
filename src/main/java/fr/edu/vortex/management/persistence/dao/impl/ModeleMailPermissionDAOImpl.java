package fr.edu.vortex.management.persistence.dao.impl;

import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.ModeleMailPermissionDAO;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class ModeleMailPermissionDAOImpl extends GenericDAOImpl implements ModeleMailPermissionDAO {

	private static final String MAIN_TABLE = "modele_mail_permission";

	private static String DELETE_BY_MODELE_MAIL_ID = "DELETE FROM modele_mail_permission WHERE modele_mail_id = ? returning *";

	private static String INSERT = "INSERT INTO modele_mail_permission (modele_mail_id, permission_code) VALUES (?, ?)";

	@Override
	public Future<Integer> delete(int modele_mailId) {
		return super.delete(DELETE_BY_MODELE_MAIL_ID, modele_mailId);
	}

	@Override
	public Future<Void> create(int modele_mailId, String permissionCode) {
		Future<Void> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, INSERT);

		JsonArray params = new JsonArray();
		params.add(modele_mailId);
		params.add(permissionCode);
		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {
				future.complete();
			} else {
				future.fail(ar.cause());
			}
		});

		return future;
	}

}
