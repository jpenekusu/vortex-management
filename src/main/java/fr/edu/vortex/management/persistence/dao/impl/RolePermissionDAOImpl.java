//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.RolePermissionDAO;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class RolePermissionDAOImpl extends GenericDAOImpl implements RolePermissionDAO {

    private static final String MAIN_TABLE = "role_permission";

    private static String DELETE_BY_ROLE_ID = "DELETE FROM role_permission WHERE role_id = ? returning *";

    private static String INSERT = "INSERT INTO role_permission (role_id, permission_code, actions) VALUES (?, ?, ?)";

    @Override
    public Future<Integer> delete(int roleId) {
        return super.delete(DELETE_BY_ROLE_ID, roleId);
    }

    @Override
    public Future<Void> create(int roleId, String permissionCode, String actions) {
        Future<Void> future = Future.future();

        JsonObject jsonObjectSelect = new JsonObject();
        jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, INSERT);

        JsonArray params = new JsonArray();
        params.add(roleId);
        params.add(permissionCode);
        params.add(actions);
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
                .setHandler(ar -> {
                    if (ar.succeeded()) {
                        future.complete();
                    } else {
                        future.fail(ar.cause());
                    }
                });

        return future;
    }

}
