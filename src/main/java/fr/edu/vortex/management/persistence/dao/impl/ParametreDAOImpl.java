//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import java.util.Optional;

import fr.edu.vortex.management.parametre.Parametre;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.ParametreDAO;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class ParametreDAOImpl extends GenericDAOImpl implements ParametreDAO {

	private static final String SELECT_PARAMETRE_BY_CODE = "SELECT code, valeur FROM parametre where code= ?";

	private static final String MAIN_TABLE = "parametre";

	@Override
	public Future<Optional<Parametre>> getParametreByCode(String code) {

		Future<Optional<Parametre>> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, SELECT_PARAMETRE_BY_CODE);

		JsonArray params = new JsonArray();
		params.add(code);
		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
			if (!ar.succeeded()) {
				future.fail(ar.cause());
				return;
			}

			if (!ar.result().containsKey(MAIN_TABLE)) {
				future.fail("résultat mal formaté");
				return;
			}

			JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
			if (rows.size() == 0) {
				future.complete(Optional.empty());
				return;
			}

			if (rows.size() == 1) {
				JsonObject jsonParam = rows.getJsonObject(0);
				Parametre param = new Parametre(jsonParam.getString(Parametre.KEY_DB_CODE),
						jsonParam.getString(Parametre.KEY_DB_VALEUR));
				future.complete(Optional.of(param));
			}

		});

		return future;

	}
}
