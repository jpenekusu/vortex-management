package fr.edu.vortex.management.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.hazelcast.util.StringUtil;

import fr.edu.echanges.nat.vortex.common.adresses.persistence.PersistenceAdressesManagement;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.mail.ModeleMail;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.ModeleMailDAO;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ModeleMailDAOImpl extends GenericDAOImpl implements ModeleMailDAO{

	private static final Logger logger = LoggerFactory.getLogger(ModeleMailDAOImpl.class);

	private static final String MAIN_TABLE = "modele_mail";

	private static final String SELECT_GENERIQUE = "" +
			"SELECT " +
			"    m.id, " +
			"    m.nom, " +
			"    m.email, " +
			"    m.destinataire, " +
			"    m.contenu, " +
			"    m.systeme, " +
			"    p.code as perm_code, " +
			"    p.libelle as perm_libelle, " +
			"    p.scope as perm_scope, " +
			"    p.ressource as perm_ressource, " +
			"    p.contexte as perm_contexte " +
			"FROM " +
			" modele_mail m " +
			"LEFT OUTER JOIN " +
			" modele_mail_permission mp " +
			"ON " +
			" ( " +
			" m.id = mp.modele_mail_id) " +
			"LEFT OUTER JOIN " +
			" permission p " +
			"ON " +
			" ( " +
			" mp.permission_code = p.code) ";
	
	private static final String SELECT_ID_MODELES = "" +
			"SELECT " +
			"    m.id, " +
			"    m.nom, " +
			"    m.email, " +
			"    m.destinataire, " +
			"    m.contenu, " +
			"    m.systeme " +
			"FROM " +
			" modele_mail m ";
	
	private static final String SUB_SELECT_ID_BY_PERMISSION = "" +
			"SELECT mp.modele_mail_id "
			+ "FROM modele_mail_permission mp, permission p "
			+ "WHERE mp.permission_code = p.code ";	
	
	private static final String REQ_COUNT_MODELE = "" +
			"SELECT " +
			" count(distinct(m.id)) as nb_modeles " +
			"FROM " +
			" modele_mail m " +
			"LEFT OUTER JOIN " +
			" modele_mail_permission mp " +
			"ON " +
			" ( " +
			" m.id = mp.modele_mail_id) " +
			"LEFT OUTER JOIN " +
			" permission p " +
			"ON " +
			" ( " +
			" mp.permission_code = p.code) ";

	private static final String DELETE_BY_ID = "DELETE FROM modele_mail WHERE id = ? returning *";

	private static final String INSERT = "INSERT INTO modele_mail (nom, email, destinataire, systeme, contenu) VALUES (?,?,?,?,?::json) returning id";

	private static final String SELECT_PAR_ID_MODELE = SELECT_GENERIQUE +" where m.id = ? ";

	private static final String SELECT_PAR_NOM = SELECT_GENERIQUE + " where m.nom = ? ORDER BY m.nom ASC, p.libelle ASC";

	@Override
	public Future<ModeleMail> creer(ModeleMail modeleMail) {
		Future<ModeleMail> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, INSERT);

		JsonArray params = new JsonArray();
		params.add(modeleMail.getNom());
		params.add(modeleMail.getEmail());
		params.add(modeleMail.getDestinataire());
		params.add(modeleMail.getSysteme());
		if (modeleMail.getContenu() != null) {
			params.add(modeleMail.getContenu().toString());
		} else {
			params.add("''");			
		}
		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {

				if (!ar.result().containsKey(MAIN_TABLE)) {
					future.fail("résultat mal formaté");
					return;
				}

				JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
				if (rows.size() == 0) {
					future.fail("erreur lors de la création du modele_mail");
					return;
				}

				modeleMail.setId(rows.getJsonObject(0).getInteger("id"));

				future.complete(modeleMail);

			} else {
				future.fail(ar.cause());
			}
		});

		return future;
	}

	@Override
	public Future<ModeleMail> modifier(ModeleMail modeleMail) {

		Future<ModeleMail> future = Future.future();

		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append("UPDATE " + MAIN_TABLE + " SET ");
		sqlFilters.append(ModeleMail.KEY_JSON_NOM + " = '").append(getStringSQLCorrect(modeleMail.getNom().trim())).append("', ");
		sqlFilters.append(ModeleMail.KEY_JSON_EMAIL + " = '").append(getStringSQLCorrect(modeleMail.getEmail())).append("', ");
		sqlFilters.append(ModeleMail.KEY_JSON_CONTENU + " = '").append(getStringSQLCorrect(String.valueOf(modeleMail.getContenu()))).append("', ");
		sqlFilters.append(ModeleMail.KEY_JSON_DESTINATAIRE + " = '").append(getStringSQLCorrect(modeleMail.getDestinataire())).append("' ");
		sqlFilters.append("WHERE id = ").append(String.valueOf(modeleMail.getId()));

		JsonObject jsonRequete = new JsonObject();
		jsonRequete.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());

		envoiRequeteAuVerticleBD(PersistenceAdressesManagement.UPDATE, jsonRequete)
		.setHandler(ar -> {
			if (ar.succeeded()) {

				if (!ar.result().containsKey("keys")) {
					future.fail("résultat mal formaté");
					return;
				}

				JsonArray keys = ((JsonObject) ar.result()).getJsonArray("keys");

				Object contenuDB =  keys.getValue(4);
				JsonObject contenu = null;
				if (contenuDB instanceof String) {
					contenu = new JsonObject((String)contenuDB);	
				} else {
					contenu = keys.getJsonObject(4);	
				}
				ModeleMail modele_mailUpdated = new ModeleMail(
						keys.getInteger(0), 
						keys.getString(1), 
						keys.getString(2), 
						keys.getString(3), 
						contenu, 
						keys.getBoolean(5), 
						(List<Permission>) null);

				future.complete(modele_mailUpdated);

			} else {
				future.fail(ar.cause());
			}
		});

		return future;       
	}


	private Future<List<ModeleMail>> generiqueSelect(String sql, JsonArray params) {
		Future<List<ModeleMail>> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);
		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sql);
		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {

				if (!ar.result().containsKey(MAIN_TABLE)) {
					future.fail("résultat mal formaté");
					return;
				}

				JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
				if (rows.size() == 0) {
					future.complete(new ArrayList<>());
					return;
				}

				List<ModeleMail> modeles = rowsSqlEnModeleMails(rows);

				future.complete(modeles);

			} else {
				future.fail(ar.cause());
			}
		});

		return future;
	}


	public Future<List<ModeleMail>> listerModeles(int borneDebut, int borneFin, JsonObject filtres, String sort,
			String sortOrder) {

		String action = "listerModele(int borneDebut, int borneFin, Map<String, String> parameters, String sort, String desc)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<List<ModeleMail>> future = Future.future();

		int nbFlux = borneFin - borneDebut + 1;
		if (borneDebut >= 0 && borneFin >= 0 && nbFlux > 0) {


			String order = buildOrder(sort, sortOrder);

			JsonObject jsonObjectSelect = new JsonObject();
			JsonArray params = new JsonArray();

			StringBuffer sqlFilters = getRequete(borneDebut, filtres, nbFlux, order, params);

			jsonObjectSelect.put("JsonArrayName", "ids");
			jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
			jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

			List<ModeleMail> allModeles = new ArrayList<ModeleMail>();

			envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
			.setHandler(arIds -> {
				if (arIds.succeeded()) {
					try {
						JsonObject allIdsJson = arIds.result();
						JsonArray rowsIds = allIdsJson.getJsonArray("ids");

	                    if (rowsIds.size() <1){
	                        future.complete(allModeles);
	                        return;	                    	
	                    }
	                    
		                StringBuilder listeIdModeles = new StringBuilder("(");
		                boolean first = true;
						for (int i = 0; i < rowsIds.size(); i++) {
	                        JsonObject row = rowsIds.getJsonObject(i);
	                        Integer id = row.getInteger("id");
							if (first){
								first = false;
							} else {
								listeIdModeles.append(",");
							}
							listeIdModeles.append(id.toString());					
	                    }
						listeIdModeles.append(")");
						
						String req = SELECT_GENERIQUE + " WHERE m.id IN " + listeIdModeles.toString() + order;

						JsonObject jsonObjectReq = new JsonObject();

						jsonObjectReq.put("JsonArrayName", "modeles");
						jsonObjectReq.put(DatabaseVerticle.KEY_REQUETE, req);

		                envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectReq).setHandler(arModeles -> {
		                    if (arModeles.succeeded()) {
								JsonObject allModelesJson = arModeles.result();
								JsonArray rows = allModelesJson.getJsonArray("modeles");
								allModeles.addAll(rowsSqlEnModeleMails(rows));

								future.complete(allModeles);
		                    } else {
		                        future.fail(arModeles.cause().getMessage());                   	
		                    }
		                });
					} catch (Exception e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						future.fail(e.getMessage());
					}

				} else {
					future.fail(arIds.cause());
				}
			});
		}

		else {
			future.fail("Les valeurs renseignées dans les bornes de début et de fin sont invalides");
		}

		return future;
	}


	@Override
	public Future<Integer> delete(int id) {
		return super.delete(DELETE_BY_ID, id);
	}

	@Override
	public Future<Optional<ModeleMail>> chercherModeleMailByID(int id) {

		Future<Optional<ModeleMail>> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, SELECT_PAR_ID_MODELE);

		JsonArray params = new JsonArray();
		params.add(id);
		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
			if (!ar.succeeded()) {
				future.fail(ar.cause());
				return;
			}

			if (!ar.result().containsKey(MAIN_TABLE)) {
				future.fail("résultat mal formaté");
				return;
			}

			JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
			if (rows.size() == 0) {
				future.complete(Optional.empty());
				return;
			}

			if(rows.size()>=1) {
				List<ModeleMail> modele_mails = rowsSqlEnModeleMails(rows);
				ModeleMail modele_mail = modele_mails.get(0);
				future.complete(Optional.of(modele_mail));
			}

		});

		return future;

	}

	/**
	 * Transforme plusieurs ligne sql en un ensemble de modele_mails.
	 * 
	 * @param rows
	 * @return
	 */
	private List<ModeleMail> rowsSqlEnModeleMails(JsonArray rows) {
		List<ModeleMail> modele_mails = new ArrayList<>();
		rows.stream()
		.map(o -> (JsonObject) o)
		.map(this::transform)
		.forEach(currentModeleMail -> {
			Optional<ModeleMail> find = modele_mails.stream()
					.filter(r -> r.getId() == currentModeleMail.getId())
					.findFirst();

			if (find.isPresent()) {
				find.get().getPermissions().add(currentModeleMail.getPermissions().get(0));
			} else {
				modele_mails.add(currentModeleMail);
			}

		});
		return modele_mails;
	}


	/**
	 * Transforme une ligne d'un select en un object modele_mail (a une permission)
	 * 
	 * @param row
	 * @return
	 */
	private ModeleMail transform(JsonObject row) {

		Permission perm = new Permission(
				row.getString("perm_code"),
				row.getString("perm_libelle"),
				row.getString("perm_scope"),
				row.getString("perm_ressource"),
				row.getString("perm_contexte"),
				row.getString("perm_actions"));

		List<Permission> perms = new ArrayList<>();
		if (!StringUtil.isNullOrEmpty(perm.getCode())) {
			perms.add(perm);
		}

		Object contenuDB =  row.getValue("contenu");
		JsonObject contenu = null;
		if (contenuDB instanceof String) {
			contenu = new JsonObject((String)contenuDB);	
		} else {
			contenu = row.getJsonObject("contenu");	
		}

		return new ModeleMail(
				row.getInteger("id"),
				row.getString("nom"),
				row.getString("email"),
				row.getString("destinataire"),
				contenu,
				row.getBoolean("systeme"),
				perms);

	}


	@Override
	public Future<List<ModeleMail>> chercherModeleMailParNom(String nom) {

		JsonArray params = new JsonArray();
		params.add(nom);

		return generiqueSelect(SELECT_PAR_NOM, params);
	}

	@Override
	public Future<Integer> countModeles(JsonObject filtres) {
		String action = "countModeles";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<Integer> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
        JsonArray params = new JsonArray();

        StringBuffer sqlFilters = getRequeteCount(filtres, params);

		jsonObjectSelect.put("JsonArrayName", "count");
        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
			if (ar.succeeded()) {
				Integer nbModeles = null;
				try {
					JsonObject allJson = ar.result();
					JsonArray arrayJson = allJson.getJsonArray("count");
					for (int i = 0; i < arrayJson.size(); i++) {
						JsonObject messageJson = arrayJson.getJsonObject(i);
						nbModeles = new Integer(messageJson.getInteger("nb_modeles"));
					}
				} catch (Exception e) {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
					future.fail(e.getMessage());
				}
				future.complete(nbModeles);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
				future.fail(ar.cause().getMessage());
			}
		});

		return future;
	}

	private StringBuffer getRequete(int borneDebut, JsonObject filtres, int nbFlux, String order, JsonArray params) {
		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append("(");
		sqlFilters.append(SELECT_ID_MODELES);
		getFiltersNoJoin(filtres, params, sqlFilters);
		getFiltersPerm(filtres, params, sqlFilters);
		sqlFilters.append(String.format(") %s ", order));
		sqlFilters.append(String.format(" limit %s offset %s", nbFlux, borneDebut));
		return sqlFilters;
	}

	private StringBuffer getRequeteCount(JsonObject filtres, JsonArray params) {
		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append(REQ_COUNT_MODELE);
		getFilters(filtres, params, sqlFilters);
		return sqlFilters;
	}

	private void getFilters(JsonObject filtres, JsonArray params, StringBuffer sqlFilters) {
		sqlFilters.append(" WHERE 1=1 ");
		if (filtres != null) {
			filtres.getMap().forEach((property, value) -> {
				if (value != null) {
					if (ModeleMail.KEY_JSON_PERMISSIONS.contentEquals(property)) {
						if (value.toString().startsWith("=")) {
							sqlFilters.append(" AND " + "upper(p.libelle) = upper(?) ");
							params.add(value.toString().substring(1));
						} else {
							sqlFilters.append(" AND " + "upper(p.libelle) like upper(?) ");
							params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
						}		        							
					} else if (ModeleMail.KEY_JSON_SYSTEME.contentEquals(property)) {
						sqlFilters.append(" AND m." + property + " = ? ");		    		        		
						params.add(value);						
					} else {              	
						if (value.toString().startsWith("=")) {
							sqlFilters.append(" AND " + "upper(m." + property + ") = upper(?) ");
							params.add(value.toString().substring(1));
						} else {
							sqlFilters.append(" AND " + "upper(m." + property + ") like upper(?) ");
							params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
						}
					}
				}
			});
		}
	}

	
	private void getFiltersPerm(JsonObject filtres, JsonArray params, StringBuffer sqlFilters) {
		sqlFilters.append(" AND m.id IN (");
		sqlFilters.append(SUB_SELECT_ID_BY_PERMISSION);
		if (filtres != null) {
			filtres.getMap().forEach((property, value) -> {
				if (value != null && ModeleMail.KEY_JSON_PERMISSIONS.contentEquals(property)) {
					if (value.toString().startsWith("=")) {
						sqlFilters.append(" AND " + "upper(p.libelle) = upper(?) ");
						params.add(value.toString().substring(1));
					} else {
						sqlFilters.append(" AND " + "upper(p.libelle) like upper(?) ");
						params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
					}		        							
				}
			});
		}
		sqlFilters.append(")");
	}
	
	private void getFiltersNoJoin(JsonObject filtres, JsonArray params, StringBuffer sqlFilters) {
		sqlFilters.append(" WHERE 1=1 ");
		if (filtres != null) {
			filtres.getMap().forEach((property, value) -> {
				if (value != null && !ModeleMail.KEY_JSON_PERMISSIONS.contentEquals(property)) {
					if (ModeleMail.KEY_JSON_SYSTEME.contentEquals(property)) {
						sqlFilters.append(" AND m." + property + " = ? ");		    		        		
						params.add(value);						
					} else {              	
						if (value.toString().startsWith("=")) {
							sqlFilters.append(" AND " + "upper(m." + property + ") = upper(?) ");
							params.add(value.toString().substring(1));
						} else {
							sqlFilters.append(" AND " + "upper(m." + property + ") like upper(?) ");
							params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
						}
					}
				}
			});
		}
	}


	private String buildOrder(String sort, String sortOrder) {
		StringBuffer order = new StringBuffer();
		if (sort != null) {
			order.append(" ORDER BY ");
			order.append(sort);
			if (sortOrder != null) {
				order.append(" " + sortOrder);
			}
		} else {
			order.append(" ORDER BY " + ModeleMail.KEY_JSON_NOM + " ASC");
		}
		// rajout pour chaque order by
		order.append(", " + ModeleMail.KEY_JSON_ID + " ASC");

		return order.toString();
	}

}
