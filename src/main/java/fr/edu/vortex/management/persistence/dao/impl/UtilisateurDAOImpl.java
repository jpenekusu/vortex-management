//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import static fr.edu.vortex.management.Constants.LOG_MESSAGE_WITH_DATA;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

import com.hazelcast.util.StringUtil;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.UtilisateurDAO;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class UtilisateurDAOImpl extends GenericDAOImpl implements UtilisateurDAO {

    private static final String RETOUR_DAO_LISTER = "retour de la dao sur une demande de liste d'utilisateurs";

    private static final Logger logger = LoggerFactory.getLogger(UtilisateurDAOImpl.class);

    private static final String REQ_COUNT_USERS = "" +
            "SELECT count(u.id) AS nb_users " +
            "FROM " +
            "    management.utilisateur u " +
            "LEFT OUTER JOIN " +
            "    management.role r " +
            "ON " +
            "    ( " +
            "        r.id = u.role_id) ";

    private static final String SELECT_ALL = "" +
            "SELECT " +
            "    u.id, " +
            "    u.nom, " +
            "    u.prenom, " +
            "    u.login, " +
            "    u.hash, " +
            "    u.sel, " +
            "    u.nombre_iteration, " +
            "    u.email, " +
            "    u.type_authentification, " +
            "    u.systeme, " +
            "    r.id        AS role_id, " +
            "    r.libelle   AS role_libelle, " +
            "    r.systeme   AS role_systeme " +
             "FROM " +
            "    management.utilisateur u " +
            "LEFT OUTER JOIN " +
            "    management.role r " +
            "ON " +
            "    ( " +
            "        r.id = u.role_id) ";
    
    private static final String SELECT_PERMISSIONS = "" +
            "SELECT " +
            "    r.id        AS role_id, " +
            "    p.code      AS perm_code, " +
            "    p.libelle   AS perm_libelle, " +
            "    p.scope     AS perm_scope, " +
            "    p.ressource AS perm_ressource, " +
            "    p.contexte  AS perm_contexte, " +
            "    rp.actions    AS perm_actions " +
            "FROM " +
            "    management.role r, " +
            "    management.role_permission rp, " +
            "    management.permission p " +
            "WHERE r.id = rp.role_id " +
            "AND rp.permission_code = p.code ";

    private static final String DELETE_BY_ID = "DELETE FROM utilisateur WHERE id = ? returning *";

    private static final String INSERT = "INSERT INTO utilisateur "
            + "(nom, prenom, login, hash, sel, nombre_iteration, email, type_authentification, systeme, role_id) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, COALESCE(?,'LOCALE'), ?, ?) returning id";

    private static final String MAIN_TABLE = "utilisateur";

    private static final String SELECT_BY_ID = "SELECT * FROM utilisateur WHERE id = ?";

    @Override
    public Future<Utilisateur> creer(Utilisateur utilisateur) {
        Future<Utilisateur> future = Future.future();

        JsonObject jsonObjectSelect = new JsonObject();
        jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, INSERT);

        BiFunction<Object, JsonArray, JsonArray> addParams = (data, jsonArray) -> {
            return (data == null) ? jsonArray.addNull() : jsonArray.add(data);
        };

        JsonArray params = new JsonArray();
        params = addParams.apply(utilisateur.getNom(), params);
        params = addParams.apply(utilisateur.getPrenom(), params);
        params = addParams.apply(utilisateur.getLogin(), params);
        params = addParams.apply(utilisateur.getHash(), params);
        params = addParams.apply(utilisateur.getSel(), params);
        params = addParams.apply(utilisateur.getNombreIteration(), params);
        params = addParams.apply(utilisateur.getEmail(), params);
        params = addParams.apply(utilisateur.getTypeAuthentification(), params);
        params = addParams.apply(utilisateur.isSysteme(), params);

        if (utilisateur.getRole() != null) {
            params = addParams.apply(utilisateur.getRole().getId(), params);
        } else {
            params.addNull();
        }

        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
                .setHandler(ar -> {
                    if (ar.succeeded()) {

                        if (!ar.result().containsKey(MAIN_TABLE)) {
                            future.fail("résultat mal formaté");
                            return;
                        }

                        JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
                        if (rows.size() == 0) {
                            future.fail("erreur lors de la création du role");
                            return;
                        }

                        utilisateur.setId(rows.getJsonObject(0).getInteger("id"));

                        future.complete(utilisateur);

                    } else {
                        future.fail(ar.cause());
                    }
                });

        return future;
    }

    @Override
    public Future<Integer> delete(int id) {
        Future<Integer> future = Future.future();

        JsonObject jsonObjectSelect = new JsonObject();
        jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, DELETE_BY_ID);

        JsonArray params = new JsonArray();
        params.add(id);
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
                .setHandler(ar -> {
                    if (ar.succeeded()) {
                        JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
                        future.complete(rows.size());
                    } else {
                        future.fail(ar.cause());
                    }
                });

        return future;
    }

    
    @Override
    public Future<Utilisateur> getById(Integer id) {

        Future<Utilisateur> future = Future.future();
        JsonObject jsonObjectSelect = new JsonObject();
        JsonArray params = new JsonArray();
        params.add(id);

        StringBuffer sqlFilters = new StringBuffer();
        sqlFilters.append(SELECT_BY_ID);
      
        jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);
        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
                .setHandler(ar -> {
                    if (ar.succeeded()) {

                        if (!ar.result().containsKey(MAIN_TABLE)) {
                            future.fail("résultat mal formaté");
                            return;
                        }

                        JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
                        if (rows.size() == 0) {
                            future.complete(null);
                            return;
                        }

                        Utilisateur utilisateur = new Utilisateur();
                        utilisateur.setId(rows.getJsonObject(0).getInteger("id"));
                        utilisateur.setNom(rows.getJsonObject(0).getString("nom"));
                        utilisateur.setPrenom(rows.getJsonObject(0).getString("prenom"));
                        utilisateur.setLogin(rows.getJsonObject(0).getString("login"));
                        utilisateur.setEmail(rows.getJsonObject(0).getString("email"));
                        utilisateur.setTypeAuthentification(rows.getJsonObject(0).getString("type_authentification"));
                        utilisateur.setSysteme(rows.getJsonObject(0).getBoolean("systeme"));


                        future.complete(utilisateur);

                        logger.trace(LOG_MESSAGE_WITH_DATA,
                                LOG_NO_CORRELATION_ID,
                                RETOUR_DAO_LISTER,
                                utilisateur);

                    } else {
                        future.fail(ar.cause());
                    }
                });

        return future;
        
    }
    
    @Override
    public Future<List<Utilisateur>> listerUtilisateurs(int borneDebut, int borneFin, JsonObject filtres, String sort,
    		String sortOrder) {

    	String action = "listerUtilisateurs(int borneDebut, int borneFin, Map<String, String> parameters, String sort, String desc)";
    	logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
    	Future<List<Utilisateur>> future = Future.future();

    	int nbValeurs = borneFin - borneDebut + 1;
    	if (borneDebut >= 0 && borneFin >= 0 && nbValeurs > 0) {

    		String order = buildOrder(sort, sortOrder);

    		JsonObject jsonObjectSelect = new JsonObject();
    		JsonArray params = new JsonArray();

    		StringBuffer sqlFilters = getRequete(borneDebut, filtres, nbValeurs, order, params);

    		jsonObjectSelect.put("JsonArrayName", "users");
    		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
    		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

    		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
    		.setHandler(ar -> {
    			if (ar.succeeded()) {
    				try {
    					if (!ar.result().containsKey("users")) {
    						future.fail("résultat mal formaté");
    						return;
    					}

    					JsonArray rows = ar.result().getJsonArray("users");
    					if (rows.size() == 0) {
    						future.complete(new ArrayList<>());
    						return;
    					}

    					List<Utilisateur> utilisateurs = agreger(rows);

    					String req = SELECT_PERMISSIONS;

    					JsonObject jsonObjectReq = new JsonObject();

    					jsonObjectReq.put("JsonArrayName", "roles");
    					jsonObjectReq.put(DatabaseVerticle.KEY_REQUETE, req);

    					envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectReq).setHandler(arRoles -> {
    						if (arRoles.succeeded()) {
    							try {
    								JsonObject roles = arRoles.result();
    								JsonArray rolesArrayJson = roles.getJsonArray("roles");

    								Map<Integer, List<Permission>> mapRoles = new HashMap<Integer, List<Permission>>();

    								for (int i = 0; i < rolesArrayJson.size(); i++) {
    									JsonObject roleJson = rolesArrayJson.getJsonObject(i);
    									Integer idRole = roleJson.getInteger("role_id");
    									Permission perm = new Permission();
    									perm.setCode(roleJson.getString("perm_code"));
    									perm.setLibelle(roleJson.getString("perm_libelle"));
    									perm.setScope(roleJson.getString("perm_scope"));
    									perm.setRessource(roleJson.getString("perm_ressource"));
    									perm.setContexte(roleJson.getString("perm_contexte"));
    									perm.setActions(roleJson.getString("perm_actions"));
    									if (mapRoles.get(idRole) == null){
    										mapRoles.put(idRole, new ArrayList<Permission>());
    									}
    									mapRoles.get(idRole).add(perm);
    								}
    								for (Iterator<Utilisateur> iterator = utilisateurs.iterator(); iterator.hasNext();) {
    									Utilisateur user = iterator.next();
    									if (mapRoles.get(user.getRole().getId()) != null){
    										user.getRole().getPermissions().addAll(mapRoles.get(user.getRole().getId()));
    									}
    								}                    	
    								future.complete(utilisateurs);
    							} catch (Exception e) {
    								future.fail(e.getMessage());
    							}	                          	
    						} else {
    							future.fail(arRoles.cause().getMessage());                   	
    						}
    					});
    					logger.trace(LOG_MESSAGE_WITH_DATA,
    							LOG_NO_CORRELATION_ID,
    							RETOUR_DAO_LISTER,
    							utilisateurs);
    				} catch (Exception e) {
    					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
    					future.fail(e.getMessage());
    				}
    			} else {
    				future.fail(ar.cause());
    			}
    		});
    	} else {
    		future.fail("Les valeurs renseignées dans les bornes de début et de fin sont invalides");
    	}
    	return future;
    }

    private List<Utilisateur> agreger(JsonArray rows) {
        // TODO je pense qu'on peut faire mieux et plus simple
        List<Utilisateur> utilisateurs = new ArrayList<>();
        rows.stream()
                .map(o -> (JsonObject) o)
                .map(this::transform)
                .forEach(utiCourant -> {
                    Optional<Utilisateur> find = utilisateurs.stream()
                            .filter(u -> u.getId() == utiCourant.getId())
                            .findFirst();

                    if (find.isPresent()) {
                        find.get()
                                .getRole()
                                .getPermissions()
                                .add(utiCourant.getRole().getPermissions().get(0));
                    } else {
                        utilisateurs.add(utiCourant);
                    }
                });
        return utilisateurs;
    }

    private Utilisateur transform(JsonObject row) {
        Utilisateur user = new Utilisateur();

        user.setId(row.getInteger("id"));
        user.setNom(row.getString("nom"));
        user.setPrenom(row.getString("prenom"));
        user.setLogin(row.getString("login"));
        user.setHash(row.getString("hash"));
        user.setSel(row.getString("sel"));
        if (row.getInteger("nombre_iteration") != null) {
            user.setNombreIteration(row.getInteger("nombre_iteration"));
        }
        user.setEmail(row.getString("email"));
        user.setTypeAuthentification(row.getString("type_authentification"));
        user.setSysteme(row.getBoolean("systeme"));

        if (row.getInteger("role_id") != null) {
            Role role = new Role();
            role.setId(row.getInteger("role_id"));
            role.setLibelle(row.getString("role_libelle"));
            role.setSysteme(row.getBoolean("role_systeme"));

            List<Permission> perms = new ArrayList<>();

            if (!StringUtil.isNullOrEmpty(row.getString("perm_code"))) {
                Permission perm = new Permission();
                perm.setCode(row.getString("perm_code"));
                perm.setLibelle(row.getString("perm_libelle"));
                perm.setScope(row.getString("perm_scope"));
                perm.setRessource(row.getString("perm_ressource"));
                perm.setContexte(row.getString("perm_contexte"));
                perm.setActions(row.getString("perm_actions"));

                perms.add(perm);
            }

            role.setPermissions(perms);

            user.setRole(role);

        }
        return user;
    }
    
    @Override
    public Future<Utilisateur> modifierMotDePasse(JsonObject userModif) {

        Future<Utilisateur> future = Future.future();

        BiFunction<Object, JsonArray, JsonArray> addParams = (data, jsonArray) -> {
            return (data == null) ? jsonArray.addNull() : jsonArray.add(data);
        };

        StringBuffer sqlFilters = new StringBuffer();
        sqlFilters.append("UPDATE " + MAIN_TABLE + " SET ");
        JsonArray params = new JsonArray();
        sqlFilters.append("hash = ?,");
        params = addParams.apply(userModif.getString("hash"), params);
        sqlFilters.append("sel = ?,");
        params = addParams.apply(userModif.getString("sel"), params);
        sqlFilters.append("nombre_iteration = ? ");        
        params = addParams.apply(userModif.getInteger("nbIterations"), params);          	
        sqlFilters.append(" WHERE id = ?  returning * ");
        params = addParams.apply(userModif.getInteger("id"), params);
       
        JsonObject jsonRequete = new JsonObject();
        jsonRequete.put("JsonArrayName", "keys");
        jsonRequete.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
        jsonRequete.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonRequete).setHandler(ar -> {
            if (ar.succeeded()) {
               	JsonArray rows = ar.result().getJsonArray("keys");
                if (rows.size() == 0) {
                    future.complete(null);
                    return;
                }

                Utilisateur user = new Utilisateur();
                user.setId(rows.getJsonObject(0).getInteger("id"));
                user.setNom(rows.getJsonObject(0).getString("nom"));
                user.setPrenom(rows.getJsonObject(0).getString("prenom"));
                user.setLogin(rows.getJsonObject(0).getString("login"));
                user.setEmail(rows.getJsonObject(0).getString("email"));
                user.setTypeAuthentification(rows.getJsonObject(0).getString("type_authentification"));
                user.setSysteme(rows.getJsonObject(0).getBoolean("systeme"));

                future.complete(user);
            } else {
                future.fail(ar.cause().getMessage());
            }
        });

        return future;
    }
    
    
    @Override
    public Future<Utilisateur> modifierLOCAL(Utilisateur utilisateur) {

        Future<Utilisateur> future = Future.future();

        BiFunction<Object, JsonArray, JsonArray> addParams = (data, jsonArray) -> {
            return (data == null) ? jsonArray.addNull() : jsonArray.add(data);
        };
        
        StringBuffer sqlFilters = new StringBuffer();
        sqlFilters.append("UPDATE " + MAIN_TABLE + " SET ");
        JsonArray params = new JsonArray();
        sqlFilters.append("nom = ?,");
        params = addParams.apply(utilisateur.getNom(), params);        
        sqlFilters.append("prenom = ?,");
        params = addParams.apply(utilisateur.getPrenom(), params);
        if (utilisateur.getMotDePasse() != null) {
            sqlFilters.append("hash = ?,");
            params = addParams.apply(utilisateur.getHash(), params);
            sqlFilters.append("sel = ?,");
            params = addParams.apply(utilisateur.getSel(), params);
            sqlFilters.append("nombre_iteration = ?,");        
            params = addParams.apply(utilisateur.getNombreIteration(), params);          	
        }
        sqlFilters.append("email = ?,");
        params = addParams.apply(utilisateur.getEmail(), params);
        sqlFilters.append("role_id = ? ");
        params = addParams.apply(utilisateur.getRole().getId(), params);
        sqlFilters.append(" WHERE id = ? returning * ");
        params = addParams.apply(utilisateur.getId(), params);
        
        JsonObject jsonRequete = new JsonObject();
        jsonRequete.put("JsonArrayName", "keys");
        jsonRequete.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
        jsonRequete.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonRequete).setHandler(ar -> {
            if (ar.succeeded()) {
            	JsonArray rows = ar.result().getJsonArray("keys");
            	if (rows.size() == 0) {
            		future.complete(null);
            		return;
            	}

            	Utilisateur user = new Utilisateur();
            	user.setId(rows.getJsonObject(0).getInteger("id"));
            	user.setNom(rows.getJsonObject(0).getString("nom"));
            	user.setPrenom(rows.getJsonObject(0).getString("prenom"));
            	user.setLogin(rows.getJsonObject(0).getString("login"));
            	user.setEmail(rows.getJsonObject(0).getString("email"));
            	user.setTypeAuthentification(rows.getJsonObject(0).getString("type_authentification"));
            	user.setSysteme(rows.getJsonObject(0).getBoolean("systeme"));

            	future.complete(user);
            } else {
                future.fail(ar.cause().getMessage());
            }
        });

        return future;
    }

   @Override
   public Future<Utilisateur> modifierLDAP(Utilisateur utilisateur) {

        Future<Utilisateur> future = Future.future();

        BiFunction<Object, JsonArray, JsonArray> addParams = (data, jsonArray) -> {
            return (data == null) ? jsonArray.addNull() : jsonArray.add(data);
        };
 
        StringBuffer sqlFilters = new StringBuffer();
        sqlFilters.append("UPDATE " + MAIN_TABLE + " SET ");
        JsonArray params = new JsonArray();
        sqlFilters.append("nom = ?,");
        params = addParams.apply(utilisateur.getNom(), params);        
        sqlFilters.append("prenom = ?,");
        params = addParams.apply(utilisateur.getPrenom(), params);
        sqlFilters.append("email = ?,");
        params = addParams.apply(utilisateur.getEmail(), params);
        sqlFilters.append("role_id = ? ");
        params = addParams.apply(utilisateur.getRole().getId(), params);
        sqlFilters.append(" WHERE id = ? returning * ");
        params = addParams.apply(utilisateur.getId(), params);
     
        JsonObject jsonRequete = new JsonObject();
        jsonRequete.put("JsonArrayName", "keys");
        jsonRequete.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
        jsonRequete.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonRequete).setHandler(ar -> {
            if (ar.succeeded()) {
               	JsonArray rows = ar.result().getJsonArray("keys");
                if (rows.size() == 0) {
                    future.complete(null);
                    return;
                }

                Utilisateur user = new Utilisateur();
                user.setId(rows.getJsonObject(0).getInteger("id"));
                user.setNom(rows.getJsonObject(0).getString("nom"));
                user.setPrenom(rows.getJsonObject(0).getString("prenom"));
                user.setLogin(rows.getJsonObject(0).getString("login"));
                user.setEmail(rows.getJsonObject(0).getString("email"));
                user.setTypeAuthentification(rows.getJsonObject(0).getString("type_authentification"));
                user.setSysteme(rows.getJsonObject(0).getBoolean("systeme"));

                future.complete(user);
            } else {
                future.fail(ar.cause().getMessage());
            }
        });

        return future;
    }
 
   @Override
   public Future<Integer> countUtilisateurs(JsonObject filtres) {
	   String action = "countUtilisateurs";
	   logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
	   Future<Integer> future = Future.future();

	   JsonObject jsonObjectSelect = new JsonObject();
	   JsonArray params = new JsonArray();

	   StringBuffer sqlFilters = getRequeteCount(filtres, params);

	   jsonObjectSelect.put("JsonArrayName", "count");
	   jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
	   jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

	   envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
		   if (ar.succeeded()) {
			   Integer nbUsers = null;
			   try {
				   JsonObject allJson = ar.result();
				   JsonArray arrayJson = allJson.getJsonArray("count");
				   for (int i = 0; i < arrayJson.size(); i++) {
					   JsonObject messageJson = arrayJson.getJsonObject(i);
					   nbUsers = new Integer(messageJson.getInteger("nb_users"));
				   }
			   } catch (Exception e) {
				   logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
				   future.fail(e.getMessage());
			   }
			   future.complete(nbUsers);
		   } else {
			   logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
			   future.fail(ar.cause().getMessage());
		   }
	   });

	   return future;
   }
   
   private StringBuffer getRequete(int borneDebut, JsonObject filtres, int nbFlux, String order, JsonArray params) {
	   StringBuffer sqlFilters = new StringBuffer();
	   sqlFilters.append("(");
	   sqlFilters.append(SELECT_ALL);
	   getFilters(filtres, params, sqlFilters);
	   sqlFilters.append(String.format(") %s ", order));
	   sqlFilters.append(String.format(" limit %s offset %s", nbFlux, borneDebut));
	   return sqlFilters;
   }

   private StringBuffer getRequeteCount(JsonObject filtres, JsonArray params) {
	   StringBuffer sqlFilters = new StringBuffer();
	   sqlFilters.append(REQ_COUNT_USERS);
	   getFilters(filtres, params, sqlFilters);
	   return sqlFilters;
   }

   private void getFilters(JsonObject filtres, JsonArray params, StringBuffer sqlFilters) {
	   sqlFilters.append(" WHERE 1=1 ");
	   if (filtres != null) {
		   filtres.getMap().forEach((property, value) -> {
			   if (value != null) {
				   if (UtilisateurVerticle.KEY_JSON_ROLE.contentEquals(property)) {
					   if (value.toString().startsWith("=")) {
						   sqlFilters.append(" AND " + "upper(r.libelle) = upper(?) ");
						   params.add(value.toString().substring(1));
					   } else {
						   sqlFilters.append(" AND " + "upper(r.libelle) like upper(?) ");
						   params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
					   }		        							
				   } else if (UtilisateurVerticle.KEY_JSON_ROLE_ID.contentEquals(property)){
					   sqlFilters.append(" AND " + "r.id = ? ");
					   params.add(value);
				   } else if (Utilisateur.KEY_JSON_SYSTEME.contentEquals(property)) {
					   sqlFilters.append(" AND u." + property + " = ? ");		    		        		
					   params.add(value);						
				   } else {              	
					   if (value.toString().startsWith("=")) {
						   sqlFilters.append(" AND " + "upper(u." + property + ") = upper(?) ");
						   params.add(value.toString().substring(1));
					   } else {
						   sqlFilters.append(" AND " + "upper(u." + property + ") like upper(?) ");
						   params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
					   }
				   }
			   }
		   });
	   }
   }

   private String buildOrder(String sort, String sortOrder) {
	   StringBuffer order = new StringBuffer();
	   if (sort != null) {
		   order.append(" ORDER BY ");
		   if (UtilisateurVerticle.KEY_JSON_ROLE.equals(sort)) {
			   order.append("role_libelle");  
		   } else {
			   order.append(sort);
		   }
		   if (sortOrder != null) {
			   order.append(" " + sortOrder);
		   }
	   } else {
		   order.append("ORDER BY " + Utilisateur.KEY_JSON_NOM + " ASC");
	   }
	   // rajout pour chaque order by
	   order.append(", " + Utilisateur.KEY_DB_ID + " ASC");

	   return order.toString();
   }

}
