//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.RapportDAO;
import fr.edu.vortex.management.utilisateur.pojo.Rapport;
import fr.edu.vortex.management.utilisateur.pojo.Trace;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;


public class RapportDAOImpl extends GenericDAOImpl implements RapportDAO{

	private static final Logger logger = LoggerFactory.getLogger(RapportDAOImpl.class);

	private static final String SELECT_RAPPORT = "" +
			"SELECT " +
			"    r.id, " +
			"    r.date_rapport, " +
			"    r.login, " +
			"    r.nom, " +
			"    r.prenom, " +
			"    r.type_rapport " +
			"FROM " +
			"    rapport r ";

	private static final String SELECT_RAPPORT_PAR_ID = "" +
			"SELECT " +
			"    r.id, " +
			"    r.date_rapport, " +
			"    r.login, " +
			"    r.nom, " +
			"    r.prenom, " +
			"    r.type_rapport, " +
			"    r.contenu " +
			"FROM " +
			"    rapport r WHERE 1=1 and r.id = ?";

	private static final String INSERT = "INSERT INTO rapport "
			+ "(date_rapport, login, nom, prenom, type_rapport, contenu) "
			+ "VALUES (?::timestamptz, ?, ?, ?, ?, ?::json) returning id";

	private static final String REQ_COUNT_ALL_RAPPORT = "select count(1) as nb_rapports from rapport r ";


	private static final String MAIN_TABLE = "rapport";

	@Override
	public Future<Rapport> creer(Rapport rapport) {
		Future<Rapport> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, INSERT);

		List<String> valeurs = new ArrayList<>();
		if (rapport.getDate_rapport() != null) {
			valeurs.add("'" + rapport.getDate_rapport().toString() + "'");
		} else {
			valeurs.add("''");
		}
		valeurs.add(rapport.getLogin());
		valeurs.add(rapport.getNom());
		valeurs.add(rapport.getPrenom());
		valeurs.add(rapport.getType_rapport());

		if (rapport.getContenu() != null) {
			valeurs.add(rapport.getContenu().toString());
		} else {
			valeurs.add("''");			
		}

		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, valeurs);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {

				if (!ar.result().containsKey(MAIN_TABLE)) {
					future.fail("résultat mal formaté");
					return;
				}

				JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
				if (rows.size() == 0) {
					future.fail("erreur lors de la création du rapport");
					return;
				}

				rapport.setId(rows.getJsonObject(0).getInteger("id"));

				future.complete(rapport);

			} else {
				future.fail(ar.cause());
			}
		});

		return future;
	}

	@Override
	public Future<List<Rapport>> listerRapport(int borneDebut, int borneFin, Map<String, Object> filters, String sort,
			String sortOrder) {
		String action = "listerRapport(int borneDebut, int borneFin, Map<String, String> parameters, String sort, String desc)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		Future<List<Rapport>> future = Future.future();

		int nbRapport = borneFin - borneDebut + 1;
		if (borneDebut >= 0 && borneFin >= 0 && nbRapport > 0) {
			// Construction de la requête
			String order = buildOrder(sort, sortOrder);

			JsonObject jsonObjectSelect = new JsonObject();
			JsonArray params = new JsonArray();

			StringBuffer sqlFilters = getRequete(borneDebut, filters, nbRapport, order, params);

			jsonObjectSelect.put("JsonArrayName", "rapports");
			jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
			jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

			envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
			.setHandler(ar -> {
				if (ar.succeeded()) {

					if (!ar.result().containsKey("rapports")) {
						future.fail("résultat mal formaté");
						return;
					}

					JsonArray rows = ar.result().getJsonArray("rapports");
					if (rows.size() == 0) {
						future.complete(new ArrayList<>());
						return;
					}

					List<Rapport> rapport = rows.stream()
							.map(o -> (JsonObject) o)
							.map(Rapport::fromJson)
							.collect(Collectors.toList());

					future.complete(rapport);

				} else {
					future.fail(ar.cause());
				}
			});
		}
		else {
			future.fail("Les valeurs renseignées dans les bornes de début et de fin sont invalides");
		}	

		return future;
	}


	public Future<Optional<Rapport>> chercherRapportByID(int id) {

		Future<Optional<Rapport>> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, SELECT_RAPPORT_PAR_ID);

		JsonArray params = new JsonArray();
		params.add(id);
		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
			if (!ar.succeeded()) {
				future.fail(ar.cause());
				return;
			}

			if (!ar.result().containsKey(MAIN_TABLE)) {
				future.fail("résultat mal formaté");
				return;
			}

			JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
			if (rows.size() == 0) {
				future.complete(Optional.empty());
				return;
			}

			if(rows.size()>=1) {
				List<Rapport> rapports = rows.stream()
						.map(o -> (JsonObject) o)
						.map(Rapport::fromJson)
						.collect(Collectors.toList());

				Rapport rapport = rapports.get(0);
				future.complete(Optional.of(rapport));
			}

		});

		return future;

	}

	@Override
	public Future<Integer> countRapport(Map<String, Object> filters) {
		String action = "countRapport";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<Integer> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
        JsonArray params = new JsonArray();

        StringBuffer sqlFilters = getRequeteCount(filters, params);

		jsonObjectSelect.put("JsonArrayName", "count");
        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
			if (ar.succeeded()) {
				Integer nbRapports = null;
				try {

					JsonObject allJson = ar.result();
					JsonArray arrayJson = allJson.getJsonArray("count");
					for (int i = 0; i < arrayJson.size(); i++) {
						JsonObject messageJson = arrayJson.getJsonObject(i);
						nbRapports = new Integer(messageJson.getInteger("nb_rapports"));
					}
				} catch (Exception e) {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
					future.fail(e.getMessage());
				}

				future.complete(nbRapports);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
				future.fail(ar.cause().getMessage());
			}
		});

		return future;
	}

	private void getFilters(Map<String, Object> filters, JsonArray params, StringBuffer sqlFilters) {
			sqlFilters.append(" WHERE 1=1 ");
			if (filters != null) {
				filters.forEach((property, value) -> {
				if (value != null) {
					if (property.equals(FluxVerticle.KEY_JSON_START_DATE_FILTER)) {
						sqlFilters.append(" AND " + "r." + Rapport.KEY_JSON_DATE_RAPPORT + " >= '" + value + "' ");
					} else if (property.equals(FluxVerticle.KEY_JSON_END_DATE_FILTER)) {
						sqlFilters.append(" AND " + "r." + Rapport.KEY_JSON_DATE_RAPPORT + " <= '" + value + "' ");
					} else if (property.equals(Rapport.KEY_JSON_TYPE_RAPPORT)) {
						String[] data = StringUtils.split((String) value, "-");
						if (data != null) {
							sqlFilters.append(" AND " +  property + " in (");
							boolean first = true;
							for (int i = 0; i < data.length; i++) {
								if (first) {
									first = false;
								} else {
									sqlFilters.append(",");
								}
								sqlFilters.append("'" + data[i] + "'");
							}
							sqlFilters.append(")");
						}
					} else {
						if (value.toString().startsWith("=")) {
							sqlFilters.append(" AND " + "upper(r." + property + ") = upper(?) ");
							params.add(value.toString().substring(1));						
						} else {
							sqlFilters.append(" AND " + "upper(r." + property + ") like upper(?) ");
							params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
						}
					}
				}
			});
		}
	}

	private String buildOrder(String sort, String sortOrder) {
		StringBuffer order = new StringBuffer();
		if (sort != null) {
			order.append(" ORDER BY ");
			order.append(sort);
			if (sortOrder != null) {
				order.append(" " + sortOrder);
			}
		} else {
			order.append(" ORDER BY " + Rapport.KEY_JSON_DATE_RAPPORT + " DESC");
		}
		// rajout pour chaque order by
		order.append(", " + Trace.KEY_JSON_ID + " ASC");
		return order.toString();
	}

	private StringBuffer getRequete(int borneDebut, Map<String, Object> filters, int nbFlux, String order, JsonArray params) {
		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append("(");
		sqlFilters.append(SELECT_RAPPORT);
		getFilters(filters, params, sqlFilters);
		sqlFilters.append(String.format(") %s ", order));
		sqlFilters.append(String.format(" limit %s offset %s", nbFlux, borneDebut));
		return sqlFilters;
	}

	private StringBuffer getRequeteCount(Map<String, Object> filters, JsonArray params) {
		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append(REQ_COUNT_ALL_RAPPORT);
		getFilters(filters, params, sqlFilters);
		return sqlFilters;
	}

}
