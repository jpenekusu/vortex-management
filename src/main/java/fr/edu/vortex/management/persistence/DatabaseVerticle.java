//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence;

import fr.edu.echanges.nat.vortex.common.adresses.persistence.PersistenceAdressesManagement;
import fr.edu.vortex.management.Constants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;

public class DatabaseVerticle extends AbstractVerticle {

    public static final String KEY_REQUETE = "requete";

    public static final String KEY_PARAMS = "params";

    private static final Logger logger = LoggerFactory.getLogger(DatabaseVerticle.class);

    public static final String SELECT_PARAMS = "vortex.management.persistence.selectWithParams";
    
    public static final String DELETE_PARAMS = "vortex.management.persistence.deleteWithParams";

    private JDBCClient client;

    @Override
    public void start(Future<Void> startFuture) {
        try {
            client = JDBCClient.createShared(vertx, config());
            vertx.eventBus().consumer(PersistenceAdressesManagement.SELECT).handler(this::selectHandler);
            vertx.eventBus().consumer(PersistenceAdressesManagement.INSERT)
                    .handler(this::insertOrDeleteOrUpdateHandler);
            vertx.eventBus().consumer(PersistenceAdressesManagement.DELETE)
                    .handler(this::insertOrDeleteOrUpdateHandler);
            vertx.eventBus().consumer(PersistenceAdressesManagement.UPDATE)
                    .handler(this::insertOrDeleteOrUpdateHandler);
            vertx.eventBus().consumer(SELECT_PARAMS)
                    .handler(this::selectHandlerWithParameters);
            vertx.eventBus().consumer(DELETE_PARAMS)
            .handler(this::deleteWithParameters);            

            startFuture.complete();
        } catch (Exception e) {
            startFuture.fail(e);
        }

    }

    /**
     * Effectue un ordre SELECT en BD
     * 
     * @param message
     */
    public void selectHandler(Message<Object> message) {
        JsonObject requeteJSON = (JsonObject) message.body();
        String requeteAExecuter = requeteJSON.getString(KEY_REQUETE);
        client.getConnection(handler -> {
            if (handler.succeeded()) {
                SQLConnection connection = handler.result();
                // String action = "selectHandler - requete :";
                logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "selectHandler - requete :",
                        requeteAExecuter);
                logger.info(requeteAExecuter);
                connection.query(requeteAExecuter, handlerQuery -> {
                    if (handlerQuery.succeeded()) {
                        JsonArray rows = handlerQuery.result().toJson().getJsonArray("rows");
                        // action = "selectHandler - rows :";
                        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "selectHandler - rows :",
                                rows.encode());

                        JsonObject jsonObj = new JsonObject();
                        jsonObj.put(requeteJSON.getString("JsonArrayName"), rows);
                        connection.close();
                        message.reply(jsonObj);
                    } else {
                        connection.close();
                        message.fail(404, "erreur lors du select :" + requeteAExecuter);
                    }
                });
            } else {
                message.fail(404, handler.cause().getMessage());
            }
        });
    }

    /**
     * Effectue un ordre INSERT, DELETE ou UPDATE en BD
     * 
     * @param message
     */
    public void insertOrDeleteOrUpdateHandler(Message<Object> message) {
        JsonObject requeteJSON = (JsonObject) message.body();
        String requeteAExecuter = requeteJSON.getString(KEY_REQUETE);
        String action = "insertOrDeleteOrUpdateHandler - requete :";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action, requeteAExecuter);

        client.getConnection(handler -> {
            if (handler.succeeded()) {
                SQLConnection connection = handler.result();
                connection.update(requeteAExecuter, handlerQuery -> {
                    if (handlerQuery.succeeded()) {
                        connection.close();
                        JsonObject json = handlerQuery.result().toJson();
                        message.reply(json);
                    } else {
                        handlerQuery.cause().printStackTrace();
                        message.fail(404, "Erreur lors de l'execution de la requete  :" + requeteAExecuter);
                    }
                });
            } else {
                message.fail(404, handler.cause().getMessage());
            }
        });
    }
    
    /**
     * Effectue un ordre DELETE en BD avec des paramètres dans l'objet JSON
     * 
     * @param message
     */
    public void deleteWithParameters(Message<Object> message) {
    	JsonObject requeteJSON = (JsonObject) message.body();
        String sql = requeteJSON.getString(KEY_REQUETE);
        JsonArray params;
        if (requeteJSON.containsKey(KEY_PARAMS) && !requeteJSON.getJsonArray(KEY_PARAMS).isEmpty()) {
            params = requeteJSON.getJsonArray(KEY_PARAMS);
        } else {
            params = new JsonArray();
        }

        logger.debug(Constants.LOG_SQL_EXEC_INCOMING, Constants.LOG_NO_CORRELATION_ID, sql, params);

        client.getConnection(handler -> {
            if (handler.succeeded()) {
                SQLConnection connection = handler.result();
                connection.updateWithParams(sql, params, handlerQuery -> {
                    if (handlerQuery.succeeded()) {
                        connection.close();
                        JsonObject json = handlerQuery.result().toJson();
                        message.reply(json);
                    } else {
                        logger.error(
                                Constants.LOG_ERROR,
                                Constants.LOG_NO_CORRELATION_ID,
                                handlerQuery.cause().getMessage(),
                                "ERR_MANAGEMENT_TECH");
                        message.fail(500, "erreur lors du delete :" + sql);
                    }
                });
            } else {
                message.fail(500, handler.cause().getMessage());
            }
        });
    }

    /**
     * Effectue un ordre SELECT en BD
     * 
     * @param message
     */
    public void selectHandlerWithParameters(Message<Object> message) {

        JsonObject requeteJSON = (JsonObject) message.body();

        String sql = requeteJSON.getString(KEY_REQUETE);
        JsonArray params;
        if (requeteJSON.containsKey(KEY_PARAMS) && !requeteJSON.getJsonArray(KEY_PARAMS).isEmpty()) {
            params = requeteJSON.getJsonArray(KEY_PARAMS);
        } else {
            params = new JsonArray();
        }
        logger.debug(Constants.LOG_SQL_EXEC_INCOMING, Constants.LOG_NO_CORRELATION_ID, sql, params);

        client.getConnection(handler -> {
            if (handler.succeeded()) {

                SQLConnection connection = handler.result();

                connection.queryWithParams(sql, params, handlerQuery -> {
                    connection.close();
                    if (handlerQuery.succeeded()) {

                        JsonArray rows;
                        if (handlerQuery.result() != null && handlerQuery.result().toJson().containsKey("rows")) {
                            rows = handlerQuery.result().toJson().getJsonArray("rows");
                        } else {
                            rows = new JsonArray();
                        }

                        logger.debug(
                                Constants.LOG_SQL_EXEC_RESULT,
                                Constants.LOG_NO_CORRELATION_ID,
                                rows);

                        JsonObject jsonObj = new JsonObject();
                        jsonObj.put(requeteJSON.getString("JsonArrayName"), rows);
                        message.reply(jsonObj);
                    } else {
                        logger.error(
                                Constants.LOG_ERROR,
                                Constants.LOG_NO_CORRELATION_ID,
                                handlerQuery.cause().getMessage(),
                                "ERR_MANAGEMENT_TECH");
                        message.fail(500, "erreur lors du select :" + sql);
                    }
                });
            } else {
                message.fail(500, handler.cause().getMessage());
            }
        });
    }

}
