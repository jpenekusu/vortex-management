package fr.edu.vortex.management.persistence.dao;

import java.util.List;
import java.util.Map;

import fr.edu.vortex.management.utilisateur.pojo.Filtre;
import io.vertx.core.Future;

public interface FiltreDAO extends GenericDAO {

    Future<Filtre> creer(Filtre filtre);
    
    Future<Filtre> modifier(Filtre Filtre);

    Future<List<Filtre>> lister(Map<String, Object> eqFilters);

    Future<Integer> delete(int id);
    
    Future<Integer> supprimerParUtilisateur(int utilisateurId);

}
