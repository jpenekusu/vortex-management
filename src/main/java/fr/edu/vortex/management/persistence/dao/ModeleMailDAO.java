package fr.edu.vortex.management.persistence.dao;

import java.util.List;
import java.util.Optional;

import fr.edu.vortex.management.mail.ModeleMail;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

public interface ModeleMailDAO extends GenericDAO {

    Future<ModeleMail> creer(ModeleMail modeleMail);
    
    Future<ModeleMail> modifier(ModeleMail modeleMail);

    Future<Integer> delete(int id);

    Future<Optional<ModeleMail>> chercherModeleMailByID(int id);

    Future<List<ModeleMail>> chercherModeleMailParNom(String nom);

    Future<List<ModeleMail>> listerModeles(int borneDebut, int borneFin, JsonObject filtres, String sort,
            String sortOrder);
	
	Future<Integer> countModeles(JsonObject filtres);

}
