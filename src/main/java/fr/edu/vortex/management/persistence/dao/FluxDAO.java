//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao;

import java.util.List;
import java.util.Map;

import fr.edu.vortex.management.flux.Flux;
import fr.edu.vortex.management.flux.StatusEnum;
import fr.edu.vortex.management.flux.StatusHistory;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

public interface FluxDAO extends GenericDAO {

    Future<List<Flux>> loadFlux(int borneDebut, int borneFin, Map<String, Object> likeFilters, String sort,
            String sortOrder);

    Future<List<Flux>> getFluxByCorrelationId(String correlationId);

    Future<Flux> getFluxByConsumerCorrelationId(String consumerCorrelationId);

    Future<List<Flux>> getFluxByCorrelationIdAndConsumerCorrelationId(String correlationId,
            String consumerCorrelationId);

    Future<Integer> countFlux(Map<String, Object> likeFilters);

    Future<Flux> save(Flux flux);

    Future<Void> delete(String correlationId);
    
    Future<Void> deleteByCorrelationIdAndStatus(String correlationId, StatusEnum status);
    
    Future<List<StatusHistory>> loadStatusHistoryFluxByCorrelationId(String correlationId);

    Future<List<StatusHistory>> loadAllStatusHistoryFluxByCorrelationsId(String correlationId,
            String consumerCorrelationId);
    
    Future<List<JsonObject>> getRepartition(Map<String, Object> likeFilters);

	Future<List<Flux>> getFluxByCorrelationIdAndStatus(String correlationId, String status);

	Future<Void> deleteByConsumerCorrelationIdAndStatusGreaterThanOrEqual(String consumerCorrelationId,
			StatusEnum status);
	
	Future<List<String>> loadFluxLot(Map<String, Object> likeFilters);

	
}
