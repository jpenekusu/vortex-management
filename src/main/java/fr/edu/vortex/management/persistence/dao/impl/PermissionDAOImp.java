//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.PermissionDAO;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class PermissionDAOImp extends GenericDAOImpl implements PermissionDAO {

    private static final String SELECT_ALL = "SELECT * FROM permission ORDER BY libelle ASC";

    @Override
    public Future<List<Permission>> lister() {
        Future<List<Permission>> future = Future.future();

        JsonObject jsonObjectSelect = new JsonObject();
        jsonObjectSelect.put("JsonArrayName", "permission");

        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, SELECT_ALL);

        JsonArray params = new JsonArray();
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
                .setHandler(ar -> {
                    if (ar.succeeded()) {

                        if (!ar.result().containsKey("permission")) {
                            future.fail("résultat mal formaté");
                            return;
                        }

                        JsonArray rows = ar.result().getJsonArray("permission");
                        if (rows.size() == 0) {
                            future.complete(new ArrayList<>());
                            return;
                        }

                        List<Permission> perms = rows.stream()
                                .map(o -> (JsonObject) o)
                                .map(Permission::fromJson)
                                .collect(Collectors.toList());

                        future.complete(perms);

                    } else {
                        future.fail(ar.cause());
                    }
                });

        return future;
    }

}
