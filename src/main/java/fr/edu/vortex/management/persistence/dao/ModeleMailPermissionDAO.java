package fr.edu.vortex.management.persistence.dao;

import io.vertx.core.Future;

public interface ModeleMailPermissionDAO extends GenericDAO {

    Future<Integer> delete(int modeleId);

    Future<Void> create(int modeleMailId, String permissionCode);

}
