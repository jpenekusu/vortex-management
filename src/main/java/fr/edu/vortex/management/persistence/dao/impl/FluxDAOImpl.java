//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import fr.edu.echanges.nat.vortex.common.adresses.persistence.PersistenceAdressesManagement;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.api.DashboardApi;
import fr.edu.vortex.management.flux.Flux;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.flux.StatusEnum;
import fr.edu.vortex.management.flux.StatusHistory;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.FluxDAO;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import net.logstash.logback.encoder.org.apache.commons.lang.StringEscapeUtils;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

/**
 * Une implementation NOP( No operation) du service de persistence.
 *
 */

public class FluxDAOImpl extends GenericDAOImpl implements FluxDAO {

	private static final Logger logger = LoggerFactory.getLogger(FluxDAOImpl.class);

	static final String TABLE_FLUX = "flux";

	private static final String DELETE_FLUX_BY_CORRELATION_ID = "DELETE FROM flux WHERE correlation_id = '%s' ";
	
	private static final String DELETE_FLUX_BY_CORRELATION_ID_AND_STATUS = DELETE_FLUX_BY_CORRELATION_ID + " AND status = '%s'";
		
	private static final String DELETE_FLUX_BY_CONSUMER_CORRELATION_ID_AND_STATUS_ORDER_GREATER_THAN_OU_EQUAL = "DELETE FROM flux WHERE consumer_correlation_id = '%s'  AND status_order >= %s";

	static final String REQ_SELECT_ALL_FLUX = "SELECT id, correlation_id, status, status_order, date, producer, exchange_name, routing_key, consumer_correlation_id, consumer_business_id, consumer_queue_name, file_name, file_size, file_url, error_message, stacktrace, error_code "
			+ " FROM flux WHERE 1=1 ";
	
	static final String REQ_SELECT_ALL_FLUX_UNIQUE = "SELECT id, correlation_id, status, status_order, date, producer, exchange_name, routing_key, consumer_correlation_id, consumer_business_id, consumer_queue_name, file_name, file_size, file_url, error_message, stacktrace, error_code "
			+ " FROM flux_unique f WHERE 1=1 ";

	static final String REQ_SELECT_FLUX_UNIQUE = "SELECT id, correlation_id, status, status_order, date, producer, exchange_name, routing_key, consumer_correlation_id, consumer_business_id, consumer_queue_name, file_name, file_size, file_url, error_message, stacktrace, error_code "
			+ " FROM flux_unique f ";

	static final String REQ_SELECT_FLUX_UNIQUE_BY_CORRELATION_ID = REQ_SELECT_ALL_FLUX_UNIQUE + " and correlation_id= '%s' ";
	
	static final String REQ_SELECT_FLUX_BY_CORRELATION_ID_AND_STATUS = REQ_SELECT_ALL_FLUX  + " and correlation_id= '%s' and status = '%s' ";

	static final String REQ_SELECT_FLUX_UNIQUE_BY_CONSUMER_CORRELATION_ID = REQ_SELECT_ALL_FLUX_UNIQUE
			+ " and consumer_correlation_id= '%s'";

	static final String REQ_SELECT_FLUX_UNIQUE_BY_CORRELATION_ID_AND_CONSUMER_CORRELATION_ID = REQ_SELECT_ALL_FLUX_UNIQUE
			+ " and correlation_id= ? and consumer_correlation_id like ?";

	static final String REQ_COUNT_FLUX_UNIQUE = "select count(1) as nb_flux from flux_unique f ";

	static final String REQ_SELECT_FLUX_HISTORY_BY_CORRELATION_ID = "SELECT status, status_order, date from flux  where correlation_id= ? and consumer_correlation_id is null order by status_order";
	static final String REQ_SELECT_FLUX_HISTORY_BY_CORRELATIONS_ID = "(SELECT status, status_order, date from flux  where correlation_id= ? and consumer_correlation_id is null UNION select status, status_order, date from flux  where correlation_id= ? and consumer_correlation_id like ? ) order by status_order";

    static final String REQ_REPARTITION = "select count(f.id), f.status from management.flux_unique f where 1=1";

	static final String REQ_SELECT_LOT_FLUX_UNIQUE = "SELECT correlation_id, file_name FROM flux_unique f WHERE 1=1 ";

	@Override
	public Future<Flux> save(Flux flux) {
		String action = "save (Flux flux)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<Flux> future = Future.future();

		StringBuilder requete = new StringBuilder();
		requete.append("INSERT INTO ").append(TABLE_FLUX).append(" (");

		StringBuilder values = new StringBuilder(" VALUES (");

		JsonArray params = new JsonArray();

		requete.append(Flux.KEY_DB_CORRELATION_ID);
		values.append("?");
		params.add(flux.getCorrelationId());

		requete.append("," + Flux.KEY_DB_STATUS);
		values.append(",?");
		params.add(flux.getStatus().getValue());

		requete.append("," + Flux.KEY_DB_STATUS_ORDER);
		values.append(",?");
		params.add(flux.getStatus().getOrder());

		requete.append("," + Flux.KEY_DB_DATE);
		values.append(",?");
		params.add(flux.getDate());

		requete.append("," + Flux.KEY_DB_PRODUCER);
		values.append(",?");
		params.add(flux.getProducer());

		if (flux.getExchangeName() != null) {
			requete.append("," + Flux.KEY_DB_EXCHANGE_NAME);
			values.append(",?");
			params.add(flux.getExchangeName());
		}

		if (flux.getRoutingKey() != null) {
			requete.append("," + Flux.KEY_DB_ROUTING_KEY);
			values.append(",?");
			params.add(flux.getRoutingKey());
		}

		if (flux.getConsumerCorrelationId() != null) {
			requete.append("," + Flux.KEY_DB_CONSUMER_CORRELATION_ID);
			values.append(",?");
			params.add(flux.getConsumerCorrelationId());
		}

		if (flux.getConsumerBusinessId() != null) {
			requete.append("," + Flux.KEY_DB_CONSUMER_BUSINESS_ID);
			values.append(",?");
			params.add(flux.getConsumerBusinessId());
		}

		if (flux.getConsumerQueueName() != null) {
			requete.append("," + Flux.KEY_DB_CONSUMER_QUEUE_NAME);
			values.append(",?");
			params.add(flux.getConsumerQueueName());
		}

		if (flux.getFilename() != null) {
			requete.append("," + Flux.KEY_DB_FILENAME);
			values.append(",?");
			params.add(flux.getFilename());
		}

		if (flux.getFilesize() != null) {
			requete.append("," + Flux.KEY_DB_FILESIZE);
			values.append(",?");
			params.add(flux.getFilesize());
		}

		if (flux.getFileUrl() != null) {
			requete.append("," + Flux.KEY_DB_FILE_URL);
			values.append(",?");
			params.add(flux.getFileUrl());
		}

		if (flux.getErrorCode() != null) {
			requete.append("," + Flux.KEY_DB_ERROR_CODE);
			values.append(",?");
			params.add(StringEscapeUtils.escapeSql(flux.getErrorCode()));
		}

		if (flux.getErrorMessage() != null) {
			requete.append("," + Flux.KEY_DB_ERROR_MESSAGE);
			values.append(",?");
			params.add(StringEscapeUtils.escapeSql(flux.getErrorMessage()));
		}

		if (flux.getStacktrace() != null) {
			requete.append("," + Flux.KEY_DB_STACKTRACE);
			values.append(",?");
			params.add(StringEscapeUtils.escapeSql(flux.getStacktrace()));
		}

		values.append(") returning *");

		requete.append(") ").append(values.toString());

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", TABLE_FLUX);
		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, requete.toString());
		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {		
			if (ar.succeeded()) {
				Flux newFlux = null;
				try {
					JsonArray keys = ((JsonObject) ar.result()).getJsonArray(TABLE_FLUX);
					if (keys !=  null && keys.getJsonObject(0) != null) {
						newFlux = jsonObjectToFlux(keys.getJsonObject(0));
					}
				} catch (Exception e) {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
					future.fail(e.getMessage());
				}
				future.complete(newFlux);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
				future.fail(ar.cause().getMessage());
			}
		});

		return future;
	}


	@Override
	public Future<List<Flux>> getFluxByCorrelationId(String correlationId) {
		String action = "getFluxByCorrelationId(String correlationId)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<List<Flux>> future = Future.future();

		if (correlationId != null) {
			JsonObject jsonObjectSelect = new JsonObject();
			jsonObjectSelect.put("JsonArrayName", "flux");
			jsonObjectSelect.put("requete",
					String.format(REQ_SELECT_FLUX_UNIQUE_BY_CORRELATION_ID, correlationId));

			envoiRequeteAuVerticleBD(PersistenceAdressesManagement.SELECT, jsonObjectSelect).setHandler(ar -> {
				if (ar.succeeded()) {
					List<Flux> fluxList = new ArrayList<>();
					try {
						JsonObject allFluxJson = ar.result();
						JsonArray fluxArrayJson = allFluxJson.getJsonArray("flux");
						for (int i = 0; i < fluxArrayJson.size(); i++) {
							fluxList.add(jsonObjectToFlux(fluxArrayJson.getJsonObject(i)));
						}

					} catch (Exception e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						future.fail(e.getMessage());
					}
					future.complete(fluxList);
				} else {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
					future.fail(ar.cause().getMessage());
				}
			});
		}

		else {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
					"La valeur renseignée pour le correlation_id est invalide");
			future.fail("La valeur renseignée pour le correlation_id est invalide");
		}

		return future;
	}

	@Override
	public Future<List<Flux>> getFluxByCorrelationIdAndStatus(String correlationId, String status) {
		String action = "getFluxByCorrelationIdAndStatus";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<List<Flux>> future = Future.future();

		if (correlationId != null) {
			JsonObject jsonObjectSelect = new JsonObject();
			jsonObjectSelect.put("JsonArrayName", "flux");
			jsonObjectSelect.put("requete",
					String.format(REQ_SELECT_FLUX_BY_CORRELATION_ID_AND_STATUS, correlationId, status));
			
			envoiRequeteAuVerticleBD(PersistenceAdressesManagement.SELECT, jsonObjectSelect).setHandler(ar -> {
				if (ar.succeeded()) {
					List<Flux> fluxList = new ArrayList<>();
					try {
						JsonObject allFluxJson = ar.result();
						JsonArray fluxArrayJson = allFluxJson.getJsonArray("flux");
						for (int i = 0; i < fluxArrayJson.size(); i++) {
							fluxList.add(jsonObjectToFlux(fluxArrayJson.getJsonObject(i)));
						}

					} catch (Exception e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						future.fail(e.getMessage());
					}
					future.complete(fluxList);
				} else {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
					future.fail(ar.cause().getMessage());
				}
			});
		}

		else {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
					"La valeur renseignée pour le correlation_id est invalide");
			future.fail("La valeur renseignée pour le correlation_id est invalide");
		}

		return future;
	}
	
	@Override
	public Future<Flux> getFluxByConsumerCorrelationId(String consumerCorrelationId) {
		String action = "getFluxByConsumerCorrelationId(String consumerCorrelationId)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<Flux> future = Future.future();

		if (consumerCorrelationId != null) {
			JsonObject jsonObjectSelect = new JsonObject();
			jsonObjectSelect.put("JsonArrayName", "flux");

			jsonObjectSelect.put("requete",
					String.format(REQ_SELECT_FLUX_UNIQUE_BY_CONSUMER_CORRELATION_ID, consumerCorrelationId));

			envoiRequeteAuVerticleBD(PersistenceAdressesManagement.SELECT, jsonObjectSelect).setHandler(ar -> {
				if (ar.succeeded()) {
					Flux flux = null;
					try {
						JsonObject allFluxJson = ar.result();
						JsonArray fluxArrayJson = allFluxJson.getJsonArray("flux");
						if (fluxArrayJson != null) {
							if (fluxArrayJson.size() == 1) {
								flux = jsonObjectToFlux(fluxArrayJson.getJsonObject(0));
							} else {
								if (fluxArrayJson.size() > 1) {
									future.fail("Le nombre de résultat retourné est incorrect.");
								}
							}
						}

					} catch (Exception e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						future.fail(e.getMessage());
					}
					future.complete(flux);
				} else {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
					future.fail(ar.cause().getMessage());
				}
			});
		}

		else {
			future.fail("La valeur renseignée du correlation_id du flux est invalide");
		}

		return future;
	}

	@Override
	public Future<List<Flux>> getFluxByCorrelationIdAndConsumerCorrelationId(String correlationId,
			String consumerCorrelationId) {
		String action = "getFluxByCorrelationIdAndConsumerCorrelationId(String correlationId, String consumerCorrelationId)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<List<Flux>> future = Future.future();

		if (correlationId != null && consumerCorrelationId != null) {
			JsonObject jsonObjectSelect = new JsonObject();
			jsonObjectSelect.put("JsonArrayName", "flux");

			jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE,
					REQ_SELECT_FLUX_UNIQUE_BY_CORRELATION_ID_AND_CONSUMER_CORRELATION_ID);

            JsonArray params = new JsonArray()
                    .add(correlationId)
                    .add('%' + consumerCorrelationId + '%');
            jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

			envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
				if (ar.succeeded()) {
					List<Flux> fluxList = new ArrayList<>();
					try {
						JsonObject allFluxJson = ar.result();
						JsonArray fluxArrayJson = allFluxJson.getJsonArray("flux");
						for (int i = 0; i < fluxArrayJson.size(); i++) {
							fluxList.add(jsonObjectToFlux(fluxArrayJson.getJsonObject(i)));
						}

					} catch (Exception e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						future.fail(e.getMessage());
					}
					future.complete(fluxList);
				} else {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
					future.fail(ar.cause().getMessage());
				}
			});
		} else {
			future.fail("La valeur renseignée du correlation_id du flux est invalide");
		}

		return future;
	}

	@Override
	public Future<List<Flux>> loadFlux(int borneDebut, int borneFin, Map<String, Object> filters, String sort,
			String sortOrder) {
		String action = "loadFlux(int borneDebut, int borneFin, Map<String, String> parameters, String sort, String desc)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<List<Flux>> future = Future.future();

		int nbFlux = borneFin - borneDebut + 1;
		if (borneDebut >= 0 && borneFin >= 0 && nbFlux > 0) {
			
			// Construction de la requête
			String order = buildOrder(sort, sortOrder);

			JsonObject jsonObjectSelect = new JsonObject();
			JsonArray params = new JsonArray();

			StringBuffer sqlFilters = getRequete(borneDebut, filters, nbFlux, order, params);

			jsonObjectSelect.put("JsonArrayName", "flux");
			jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
			jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

			List<Flux> allFlux = new ArrayList<Flux>();

			envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
				if (ar.succeeded()) {
					try {
						JsonObject allFluxJson = ar.result();
						JsonArray fluxArrayJson = allFluxJson.getJsonArray("flux");

						for (int i = 0; i < fluxArrayJson.size(); i++) {
							JsonObject messageJson = fluxArrayJson.getJsonObject(i);
							allFlux.add(jsonObjectToFlux(messageJson));
						}
					} catch (Exception e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						future.fail(e.getMessage());
					}
					future.complete(allFlux);
				} else {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
					future.fail(ar.cause().getMessage());
				}
			});
		}

		else {
			future.fail("Les valeurs renseignées dans les bornes de début et de fin sont invalides");
		}

		return future;
	}
	
	public Future<List<String>> loadFluxLot(Map<String, Object> likeFilters) {
		String action = "loadFluxLot(Map<String, String> parameters)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<List<String>> future = Future.future();


		// Construction de la requête
		String sqlFilters = buildFilters(likeFilters);

		StringBuffer requete = new StringBuffer();
		requete.append(String.format(REQ_SELECT_ALL_FLUX_UNIQUE + " %s", sqlFilters));

		List<String> allFlux = new ArrayList<String>();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", "flux");
		jsonObjectSelect.put("requete", requete);

		envoiRequeteAuVerticleBD(PersistenceAdressesManagement.SELECT, jsonObjectSelect).setHandler(ar -> {
			if (ar.succeeded()) {
				try {
					JsonObject allFluxJson = ar.result();
					JsonArray fluxArrayJson = allFluxJson.getJsonArray("flux");

					for (int i = 0; i < fluxArrayJson.size(); i++) {
						JsonObject messageJson = fluxArrayJson.getJsonObject(i);
						allFlux.add(messageJson.getString("correlation_id"));
						allFlux.add(messageJson.getString("file_name"));
					}
				} catch (Exception e) {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
					future.fail(e.getMessage());
				}
				future.complete(allFlux);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
				future.fail(ar.cause().getMessage());
			}
		});


		return future;
	}


	private String buildFilters(Map<String, Object> filters) {
		StringBuffer sqlFilters = new StringBuffer();
		if (filters != null) {
			filters.forEach((property, value) -> {
				if (value != null) {
					if (property.equals(FluxVerticle.KEY_JSON_START_DATE_FILTER)) {
						sqlFilters.append(" AND " + Flux.KEY_DB_DATE + " >= '" + value + "' ");
					} else if (property.equals(FluxVerticle.KEY_JSON_END_DATE_FILTER)) {
						sqlFilters.append(" AND " + Flux.KEY_DB_DATE + " <= '" + value + "' ");
					} else if (property.equals(Flux.KEY_JSON_STATUS)) {
						String[] data = StringUtils.split((String) value, "-");
						if (data != null) {
							sqlFilters.append(" AND " +  property + " in (");
							boolean first = true;
							for (int i = 0; i < data.length; i++) {
								if (first) {
									first = false;
								} else {
									sqlFilters.append(",");
								}
								sqlFilters.append("'" + data[i] + "'");
							}
							sqlFilters.append(")");
						}
					} else if (property.equals(Flux.KEY_JSON_LIST_CORRELATION_ID) 
							|| property.equals(Flux.KEY_JSON_LIST_FILENAME)) {
						String[] data = StringUtils.split((String) value, ",");
						if (data != null) {
							if (property.equals(Flux.KEY_JSON_LIST_CORRELATION_ID)) {
								sqlFilters.append(" AND " +  Flux.KEY_DB_CORRELATION_ID + " in (");							
							} else if (property.equals(Flux.KEY_JSON_LIST_FILENAME)) {
								sqlFilters.append(" AND " +  Flux.KEY_DB_FILENAME + " in (");							
							}
							boolean first = true;
							for (int i = 0; i < data.length; i++) {
								if (first) {
									first = false;
								} else {
									sqlFilters.append(",");
								}
								sqlFilters.append("'" + data[i] + "'");
							}
							sqlFilters.append(")");
						}
					} else {
						if (value.toString().startsWith("=")) {
							sqlFilters.append(" AND " + "upper(" + property + ") = upper('" + value.toString().substring(1) + "') ");							
						} else {
							sqlFilters.append(" AND " + "upper(" + property + ") like upper('%" + value + "%') ");
						}
					}

                }
            });
        }
        return sqlFilters.toString();
    }
    
    private String buildEqualFilters(Map<String, Object> filters) {
        StringBuffer sqlFilters = new StringBuffer();
        if (filters != null) {
            filters.forEach((property, value) -> {
                if (value != null) {
                    if (property.equals(FluxVerticle.KEY_JSON_START_DATE_FILTER)) {
                        sqlFilters.append(" AND " + "f." + Flux.KEY_DB_DATE + " >= '" + value + "' ");
                    } else if (property.equals(FluxVerticle.KEY_JSON_END_DATE_FILTER)) {
                        sqlFilters.append(" AND " + "f." + Flux.KEY_DB_DATE + " <= '" + value + "' ");
                    } else if (property.equals(DashboardApi.KEY_JSON_PARTENAIRE)) {
                        sqlFilters.append(" AND (");
                        sqlFilters.append("f." + Flux.KEY_JSON_CONSUMER_BUSINESS_ID + " = '" + value + "'");
                        sqlFilters.append(" OR ");
                        sqlFilters.append("f." + Flux.KEY_JSON_PRODUCER + " = '" + value + "'");
                        sqlFilters.append(")");
                    } else if (property.equals(Flux.KEY_JSON_STATUS)) {
                        String[] data = StringUtils.split((String) value, "-");
                        if (data != null) {
                            sqlFilters.append(" AND " + "f." + property + " in (");
                            boolean first = true;
                            for (int i = 0; i < data.length; i++) {
                                if (first) {
                                    first = false;
                                } else {
                                    sqlFilters.append(",");
                                }
                                sqlFilters.append("'" + data[i] + "'");
                            }
                            sqlFilters.append(")");
                        }
                    } else {
                        sqlFilters.append(" AND " + "f." + property + " = '" + value + "'");
                    }

                }
            });
        }
        return sqlFilters.toString();
    }

    private String buildOrder(String sort, String sortOrder) {
        StringBuffer order = new StringBuffer();
        if (sort != null) {
            order.append(" ORDER BY ");
            order.append(sort);
            if (sortOrder != null) {
                order.append(" " + sortOrder);
            }
        } else {
            order.append(" ORDER BY " + Flux.KEY_DB_DATE + " DESC");
        }
        // rajout pour chaque order by
        order.append(", " + Flux.KEY_DB_ID + " ASC");

        return order.toString();
    }

	private Flux jsonObjectToFlux(JsonObject messageJson) {
		Flux flux = new Flux(messageJson.getInteger(Flux.KEY_DB_ID), messageJson.getString(Flux.KEY_DB_CORRELATION_ID),
				StatusEnum.valueOf(messageJson.getString(Flux.KEY_DB_STATUS)),
				Instant.from(messageJson.getInstant(Flux.KEY_DB_DATE)), messageJson.getString(Flux.KEY_DB_PRODUCER),
				messageJson.getString(Flux.KEY_DB_EXCHANGE_NAME), messageJson.getString(Flux.KEY_DB_ROUTING_KEY),
				messageJson.getString(Flux.KEY_DB_CONSUMER_CORRELATION_ID),
				messageJson.getString(Flux.KEY_DB_CONSUMER_BUSINESS_ID),
				messageJson.getString(Flux.KEY_DB_CONSUMER_QUEUE_NAME), messageJson.getString(Flux.KEY_DB_FILENAME),
				messageJson.getLong(Flux.KEY_DB_FILESIZE), messageJson.getString(Flux.KEY_DB_FILE_URL),
				messageJson.getString(Flux.KEY_DB_ERROR_MESSAGE), messageJson.getString(Flux.KEY_DB_STACKTRACE),
				messageJson.getString(Flux.KEY_DB_ERROR_CODE));

		return flux;
	}

	@Override
	public Future<Integer> countFlux(Map<String, Object> filters) {
		String action = "countFlux()";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<Integer> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
        JsonArray params = new JsonArray();

        StringBuffer sqlFilters = getRequeteCount(filters, params);

		jsonObjectSelect.put("JsonArrayName", "count");
        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
          if (ar.succeeded()) {
                Integer nbFlux = null;
                try {

					JsonObject allQueuesJson = ar.result();
					JsonArray queuesArrayJson = allQueuesJson.getJsonArray("count");
					for (int i = 0; i < queuesArrayJson.size(); i++) {
						JsonObject messageJson = queuesArrayJson.getJsonObject(i);
						nbFlux = new Integer(messageJson.getInteger("nb_flux"));
					}
				} catch (Exception e) {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
					future.fail(e.getMessage());
				}

				future.complete(nbFlux);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
				future.fail(ar.cause().getMessage());
			}
		});

		return future;
	}

	@Override
	public Future<List<StatusHistory>> loadStatusHistoryFluxByCorrelationId(String correlationId) {
		String action = "loadStatusHistoryFluxByCorrelationId";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		return getStatusHistoryByCorrelationsId(correlationId, null);
	}

	@Override
	public Future<List<StatusHistory>> loadAllStatusHistoryFluxByCorrelationsId(String correlationId,
			String consumerCorrelationId) {
		String action = "loadAllStatusHistoryFluxByCorrelationsId";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		return getStatusHistoryByCorrelationsId(correlationId, consumerCorrelationId);
	}

	private Future<List<StatusHistory>> getStatusHistoryByCorrelationsId(String correlationId,
			String consumerCorrelationId) {
		String action = "getStatusHistoryByCorrelationsId(String correlationId, String consumerCorrelationId)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<List<StatusHistory>> future = Future.future();

		if (correlationId != null) {
			JsonObject jsonObjectSelect = new JsonObject();
			jsonObjectSelect.put("JsonArrayName", "statusHistory");

            JsonArray params = new JsonArray()
                    .add(correlationId);

			if (consumerCorrelationId == null) {
				jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE,
						String.format(REQ_SELECT_FLUX_HISTORY_BY_CORRELATION_ID));
			} else {
				jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE,
						String.format(REQ_SELECT_FLUX_HISTORY_BY_CORRELATIONS_ID));
				params.add(correlationId);
				params.add('%' + consumerCorrelationId + '%');
			}

			jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

			envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
				if (ar.succeeded()) {
					List<StatusHistory> statusHistoryList = new ArrayList<StatusHistory>();
					try {
						JsonArray statusHistoryArray = ((JsonObject) ar.result()).getJsonArray("statusHistory");
						for (int i = 0; i < statusHistoryArray.size(); i++) {
							JsonObject jsonObject = statusHistoryArray.getJsonObject(i);
							Instant strToDate = Instant.parse(jsonObject.getString(StatusHistory.KEY_DB_DATE));
							Date date = Date.from(strToDate);

							StatusHistory statusHistory = new StatusHistory(
									StatusEnum.valueOf(jsonObject.getString(StatusHistory.KEY_DB_STATUS)), date);
							statusHistoryList.add(statusHistory);
						}

					} catch (Exception e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						future.fail(e.getMessage());
					}
					future.complete(statusHistoryList);
				} else {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
					future.fail(ar.cause().getMessage());
				}
			});
		}

		else {
			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
					"La valeur renseignée de l'ID est invalide");
			future.fail("La valeur renseignée de l'ID est invalide");
		}

		return future;
	}

	@Override
    public Future<Void> delete(String correlationId) {

        String action = "delete(String correlationId)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        Future<Void> future = Future.future();

        if (correlationId != null) {
            JsonObject jsonObjectDelete = new JsonObject();
            jsonObjectDelete.put("JsonArrayName", "flux");
            jsonObjectDelete.put("requete", String.format(DELETE_FLUX_BY_CORRELATION_ID, correlationId));

            envoiRequeteAuVerticleBD(PersistenceAdressesManagement.DELETE, jsonObjectDelete).setHandler(ar -> {
                if (ar.succeeded()) {
                    future.complete();
                } else {
                    logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
                    future.fail(ar.cause().getMessage());
                }
            });
        }

        else {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
                    "La valeur renseignée de l'ID est invalide");
            future.fail("La valeur renseignée de l'ID est invalide");
        }

        return future;
    }
	
	@Override
    public Future<Void> deleteByCorrelationIdAndStatus(String correlationId, StatusEnum status) {

        String action = "deleteByCorrelationIdAndStatus";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        Future<Void> future = Future.future();

        if (correlationId != null && status !=null) {
            JsonObject jsonObjectDelete = new JsonObject();
            jsonObjectDelete.put("JsonArrayName", "flux");
            jsonObjectDelete.put("requete", String.format(DELETE_FLUX_BY_CORRELATION_ID_AND_STATUS, correlationId, status.getValue()));

            envoiRequeteAuVerticleBD(PersistenceAdressesManagement.DELETE, jsonObjectDelete).setHandler(ar -> {
                if (ar.succeeded()) {
                    future.complete();
                } else {
                    logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
                    future.fail(ar.cause().getMessage());
                }
            });
        }

        else {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
                    "Les valeurs passées en paramêtres sont invalides");
            future.fail("Les valeurs passées en paramêtres sont invalides");
        }

        return future;
    }
		
	@Override
    public Future<Void> deleteByConsumerCorrelationIdAndStatusGreaterThanOrEqual(String consumerCorrelationId, StatusEnum status) {

        String action = "deleteByConsumerCorrelationIdAndStatusGreaterThanOrEqual";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        Future<Void> future = Future.future();

        if (consumerCorrelationId != null && status !=null) {
            JsonObject jsonObjectDelete = new JsonObject();
            jsonObjectDelete.put("JsonArrayName", "flux");
            jsonObjectDelete.put("requete", String.format(DELETE_FLUX_BY_CONSUMER_CORRELATION_ID_AND_STATUS_ORDER_GREATER_THAN_OU_EQUAL, consumerCorrelationId, status.getOrder()));

            envoiRequeteAuVerticleBD(PersistenceAdressesManagement.DELETE, jsonObjectDelete).setHandler(ar -> {
                if (ar.succeeded()) {
                    future.complete();
                } else {
                    logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
                    future.fail(ar.cause().getMessage());
                }
            });
        }

        else {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
                    "Les valeurs passées en paramêtres sont invalides");
            future.fail("Les valeurs passées en paramêtres sont invalides");
        }

        return future;
    }
	

    @Override
    public Future<List<JsonObject>> getRepartition(Map<String, Object> likeFilters) {
    	String action = "getRepartition(Map<String, String> parameters)";
    	logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
    	Future<List<JsonObject>> future = Future.future();


    	// Construction de la requête
    	String sqlFilters = buildEqualFilters(likeFilters);

    	StringBuffer requete = new StringBuffer();
    	requete.append(String.format(REQ_REPARTITION + " %s", sqlFilters));
    	requete.append(String.format(" GROUP BY f.status"));

    	List<JsonObject> allFlux = new ArrayList<JsonObject>();

    	JsonObject jsonObjectSelect = new JsonObject();
    	jsonObjectSelect.put("JsonArrayName", "rep");
    	jsonObjectSelect.put("requete", requete);

    	envoiRequeteAuVerticleBD(PersistenceAdressesManagement.SELECT, jsonObjectSelect).setHandler(ar -> {
    		if (ar.succeeded()) {
    			try {
    				JsonObject allRepartitions = ar.result();
    				JsonArray repartition = allRepartitions.getJsonArray("rep");
    				
    				for (int i = 0; i < repartition.size(); i++) {
    					JsonObject json = repartition.getJsonObject(i);
    					allFlux.add(json);
    				}
    			} catch (Exception e) {
    				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
    				future.fail(e.getMessage());
    			}
    			future.complete(allFlux);
    		} else {
    			logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
    			future.fail(ar.cause().getMessage());
    		}
    	});

    	return future;
    }
    
	private StringBuffer getRequete(int borneDebut, Map<String, Object> filters, int nbFlux, String order, JsonArray params) {
		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append("(");
		sqlFilters.append(REQ_SELECT_FLUX_UNIQUE);
		getFilters(filters, params, sqlFilters);
		sqlFilters.append(String.format(") %s ", order));
		sqlFilters.append(String.format(" limit %s offset %s", nbFlux, borneDebut));
		return sqlFilters;
	}

	private StringBuffer getRequeteCount(Map<String, Object> filters, JsonArray params) {
		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append(REQ_COUNT_FLUX_UNIQUE);
		getFilters(filters, params, sqlFilters);
		return sqlFilters;
	}

	private void getFilters(Map<String, Object> filters, JsonArray params, StringBuffer sqlFilters) {
		sqlFilters.append(" WHERE 1=1 ");
		if (filters != null) {
			filters.forEach((property, value) -> {
				if (value != null) {
					if (property.equals(FluxVerticle.KEY_JSON_START_DATE_FILTER)) {
						sqlFilters.append(" AND " + Flux.KEY_DB_DATE + " >= '" + value + "' ");
					} else if (property.equals(FluxVerticle.KEY_JSON_END_DATE_FILTER)) {
						sqlFilters.append(" AND " + Flux.KEY_DB_DATE + " <= '" + value + "' ");
					} else if (property.equals(Flux.KEY_JSON_STATUS)) {
						String[] data = StringUtils.split((String) value, "-");
						if (data != null) {
							sqlFilters.append(" AND " +  property + " in (");
							boolean first = true;
							for (int i = 0; i < data.length; i++) {
								if (first) {
									first = false;
								} else {
									sqlFilters.append(",");
								}
								sqlFilters.append("'" + data[i] + "'");
							}
							sqlFilters.append(")");
						}
					} else if (property.equals(Flux.KEY_JSON_LIST_CORRELATION_ID) 
							|| property.equals(Flux.KEY_JSON_LIST_FILENAME)) {
						String[] data = StringUtils.split((String) value, ",");
						if (data != null) {
							if (property.equals(Flux.KEY_JSON_LIST_CORRELATION_ID)) {
								sqlFilters.append(" AND " +  Flux.KEY_DB_CORRELATION_ID + " in (");							
							} else if (property.equals(Flux.KEY_JSON_LIST_FILENAME)) {
								sqlFilters.append(" AND " +  Flux.KEY_DB_FILENAME + " in (");							
							}
							boolean first = true;
							for (int i = 0; i < data.length; i++) {
								if (first) {
									first = false;
								} else {
									sqlFilters.append(",");
								}
								sqlFilters.append("'" + data[i] + "'");
							}
							sqlFilters.append(")");
						}
					} else {
						if (value.toString().startsWith("=")) {
							sqlFilters.append(" AND " + "upper(f." + property + ") = upper(?) ");
							params.add(value.toString().substring(1));						
						} else {
							sqlFilters.append(" AND " + "upper(f." + property + ") like upper(?) ");
							params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
						}
					}
				}
			});
		}
	}


}
