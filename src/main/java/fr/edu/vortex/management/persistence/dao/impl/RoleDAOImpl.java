//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.hazelcast.util.StringUtil;

import fr.edu.echanges.nat.vortex.common.adresses.persistence.PersistenceAdressesManagement;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.RoleDAO;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class RoleDAOImpl extends GenericDAOImpl implements RoleDAO {

    private static final Logger logger = LoggerFactory.getLogger(RoleDAOImpl.class);

    private static final String MAIN_TABLE = "role";

    private static final String SELECT_ALL = "" +
            "SELECT " +
            "    distinct r.id, " +
            "    r.libelle, " +
            "    r.systeme " +
            "FROM " +
            "    role r " +
            "LEFT OUTER JOIN " +
            "    role_permission rp " +
            "ON " +
            "    ( " +
            "        r.id = rp.role_id) " +
            "LEFT OUTER JOIN " +
            "    permission p " +
            "ON " +
            "    ( " +
            "        rp.permission_code = p.code) ";
    
    private static final String SELECT_PERMISSIONS = "" +
            "SELECT " +
            "    r.id        AS role_id, " +
            "    p.code      AS perm_code, " +
            "    p.libelle   AS perm_libelle, " +
            "    p.scope     AS perm_scope, " +
            "    p.ressource AS perm_ressource, " +
            "    p.contexte  AS perm_contexte, " +
            "    rp.actions    AS perm_actions " +
            "FROM " +
            "    management.role r, " +
            "    management.role_permission rp, " +
            "    management.permission p " +
            "WHERE r.id = rp.role_id " +
            "AND rp.permission_code = p.code ";
    
    private static final String REQ_COUNT_ROLES = "" +
            "SELECT " +
            "    count(distinct(r.id)) as nb_roles " +
            "FROM " +
            "    role r " +
            "LEFT OUTER JOIN " +
            "    role_permission rp " +
            "ON " +
            "    ( " +
            "        r.id = rp.role_id) " +
            "LEFT OUTER JOIN " +
            "    permission p " +
            "ON " +
            "    ( " +
            "        rp.permission_code = p.code) ";


    private static final String DELETE_BY_ID = "DELETE FROM role WHERE id = ? returning *";

    private static final String INSERT = "INSERT INTO role (libelle, systeme) VALUES (?,?) returning id";

    private static final String SELECT_GENERIQUE = "" +
            "SELECT " +
            "    r.id, " +
            "    r.libelle, " +
            "    r.systeme, " +
            "    p.code as perm_code, " +
            "    p.libelle as perm_libelle, " +
            "    p.scope as perm_scope, " +
            "    p.ressource as perm_ressource, " +
            "    p.contexte as perm_contexte, " +
            "    rp.actions as perm_actions " +
            "FROM " +
            "    role r " +
            "LEFT OUTER JOIN " +
            "    role_permission rp " +
            "ON " +
            "    ( " +
            "        r.id = rp.role_id) " +
            "LEFT OUTER JOIN " +
            "    permission p " +
            "ON " +
            "    ( " +
            "        rp.permission_code = p.code) ";
    
    private static final String SELECT_PAR_ID_ROLE = SELECT_GENERIQUE +" where r.id = ? ";

    private static final String SELECT_PAR_LIBELLE = SELECT_GENERIQUE + " where r.libelle = ? ORDER BY r.libelle ASC, p.libelle ASC";

    private static final String SELECT_PAR_SYSTEME = SELECT_GENERIQUE + " where r.systeme = ? ORDER BY r.libelle ASC, p.libelle ASC";

    @Override
    public Future<Role> creer(Role role) {
        Future<Role> future = Future.future();

        JsonObject jsonObjectSelect = new JsonObject();
        jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, INSERT);

        JsonArray params = new JsonArray();
        params.add(role.getLibelle());
        params.add(role.isSysteme());
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
                .setHandler(ar -> {
                    if (ar.succeeded()) {

                        if (!ar.result().containsKey(MAIN_TABLE)) {
                            future.fail("résultat mal formaté");
                            return;
                        }

                        JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
                        if (rows.size() == 0) {
                            future.fail("erreur lors de la création du role");
                            return;
                        }

                        role.setId(rows.getJsonObject(0).getInteger("id"));

                        future.complete(role);

                    } else {
                        future.fail(ar.cause());
                    }
                });

        return future;
    }
    
    @Override
    public Future<Role> modifier(Role role) {
        
        Future<Role> future = Future.future();

        StringBuffer sqlFilters = new StringBuffer();
        sqlFilters.append("UPDATE " + MAIN_TABLE + " SET ");
        sqlFilters.append("libelle = '").append(role.getLibelle()).append("' ");
        sqlFilters.append("WHERE id = ").append(String.valueOf(role.getId()));
        
        JsonObject jsonRequete = new JsonObject();
        jsonRequete.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());

        envoiRequeteAuVerticleBD(PersistenceAdressesManagement.UPDATE, jsonRequete)
            .setHandler(ar -> {
                if (ar.succeeded()) {

                    if (!ar.result().containsKey("keys")) {
                        future.fail("résultat mal formaté");
                        return;
                    }
              	
                	JsonArray keys = ((JsonObject) ar.result()).getJsonArray("keys");

                	Role roleUpdated = new Role(
                			keys.getInteger(0), 
                			keys.getString(1), 
                			keys.getBoolean(2), 
                			(List<Permission>) null);
 
                    future.complete(roleUpdated);

                } else {
                    future.fail(ar.cause());
                }
            });

        return future;       
    }

    @Override
    public Future<List<Role>> listerRoles(int borneDebut, int borneFin, JsonObject filtres, String sort,
    		String sortOrder) {

    	String action = "listerRoles(int borneDebut, int borneFin, Map<String, String> parameters, String sort, String desc)";
    	logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
    	Future<List<Role>> future = Future.future();

    	int nbValeurs = borneFin - borneDebut + 1;
    	if (borneDebut >= 0 && borneFin >= 0 && nbValeurs > 0) {

    		String order = buildOrder(sort, sortOrder);

    		JsonObject jsonObjectSelect = new JsonObject();
    		JsonArray params = new JsonArray();

    		StringBuffer sqlFilters = getRequete(borneDebut, filtres, nbValeurs, order, params);

    		jsonObjectSelect.put("JsonArrayName", "roles");
    		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
    		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

    		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
    		.setHandler(ar -> {
    			if (ar.succeeded()) {
    				try {
    					if (!ar.result().containsKey("roles")) {
    						future.fail("résultat mal formaté");
    						return;
    					}

    					JsonArray rows = ar.result().getJsonArray("roles");
    					if (rows.size() == 0) {
    						future.complete(new ArrayList<>());
    						return;
    					}

                        List<Role> roles = rowsSqlEnRoles(rows);

    					String req = SELECT_PERMISSIONS;

    					JsonObject jsonObjectReq = new JsonObject();

    					jsonObjectReq.put("JsonArrayName", "perms");
    					jsonObjectReq.put(DatabaseVerticle.KEY_REQUETE, req);

    					envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectReq).setHandler(arRoles -> {
    						if (arRoles.succeeded()) {
    							try {
    								JsonObject permissions = arRoles.result();
    								JsonArray permArrayJson = permissions.getJsonArray("perms");

    								Map<Integer, List<Permission>> mapPerm = new HashMap<Integer, List<Permission>>();

    								for (int i = 0; i < permArrayJson.size(); i++) {
    									JsonObject roleJson = permArrayJson.getJsonObject(i);
    									Integer idRole = roleJson.getInteger("role_id");
    									Permission perm = new Permission();
    									perm.setCode(roleJson.getString("perm_code"));
    									perm.setLibelle(roleJson.getString("perm_libelle"));
    									perm.setScope(roleJson.getString("perm_scope"));
    									perm.setRessource(roleJson.getString("perm_ressource"));
    									perm.setContexte(roleJson.getString("perm_contexte"));
    									perm.setActions(roleJson.getString("perm_actions"));
    									if (mapPerm.get(idRole) == null){
    										mapPerm.put(idRole, new ArrayList<Permission>());
    									}
    									mapPerm.get(idRole).add(perm);
    								}
    								for (Iterator<Role> iterator = roles.iterator(); iterator.hasNext();) {
    									Role role = iterator.next();
    									if (mapPerm.get(role.getId()) != null){
    										role.getPermissions().addAll(mapPerm.get(role.getId()));
    									}
    								}                    	
    								future.complete(roles);
    							} catch (Exception e) {
    								future.fail(e.getMessage());
    							}	                          	
    						} else {
    							future.fail(arRoles.cause().getMessage());                   	
    						}
    					});
    				} catch (Exception e) {
    					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
    					future.fail(e.getMessage());
    				}
    			} else {
    				future.fail(ar.cause());
    			}
    		});
    	} else {
    		future.fail("Les valeurs renseignées dans les bornes de début et de fin sont invalides");
    	}
    	return future;
    }
    
    public Future<List<Role>> lister(JsonArray params) {
    	if (params.getBoolean(0)) {
            return generiqueSelect(SELECT_ALL, new JsonArray());
    	} else {
            return generiqueSelect(SELECT_PAR_SYSTEME, params);
  		
    	}  	
    }

    @Override
    public Future<Integer> delete(int id) {
        return super.delete(DELETE_BY_ID, id);
    }

    @Override
    public Future<Optional<Role>> chercherRoleByID(int id) {

        Future<Optional<Role>> future = Future.future();

        JsonObject jsonObjectSelect = new JsonObject();
        jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, SELECT_PAR_ID_ROLE);

        JsonArray params = new JsonArray();
        params.add(id);
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
            if (!ar.succeeded()) {
                future.fail(ar.cause());
                return;
            }

            if (!ar.result().containsKey(MAIN_TABLE)) {
                future.fail("résultat mal formaté");
                return;
            }

            JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
            if (rows.size() == 0) {
                future.complete(Optional.empty());
                return;
            }

            if(rows.size()>=1) {
        	List<Role> roles = rowsSqlEnRoles(rows);
        	Role role = roles.get(0);
		future.complete(Optional.of(role));
            }

        });

        return future;

    }

    @Override
    public Future<List<Role>> chercherRoleParLibelle(String libelle) {

        JsonArray params = new JsonArray();
        params.add(libelle);

        return generiqueSelect(SELECT_PAR_LIBELLE, params);
    }

    private Future<List<Role>> generiqueSelect(String sql, JsonArray params) {
        Future<List<Role>> future = Future.future();

        JsonObject jsonObjectSelect = new JsonObject();
        jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);
        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sql);
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
                .setHandler(ar -> {
                    if (ar.succeeded()) {

                        if (!ar.result().containsKey(MAIN_TABLE)) {
                            future.fail("résultat mal formaté");
                            return;
                        }

                        JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
                        if (rows.size() == 0) {
                            future.complete(new ArrayList<>());
                            return;
                        }

                        List<Role> roles = rowsSqlEnRoles(rows);

                        future.complete(roles);

                    } else {
                        future.fail(ar.cause());
                    }
                });

        return future;
    }

    /**
     * Transforme plusieurs ligne sql en un ensemble de roles.
     * 
     * @param rows
     * @return
     */
    private List<Role> rowsSqlEnRoles(JsonArray rows) {
        List<Role> roles = new ArrayList<>();
        rows.stream()
                .map(o -> (JsonObject) o)
                .map(this::transform)
                .forEach(currentRole -> {
                    Optional<Role> find = roles.stream()
                            .filter(r -> r.getId() == currentRole.getId())
                            .findFirst();

                    if (find.isPresent()) {
                    	if (currentRole.getPermissions().size() > 0){
                    		find.get().getPermissions().add(currentRole.getPermissions().get(0));
                    	}
                    } else {
                        roles.add(currentRole);
                    }

                });
        return roles;
    }

    /**
     * Transforme une ligne d'un select en un object role (a une permission)
     * 
     * @param row
     * @return
     */
    private Role transform(JsonObject row) {

        Permission perm = new Permission(
                row.getString("perm_code"),
                row.getString("perm_libelle"),
                row.getString("perm_scope"),
                row.getString("perm_ressource"),
                row.getString("perm_contexte"),
                row.getString("perm_actions"));

        List<Permission> perms = new ArrayList<>();
        if (!StringUtil.isNullOrEmpty(perm.getCode())) {
            perms.add(perm);
        }

        return new Role(
                row.getInteger("id"),
                row.getString("libelle"),
                row.getBoolean("systeme"),
                perms);

    }

    @Override
    public Future<Integer> countRoles(JsonObject filtres) {
 	   String action = "countRoles";
 	   logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
 	   Future<Integer> future = Future.future();

 	   JsonObject jsonObjectSelect = new JsonObject();
 	   JsonArray params = new JsonArray();

 	   StringBuffer sqlFilters = getRequeteCount(filtres, params);

 	   jsonObjectSelect.put("JsonArrayName", "count");
 	   jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
 	   jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

 	   envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
 		   if (ar.succeeded()) {
 			   Integer nbRoles = null;
 			   try {
 				   JsonObject allJson = ar.result();
 				   JsonArray arrayJson = allJson.getJsonArray("count");
 				   for (int i = 0; i < arrayJson.size(); i++) {
 					   JsonObject messageJson = arrayJson.getJsonObject(i);
 					   nbRoles = new Integer(messageJson.getInteger("nb_roles"));
 				   }
 			   } catch (Exception e) {
 				   logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
 				   future.fail(e.getMessage());
 			   }
 			   future.complete(nbRoles);
 		   } else {
 			   logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
 			   future.fail(ar.cause().getMessage());
 		   }
 	   });

 	   return future;
    }
    
    private StringBuffer getRequete(int borneDebut, JsonObject filtres, int nbFlux, String order, JsonArray params) {
 	   StringBuffer sqlFilters = new StringBuffer();
 	   sqlFilters.append("(");
 	   sqlFilters.append(SELECT_ALL);
 	   getFilters(filtres, params, sqlFilters);
 	   sqlFilters.append(String.format(") %s ", order));
 	   sqlFilters.append(String.format(" limit %s offset %s", nbFlux, borneDebut));
 	   return sqlFilters;
    }

    private StringBuffer getRequeteCount(JsonObject filtres, JsonArray params) {
 	   StringBuffer sqlFilters = new StringBuffer();
 	   sqlFilters.append(REQ_COUNT_ROLES);
 	   getFilters(filtres, params, sqlFilters);
 	   return sqlFilters;
    }

    private void getFilters(JsonObject filtres, JsonArray params, StringBuffer sqlFilters) {
 	   sqlFilters.append(" WHERE 1=1 ");
 	   if (filtres != null) {
 		   filtres.getMap().forEach((property, value) -> {
 			   if (value != null) {
 				   if (Role.KEY_JSON_PERMISSIONS.contentEquals(property)) {
 					   if (value.toString().startsWith("=")) {
 						   sqlFilters.append(" AND (" + "upper(p.libelle) = upper(?) ");
 						   sqlFilters.append(" OR " + "upper(p.scope) = upper(?) ");
 						   sqlFilters.append(" OR " + "upper(p.ressource) = upper(?)) ");
 						   params.add(value.toString().substring(1));
 						   params.add(value.toString().substring(1));
 						   params.add(value.toString().substring(1));
 					   } else {
 						   sqlFilters.append(" AND (" + "upper(p.libelle) like upper(?) ");
 						   sqlFilters.append(" OR " + "upper(p.scope) like upper(?) ");
 						   sqlFilters.append(" OR " + "upper(p.ressource) like upper(?)) ");
 						   params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
 						   params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
 						   params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
 					   }		        							
 				   } else if (Role.KEY_JSON_SYSTEME.contentEquals(property)) {
 					   sqlFilters.append(" AND r." + property + " = ? ");		    		        		
 					   params.add(value);						
 				   } else {              	
 					   if (value.toString().startsWith("=")) {
 						   sqlFilters.append(" AND " + "upper(r." + property + ") = upper(?) ");
 						   params.add(value.toString().substring(1));
 					   } else {
 						   sqlFilters.append(" AND " + "upper(r." + property + ") like upper(?) ");
 						   params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
 					   }
 				   }
 			   }
 		   });
 	   }
    }

    private String buildOrder(String sort, String sortOrder) {
 	   StringBuffer order = new StringBuffer();
 	   if (sort != null) {
 		   order.append(" ORDER BY ");
 		   order.append(sort);
 		   if (sortOrder != null) {
 			   order.append(" " + sortOrder);
 		   }
 	   } else {
 		   order.append("ORDER BY " + Role.KEY_JSON_LIBELLE + " ASC");
 	   }
 	   // rajout pour chaque order by
 	   order.append(", " + Role.KEY_JSON_ID + " ASC");

 	   return order.toString();
    }
}
