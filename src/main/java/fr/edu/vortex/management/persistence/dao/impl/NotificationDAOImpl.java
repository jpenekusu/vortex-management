//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;

import fr.edu.echanges.nat.vortex.common.adresses.persistence.PersistenceAdresses;
import fr.edu.echanges.nat.vortex.common.adresses.persistence.PersistenceAdressesManagement;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.notification.EtatEmissionNotificationEnum;
import fr.edu.vortex.management.notification.EtatNotificationEnum;
import fr.edu.vortex.management.notification.Notification;
import fr.edu.vortex.management.notification.TypeNotificationEnum;
import fr.edu.vortex.management.persistence.dao.NotificationDAO;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class NotificationDAOImpl extends GenericDAOImpl implements NotificationDAO {

    private static final Logger logger = LoggerFactory.getLogger(NotificationDAOImpl.class);

    static final String TABLE_NOTIFICATION = "notification";

    static final String REQ_SELECT_ALL_NOTIFICATIONS = "SELECT id, correlation_id, type, etat, etat_emission FROM notification WHERE correlation_id= '%s'";
    static final String REQ_SELECT_NOTIFICATION_BY_TYPE_AND_CORRELATION_ID = "SELECT id, correlation_id, type, etat, etat_emission FROM notification WHERE type='%s' AND correlation_id= '%s'";
    static final String REQ_UPDATE_ETAT_EMISSION_NOTIFICATION = "update notification set etat_emission = %s WHERE id= %s";
    private static final String REQ_SUPPRIMER_BY_CORRELATION_ID = "DELETE FROM notification WHERE correlation_id= '%s' ";

    @Override
    public Future<Notification> save(Notification notif) {
        String action = "save(Notification notif)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        Future<Notification> future = Future.future();
        List<String> colonnes = new ArrayList<>();
        List<String> valeurs = new ArrayList<>();

        colonnes.add(Notification.KEY_DB_CORRELATION_ID);
        valeurs.add("'" + notif.getCorrelationId() + "'");

        if (notif.getType() != null) {
            colonnes.add(Notification.KEY_DB_TYPE_NOTIFICATION);
            valeurs.add("'" + notif.getType().getValue() + "'");
        }

        if (notif.getEtat() != null) {
            colonnes.add(Notification.KEY_DB_ETAT);
            valeurs.add("'" + notif.getEtat().getValue() + "'");
        }

        if (notif.getEtatEmission() != null) {
            colonnes.add(Notification.KEY_DB_ETAT_EMISSION);
            valeurs.add("'" + notif.getEtatEmission().getValue() + "'");
        }

        String creationRequeteInsert = creationRequeteInsert(TABLE_NOTIFICATION, colonnes, valeurs);
        JsonObject jsonRequete = new JsonObject();
        jsonRequete.put("requete", creationRequeteInsert);

        envoiRequeteAuVerticleBD(PersistenceAdressesManagement.INSERT, jsonRequete).setHandler(ar -> {
            if (ar.succeeded()) {
                Notification newNotif = null;
                try {
                    JsonArray keys = ((JsonObject) ar.result()).getJsonArray("keys");
                    newNotif = jsonArrayToNotification(keys);
                } catch (Exception e) {
                    logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
                    future.fail(e.getMessage());
                }
                future.complete(newNotif);
            } else {
                logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
                future.fail(ar.cause().getMessage());
            }
        });

        return future;
    }

    @Override
    public Future<Notification> updateEtatEmissionNotification(Integer idNotification,
            EtatEmissionNotificationEnum etatEmission) {
        String action = "updateNotification(Notification notif)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        Future<Notification> future = Future.future();

        JsonObject jsonRequete = new JsonObject();
        jsonRequete.put("requete",
                String.format(REQ_UPDATE_ETAT_EMISSION_NOTIFICATION, etatEmission.getValue(),
                        idNotification));

        envoiRequeteAuVerticleBD(PersistenceAdressesManagement.UPDATE, jsonRequete).setHandler(ar -> {
            if (ar.succeeded()) {
                Notification updatedNotif = null;
                try {
                    JsonArray keys = ((JsonObject) ar.result()).getJsonArray("keys");
                    updatedNotif = jsonArrayToNotification(keys);
                } catch (Exception e) {
                    future.fail(e.getMessage());
                }
                future.complete(updatedNotif);
            } else {
                ar.cause().printStackTrace();
                future.fail(ar.cause().getMessage());
            }
        });

        return future;
    }

    @Override
    public Future<Notification> getNotificationByCorrelationIdAndTypeNotification(String correlationId,
            TypeNotificationEnum type) {
        String action = "getNotificationByCorrelationIdAndTypeNotification(String correlationId, TypeNotificationEnum type)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        Future<Notification> future = Future.future();

        if (correlationId != null && type != null) {
            JsonObject jsonObjectSelect = new JsonObject();
            jsonObjectSelect.put("JsonArrayName", "notification");

            jsonObjectSelect.put("requete",
                    String.format(REQ_SELECT_NOTIFICATION_BY_TYPE_AND_CORRELATION_ID, type.getValue(),
                            correlationId));

            envoiRequeteAuVerticleBD(PersistenceAdressesManagement.SELECT, jsonObjectSelect).setHandler(ar -> {
                if (ar.succeeded()) {
                    Notification notif = null;
                    try {
                        JsonObject allNotifsJson = ar.result();
                        JsonArray notifArrayJson = allNotifsJson.getJsonArray("notification");
                        if (notifArrayJson != null) {
                            if (notifArrayJson.size() == 1) {
                                notif = jsonObjectToNotification(notifArrayJson.getJsonObject(0));
                            } else {
                                if (notifArrayJson.size() > 1) {
                                    future.fail("Le nombre de résultat retourné est incorrect.");
                                }
                            }
                        }

                    } catch (Exception e) {
                        logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
                        future.fail(e.getMessage());
                    }
                    future.complete(notif);
                } else {
                    logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
                    future.fail(ar.cause().getMessage());
                }
            });
        }

        else {
            future.fail("La valeur renseignée du correlation_id du flux est invalide");
        }

        return future;
    }

    @Override
    public Future<List<Notification>> getAllNotificationsByCorrelationId(String correlationId) {
        String action = "getAllNotificationsByCorrelationId(String correlationId)";
        logger.debug(Constants.LOG_MESSAGE, correlationId, action);
        Future<List<Notification>> future = Future.future();

        if (correlationId != null) {
            JsonObject jsonObjectSelect = new JsonObject();
            jsonObjectSelect.put("JsonArrayName", "notification");
            jsonObjectSelect.put("requete",
                    String.format(REQ_SELECT_ALL_NOTIFICATIONS, correlationId));

            envoiRequeteAuVerticleBD(PersistenceAdressesManagement.SELECT, jsonObjectSelect).setHandler(ar -> {
                if (ar.succeeded()) {
                    List<Notification> notifsList = new ArrayList<>();
                    try {
                        JsonObject allnotifsJson = ar.result();
                        JsonArray notifArrayJson = allnotifsJson.getJsonArray("notification");
                        for (int i = 0; i < notifArrayJson.size(); i++) {
                            notifsList.add(jsonObjectToNotification(notifArrayJson.getJsonObject(i)));
                        }

                    } catch (Exception e) {
                        logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
                        future.fail(e.getMessage());
                    }
                    future.complete(notifsList);
                } else {
                    logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
                    future.fail(ar.cause().getMessage());
                }
            });
        }

        else {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "",
                    "La valeur renseignée pour le correlation_id est invalide");
            future.fail("La valeur renseignée pour le correlation_id est invalide");
        }

        return future;
    }

    private Notification jsonArrayToNotification(JsonArray keys) {
        Notification notif = null;
        notif = new Notification(keys.getInteger(0), keys.getString(1), TypeNotificationEnum.valueOf(keys.getString(2)),
                EtatNotificationEnum.valueOf(keys.getInteger(3)),
                EtatEmissionNotificationEnum.valueOf(keys.getInteger(4)));
        return notif;
    }

    private Notification jsonObjectToNotification(JsonObject messageJson) {
        String action = "jsonObjectToNotification(JsonObject messageJson)";
        Notification notif = null;
        try {
            notif = new Notification(messageJson.getInteger(Notification.KEY_DB_ID),
                    messageJson.getString(Notification.KEY_DB_CORRELATION_ID),
                    TypeNotificationEnum
                            .valueOf(messageJson.getString(Notification.KEY_DB_TYPE_NOTIFICATION)),
                    EtatNotificationEnum.valueOf(messageJson.getInteger(Notification.KEY_DB_ETAT)),
                    EtatEmissionNotificationEnum
                            .valueOf(messageJson.getInteger(Notification.KEY_DB_ETAT_EMISSION)));
        } catch (Exception e) {
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
        }
        return notif;
    }
    
    @Override
    public Future<Void> deleteNotification(String correlation_id) {
        Future<Void> future = Future.future();

        JsonObject jsonObjectSelect = new JsonObject();

        jsonObjectSelect.put("requete", String.format(REQ_SUPPRIMER_BY_CORRELATION_ID, correlation_id));

        envoiRequeteAuVerticleBD(PersistenceAdressesManagement.DELETE, jsonObjectSelect).setHandler(ar -> {
            if (ar.succeeded()) {
                future.complete();
            } else {
                future.fail(ar.cause());
            }
        });
        return future;
    }

}
