//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.TraceDAO;
import fr.edu.vortex.management.utilisateur.pojo.Trace;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;


public class TraceDAOImpl extends GenericDAOImpl implements TraceDAO{


	private static final Logger logger = LoggerFactory.getLogger(TraceDAOImpl.class);

	private static final String SELECT_TRACES = "" +
			"SELECT " +
			"    t.id, " +
			"    t.date_trace, " +
			"    t.login, " +
			"    t.nom, " +
			"    t.prenom, " +
			"    t.origine, " +
			"    t.event, " +
			"    t.id_element, " +
			"    t.nom_element, " +
			"    t.type_element, " +
			"    t.contenu " +
			"FROM " +
			"    management.trace t ";

	private static final String REQ_COUNT_ALL_TRACE = "select count(1) as nb_traces from trace t ";

	private static final String INSERT = "INSERT INTO trace "
			+ "(date_trace, login, nom, prenom, origine, event, id_element, nom_element, type_element, contenu) "
			+ "VALUES (?::timestamptz, ?, ?, ?, ?, ?, ?, ?, ?, ?::json) returning id";

	private static final String INSERT_CONNEXION = "INSERT INTO trace "
			+ "(date_trace, login, nom, prenom, origine, event, type_element, nom_element, id_element) "
			+ "VALUES (?::timestamptz, ?, ?, ?, ?, ?, ?, ?, ?) returning id";

	private static final String INSERT_CONNEXION_NO_ID = "INSERT INTO trace "
			+ "(date_trace, login, nom, prenom, origine, event, type_element, nom_element) "
			+ "VALUES (?::timestamptz, ?, ?, ?, ?, ?, ?, ?) returning id";

	private static final String MAIN_TABLE = "trace";


	@Override
	public Future<Trace> creer(Trace trace) {
		Future<Trace> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, INSERT);

		List<String> valeurs = new ArrayList<>();
		if (trace.getDateTrace() != null) {
			valeurs.add("'" + trace.getDateTrace().toString() + "'");
		} else {
			valeurs.add("''");
		}
		valeurs.add(trace.getLogin());
		valeurs.add(trace.getNom());
		valeurs.add(trace.getPrenom());
		valeurs.add(trace.getOrigine());
		valeurs.add(trace.getEvent());
		valeurs.add(trace.getId_element());
		valeurs.add(trace.getNom_Element());
		valeurs.add(trace.getType_Element());
		if (trace.getContenu() != null) {
			valeurs.add(trace.getContenu().toString());
		} else {
			valeurs.add("''");			
		}

		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, valeurs);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {

				if (!ar.result().containsKey(MAIN_TABLE)) {
					future.fail("résultat mal formaté");
					return;
				}

				JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
				if (rows.size() == 0) {
					future.fail("erreur lors de la création de la trace");
					return;
				}

				trace.setId(rows.getJsonObject(0).getInteger("id"));

				future.complete(trace);

			} else {
				future.fail(ar.cause());
			}
		});

		return future;
	}


	@Override
	public Future<Trace> creerTraceConnexion(Trace trace) {
		Future<Trace> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", MAIN_TABLE);

		if (trace.getId_element() != null) {
			jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, INSERT_CONNEXION);
		} else {
			jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, INSERT_CONNEXION_NO_ID);			
		}

		List<String> valeurs = new ArrayList<>();
		if (trace.getDateTrace() != null) {
			valeurs.add("'" + trace.getDateTrace().toString() + "'");
		} else {
			valeurs.add("''");
		}
		valeurs.add(trace.getLogin());
		valeurs.add(trace.getNom());
		valeurs.add(trace.getPrenom());
		valeurs.add(trace.getOrigine());
		valeurs.add(trace.getEvent());
		valeurs.add(trace.getType_Element());
		valeurs.add(trace.getNom_Element());
		if (trace.getId_element() != null) {
			valeurs.add(trace.getId_element());
		}


		jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, valeurs);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {

				if (!ar.result().containsKey(MAIN_TABLE)) {
					future.fail("résultat mal formaté");
					return;
				}

				JsonArray rows = ar.result().getJsonArray(MAIN_TABLE);
				if (rows.size() == 0) {
					future.fail("erreur lors de la création de la trace");
					return;
				}

				trace.setId(rows.getJsonObject(0).getInteger("id"));

				future.complete(trace);

			} else {
				future.fail(ar.cause());
			}
		});

		return future;
	}


	@Override
	public Future<List<Trace>> listerTrace(int borneDebut, int borneFin, Map<String, Object> filters, String sort,
			String sortOrder) {

		String action = "listerTrace(int borneDebut, int borneFin, Map<String, String> parameters, String sort, String desc)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<List<Trace>> future = Future.future();

		int nbFlux = borneFin - borneDebut + 1;
		if (borneDebut >= 0 && borneFin >= 0 && nbFlux > 0) {

			// Construction de la requête
			String order = buildOrder(sort, sortOrder);

			JsonObject jsonObjectSelect = new JsonObject();
			JsonArray params = new JsonArray();

			StringBuffer sqlFilters = getRequete(borneDebut, filters, nbFlux, order, params);

			jsonObjectSelect.put("JsonArrayName", "traces");
			jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
			jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

			List<Trace> allTraces = new ArrayList<Trace>();

			envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
			.setHandler(ar -> {
				if (ar.succeeded()) {
					try {
						JsonObject allTracesJson = ar.result();
						JsonArray rows = allTracesJson.getJsonArray("traces");

						for (int i = 0; i < rows.size(); i++) {
							JsonObject trace = rows.getJsonObject(i);
							allTraces.add(Trace.fromJson(trace));
						}
					} catch (Exception e) {
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
						future.fail(e.getMessage());
					}
					future.complete(allTraces);

				} else {
					future.fail(ar.cause());
				}
			});
		}

		else {
			future.fail("Les valeurs renseignées dans les bornes de début et de fin sont invalides");
		}

		return future;
	}

	@Override
	public Future<Integer> countTrace(Map<String, Object> filters) {
		String action = "countTrace";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<Integer> future = Future.future();

		JsonObject jsonObjectSelect = new JsonObject();
        JsonArray params = new JsonArray();

        StringBuffer sqlFilters = getRequeteCount(filters, params);

		jsonObjectSelect.put("JsonArrayName", "count");
        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sqlFilters.toString());
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect).setHandler(ar -> {
			if (ar.succeeded()) {
				Integer nbTraces = null;
				try {
					JsonObject allQueuesJson = ar.result();
					JsonArray queuesArrayJson = allQueuesJson.getJsonArray("count");
					for (int i = 0; i < queuesArrayJson.size(); i++) {
						JsonObject messageJson = queuesArrayJson.getJsonObject(i);
						nbTraces = new Integer(messageJson.getInteger("nb_traces"));
					}
				} catch (Exception e) {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
					future.fail(e.getMessage());
				}

				future.complete(nbTraces);
			} else {
				logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", ar.cause());
				future.fail(ar.cause().getMessage());
			}
		});
		return future;
	}
	
	
	private String buildEqFilters(Map<String, Object> filters) {
		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append(" WHERE 1=1 ");
		if (filters != null) {
			filters.forEach((property, value) -> {
				if (value != null) {
					sqlFilters.append(" AND " + "t." + property + " = '" + getStringSQLCorrect(value.toString()) + "' ");
				}
			});
		}
		return sqlFilters.toString();
	}


	@Override
	public Future<List<Trace>> listerHistoriqueTrace(Map<String, Object> equalFilters) {
		String action = "listerHistoriqueTrace(int borneDebut, int borneFin, Map<String, String> parameters, String sort, String desc)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		Future<List<Trace>> future = Future.future();

		// Construction de la requête
		String sqlFilters = buildEqFilters(equalFilters);
		String order = " ORDER BY " + Trace.KEY_JSON_DATE_TRACE + " DESC";

		StringBuffer requete = new StringBuffer();
		requete.append(String.format(SELECT_TRACES + " %s", sqlFilters));
		requete.append(String.format(" %s ", order));

		List<Trace> allTraces = new ArrayList<Trace>();

		JsonObject jsonObjectSelect = new JsonObject();
		jsonObjectSelect.put("JsonArrayName", "traces");
		jsonObjectSelect.put("requete", requete);

		envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
		.setHandler(ar -> {
			if (ar.succeeded()) {
				try {
					JsonObject allTracesJson = ar.result();
					JsonArray rows = allTracesJson.getJsonArray("traces");

					for (int i = 0; i < rows.size(); i++) {
						JsonObject trace = rows.getJsonObject(i);
						allTraces.add(Trace.fromJson(trace));
					}
				} catch (Exception e) {
					logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, "", e);
					future.fail(e.getMessage());
				}
				future.complete(allTraces);

			} else {
				future.fail(ar.cause());
			}
		});



		return future;

	}
	
    private String buildOrder(String sort, String sortOrder) {
        StringBuffer order = new StringBuffer();
        if (sort != null) {
            order.append(" ORDER BY ");
            order.append(sort);
            if (sortOrder != null) {
                order.append(" " + sortOrder);
            }
        } else {
            order.append(" ORDER BY " + Trace.KEY_JSON_DATE_TRACE + " DESC");
        }
        // rajout pour chaque order by
        order.append(", " + Trace.KEY_JSON_ID + " ASC");
        return order.toString();
    }
    
	private StringBuffer getRequete(int borneDebut, Map<String, Object> filters, int nbFlux, String order, JsonArray params) {
		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append("(");
		sqlFilters.append(SELECT_TRACES);
		getFilters(filters, params, sqlFilters);
		sqlFilters.append(String.format(") %s ", order));
		sqlFilters.append(String.format(" limit %s offset %s", nbFlux, borneDebut));
		return sqlFilters;
	}

	private StringBuffer getRequeteCount(Map<String, Object> filters, JsonArray params) {
		StringBuffer sqlFilters = new StringBuffer();
		sqlFilters.append(REQ_COUNT_ALL_TRACE);
		getFilters(filters, params, sqlFilters);
		return sqlFilters;
	}

	private void getFilters(Map<String, Object> filters, JsonArray params, StringBuffer sqlFilters) {
		sqlFilters.append(" WHERE 1=1 ");
		if (filters != null) {
			filters.forEach((property, value) -> {
				if (value != null) {
					if (property.equals(FluxVerticle.KEY_JSON_START_DATE_FILTER)) {
						sqlFilters.append(" AND " + "t." + Trace.KEY_JSON_DATE_TRACE + " >= '" + value + "' ");
					} else if (property.equals(FluxVerticle.KEY_JSON_END_DATE_FILTER)) {
						sqlFilters.append(" AND " + "t." + Trace.KEY_JSON_DATE_TRACE + " <= '" + value + "' ");
					} else if (property.equals(Trace.KEY_JSON_EVENT) || property.equals(Trace.KEY_JSON_TYPE_ELEMENT)) {
						String[] data = StringUtils.split((String) value, "-");
						if (data != null) {
							sqlFilters.append(" AND " +  property + " in (");
							boolean first = true;
							for (int i = 0; i < data.length; i++) {
								if (first) {
									first = false;
								} else {
									sqlFilters.append(",");
								}
								sqlFilters.append("'" + data[i] + "'");
							}
							sqlFilters.append(")");
						}
					} else {
						if (value.toString().startsWith("=")) {
							sqlFilters.append(" AND " + "upper(t." + property + ") = upper(?) ");
							params.add(value.toString().substring(1));						
						} else {
							sqlFilters.append(" AND " + "upper(t." + property + ") like upper(?) ");
							params.add("%" + escapeSQLCharacters(value.toString()) + "%");						
						}
					}
				}
			});
		}
	}

}
