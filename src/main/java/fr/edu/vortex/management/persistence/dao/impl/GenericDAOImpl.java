//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao.impl;

import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.GenericDAO;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public abstract class GenericDAOImpl implements GenericDAO {

    private static final Logger logger = LoggerFactory.getLogger(GenericDAOImpl.class);

    protected Vertx vertx;

    @Override
    public void init(Vertx vertx) {
        this.vertx = vertx;
    }

    protected Future<JsonObject> envoiRequeteAuVerticleBD(String adresse, JsonObject requeteJson) {
        Future<JsonObject> future = Future.future();
        if (vertx == null) {
            String action = "*************************************** Vertx NULL";
            logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        }
        vertx.eventBus().send(adresse, requeteJson, ar -> {
            if (ar.succeeded()) {
                JsonObject jsonObj = (JsonObject) ar.result().body();
                future.complete(jsonObj);
            } else {
                ar.cause().printStackTrace();
                String action = "Problème de réception sur l'event bus : ";
                logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, ar.cause().getMessage());
                future.fail(ar.cause().getMessage());
            }
        });
        return future;

    }

    protected String creationRequeteInsert(String nomTable, List<String> colonnesParametres,
            List<String> valeurParametres) {
        StringBuilder requete = new StringBuilder();
        String colonne = "(" + String.join(",", colonnesParametres) + ")";
        String valeursColonnes = "(" + String.join(",", valeurParametres) + ")";
        requete.append("INSERT INTO ").append(nomTable).append(colonne).append(" VALUES ").append(valeursColonnes);
        return requete.toString();

    }

    protected Future<Integer> delete(String sql, int id) {
        Future<Integer> future = Future.future();

        JsonObject jsonObjectSelect = new JsonObject();
        jsonObjectSelect.put("JsonArrayName", "deleted_rows");

        jsonObjectSelect.put(DatabaseVerticle.KEY_REQUETE, sql);

        JsonArray params = new JsonArray();
        params.add(id);
        jsonObjectSelect.put(DatabaseVerticle.KEY_PARAMS, params);

        envoiRequeteAuVerticleBD(DatabaseVerticle.SELECT_PARAMS, jsonObjectSelect)
                .setHandler(ar -> {
                    if (ar.succeeded()) {
                        JsonArray rows = ar.result().getJsonArray("deleted_rows");
                        future.complete(rows.size());
                    } else {
                        future.fail(ar.cause());
                    }
                });

        return future;
    }

    
	/**
	 * Formatage sur les quotes
	 * @param str
	 * @return
	 */
    protected String getStringSQLCorrect(String str){
		String strSQL = null;
		if (str != null) {
			strSQL = str.replaceAll("'", "''");
		}
		return strSQL;
	}
    
	/**
	 * Formatage sur les "_" et "%"
	 * @param str
	 * @return
	 */
    protected String escapeSQLCharacters(String str){
		String strSQL = null;
		if (str != null) {
			strSQL = str.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_");
		}
		return strSQL;
	}
}
