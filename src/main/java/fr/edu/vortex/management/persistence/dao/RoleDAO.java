//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence.dao;

import java.util.List;
import java.util.Optional;

import fr.edu.vortex.management.utilisateur.pojo.Role;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

public interface RoleDAO extends GenericDAO {

    Future<Role> creer(Role role);
    
    Future<Role> modifier(Role role);

    Future<Integer> delete(int id);

    Future<Optional<Role>> chercherRoleByID(int id);

    Future<List<Role>> chercherRoleParLibelle(String libelle);

    Future<List<Role>> listerRoles(int borneDebut, int borneFin, JsonObject filtres, String sort,
            String sortOrder);
	
	Future<Integer> countRoles(JsonObject filtres);
}
