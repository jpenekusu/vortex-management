//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.flux.FluxAdresses;
import fr.edu.vortex.management.utilisateur.pojo.Rapport;
import fr.edu.vortex.management.utilisateur.verticle.RapportVerticle;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ActionsApi extends AbstractApi {

	private static final Logger logger = LoggerFactory.getLogger(ActionsApi.class);

	public static final String URI_REFRESH_DATA = "/data_refresh";

	public static final String URI_REMETTRE_A_DISPOSITION = "/remettre_a_disposition";
	
	public static final String URI_LISTE_UTILISATEURS = "/liste_utilisateurs";
	
	public static final String URI_AUTH_REGISTER = "/authregister";

	protected static final String EVENT_BUS_REFRESH_DATA = "vortex.core.load.refreshData";

	private Vertx vertx;

	@Override
	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;
		Router router = Router.router(vertx);

		router.post(URI_REFRESH_DATA).handler(this::refreshData);

		router.post(URI_REMETTRE_A_DISPOSITION).handler(BodyHandler.create());
		
		router.post(URI_REMETTRE_A_DISPOSITION).handler(this::remettreADisposition);
		
		router.post(URI_LISTE_UTILISATEURS).handler(BodyHandler.create());
		router.post(URI_LISTE_UTILISATEURS).handler(this::importerUtilisateurs);
		
        router.post(URI_AUTH_REGISTER).handler(BodyHandler.create());
		router.post(URI_AUTH_REGISTER).handler(this::inscription);

		List<Route> routes = router.getRoutes();
		for (Route route : routes) {
			logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "route Action:" + route.getPath());
		}
		return router;
	}

	private void refreshData(RoutingContext ctx) {
		String action = "rafraichir les données";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		vertx.eventBus().send(EVENT_BUS_REFRESH_DATA, null, ar -> {
			if (ar.succeeded()) {
				sendEmpty200(ctx);
			} else {
				sendError500(ctx, action, ar.cause().getMessage());
			}
		});
	}

	private void remettreADisposition(RoutingContext context) {
		String action = "Remettre à disposition des messages";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		JsonObject json = null;
		try {
			json = context.getBodyAsJson();
		} catch (Exception e) {
			sendError(context, 400, action, "body manquant ou mal formaté");
		}
		if (json.isEmpty()) {
			sendError(context, 400, action, "body manquant ou mal formaté");
		} else {
    		String header = context.request().getHeader("Authorization");
    		json.put("header", header);
    		json.put("origine", Constants.TRACE_ORIGINE_CASSIS);
			vertx.eventBus().send(FluxAdresses.REMETTRE_A_DISPOSITION_MESSAGE, json, ar -> {
				if (ar.succeeded()) {
					this.replyDefaultNoContent(context, action, ar);
				} else {
					ReplyException cause = (ReplyException) ar.cause();
					sendError(context, cause.failureCode(), cause.getMessage());
				}
			});

		}

	}
	
	
	private void importerUtilisateurs(RoutingContext context) {
		String action = "Importer une liste d'utilisateurs";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		JsonObject json = null;
		try {
			json = context.getBodyAsJson();
		} catch (Exception e) {
			sendError(context, 400, action, "body manquant ou mal formaté");
		}
		if (json.isEmpty()) {
			sendError(context, 400, action, "body manquant ou mal formaté");
		} else {
		    	String header = context.request().getHeader("Authorization");
		    	json.put("header", header);
		    	json.put("origine", Constants.TRACE_ORIGINE_CASSIS);
			vertx.eventBus().send(UtilisateurVerticle.IMPORTER_UTILISATEURS, json, ar ->
			this.replyToClient(ar, context, 201, "importUtilisateur",header,Constants.TRACE_ORIGINE_CASSIS, Constants.TYPE_RAPPORT_IMPORT_UTILISATEUR)
			);

		}		
	}			
	
	private void inscription(RoutingContext context) {
		String action = "Enregistrer un nouvel utilisateur";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		JsonObject json = null;
		try {
			json = context.getBodyAsJson();
		} catch (Exception e) {
			sendError(context, 400, action, "body manquant ou mal formaté");
		}
		if (json.isEmpty()) {
			sendError(context, 400, action, "body manquant ou mal formaté");
		} else {
    		vertx.eventBus().send(UtilisateurVerticle.INSCRIRE_UTILISATEUR, json, ar -> {
				if (ar.succeeded()) {
					this.replyDefaultPost(context, action, ar);
				} else {
					ReplyException cause = (ReplyException) ar.cause();
					sendError(context, cause.failureCode(), cause.getMessage());
				}
			});

		}

	}

	
	private void replyToClient(AsyncResult<Message<Object>> asyncResult, RoutingContext ctx, int succesCode, 
		String operation, String header, String origine, String type_rapport) {
	ctx.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);
	if (asyncResult.succeeded()) {
		try {
			Message<Object> result = asyncResult.result();
			if (result.body() instanceof JsonObject) {
				JsonObject body = (JsonObject) result.body();
				body.put("header", header);
				body.put("origine", origine);
				body.put("type_rapport", type_rapport);
				
			
				vertx.eventBus().send(RapportVerticle.CREER_RAPPORT, body, ar -> {
        	                	if(ar.succeeded()) {
        	                	    JsonObject jsonResult = (JsonObject) ar.result().body();
        	                	    String dateRapport = jsonResult.getString(Rapport.KEY_JSON_DATE_RAPPORT);
        	                	    body.put(Rapport.KEY_JSON_DATE_RAPPORT, dateRapport);
        	                	    body.remove("header");	                	
        	                	    ctx.response().setStatusCode(succesCode).end(body.encode());	                	    
        	                	}else {
        	                	    ReplyException cause = (ReplyException) ar.cause();
        	                	    sendError(ctx, cause.failureCode(), operation, cause.getMessage());
        	                	}
	                    });
				
				
			} else {
				JsonArray body = (JsonArray) result.body();
				ctx.response().setStatusCode(succesCode).end(body.encode());
			}
		} catch (Exception e) {
			sendError500(ctx, operation, e.getMessage());
		}
	} else {
		try {
			ReplyException cause = (ReplyException) asyncResult.cause();
			sendError(ctx, cause.failureCode(), operation, cause.getMessage());
		} catch (Exception e) {
			sendError500(ctx, operation, e.getMessage());
		}
	}
}

}
