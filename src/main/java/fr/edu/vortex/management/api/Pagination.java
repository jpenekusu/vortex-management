//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.List;
import java.util.regex.Pattern;

//TODO javadoc
public class Pagination<T> {

    private int borneMoins;
    private int bornePlus;

    public int getBorneMoins() {
        return borneMoins;
    }

    public int getBornePlus() {
        return bornePlus;
    }

    public Pagination(int borneMoins, int bornePlus) {
        super();
        this.borneMoins = borneMoins;
        this.bornePlus = bornePlus;
    }

    public Pagination(String range) throws PaginationException {
        super();
        boolean matches = Pattern.matches("\\d+-\\d+", range);
        if (matches) {
            String[] split = range.split("-");
            if (Integer.parseInt(split[0]) > Integer.parseInt(split[1])) {
                throw new PaginationException();
            }
            borneMoins = Integer.parseInt(split[0]);
            bornePlus = Integer.parseInt(split[1]);
        } else {
            throw new PaginationException();
        }
    }

    public Pagination(String page, String pageLimit) throws PaginationException {
        super();
        int pageInt;
        int pageLimitInt;
        try {
            pageInt = Integer.parseInt(page);
            pageLimitInt = Integer.parseInt(pageLimit);
        } catch (NumberFormatException e) {
            throw new PaginationException();
        }
        if (pageInt <= 0 || pageLimitInt <= 0) {
            throw new PaginationException();
        }

        borneMoins = pageInt * pageLimitInt - pageLimitInt + 1;
        bornePlus = pageInt * pageLimitInt;

    }

    public List<T> apply(List<T> data) {
        borneMoins = (data.size() < borneMoins) ? data.size() : borneMoins;
        bornePlus = (data.size() < bornePlus) ? data.size() : bornePlus;
        return data.subList(borneMoins, bornePlus);
    }

}
