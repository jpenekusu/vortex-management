//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.net.HttpURLConnection;
import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.flux.Flux;
import fr.edu.vortex.management.flux.FluxAdresses;
import fr.edu.vortex.management.flux.FluxVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class FluxApi extends AbstractApi {

	private static final Logger logger = LoggerFactory.getLogger(FluxApi.class);

	private static final String URI_FLUX = "/flux";
	private static final String CORRELATION_ID = "/:correlation_id";

	private static final String URI_FLUX_PAR_LOT_NON_TROUVES = "/flux_not_found";


	Vertx vertx;

	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;

		Router router = Router.router(vertx);

		router.get(URI_FLUX).handler(this::getAllFlux);
		router.get(URI_FLUX + CORRELATION_ID).handler(this::getFluxByCorrelationId);
		router.get(URI_FLUX_PAR_LOT_NON_TROUVES).handler(this::getFluxNonTrouves);

		List<Route> routes = router.getRoutes();
		for (Route route : routes) {
			logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "Route Flux :" + route.getPath());
		}

		return router;

	}

	private void getAllFlux(RoutingContext ctx) {
		String action = "appel du webservice getAllFlux";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		JsonObject json = new JsonObject();
		json.put(FluxVerticle.KEY_JSON_PAGE, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE));
		json.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE_LIMIT));
		json.put(FluxVerticle.KEY_JSON_SORT, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT));
		json.put(FluxVerticle.KEY_JSON_SORT_ORDER, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT_ORDER));
		json.put(Flux.KEY_JSON_CORRELATION_ID, ctx.request().getParam(Flux.KEY_JSON_CORRELATION_ID));
		json.put(Flux.KEY_JSON_STATUS, ctx.request().getParam(Flux.KEY_JSON_STATUS));
		json.put(Flux.KEY_JSON_DATE, ctx.request().getParam(Flux.KEY_JSON_DATE));
		json.put(Flux.KEY_JSON_PRODUCER, ctx.request().getParam(Flux.KEY_JSON_PRODUCER));
		json.put(Flux.KEY_JSON_EXCHANGE_NAME, ctx.request().getParam(Flux.KEY_JSON_EXCHANGE_NAME));
		json.put(Flux.KEY_JSON_ROUTING_KEY, ctx.request().getParam(Flux.KEY_JSON_ROUTING_KEY));
		json.put(Flux.KEY_JSON_CONSUMER_CORRELATION_ID, ctx.request().getParam(Flux.KEY_JSON_CONSUMER_CORRELATION_ID));
		json.put(Flux.KEY_JSON_CONSUMER_BUSINESS_ID, ctx.request().getParam(Flux.KEY_JSON_CONSUMER_BUSINESS_ID));
		json.put(Flux.KEY_JSON_CONSUMER_QUEUE_NAME, ctx.request().getParam(Flux.KEY_JSON_CONSUMER_QUEUE_NAME));
		json.put(Flux.KEY_JSON_ERROR_CODE, ctx.request().getParam(Flux.KEY_JSON_ERROR_CODE));
		json.put(Flux.KEY_JSON_FILENAME, ctx.request().getParam(Flux.KEY_JSON_FILENAME));
		json.put(Flux.KEY_JSON_LIST_FILENAME, ctx.request().getParam(Flux.KEY_JSON_LIST_FILENAME));
		json.put(Flux.KEY_JSON_LIST_CORRELATION_ID, ctx.request().getParam(Flux.KEY_JSON_LIST_CORRELATION_ID));


        vertx.eventBus().send(FluxAdresses.LIST_FLUX, json, ar -> {
            if (ar.succeeded()) {
                String count = ar.result().headers().get("count");
                if (count != null) {
                    send206(ctx, count, (JsonArray) ar.result().body());
                } else {
                    this.replyDefaultGetJsonArray(ctx, "getAllFlux", ar);
                }
            } else {
                ReplyException cause = (ReplyException) ar.cause();
                sendError(ctx, cause.failureCode(), cause.getMessage());
            }
        });

	}
	
	private void getFluxNonTrouves(RoutingContext ctx) {
		String action = "appel du webservice getFluxNonTrouves";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		JsonObject json = new JsonObject();
		json.put(Flux.KEY_JSON_LIST_FILENAME, ctx.request().getParam(Flux.KEY_JSON_LIST_FILENAME));
		json.put(Flux.KEY_JSON_LIST_CORRELATION_ID, ctx.request().getParam(Flux.KEY_JSON_LIST_CORRELATION_ID));


        vertx.eventBus().send(FluxAdresses.LIST_FLUX_NON_TROUVES, json, ar -> {
            if (ar.succeeded()) {
            	JsonArray resultat = (JsonArray) ar.result().body();	
        		String count = String.valueOf(resultat.size());
                send206(ctx, count, resultat);
            } else {
                ReplyException cause = (ReplyException) ar.cause();
                sendError(ctx, cause.failureCode(), cause.getMessage());
            }
        });

	}


	private void getFluxByCorrelationId(RoutingContext ctx) {
		String correlationId = ctx.request().getParam("correlation_id");
		String consumerCorrelationId = ctx.request().getParam("consumer_correlation_id");
		String action = "appel du webservice getFluxBycorrelationId";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		if (consumerCorrelationId == null) {
			vertx.eventBus().send(FluxAdresses.GET_FLUX_BY_CORRELATION_ID, correlationId, ar -> {
				this.replyDefaultGetJsonArray(ctx, "getFluxByCorrelationId", ar);
			});
		} else {
			JsonObject json = new JsonObject();
			json.put(Flux.KEY_JSON_CORRELATION_ID, correlationId);
			json.put(Flux.KEY_JSON_CONSUMER_CORRELATION_ID, consumerCorrelationId);

			vertx.eventBus().send(FluxAdresses.GET_FLUX_BY_CORRELATIONS_ID_FLUX_AND_CONSUMER, json, ar -> {
				this.replyDefaultGetJsonArray(ctx, "getFluxByCorrelationsId", ar);
			});
		}

	}


    protected void send206(RoutingContext ctx, String count, JsonArray body) {
        ctx.response().setStatusCode(HttpURLConnection.HTTP_PARTIAL)
                .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8)
                .putHeader("x-total-count", count).end(body.encode());
    }

}
