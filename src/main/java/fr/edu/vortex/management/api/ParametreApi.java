//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

/**
 * 
 */
package fr.edu.vortex.management.api;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.parametre.Parametre;
import fr.edu.vortex.management.parametre.ParametreAdresses;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class ParametreApi extends AbstractApi {

	public static final String URI_PARAMETRE = "/parametres";
	private static final String CODE = "/:code";

	private static final Logger logger = LoggerFactory.getLogger(ParametreApi.class);

	Vertx vertx;

	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;

		Router router = Router.router(vertx);

		router.get(URI_PARAMETRE + CODE).handler(this::getParametreByCode);

		return router;

	}

	private void getParametreByCode(RoutingContext ctx) {
		String action = "appel du webservice getParametreByCode";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		String code = ctx.request().getParam("code");

		JsonObject json = new JsonObject();
		json.put(Parametre.KEY_JSON_CODE, code);

		vertx.eventBus().send(ParametreAdresses.GET_PARAMETRE_BY_CODE, json, ar -> {
			this.replyDefaultGetJsonObject(ctx, "getParametreByCode", ar);
		});

	}
}
