//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.edu.echanges.nat.vortex.common.adresses.core.health.CoreHealthAdresses;
import fr.edu.echanges.nat.vortex.common.adresses.gateway.health.GatewayHealthAdresses;
import fr.edu.echanges.nat.vortex.common.adresses.management.health.ManagementHealthAdresses;
import fr.edu.vortex.management.Constants;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class RacvisionApi extends AbstractApi {
    private static final String URI_RACVISION = "/racvision";

    private static final String RESPONSE_KEY_NAME = "name";

    private static final String MANAGEMENT = "vortex-management";
    private static final String GATEWAY = "vortex-http-api";
    private static final String CORE = "vortex-core";

    private static final String DATABASE_CORE = "vortex-core-db";
    private static final String DATABASE_GATEWAY = "vortex-http-api-db";
    private static final String DATABASE_MANAGEMENT = "vortex-management-db";

    private static final String VERTX_HEALTH_CHECK_KEY_CHECKS = "checks";
    private static final String VERTX_HEALTH_CHECK_KEY_ID = "id";
    private static final String VERTX_HEALTH_CHECK_KEY_MAIN = "main";
    private static final String VERTX_HEALTH_CHECK_KEY_DATA = "data";
    private static final String VERTX_HEALTH_CHECK_KEY_OUTCOME = "outcome";
    private static final String VERTX_HEALTH_CHECK_UP = "UP";

    private static final String KEY_STATE = "state";
    private static final String KEY_CAUSE = "cause";

    private static final Logger logger = LoggerFactory.getLogger(RacvisionApi.class);

    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private Vertx vertx;

    @Override
    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;

        Router router = Router.router(vertx);
        router.get(URI_RACVISION).handler(this::getRacvision);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "route Racvision:" + route.getPath());
        }

        return router;
    }

    // XXX: Ajouter quelque part (fichier de config ?), le nom de l'appli et la
    // version !
    private void getRacvision(RoutingContext context) {
        Future<JsonObject> futCore = callEventBus(CoreHealthAdresses.GET_HEALTH);
        Future<JsonObject> futDbCore = callEventBus(CoreHealthAdresses.GET_DATABASE_HEALTH);
        Future<JsonObject> futGateway = callEventBus(GatewayHealthAdresses.GET_HEALTH);
        Future<JsonObject> futDbGateway = callEventBus(GatewayHealthAdresses.GET_DATABASE_HEALTH);
        Future<JsonObject> futManage = callEventBus(ManagementHealthAdresses.GET_HEALTH);
        Future<JsonObject> futDbManage = callEventBus(ManagementHealthAdresses.GET_DATABASE_HEALTH);

        CompositeFuture.join(futCore, futDbCore, futGateway, futDbGateway, futManage, futDbManage).setHandler(handler -> {
            List<JsonObject> statusList = new ArrayList<>();
            statusList.add((futCore.succeeded()) ? futCore.result() : createStateFalse(CORE, futCore.cause().getMessage()));
            statusList.add((futDbCore.succeeded()) ? futDbCore.result() : createStateFalse(DATABASE_CORE, futDbCore.cause().getMessage()));
            statusList.add((futGateway.succeeded()) ? futGateway.result() : createStateFalse(GATEWAY, futGateway.cause().getMessage()));
            statusList.add((futDbGateway.succeeded()) ? futDbGateway.result() : createStateFalse(DATABASE_GATEWAY, futDbGateway.cause().getMessage()));
            statusList.add((futManage.succeeded()) ? futManage.result() : createStateFalse(MANAGEMENT, futManage.cause().getMessage()));
            statusList.add((futDbManage.succeeded()) ? futDbManage.result() : createStateFalse(DATABASE_MANAGEMENT, futDbManage.cause().getMessage()));

            StringBuilder sb = new StringBuilder();

            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            sb.append(
                    "<!DOCTYPE apptest SYSTEM \"http://racvision.orion.education.fr/page-de-test/apptest-1.8.dtd\">\n");
            sb.append("<apptest>\n");
            sb.append(String.format("  <application id=\"%s\">%n", "OMOGEN"));
            sb.append(String.format("    <name>%s</name>%n", "OMOGEN"));
            sb.append(String.format("    <version>%s</version>%n", "1.0.0"));
            sb.append(String.format("    <description>%s</description>%n", "SE du MEN"));

            for (JsonObject status : statusList) {
                if (status.getBoolean(KEY_STATE)) {
                    sb.append(String.format("    <test id=\"%s-%s\">%n", status.getString(RESPONSE_KEY_NAME),
                            status.getString("host")));
                    sb.append(String.format("      <description>Test de disponibilité du service %s</description>%n",
                            status.getString(RESPONSE_KEY_NAME)));
                    sb.append("      <state val=\"OK\" />\n");
                    sb.append("    </test>\n");
                } else {
                    sb.append(String.format("    <test id=\"%s\">%n", status.getString(RESPONSE_KEY_NAME)));
                    sb.append(String.format("      <description>Test de disponibilité du service %s</description>%n",
                            status.getString(RESPONSE_KEY_NAME)));
                    sb.append(String.format("      <state val=\"CRIT\">%s</state>%n", status.getString(KEY_CAUSE)));
                    sb.append("    </test>\n");
                }
            }

            sb.append("  </application>\n");
            sb.append(String.format("  <date>%s</date>%n", LocalDateTime.now().format(dtf)));
            sb.append("</apptest>");

            context.response().setStatusCode(200).putHeader(HttpHeaders.CONTENT_TYPE, "application/xml")
                    .end(sb.toString());
        });
    }

    private JsonObject createStateFalse(String component, String cause) {
        return new JsonObject().put(RESPONSE_KEY_NAME, component).put(KEY_STATE, false).put(KEY_CAUSE, cause);
    }

    private Future<JsonObject> callEventBus(String address) {
        Future<JsonObject> futEvent = Future.future();
        vertx.eventBus().send(address, null, ar -> {
            if (ar.succeeded()) {
                try {
                    JsonObject healthCheck = (JsonObject) ar.result().body();
                    JsonObject response = transformToResponse(healthCheck);
                    futEvent.complete(response);
                } catch (Exception e) {
                    futEvent.fail(e.getMessage());
                }
            } else {
                futEvent.fail(ar.cause().getMessage());
            }
        });
        return futEvent;
    }

    private JsonObject transformToResponse(JsonObject healthCheck) {
        JsonObject response;
        String status;

        if (healthCheck.containsKey(VERTX_HEALTH_CHECK_KEY_OUTCOME)) {
            status = healthCheck.getString(VERTX_HEALTH_CHECK_KEY_OUTCOME);
        } else {
            throw new DecodeException(VERTX_HEALTH_CHECK_KEY_OUTCOME + " manquante");
        }

        Optional<JsonObject> info = healthCheck.getJsonArray(VERTX_HEALTH_CHECK_KEY_CHECKS).stream()
                .map(proc -> (JsonObject) proc)
                .filter(proc -> VERTX_HEALTH_CHECK_KEY_MAIN.equals(proc.getString(VERTX_HEALTH_CHECK_KEY_ID)))
                .findFirst();
        if (info.isPresent()) {
            response = info.get().getJsonObject(VERTX_HEALTH_CHECK_KEY_DATA);
            boolean state = VERTX_HEALTH_CHECK_UP.equals(status);
            response.put(KEY_STATE, state);
        } else {
            throw new DecodeException();
        }
        return response;
    }

}
