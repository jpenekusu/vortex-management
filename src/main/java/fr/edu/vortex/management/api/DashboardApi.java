//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class DashboardApi extends AbstractApi {

	private static final Logger logger = LoggerFactory.getLogger(DashboardApi.class);

	public static final String URI = "/dashboard";
	public static final String URI_REPARTITION = "/repartition";

	public static final String URI_PARTENAIRES = "/partenaires";

	public static final String KEY_JSON_PARTENAIRE = "partenaire";

	public static final String ADRESS_GET_REPARTITION = "vortex.management.dashboard.getRepartitionMessages";
	public static final String ADRESS_GET_PARTENAIRES_SECURISES = "vortex.management.dashboard.getPartenaires";

	Vertx vertx;

	@Override
	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;
		Router router = Router.router(vertx);

		router.post(URI + URI_REPARTITION).handler(BodyHandler.create());
		router.post(URI + URI_REPARTITION).handler(this::getRepartition);

		router.get(URI + URI_PARTENAIRES).handler(this::getPartenairesSecurises);

		return router;
	}

	private void getRepartition(RoutingContext context) {
		String action = "appel du webservice getRepartition";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		JsonObject filters = new JsonObject();       
		try {
			filters = context.getBodyAsJson();
        } catch (Exception e) {
            //pas grave on ne filtre pas
        }
  
//		String partenaire = context.request().getParam(KEY_JSON_PARTENAIRE);
//		String depot = context.request().getParam(Flux.KEY_DB_EXCHANGE_NAME);
//		String zoneReception = context.request().getParam(Flux.KEY_DB_CONSUMER_QUEUE_NAME);
//		String startDate = context.request().getParam(FluxVerticle.KEY_JSON_START_DATE_FILTER);
//		String endDate = context.request().getParam(FluxVerticle.KEY_JSON_END_DATE_FILTER);
//
//		filters.put(KEY_JSON_PARTENAIRE, partenaire);
//		filters.put(Flux.KEY_DB_EXCHANGE_NAME, depot);
//		filters.put(Flux.KEY_DB_CONSUMER_QUEUE_NAME, zoneReception);		
//		filters.put(FluxVerticle.KEY_JSON_START_DATE_FILTER, startDate);
//		filters.put(FluxVerticle.KEY_JSON_END_DATE_FILTER, endDate);

		vertx.eventBus().send(ADRESS_GET_REPARTITION, filters,
				ar -> this.replyToClient(ar, context, 200, "getRepartition"));
	}
	
	private void getPartenairesSecurises(RoutingContext context) {
		String action = "appel du webservice getPartenairesSecurises";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		JsonObject filters = new JsonObject();       

		vertx.eventBus().send(ADRESS_GET_PARTENAIRES_SECURISES, filters,
				ar -> this.replyToClient(ar, context, 200, "getPartenairesSecurises"));
	}

	private void replyToClient(AsyncResult<Message<Object>> asyncResult, RoutingContext ctx, int succesCode,
			String operation) {
		ctx.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);
		if (asyncResult.succeeded()) {
			try {
				Message<Object> result = asyncResult.result();
				if (result.body() instanceof JsonObject) {
					JsonObject body = (JsonObject) result.body();
					ctx.response().setStatusCode(succesCode).end(body.encode());
				} else {
					JsonArray body = (JsonArray) result.body();
					ctx.response().setStatusCode(succesCode).end(body.encode());
				}
			} catch (Exception e) {
				sendError500(ctx, operation, e.getMessage());
			}
		} else {
			try {
				ReplyException cause = (ReplyException) asyncResult.cause();
				sendError(ctx, cause.failureCode(), operation, cause.getMessage());
			} catch (Exception e) {
				sendError500(ctx, operation, e.getMessage());
			}
		}
	}
}
