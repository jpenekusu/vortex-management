package fr.edu.vortex.management.api;

import java.net.HttpURLConnection;
import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.mail.ModeleMail;
import fr.edu.vortex.management.mail.ModeleMailVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ModeleMailApi extends AbstractApi {

    private static final String ACTION_CREER_MODELE_MAIL = "appel du webservice creerModeleMail";
    private static final String ACTION_MODIFIER_MODELE_MAIL = "appel du webservice modifierModeleMail";
    private static final String ACTION_DELETE_MODELE_MAIL = "appel du webservice deleteModeleMail";
    private static final String ACTION_LISTER_MODELE_MAILS = "listerModeleMail(RoutingContext ctx)";
    private static final String MESSAGE_BAD_REQUEST = "body manquant ou mal formaté";

    private static final String ERR_ID_NOT_INTEGER = "l'identifiant doit être un entier";

    private static final Logger logger = LoggerFactory.getLogger(ModeleMailApi.class);

	public static final String URI = "/modele_mail";
    private static final String ID = "id";
    public static final String PARAM_ID = "/:" + ID;


    Vertx vertx;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;

        Router router = Router.router(vertx);

        router.get(URI).handler(this::listerModeleMail);

        router.delete(URI + PARAM_ID).handler(this::deleteModeleMail);

        router.post(URI).handler(BodyHandler.create());
        router.post(URI).handler(this::creerModeleMail);

        router.put(URI + PARAM_ID).handler(BodyHandler.create());
        router.put(URI + PARAM_ID).handler(this::modifierModeleMail);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID,
                    "Route modele_mail :" + route.getPath());
        }

        return router;
    }
    
    private void listerModeleMail(RoutingContext ctx) {

        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_LISTER_MODELE_MAILS);

		JsonObject jsonCritere = new JsonObject();
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE));
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE_LIMIT));
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT));
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT_ORDER, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT_ORDER));
		
		String systemeParam = ctx.request().getParam(ModeleMail.KEY_JSON_SYSTEME);
        if (ctx.request().getParam(ModeleMail.KEY_JSON_SYSTEME) != null) {
            if (systemeParam.toUpperCase().equals("TRUE") || systemeParam.toUpperCase().equals("FALSE")) {
            	jsonCritere.put(ModeleMail.KEY_JSON_SYSTEME,
                        Boolean.valueOf(systemeParam));
            } else {
                sendError(ctx, 400, ACTION_LISTER_MODELE_MAILS, "Paramètre système mal formaté");
            }
        }
		jsonCritere.put(ModeleMail.KEY_JSON_NOM, ctx.request().getParam(ModeleMail.KEY_JSON_NOM));
		jsonCritere.put(ModeleMail.KEY_JSON_DESTINATAIRE, ctx.request().getParam(ModeleMail.KEY_JSON_DESTINATAIRE));
		jsonCritere.put(ModeleMail.KEY_JSON_PERMISSIONS, ctx.request().getParam(ModeleMail.KEY_JSON_PERMISSIONS));

		vertx.eventBus().send(ModeleMailVerticle.LISTER_MODELE_MAIL, jsonCritere, ar -> {
			if (ar.succeeded()) {
				String count = ar.result().headers().get("count");
				if (count != null) {
					send206(ctx, count, (JsonArray) ar.result().body());
				} else {
					this.replyDefaultGetJsonArray(ctx, ACTION_LISTER_MODELE_MAILS, ar);
				}
			} else {
				ReplyException cause = (ReplyException) ar.cause();
				sendError(ctx, cause.failureCode(), cause.getMessage());
			}
		});		
    }

    private void deleteModeleMail(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_DELETE_MODELE_MAIL);

        String id = ctx.request().getParam(ID);
        
        try {
        	JsonObject data = new JsonObject().put(ModeleMail.KEY_JSON_ID, Integer.valueOf(id));
            

            ctx.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);
            vertx.eventBus().send(ModeleMailVerticle.SUPPRIMER_MODELE_MAIL, data,
                    ar -> replyDefaultDelete(ctx, ACTION_DELETE_MODELE_MAIL, ar));
        } catch (NumberFormatException e) {
            sendError400(ctx, ERR_ID_NOT_INTEGER);
        }
    }

    private void creerModeleMail(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_CREER_MODELE_MAIL);

        JsonObject jsonModeleMail = new JsonObject();
        try {
            jsonModeleMail = ctx.getBodyAsJson();
        } catch (Exception e) {
            sendError(ctx, HttpURLConnection.HTTP_BAD_REQUEST, ACTION_CREER_MODELE_MAIL, MESSAGE_BAD_REQUEST);
        }

        if (!jsonModeleMail.isEmpty()) {
            vertx.eventBus().send(ModeleMailVerticle.CREER_MODELE_MAIL, jsonModeleMail,
                    ar -> replyDefaultPost(ctx, ACTION_CREER_MODELE_MAIL, ar));
        }
    }
    
    private void modifierModeleMail(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_MODIFIER_MODELE_MAIL);

        JsonObject jsonModeleMail = new JsonObject();
        try {
            jsonModeleMail = ctx.getBodyAsJson();
        } catch (Exception e) {
            sendError(ctx, HttpURLConnection.HTTP_BAD_REQUEST, ACTION_MODIFIER_MODELE_MAIL, MESSAGE_BAD_REQUEST);
        }

        if (!jsonModeleMail.isEmpty()) {
            vertx.eventBus().send(ModeleMailVerticle.MODIFIER_MODELE_MAIL, jsonModeleMail,
                    ar -> replyDefaultPost(ctx, ACTION_MODIFIER_MODELE_MAIL, ar));
        }
    }

}
