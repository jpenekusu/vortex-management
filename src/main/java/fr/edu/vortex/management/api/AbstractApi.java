//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import static java.net.HttpURLConnection.HTTP_OK;

import java.net.HttpURLConnection;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.utilisateur.verticle.TokenVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public abstract class AbstractApi {

    public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json; charset=utf-8";

    protected static final String ERR_ID_NOT_INTEGER = "l'identifiant doit être un entier";

    private static final Logger logger = LoggerFactory.getLogger(AbstractApi.class);

    public abstract Router getRouterApi(Vertx vertx);

    // protected abstract Logger getLogger();

    /**
     * Repond au client une erreur 400.
     * 
     * @param context
     *            RoutingContext
     * @param message
     *            message message a logger
     */
    protected void sendError400(RoutingContext context, String message) {
        sendError(context, HttpURLConnection.HTTP_BAD_REQUEST, null, message);
    }

    /**
     * Repond au client une erreur 404.
     * 
     * @param context
     *            RoutingContext
     * @param message
     *            message message a logger
     */
    protected void sendError404(RoutingContext context, String message) {
        sendError(context, HttpURLConnection.HTTP_NOT_FOUND, null, message);
    }

    /**
     * Repond au client une erreur 500.
     * 
     * @param context
     *            RoutingContext
     * @param message
     *            message message a logger
     */
    protected void sendError500(RoutingContext context, String message) {
        sendError(context, HttpURLConnection.HTTP_INTERNAL_ERROR, null, message);
    }

    /**
     * Repond au client une erreur 500.
     * 
     * @param context
     *            RoutingContext
     * @param message
     *            message message a logger
     */
    protected void sendError500(RoutingContext context, String action, String message) {
        sendError(context, HttpURLConnection.HTTP_INTERNAL_ERROR, action, message);
    }
        
    protected void send401(RoutingContext context, String action) {
        VortexError error = new VortexError(HttpURLConnection.HTTP_UNAUTHORIZED, "Jeton invalide. Veuillez vous reconnecter.");
        logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, error);

        context.response().putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8)
        		.putHeader("WWW-Authenticate","Bearer").setStatusCode(HttpURLConnection.HTTP_UNAUTHORIZED)
                .end(error.toJsonObject().encode());
    }


    /**
     * Repond au client une erreur.
     * 
     * @param context
     *            RoutingContext
     * @param code
     *            code http retourné au client
     * @param message
     *            message a logger
     */
    protected void sendError(RoutingContext context, int code, String message) {
        sendError(context, code, null, message);
    }

    /**
     * Repond au client une erreur.
     * 
     * @param context
     *            RoutingContext
     * @param replyException
     */
    protected void sendError(RoutingContext context, ReplyException replyException) {
        sendError(context, replyException.failureCode(), replyException.getMessage());
    }

    /**
     * Repond au client une réponse 200 sans body
     * 
     * @param context
     *            RoutingContext
     */
    protected void sendEmpty200(RoutingContext context) {
        sendCodeNoBody(context, HttpURLConnection.HTTP_OK);
    }

    protected void send204(RoutingContext context) {
        sendCodeNoBody(context, HttpURLConnection.HTTP_NO_CONTENT);
    }

    protected void send200WithJsonObject(RoutingContext context, JsonObject json) {
        send200WithBody(context, json);
    }

    protected void send200WithJsonArray(RoutingContext context, JsonArray json) {
        send200WithBody(context, json);
    }

    protected void send200WithString(RoutingContext context, String body) {
        context.response().putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8)
                .setStatusCode(HttpURLConnection.HTTP_OK).end(body);
    }

    private void send200WithBody(RoutingContext context, Object json) {
        context.response().putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8)
                .setStatusCode(HttpURLConnection.HTTP_OK).end(Json.encode(json));
    }

    private void sendCodeNoBody(RoutingContext context, int code) {
        context.response().putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8).setStatusCode(code)
                .end();
    }

    protected void send206(RoutingContext ctx, int count, Pagination<JsonObject> pagi, JsonArray body) {
        ctx.response().setStatusCode(HttpURLConnection.HTTP_PARTIAL)
                .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8)
                .putHeader(HttpHeaders.CONTENT_RANGE, pagi.getBorneMoins() + "-" + pagi.getBornePlus() + "/" + count)
                .end(body.encode());
    }

    /**
     * Repond au client une erreur. context RoutingContext
     * 
     * @param code
     *            code http retourné au client
     * @param action
     *            action pour le log
     * @param message
     *            message a logger
     */
    protected void sendError(RoutingContext context, int code, String action, String message) {
        int codeHttp = (code >= 0) ? code : 500;
        VortexError error = new VortexError(codeHttp, message);
        logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, error);

        context.response().putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8).setStatusCode(codeHttp)
                .end(error.toJsonObject().encode());
    }

    /**
     * Après un retour d'un service (eventBus) qui permet de créer une ressource
     * retourne 201 et la ressource créée.
     * 
     * @param context
     *            RoutingContext
     * @param action
     *            action de log
     * @param replyHandler
     *            le retour async de l'event bus
     */
    protected void replyDefaultPost(RoutingContext context, String action, AsyncResult<Message<Object>> replyHandler) {
        if (replyHandler.succeeded()) {
            try {
                JsonObject created = (JsonObject) replyHandler.result().body();
                context.response().putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8)
                        .setStatusCode(HttpURLConnection.HTTP_CREATED).end(created.encode());
            } catch (Exception e) {
                sendError500(context, action, e.getMessage());
            }
        } else {
            ReplyException cause = (ReplyException) replyHandler.cause();
            sendError(context, cause.failureCode(), action, cause.getMessage());
        }
    }

    /**
     * Après un retour d'un service (eventBus) qui permet de supprimer une ressource
     * retourne 204 et rien dans le body.
     * 
     * @param context
     * @param action
     * @param replyHandler
     */
    protected void replyDefaultDelete(RoutingContext context, String action,
            AsyncResult<Message<Object>> replyHandler) {
        if (replyHandler.succeeded()) {
            send204(context);
        } else {
            ReplyException cause = (ReplyException) replyHandler.cause();
            sendError(context, cause.failureCode(), action, cause.getMessage());
        }
    }
    
    /**
     * Après un retour d'un service (eventBus) qui exécute une action
     * retourne 204 et rien dans le body.
     * 
     * @param context
     * @param action
     * @param replyHandler
     */
    protected void replyDefaultNoContent(RoutingContext context, String action,
            AsyncResult<Message<Object>> replyHandler) {
        if (replyHandler.succeeded()) {
            send204(context);
        } else {
            ReplyException cause = (ReplyException) replyHandler.cause();
            sendError(context, cause.failureCode(), action, cause.getMessage());
        }
    }

    /**
     * Après un retour d'un service (eventBus) qui permet de récupérer une ressource
     * sous la forme d'un json. retourne 200 et la ressource dans le body Si le json
     * est vide retourne une 204 Si le resultat asynchrone échoue retourne une
     * erreur sauf si le retour de l'exception est une 204 alors retourne une 204
     * 
     * @param context
     * @param action
     * @param replyHandler
     */
    protected void replyDefaultGetJsonObject(RoutingContext context, String action,
            AsyncResult<Message<Object>> replyHandler) {
        if (replyHandler.succeeded()) {
            try {
                JsonObject json = (JsonObject) replyHandler.result().body();
                if (json.isEmpty()) {
                    send204(context);
                } else {
                    send200WithJsonObject(context, json);
                }
            } catch (Exception e) {
                sendError500(context, e.getMessage());
            }
        } else {
            ReplyException cause = (ReplyException) replyHandler.cause();
            if (cause.failureCode() == HttpURLConnection.HTTP_NO_CONTENT) {
                send204(context);
            } else {
                sendError(context, cause.failureCode(), action, cause.getMessage());
            }
        }
    }

    /**
     * Pagination
     * 
     * @param ctx
     * @param count
     * @param body
     */
	protected void send206(RoutingContext ctx, String count, JsonArray body) {
		ctx.response().setStatusCode(HttpURLConnection.HTTP_PARTIAL)
		.putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8)
		.putHeader("x-total-count", count).end(body.encode());
	}
	
    /**
     * Après un retour d'un service (eventBus) qui permet de récupérer une ressource
     * sous la forme d'un integer. retourne 200. Si le resultat asynchrone échoue
     * retourne une erreur
     * 
     * @param context
     * @param action
     * @param replyHandler
     */
    protected void replyDefaultGetInteger(RoutingContext context, String action,
            AsyncResult<Message<Object>> replyHandler) {
        if (replyHandler.succeeded()) {
            try {
                Integer integer = (Integer) replyHandler.result().body();
                if (integer == null) {
                    send200WithString(context, "0");
                } else {
                    send200WithString(context, integer.toString());
                }
            } catch (Exception e) {
                sendError500(context, e.getMessage());
            }
        } else {
            ReplyException cause = (ReplyException) replyHandler.cause();
            if (cause.failureCode() == HttpURLConnection.HTTP_NO_CONTENT) {
                send204(context);
            } else if (cause.failureCode() == HttpURLConnection.HTTP_NOT_FOUND) {
                sendError404(context, cause.getMessage());
            } else {
                sendError(context, cause.failureCode(), action, cause.getMessage());
            }
        }
    }

    /**
     * Après un retour d'un service (eventBus) qui permet de récupérer un tableau de
     * ressource. retourne 200 et le tableau de ressource dans le body
     * 
     * @param context
     * @param action
     * @param replyHandler
     */
    protected void replyDefaultGetJsonArray(RoutingContext context, String action,
            AsyncResult<Message<Object>> replyHandler) {
        if (replyHandler.succeeded()) {
            try {
                JsonArray jsonArray = (JsonArray) replyHandler.result().body();
                sendJsonArray(context, jsonArray);
            } catch (Exception e) {
                sendError500(context, e.getMessage());
            }
        } else {
            ReplyException cause = (ReplyException) replyHandler.cause();
            sendError(context, cause.failureCode(), action, cause.getMessage());
        }
    }

    private void sendJsonArray(RoutingContext context, JsonArray json) {
        if (json.size() == 0) {
            send204(context);
        } else {
            send200WithJsonArray(context, json);
        }
    }
    
    protected Future<Integer> isTokenOK(HttpServerRequest request, Vertx vertx){
    	Future<Integer> future = Future.future();

    	String header = request.getHeader("Authorization");

    	JsonObject jsonHeader = new JsonObject();
    	jsonHeader.put("jwt", header);
    	vertx.eventBus().send(TokenVerticle.TOKEN_UTILISATEUR, jsonHeader,
    			ar -> {
    				if (ar.succeeded()) {
    					future.complete(HTTP_OK);               		
    				} else {
    					future.fail(ar.cause());               		
    				}
    			}); 
    	return future;
    }

}
