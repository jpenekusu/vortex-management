package fr.edu.vortex.management.api;

import java.net.HttpURLConnection;
import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.utilisateur.pojo.Filtre;
import fr.edu.vortex.management.utilisateur.pojo.Trace;
import fr.edu.vortex.management.utilisateur.verticle.FiltreUtilisateurVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class FiltreUtilisateurApi extends AbstractApi {

    private static final String ACTION_CREER_FILTRE_UTILISATEUR = "appel du webservice creerFiltreUtilisateur";

    private static final String ACTION_MODIFIER_FILTRE_UTILISATEUR = "appel du webservice modifierFiltreUtilisateur";


    private static final String ACTION_DELETE_FILTRE_UTILISATEUR = "appel du webservice deleteFiltreUtilisateur";

    private static final String ACTION_LISTER_FILTRE_UTILISATEURS = "listerFiltreUtilisateurs(RoutingContext ctx)";

    private static final String MESSAGE_BAD_REQUEST = "body manquant ou mal formaté";
    
    private static final Logger logger = LoggerFactory.getLogger(FiltreUtilisateurApi.class);

    public static final String URI_UTILISATEUR_FILTRE = "/utilisateurs_filtres";
    private static final String ID = "id";
    public static final String PARAM_ID = "/:" + ID;
    
    
    Vertx vertx;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;

        Router router = Router.router(vertx);

        router.get(URI_UTILISATEUR_FILTRE).handler(this::listerFiltreUtilisateurs);

        router.delete(URI_UTILISATEUR_FILTRE + PARAM_ID).handler(this::deleteFiltreUtilisateur);

        router.post(URI_UTILISATEUR_FILTRE).handler(BodyHandler.create());
        router.post(URI_UTILISATEUR_FILTRE).handler(this::creerFiltreUtilisateur);

        router.put(URI_UTILISATEUR_FILTRE + PARAM_ID).handler(BodyHandler.create());
        router.put(URI_UTILISATEUR_FILTRE + PARAM_ID).handler(this::modifierFiltreUtilisateur);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID,
                    "Route utilisateurs_filtres :" + route.getPath());
        }

        return router;
    }
     
    
    private void listerFiltreUtilisateurs(RoutingContext ctx) {

        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_LISTER_FILTRE_UTILISATEURS);

		JsonObject jsonCritere = new JsonObject();
		jsonCritere.put(Filtre.KEY_JSON_ID, ctx.request().getParam(Filtre.KEY_JSON_ID));
		jsonCritere.put(Filtre.KEY_JSON_NOM, ctx.request().getParam(Filtre.KEY_JSON_NOM));
		jsonCritere.put(Filtre.KEY_JSON_TYPE_FILTRE, ctx.request().getParam(Filtre.KEY_JSON_TYPE_FILTRE));
		jsonCritere.put(Filtre.KEY_JSON_UTILISATEUR_ID, ctx.request().getParam(Filtre.KEY_JSON_UTILISATEUR_ID));

        vertx.eventBus().send(FiltreUtilisateurVerticle.LISTER_FILTRE_UTILISATEUR, jsonCritere,
                ar -> this.replyDefaultGetJsonArray(ctx, ACTION_LISTER_FILTRE_UTILISATEURS, ar));
    }

    private void deleteFiltreUtilisateur(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_DELETE_FILTRE_UTILISATEUR);

        String id = ctx.request().getParam(ID);
        JsonObject data = new JsonObject().put(ID, id);

        ctx.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);
        vertx.eventBus().send(FiltreUtilisateurVerticle.SUPPRIMER_FILTRE_UTILISATEUR, data,
                ar -> replyDefaultDelete(ctx, ACTION_DELETE_FILTRE_UTILISATEUR, ar));
    }

    private void creerFiltreUtilisateur(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_CREER_FILTRE_UTILISATEUR);

        JsonObject jsonFiltre = new JsonObject();
        try {
            jsonFiltre = ctx.getBodyAsJson();
        } catch (Exception e) {
            sendError(ctx, HttpURLConnection.HTTP_BAD_REQUEST, ACTION_CREER_FILTRE_UTILISATEUR, MESSAGE_BAD_REQUEST);
        }

        if (!jsonFiltre.isEmpty()) {
            vertx.eventBus().send(FiltreUtilisateurVerticle.CREER_FILTRE_UTILISATEUR, jsonFiltre,
                    ar -> replyDefaultPost(ctx, ACTION_CREER_FILTRE_UTILISATEUR, ar));
        }
    }
    
    private void modifierFiltreUtilisateur(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_MODIFIER_FILTRE_UTILISATEUR);

        JsonObject jsonFiltre = new JsonObject();
        try {
            jsonFiltre = ctx.getBodyAsJson();
        } catch (Exception e) {
            sendError(ctx, HttpURLConnection.HTTP_BAD_REQUEST, ACTION_MODIFIER_FILTRE_UTILISATEUR, MESSAGE_BAD_REQUEST);
        }

        if (!jsonFiltre.isEmpty()) {
            vertx.eventBus().send(FiltreUtilisateurVerticle.MODIFIER_FILTRE_UTILISATEUR, jsonFiltre,
                    ar -> replyDefaultPost(ctx, ACTION_MODIFIER_FILTRE_UTILISATEUR, ar));
        }
    }

}
