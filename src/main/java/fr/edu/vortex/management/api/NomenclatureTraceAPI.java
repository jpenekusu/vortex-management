//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.Iterator;
import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureEvtTrace;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureTypeTrace;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class NomenclatureTraceAPI extends AbstractApi {

	private static final Logger logger = LoggerFactory.getLogger(NomenclatureTraceAPI.class);

	public static final String URI_LIST_TYPES_TRACES = "/traces_types/";
	public static final String URI_LIST_EVENEMENTS_TRACES = "/traces_evenements/";

	private static final String ACTION_GET_ALL_TYPE_TRACE = "Lister les types de traces";
	private static final String ACTION_GET_ALL_EVENEMENT_TRACE = "Lister les evenements de traces";

	Vertx vertx;

	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;
		Router router = Router.router(vertx);

		router.get(URI_LIST_TYPES_TRACES).handler(this::getTypesTraces);
		router.get(URI_LIST_EVENEMENTS_TRACES).handler(this::getEvenementsTraces);

		return router;
	}

	private void getTypesTraces( RoutingContext context) {


		context.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);

		JsonArray response = new JsonArray();

		// appel a la nomenclature recuperation de la liste des types de traces
		List<String> listeValeurs = NomenclatureTypeTrace.getListeValeurs();
		for (Iterator<String> iterator = listeValeurs.iterator(); iterator.hasNext();) {
			response.add(iterator.next());
		}

		context.response().setStatusCode(200).end(response.encode());

		logger.debug(
				Constants.LOG_MESSAGE,
				Constants.LOG_NO_CORRELATION_ID,
				ACTION_GET_ALL_TYPE_TRACE);

	}

	private void getEvenementsTraces( RoutingContext context) {


		context.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);

		JsonArray response = new JsonArray();

		// appel a la nomenclature recuperation de la liste des evenements de traces
		List<String> listeValeurs = NomenclatureEvtTrace.getListeValeurs();
		for (Iterator<String> iterator = listeValeurs.iterator(); iterator.hasNext();) {
			response.add(iterator.next());
		}

		context.response().setStatusCode(200).end(response.encode());

		logger.debug(
				Constants.LOG_MESSAGE,
				Constants.LOG_NO_CORRELATION_ID,
				ACTION_GET_ALL_EVENEMENT_TRACE);

	}


}
