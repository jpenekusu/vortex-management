//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.flux.Flux;
import fr.edu.vortex.management.flux.HistoFluxVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class HistoFluxApi extends AbstractApi {

	private static final Logger logger = LoggerFactory.getLogger(HistoFluxApi.class);

	public static final String URI_HISTO_FLUX = "/historique_flux";
	private static final String CORRELATION_ID = "/:correlation_id";	

	Vertx vertx;

	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;

		Router router = Router.router(vertx);

		router.get(URI_HISTO_FLUX + CORRELATION_ID).handler(this::getHistoFluxByCorrelationIdAndConsumerCorrelationId);

		List<Route> routes = router.getRoutes();
		for (Route route : routes) {
			logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "Route histoFLux :" + route.getPath());
		}

		return router;

	}

	private void getHistoFluxByCorrelationIdAndConsumerCorrelationId(RoutingContext ctx) {
		String action = "appel du webservice getHistoFluxByCorrelationIdAndConsumerCorrelationId";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		String correlationId = ctx.request().getParam("correlation_id");
		String consumerCorrelationId = ctx.request().getParam("consumer_correlation_id");

		if (correlationId == null) {
			this.sendError400(ctx, "Le paramêtre correlationId est manquant.");

		} else {
			JsonObject json = new JsonObject();
			json.put(Flux.KEY_JSON_CORRELATION_ID, correlationId);
			json.put(Flux.KEY_JSON_CONSUMER_CORRELATION_ID, consumerCorrelationId);

			vertx.eventBus().send(HistoFluxVerticle.GET_BY_CORRELATION_ID_AND_CONSUMER_CORRELATION_ID, json, ar -> {
				this.replyDefaultGetJsonArray(ctx, action, ar);
			});
		}

	}
}
