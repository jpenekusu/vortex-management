//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.List;

import fr.edu.echanges.nat.vortex.common.adresses.queue.QueueAdresses;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.flux.FluxVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class QueuesApi extends AbstractApi {

	private static final Logger logger = LoggerFactory.getLogger(QueuesApi.class);

	public static final String URI = "/queues/";
	public static final String URI_MESSAGES = "/messages";
	public static final String URI_COUNT = "/count";
	public static final String URI_INFO = "/info";
	public static final String KEY_JSON_SYSTEME = "systeme";
	public static final String KEY_JSON_NAME = "name";
	public static final String KEY_JSON_TYPE = "type";
	public static final String KEY_JSON_CLIENT = "partenaire";

	
	private static final String ID = ":id";

	public static final String UPDATE_QUEUE = "vortex.core.queue.updateQueue";
	public static final String LIST_QUEUES_NB_MESSAGES = "vortex.core.queue.listQueuesNbMessages";
    public static final String LIST_QUEUES_FILTRE = "vortex.core.queue.listQueuesFiltre";

	public static final String INFO_QUEUE = "vortex.core.queue.infoQueue";
	public static final String DELETE_QUEUE = "vortex.core.queue.deleteQueue";

	Vertx vertx;

	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;
		Router router = Router.router(vertx);

		router.get(URI).handler(this::getAllQueues);
		router.get(URI + "count").handler(this::getAllQueuesWithNbMessages);

		router.get(URI + ":name").handler(this::getQueuesByName);
		
		router.delete(URI + ":name" + "/:param").handler(this::deleteQueue);
        router.delete(URI_MESSAGES + "/queue/:name").handler(this::purgerMessagesByQueueName);

		router.get(URI + ":name" + URI_MESSAGES + URI_COUNT).handler(this::countMessagesbyQueueName);

        router.post(URI).handler(BodyHandler.create());
        router.post(URI).handler(this::postQueues);

        router.put(URI + ID).handler(BodyHandler.create());
        router.put(URI + ID).handler(this::putQueue);

        router.get(URI + ":name" + URI_INFO).handler(this::getInfosZoneReception);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID,
                    "route zones de réception :" + route.getPath());
        }

        return router;
    }

    private void postQueues(RoutingContext context) {
        String action = "creation de zones de réception";
        JsonObject newQueue = new JsonObject();
        try {
            newQueue = context.getBodyAsJson();
        } catch (Exception e) {
            sendError(context, 400, action, "body manquant ou mal formaté");
        }
        if (newQueue.isEmpty()) {
            sendError(context, 400, action, "body manquant ou mal formaté");
        } else {
			String header = context.request().getHeader("Authorization");
			newQueue.put("header", header);
			newQueue.put("origine", Constants.TRACE_ORIGINE_CASSIS);
            vertx.eventBus().send(QueueAdresses.CREATE_QUEUE, newQueue,
                    replyHandler -> replyDefaultPost(context, action, replyHandler));
        }
    }

    private void putQueue(RoutingContext context) {
        String action = "appel du webservice putQueue";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        JsonObject jsonQueue = context.getBodyAsJson();
		String header = context.request().getHeader("Authorization");
		jsonQueue.put("header", header);
		jsonQueue.put("origine", Constants.TRACE_ORIGINE_CASSIS);
        vertx.eventBus().send(UPDATE_QUEUE, jsonQueue,
                replyHandler -> replyDefaultGetJsonObject(context, action, replyHandler));
    }

    private void getAllQueues(RoutingContext context) {
        String action = "Obtenir toutes les zones de réception";
 
       	JsonObject filters = new JsonObject();
       	filters.put(FluxVerticle.KEY_JSON_PAGE, context.request().getParam(FluxVerticle.KEY_JSON_PAGE));
       	filters.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, context.request().getParam(FluxVerticle.KEY_JSON_PAGE_LIMIT));
       	filters.put(FluxVerticle.KEY_JSON_SORT, context.request().getParam(FluxVerticle.KEY_JSON_SORT));
       	filters.put(FluxVerticle.KEY_JSON_SORT_ORDER, context.request().getParam(FluxVerticle.KEY_JSON_SORT_ORDER));

       	filters.put(KEY_JSON_CLIENT, context.request().getParam(KEY_JSON_CLIENT));
       	filters.put(KEY_JSON_TYPE, context.request().getParam(KEY_JSON_TYPE));
       	filters.put(KEY_JSON_NAME, context.request().getParam(KEY_JSON_NAME));
        
        String systemeParam = context.request().getParam(KEY_JSON_SYSTEME);
        if (context.request().getParam(KEY_JSON_SYSTEME) != null) {
            if (systemeParam.toUpperCase().equals("TRUE") || systemeParam.toUpperCase().equals("FALSE")) {
                filters.put(KEY_JSON_SYSTEME,
                        Boolean.valueOf(systemeParam));
            } else {
                sendError(context, 400, action, "Paramètre système mal formaté");
                return;
            }
        }
        
        vertx.eventBus().send(LIST_QUEUES_FILTRE, filters, ar ->{
    		if (ar.succeeded()) {
    			String count = ar.result().headers().get("count");
    			if (count != null) {
    				send206(context, count, (JsonArray) ar.result().body());
    			} else {
    				this.replyDefaultGetJsonArray(context, "getAllQueues", ar);
    			}
    		} else {
    			ReplyException cause = (ReplyException) ar.cause();
    			sendError(context, cause.failureCode(), cause.getMessage());
    		}
    	});       
    }

    private void getAllQueuesWithNbMessages(RoutingContext context) {
        String action = "Obtenir toutes les zones de réception avec le nombre de messages associés";
        JsonObject filters = null;
        String systemeParam = context.request().getParam(KEY_JSON_SYSTEME);
        if (context.request().getParam(KEY_JSON_SYSTEME) != null) {
            if (systemeParam.toUpperCase().equals("TRUE") || systemeParam.toUpperCase().equals("FALSE")) {
                filters = new JsonObject().put(KEY_JSON_SYSTEME,
                        Boolean.valueOf(systemeParam));
            } else {
                sendError(context, 400, action, "Paramètre système mal formaté");
                return;
            }
        }
        vertx.eventBus().send(LIST_QUEUES_NB_MESSAGES, filters,
                replyHandler -> replyDefaultGetJsonArray(context, action, replyHandler));
    }
    
    private void getQueuesByName(RoutingContext context) {
        String name = context.request().getParam("name");
        String action = "Obtenir les zones de réception par nom";
        if (null == name) {
            sendError500(context, action, "name manquant");
        } else {
            vertx.eventBus().send(QueueAdresses.GET_BY_NAME, name,
                    replyHandler -> replyDefaultGetJsonObject(context, action, replyHandler));
        }
    }

    private void countMessagesbyQueueName(RoutingContext context) {
        String name = context.request().getParam("name");
        String action = "compter les messages par zones de réception";
        if (null == name) {
            sendError500(context, action, "name manquant");
        } else {
            vertx.eventBus().send(QueueAdresses.COUNT_MESSAGES_BY_QUEUE_NAME, name,
                    replyHandler -> replyDefaultGetInteger(context, action, replyHandler));
        }
    } 

    private void deleteQueue(RoutingContext context) {
        String name = context.request().getParam("name");
        String param = context.request().getParam("param");
        String action = "suppression zone de réception";
       if (null == name) {
            sendError500(context, action, "name manquant");
        } else if (null == param) {
            sendError500(context, action, "param manquant");
        } else if ("all".equals(param) || "move".equals(param)) {
        	JsonObject json = new JsonObject();
        	json.put("param", param);
        	json.put("name", name);        	
    		String header = context.request().getHeader("Authorization");
    		json.put("header", header);
    		json.put("origine", Constants.TRACE_ORIGINE_CASSIS);
           vertx.eventBus().send(DELETE_QUEUE, json,
                    replyHandler -> replyDefaultDelete(context, action, replyHandler));
    	} 
    }

	private void getInfosZoneReception(RoutingContext context) {
		String name = context.request().getParam("name");
		String action = "retourner les infos d'une zone de réception";
		if (null == name) {
			sendError500(context, action, "name manquant");
		} else {
			vertx.eventBus().send(INFO_QUEUE, name,
					replyHandler -> replyDefaultGetJsonObject(context, action, replyHandler));
		}
	}



	private void purgerMessagesByQueueName(RoutingContext context) {
		String queueName = context.request().getParam("name");
		String action = "purger les messages par zone de réception";
		if (null == queueName) {
			sendError500(context, action, "name manquant");
		} else {
			JsonObject json = new JsonObject();
			json.put("queueName", queueName);
			String header = context.request().getHeader("Authorization");
			json.put("header", header);
			json.put("origine", Constants.TRACE_ORIGINE_CASSIS);			
			vertx.eventBus().send(QueueAdresses.PURGER_MESSAGES_BY_QUEUE_NAME, json,
					replyHandler -> replyDefaultDelete(context, action, replyHandler));
		}
	}
}