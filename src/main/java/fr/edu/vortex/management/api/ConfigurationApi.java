//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.net.HttpURLConnection;
import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureTypeRapport;
import fr.edu.vortex.management.utilisateur.pojo.Rapport;
import fr.edu.vortex.management.utilisateur.verticle.RapportVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ConfigurationApi extends AbstractApi {

	private static final Logger logger = LoggerFactory.getLogger(ConfigurationApi.class);

	public static final String URI = "/configuration";
	public static final String KEY_JSON_SYSTEME = "systeme";
	public static final String KEY_JSON_TYPE = "type";

	public static final String ADRESS_GET_CONFIGURATION = "vortex.core.configuration.getConfiguration";
	public static final String ADRESS_POST_CONFIGURATION = "vortex.core.configuration.postConfiguration";

	Vertx vertx;

	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;
		Router router = Router.router(vertx);

		router.get(URI).handler(BodyHandler.create());
		router.get(URI).handler(this::getConfiguration);

		router.post(URI).handler(BodyHandler.create());
		router.post(URI).handler(this::postConfiguration);

		List<Route> routes = router.getRoutes();
		for (Route route : routes) {
			logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "route Configuration:" + route.getPath());
		}
		return router;
	}

	private void getConfiguration(RoutingContext context) {
		String action = "appel du webservice getConfiguration";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		JsonObject filters = new JsonObject();
		String systemeParam = context.request().getParam(KEY_JSON_SYSTEME);
		String typeParam = context.request().getParam(KEY_JSON_TYPE);
		if (context.request().getParam(KEY_JSON_SYSTEME) != null) {
			if (systemeParam.toUpperCase().equals("TRUE") || systemeParam.toUpperCase().equals("FALSE")) {
				filters.put(KEY_JSON_SYSTEME, Boolean.valueOf(systemeParam));
			} else {
				sendError(context, 400, action, "Paramètre système mal formaté");
			}
		}
		if (context.request().getParam(KEY_JSON_TYPE) != null) {
			filters.put(KEY_JSON_TYPE, typeParam);
		}
		vertx.eventBus().send(ADRESS_GET_CONFIGURATION, filters,
				ar -> this.replyToClient(ar, context, 200, "getConfiguration"));
	}

	private void postConfiguration(RoutingContext context) {
		String action = "appel du webservice postConfiguration";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		isTokenOK(context.request(), vertx).setHandler(artoken -> {
			if (artoken.succeeded()){
				JsonObject jsonConfiguration = new JsonObject();
				try {
					jsonConfiguration = context.getBodyAsJson();
				} catch (Exception e) {
					sendError(context, 400, "post client", "body manquant ou mal formaté");
					return;
				}
				if (!jsonConfiguration.isEmpty()) {
					String header = context.request().getHeader("Authorization");
					jsonConfiguration.put("header", header);
					jsonConfiguration.put("origine", Constants.TRACE_ORIGINE_CASSIS);
					vertx.eventBus().send(ADRESS_POST_CONFIGURATION, jsonConfiguration,
							ar -> this.replyToClient(ar, context, 201, "postConfiguration",header,Constants.TRACE_ORIGINE_CASSIS, NomenclatureTypeRapport.RapportImportParametrage.getValeur()));
				}
			} else {
				send401(context, action); 
			}
		});

	}

	private void replyToClient(AsyncResult<Message<Object>> asyncResult, RoutingContext ctx, int succesCode, 
			String operation, String header, String origine, String type_rapport) {
		ctx.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);
		if (asyncResult.succeeded()) {
			try {
				Message<Object> result = asyncResult.result();
				if (result.body() instanceof JsonObject) {
					JsonObject body = (JsonObject) result.body();
					body.put("header", header);
					body.put("origine", origine);
					body.put("type_rapport", type_rapport);
					
				
					vertx.eventBus().send(RapportVerticle.CREER_RAPPORT, body,
		                    ar ->{
		                	if(ar.succeeded()) {
        	                	    JsonObject jsonResult = (JsonObject) ar.result().body();
        	                	    String dateRapport = jsonResult.getString(Rapport.KEY_JSON_DATE_RAPPORT);
        	                	    body.put(Rapport.KEY_JSON_DATE_RAPPORT, dateRapport);
        	                	    body.remove("header");	                	
        	                	    ctx.response().setStatusCode(succesCode).end(body.encode());	                	    
        	                	}else {
        	                	    ReplyException cause = (ReplyException) ar.cause();
        	                	    sendError(ctx, cause.failureCode(), operation, cause.getMessage());
        	                	}
		                    });
					
				} else {
					JsonArray body = (JsonArray) result.body();
					ctx.response().setStatusCode(succesCode).end(body.encode());
				}
			} catch (Exception e) {
				sendError500(ctx, operation, e.getMessage());
			}
		} else {
			try {
				ReplyException cause = (ReplyException) asyncResult.cause();
				sendError(ctx, cause.failureCode(), operation, cause.getMessage());
			} catch (Exception e) {
				sendError500(ctx, operation, e.getMessage());
			}
		}
	}
	
	private void replyToClient(AsyncResult<Message<Object>> asyncResult, RoutingContext ctx, int succesCode,
			String operation) {
		ctx.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);
		if (asyncResult.succeeded()) {
			try {
				Message<Object> result = asyncResult.result();
				if (result.body() instanceof JsonObject) {
					JsonObject body = (JsonObject) result.body();
					ctx.response().setStatusCode(succesCode).end(body.encode());
				} else {
					JsonArray body = (JsonArray) result.body();
					ctx.response().setStatusCode(succesCode).end(body.encode());
				}
			} catch (Exception e) {
				sendError500(ctx, operation, e.getMessage());
			}
		} else {
			try {
				ReplyException cause = (ReplyException) asyncResult.cause();
				sendError(ctx, cause.failureCode(), operation, cause.getMessage());
			} catch (Exception e) {
				sendError500(ctx, operation, e.getMessage());
			}
		}
	}
	
}
