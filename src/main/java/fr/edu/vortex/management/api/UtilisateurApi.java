//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.net.HttpURLConnection;
import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class UtilisateurApi extends AbstractApi {

    private static final String ACTION_CREER_UTILISATEUR = "appel du webservice creerUtilisateur";

    private static final String ACTION_MODIFIER_UTILISATEUR = "appel du webservice modifierUtilisateur";

    private static final String ACTION_MODIFIER_MDP_UTILISATEUR = "appel du webservice modifierMDPUtilisateur";

    private static final String ACTION_DELETE_UTILISATEUR = "appel du webservice deleteUtilisateur";

    private static final String ACTION_LISTER_UTILISATEURS = "listerUtilisateurs(RoutingContext ctx)";

    private static final String MESSAGE_BAD_REQUEST = "body manquant ou mal formaté";
    
    private static final Logger logger = LoggerFactory.getLogger(UtilisateurApi.class);

    public static final String URI_UTILISATEUR = "/utilisateurs";
    private static final String ID = "id";
    public static final String PARAM_ID = "/:" + ID;
    public static final String URI_MDP = "/mdp";


    Vertx vertx;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;

        Router router = Router.router(vertx);

        router.get(URI_UTILISATEUR).handler(this::listerUtilisateurs);

        router.delete(URI_UTILISATEUR + PARAM_ID).handler(this::deleteUtilisateur);

        router.post(URI_UTILISATEUR).handler(BodyHandler.create());
        router.post(URI_UTILISATEUR).handler(this::creerUtilisateur);

        router.put(URI_UTILISATEUR + PARAM_ID).handler(BodyHandler.create());
        router.put(URI_UTILISATEUR + PARAM_ID).handler(this::modifierUtilisateur);

        router.put(URI_UTILISATEUR + PARAM_ID + URI_MDP).handler(BodyHandler.create());
        router.put(URI_UTILISATEUR + PARAM_ID + URI_MDP).handler(this::modifierMDPUtilisateur);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID,
                    "Route utilisateurs :" + route.getPath());
        }

        return router;

    }

    private void listerUtilisateurs(RoutingContext ctx) {

        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_LISTER_UTILISATEURS);

		JsonObject jsonCritere = new JsonObject();
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE));
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE_LIMIT));
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT));
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT_ORDER, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT_ORDER));
		
		String systemeParam = ctx.request().getParam(Utilisateur.KEY_JSON_SYSTEME);
        if (ctx.request().getParam(Utilisateur.KEY_JSON_SYSTEME) != null) {
            if (systemeParam.toUpperCase().equals("TRUE") || systemeParam.toUpperCase().equals("FALSE")) {
            	jsonCritere.put(Utilisateur.KEY_JSON_SYSTEME,
                        Boolean.valueOf(systemeParam));
            } else {
                sendError(ctx, 400, ACTION_LISTER_UTILISATEURS, "Paramètre système mal formaté");
            }
        }
		jsonCritere.put(Utilisateur.KEY_JSON_NOM, ctx.request().getParam(Utilisateur.KEY_JSON_NOM));
		jsonCritere.put(Utilisateur.KEY_JSON_PRENOM, ctx.request().getParam(Utilisateur.KEY_JSON_PRENOM));
		jsonCritere.put(Utilisateur.KEY_JSON_LOGIN, ctx.request().getParam(Utilisateur.KEY_JSON_LOGIN));
		jsonCritere.put(Utilisateur.KEY_JSON_TYPE, ctx.request().getParam(Utilisateur.KEY_JSON_TYPE));
		jsonCritere.put(Utilisateur.KEY_JSON_EMAIL, ctx.request().getParam(Utilisateur.KEY_JSON_EMAIL));
		jsonCritere.put(UtilisateurVerticle.KEY_JSON_ROLE, ctx.request().getParam(UtilisateurVerticle.KEY_JSON_ROLE));

        vertx.eventBus().send(UtilisateurVerticle.LISTER_UTILISATEUR, jsonCritere, ar -> {
    		if (ar.succeeded()) {
    			String count = ar.result().headers().get("count");
    			if (count != null) {
    				send206(ctx, count, (JsonArray) ar.result().body());
    			} else {
    				this.replyDefaultGetJsonArray(ctx, ACTION_LISTER_UTILISATEURS, ar);
    			}
    		} else {
    			ReplyException cause = (ReplyException) ar.cause();
    			sendError(ctx, cause.failureCode(), cause.getMessage());
    		}
    	});	
    }

    private void deleteUtilisateur(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_DELETE_UTILISATEUR);

        String id = ctx.request().getParam(ID);
        JsonObject data = new JsonObject().put(ID, id);
        String header = ctx.request().getHeader("Authorization");
		data.put("header", header);
		data.put("origine", Constants.TRACE_ORIGINE_CASSIS);

        ctx.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);
        vertx.eventBus().send(UtilisateurVerticle.SUPPRIMER_UTILISATEUR, data,
                ar -> replyDefaultDelete(ctx, ACTION_DELETE_UTILISATEUR, ar));
    }

    private void creerUtilisateur(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_CREER_UTILISATEUR);

        JsonObject jsonClient = new JsonObject();
        try {
            jsonClient = ctx.getBodyAsJson();
            String header = ctx.request().getHeader("Authorization");
            jsonClient.put("header", header);
            jsonClient.put("origine", Constants.TRACE_ORIGINE_CASSIS);
       } catch (Exception e) {
            sendError(ctx, HttpURLConnection.HTTP_BAD_REQUEST, ACTION_CREER_UTILISATEUR, MESSAGE_BAD_REQUEST);
        }

        if (!jsonClient.isEmpty()) {
            vertx.eventBus().send(UtilisateurVerticle.CREER_UTILISATEUR, jsonClient,
        	    ar -> replyDefaultPost(ctx, ACTION_CREER_UTILISATEUR, ar));
        }
    }
    
    private void modifierUtilisateur(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_MODIFIER_UTILISATEUR);

        JsonObject jsonClient = new JsonObject();
        try {
            jsonClient = ctx.getBodyAsJson();
            String header = ctx.request().getHeader("Authorization");
            jsonClient.put("header", header);
            jsonClient.put("origine", Constants.TRACE_ORIGINE_CASSIS);
        } catch (Exception e) {
            sendError(ctx, HttpURLConnection.HTTP_BAD_REQUEST, ACTION_MODIFIER_UTILISATEUR, MESSAGE_BAD_REQUEST);
        }

        if (!jsonClient.isEmpty()) {
            vertx.eventBus().send(UtilisateurVerticle.MODIFIER_UTILISATEUR, jsonClient,
        	    ar -> replyDefaultPost(ctx, ACTION_MODIFIER_UTILISATEUR, ar));
        }
    }
    
    private void modifierMDPUtilisateur(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_MODIFIER_MDP_UTILISATEUR);

        JsonObject jsonClient = new JsonObject();
        try {
            jsonClient = ctx.getBodyAsJson();
            String header = ctx.request().getHeader("Authorization");
            jsonClient.put("header", header);
            jsonClient.put("origine", Constants.TRACE_ORIGINE_CASSIS);
        } catch (Exception e) {
            sendError(ctx, HttpURLConnection.HTTP_BAD_REQUEST, ACTION_MODIFIER_MDP_UTILISATEUR, MESSAGE_BAD_REQUEST);
        }

        if (!jsonClient.isEmpty()) {
            vertx.eventBus().send(UtilisateurVerticle.MODIFIER_MDP_UTILISATEUR, jsonClient,
                    ar -> replyDefaultPost(ctx, ACTION_MODIFIER_MDP_UTILISATEUR, ar));
        }
    }

}
