//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.List;

import fr.edu.vortex.management.Constants;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class BuisnessRouteApi extends AbstractApi {

    private static final Logger logger = LoggerFactory.getLogger(BuisnessRouteApi.class);

    public static final String URI = "/routes/";

    public static final String ADRESS_GET_ALL_ROUTES = "vortex.core.routes.getAll";

    Vertx vertx;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;
        Router router = Router.router(vertx);

        router.get(URI).handler(this::getAllRoutes);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, route.getPath());
        }

        return router;
    }

    private void getAllRoutes(RoutingContext context) {
        String action = "Liste des routes";
        logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        vertx.eventBus().send(ADRESS_GET_ALL_ROUTES, null,
                replyHandler -> replyDefaultGetJsonArray(context, "Liste des routes", replyHandler));
    }

}