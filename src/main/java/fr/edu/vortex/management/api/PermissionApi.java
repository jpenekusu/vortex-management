//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.utilisateur.verticle.PermissionVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class PermissionApi extends AbstractApi {

    private static final Logger logger = LoggerFactory.getLogger(PermissionApi.class);

    public static final String URI_PERMISSIONS = "/permissions";

    Vertx vertx;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;

        Router router = Router.router(vertx);

        router.get(URI_PERMISSIONS).handler(this::listerPermissions);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID,
                    "Route permissions :" + route.getPath());
        }

        return router;

    }

    private void listerPermissions(RoutingContext ctx) {

        String action = "listerPermissions(RoutingContext ctx)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

        vertx.eventBus().send(PermissionVerticle.LISTER_PERMISSION, null, ar -> {
            this.replyDefaultGetJsonArray(ctx, action, ar);
        });
    }

}
