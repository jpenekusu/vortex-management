//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.net.HttpURLConnection;
import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.RoleVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class RoleApi extends AbstractApi {

    private static final String MESSAGE_BAD_REQUEST = "body manquant ou mal formaté";

    private static final String ACTION_CREER_ROLE = "appel du webservice creerRole";
    
    private static final String ACTION_MODIFIER_ROLE = "appel du webservice modifierRole";

    private static final String ACTION_DELETE_ROLE = "appel du webservice deleteRole";

    private static final String ACTION_LISTER_ROLES = "listerRoles(RoutingContext ctx)";

    private static final Logger logger = LoggerFactory.getLogger(RoleApi.class);

    public static final String URI_ROLES = "/roles";

    private static final String ID = "id";
    public static final String PARAMS_ID = "/:" + ID;


    Vertx vertx;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;

        Router router = Router.router(vertx);

        router.get(URI_ROLES).handler(this::listerRoles);
        
        router.delete(URI_ROLES + PARAMS_ID).handler(this::deleteRole);

        router.post(URI_ROLES).handler(BodyHandler.create());
        router.post(URI_ROLES).handler(this::creerRole);

        router.put(URI_ROLES + PARAMS_ID).handler(BodyHandler.create());
        router.put(URI_ROLES + PARAMS_ID).handler(this::modifierRole);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "Route roles :" + route.getPath());
        }

        return router;

    }

    private void listerRoles(RoutingContext ctx) {

        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_LISTER_ROLES);
  
		JsonObject jsonCritere = new JsonObject();
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE));
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE_LIMIT));
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT));
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT_ORDER, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT_ORDER));
		
		String systemeParam = ctx.request().getParam(Utilisateur.KEY_JSON_SYSTEME);
        if (ctx.request().getParam(Utilisateur.KEY_JSON_SYSTEME) != null) {
            if (systemeParam.toUpperCase().equals("TRUE") || systemeParam.toUpperCase().equals("FALSE")) {
            	jsonCritere.put(Utilisateur.KEY_JSON_SYSTEME,
                        Boolean.valueOf(systemeParam));
            } else {
                sendError(ctx, 400, ACTION_LISTER_ROLES, "Paramètre système mal formaté");
            }
        }
		jsonCritere.put(Role.KEY_JSON_LIBELLE, ctx.request().getParam(Role.KEY_JSON_LIBELLE));
		jsonCritere.put(Role.KEY_JSON_PERMISSIONS, ctx.request().getParam(Role.KEY_JSON_PERMISSIONS));

        vertx.eventBus().send(RoleVerticle.LISTER_ROLE, jsonCritere, ar -> {
    		if (ar.succeeded()) {
    			String count = ar.result().headers().get("count");
    			if (count != null) {
    				send206(ctx, count, (JsonArray) ar.result().body());
    			} else {
    				this.replyDefaultGetJsonArray(ctx, ACTION_LISTER_ROLES, ar);
    			}
    		} else {
    			ReplyException cause = (ReplyException) ar.cause();
    			sendError(ctx, cause.failureCode(), cause.getMessage());
    		}
    	});	
    }

    private void deleteRole(RoutingContext ctx) {

        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_DELETE_ROLE);

        String id = ctx.request().getParam(ID);
        JsonObject data = new JsonObject().put(ID, id);

        try {
            data.put(ID, Integer.valueOf(id));
			String header = ctx.request().getHeader("Authorization");
			data.put("header", header);
			data.put("origine", Constants.TRACE_ORIGINE_CASSIS);

            vertx.eventBus().send(RoleVerticle.SUPPRIMER_ROLE, data,
                    ar -> replyDefaultDelete(ctx, ACTION_DELETE_ROLE, ar));

        } catch (NumberFormatException e) {
            sendError400(ctx, ERR_ID_NOT_INTEGER);
        }
    }
	
    private void creerRole(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_CREER_ROLE);
        JsonObject jsonRole = new JsonObject();
        try {
            jsonRole = ctx.getBodyAsJson();
        } catch (Exception e) {
            sendError(ctx, HttpURLConnection.HTTP_BAD_REQUEST, ACTION_CREER_ROLE, MESSAGE_BAD_REQUEST);
        }

        if (!jsonRole.isEmpty()) {
			String header = ctx.request().getHeader("Authorization");
			jsonRole.put("header", header);
			jsonRole.put("origine", Constants.TRACE_ORIGINE_CASSIS);
            vertx.eventBus().send(RoleVerticle.CREER_ROLE, jsonRole,
                    ar -> replyDefaultPost(ctx, ACTION_CREER_ROLE, ar));
        }
    }
    
    private void modifierRole(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_MODIFIER_ROLE);
        JsonObject jsonRole = new JsonObject();
        try {
            jsonRole = ctx.getBodyAsJson();
        } catch (Exception e) {
            sendError(ctx, HttpURLConnection.HTTP_BAD_REQUEST, ACTION_MODIFIER_ROLE, MESSAGE_BAD_REQUEST);
        }

        if (!jsonRole.isEmpty()) {
			String header = ctx.request().getHeader("Authorization");
			jsonRole.put("header", header);
			jsonRole.put("origine", Constants.TRACE_ORIGINE_CASSIS);
            vertx.eventBus().send(RoleVerticle.MODIFIER_ROLE, jsonRole,
                    ar -> replyDefaultPost(ctx, ACTION_MODIFIER_ROLE, ar));
        }
    }
	
}
