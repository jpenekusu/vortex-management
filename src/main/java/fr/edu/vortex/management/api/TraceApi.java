//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

/**
 * 
 */
package fr.edu.vortex.management.api;
import java.net.HttpURLConnection;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.utilisateur.pojo.Trace;
import fr.edu.vortex.management.utilisateur.verticle.TraceVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
/**
 * @author arhannaoui
 *
 */
public class TraceApi extends AbstractApi {

	public static final String URI_TRACES = "/traces";
	public static final String URI_TRACES_HISTORIQUE = "/traces_historique";

	private static final Logger logger = LoggerFactory.getLogger(TraceApi.class);

	Vertx vertx;

	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;

		Router router = Router.router(vertx);

		router.get(URI_TRACES).handler(this::listerTrace);
		router.get(URI_TRACES_HISTORIQUE).handler(this::getHistorique);

		return router;

	}

	private void listerTrace(RoutingContext ctx) {
		String action = "chercherTrace(RoutingContext ctx)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		JsonObject jsonCritere = new JsonObject();
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE));
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE_LIMIT));
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT));
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT_ORDER, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT_ORDER));

		jsonCritere.put(Trace.KEY_JSON_ID_ELEMENT, ctx.request().getParam(Trace.KEY_JSON_ID_ELEMENT));
		jsonCritere.put(Trace.KEY_JSON_NOM_ELEMENT, ctx.request().getParam(Trace.KEY_JSON_NOM_ELEMENT));
		jsonCritere.put(Trace.KEY_JSON_TYPE_ELEMENT, ctx.request().getParam(Trace.KEY_JSON_TYPE_ELEMENT));
		jsonCritere.put(Trace.KEY_JSON_EVENT, ctx.request().getParam(Trace.KEY_JSON_EVENT));
		jsonCritere.put(Trace.KEY_JSON_ORIGINE, ctx.request().getParam(Trace.KEY_JSON_ORIGINE));
		jsonCritere.put(Trace.KEY_JSON_NOM, ctx.request().getParam(Trace.KEY_JSON_NOM));
		jsonCritere.put(Trace.KEY_JSON_PRENOM, ctx.request().getParam(Trace.KEY_JSON_PRENOM));
		jsonCritere.put(Trace.KEY_JSON_LOGIN, ctx.request().getParam(Trace.KEY_JSON_LOGIN));
		jsonCritere.put(Trace.KEY_JSON_DATE_TRACE, ctx.request().getParam(Trace.KEY_JSON_DATE_TRACE));

		vertx.eventBus().send(TraceVerticle.CHERCHER_TRACE, jsonCritere, ar -> {
			if (ar.succeeded()) {
				String count = ar.result().headers().get("count");
				if (count != null) {
					send206(ctx, count, (JsonArray) ar.result().body());
				} else {
					this.replyDefaultGetJsonArray(ctx, "listerTrace", ar);
				}
			} else {
				ReplyException cause = (ReplyException) ar.cause();
				sendError(ctx, cause.failureCode(), cause.getMessage());
			}
		});
	}
	
	private void getHistorique(RoutingContext ctx) {
		String action = "getHistorique(RoutingContext ctx)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		JsonObject jsonCritere = new JsonObject();
		jsonCritere.put(Trace.KEY_JSON_ID_ELEMENT, ctx.request().getParam(Trace.KEY_JSON_ID_ELEMENT));
		jsonCritere.put(Trace.KEY_JSON_TYPE_ELEMENT, ctx.request().getParam(Trace.KEY_JSON_TYPE_ELEMENT));

		vertx.eventBus().send(TraceVerticle.HISTORIQUE_TRACE, jsonCritere, ar -> {
			if (ar.succeeded()) {
				JsonArray result = (JsonArray) ar.result().body();
				String count = String.valueOf(result.size());
				send206(ctx, count, result);
			} else {
				ReplyException cause = (ReplyException) ar.cause();
				sendError(ctx, cause.failureCode(), cause.getMessage());
			}
		});
	}

	protected void send206(RoutingContext ctx, String count, JsonArray body) {
		ctx.response().setStatusCode(HttpURLConnection.HTTP_PARTIAL)
		.putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8)
		.putHeader("x-total-count", count).end(body.encode());
	}

}
