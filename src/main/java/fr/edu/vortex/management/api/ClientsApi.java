//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.List;

import fr.edu.echanges.nat.vortex.common.adresses.client.ClientAdresses;
import fr.edu.echanges.nat.vortex.common.adresses.exchange.ExchangeAdresses;
import fr.edu.echanges.nat.vortex.common.adresses.queue.QueueAdresses;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.flux.FluxVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ClientsApi extends AbstractApi {

    private static final Logger logger = LoggerFactory.getLogger(ClientsApi.class);

    public static final String URI = "/partenaires";
    private static final String BUSINESS_ID = "/:businessId";
    private static final String EXCHANGES = "/exchanges";
    private static final String QUEUES = "/queues";

    public static final String KEY_JSON_SYSTEME = "systeme";
    public static final String KEY_JSON_NAME = "name";
    public static final String KEY_JSON_BUSINESS_ID = "business_id";
    public static final String KEY_JSON_SECURISE = "securise";
    public static final String KEY_JSON_ID_CERTIFICAT = "id_certificat";
    public static final String KEY_JSON_DATE_VALIDITE_CERTIFICAT = "date_validite_certificat";
	public static final String KEY_JSON_EMAIL = "email";

    public static final String VORTEX_CLIENT_GET = "vortex.core.client.get";
 
    
    Vertx vertx;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;
        Router router = Router.router(vertx);

        router.post(URI).handler(BodyHandler.create());
        router.post(URI).handler(this::postClient);

        router.get(URI).handler(this::getClients);

        router.get(URI + BUSINESS_ID).handler(this::getClient);

        router.get(URI + BUSINESS_ID + EXCHANGES).handler(this::getAllExchangesByClient);
        router.get(URI + BUSINESS_ID + QUEUES).handler(this::getAllQueuesByClient);

        router.delete(URI + BUSINESS_ID).handler(this::deleteClient);

        router.put(URI + BUSINESS_ID).handler(BodyHandler.create());
        router.put(URI + BUSINESS_ID).handler(this::putClient);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "route Clients:" + route.getPath());
        }
        return router;
    }

    private void postClient(RoutingContext ctx) {
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "appel du webservice postClient");
        JsonObject jsonClient = new JsonObject();
        try {
            jsonClient = ctx.getBodyAsJson();
        } catch (Exception e) {
            sendError(ctx, 400, "post client", "body manquant ou mal formaté");
            return;
        }
        if (!jsonClient.isEmpty()) {
			String header = ctx.request().getHeader("Authorization");
			jsonClient.put("header", header);
			jsonClient.put("origine", Constants.TRACE_ORIGINE_CASSIS);
			vertx.eventBus().send(ClientAdresses.ADD_CLIENT, jsonClient,
                    ar -> this.replyToClient(ar, ctx, 201, "postClient"));
        }
    }

    private void getClients(RoutingContext context) {
    	String action = "appel du webservice getAllClients";
    	logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

    	JsonObject filters = new JsonObject();
    	filters.put(FluxVerticle.KEY_JSON_PAGE, context.request().getParam(FluxVerticle.KEY_JSON_PAGE));
    	filters.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, context.request().getParam(FluxVerticle.KEY_JSON_PAGE_LIMIT));
    	filters.put(FluxVerticle.KEY_JSON_SORT, context.request().getParam(FluxVerticle.KEY_JSON_SORT));
    	filters.put(FluxVerticle.KEY_JSON_SORT_ORDER, context.request().getParam(FluxVerticle.KEY_JSON_SORT_ORDER));

    	filters.put(KEY_JSON_NAME, context.request().getParam(KEY_JSON_NAME));
    	filters.put(KEY_JSON_BUSINESS_ID, context.request().getParam(KEY_JSON_BUSINESS_ID));
    	filters.put(KEY_JSON_ID_CERTIFICAT, context.request().getParam(KEY_JSON_ID_CERTIFICAT));
    	filters.put(KEY_JSON_DATE_VALIDITE_CERTIFICAT, context.request().getParam(KEY_JSON_DATE_VALIDITE_CERTIFICAT));
    	filters.put(KEY_JSON_EMAIL, context.request().getParam(KEY_JSON_EMAIL));

    	String systemeParam = context.request().getParam(KEY_JSON_SYSTEME);
    	if (context.request().getParam(KEY_JSON_SYSTEME) != null) {
    		if (systemeParam.toUpperCase().equals("TRUE") || systemeParam.toUpperCase().equals("FALSE")) {
    			filters.put(KEY_JSON_SYSTEME,
    					Boolean.valueOf(systemeParam));
    		} else {
    			sendError(context, 400, action, "Paramètre système mal formaté");
    		}
    	}
    	
    	String securiseParam = context.request().getParam(KEY_JSON_SECURISE);
    	if (context.request().getParam(KEY_JSON_SECURISE) != null) {
    		if (securiseParam.toUpperCase().equals("TRUE") || securiseParam.toUpperCase().equals("FALSE")) {
    			filters.put(KEY_JSON_SECURISE,
    					Boolean.valueOf(securiseParam));
    		} else {
    			sendError(context, 400, action, "Paramètre sécurisé mal formaté");
    		}
    	}
    	
    	vertx.eventBus().send(VORTEX_CLIENT_GET, filters, ar ->{
    		if (ar.succeeded()) {
    			String count = ar.result().headers().get("count");
    			if (count != null) {
    				send206(context, count, (JsonArray) ar.result().body());
    			} else {
    				this.replyDefaultGetJsonArray(context, "getAllClients", ar);
    			}
    		} else {
    			ReplyException cause = (ReplyException) ar.cause();
    			sendError(context, cause.failureCode(), cause.getMessage());
    		}
    	});
    }

    private void getClient(RoutingContext ctx) {
        String action = "appel du webservice getClient";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        String businessId = ctx.request().getParam("businessId");
        vertx.eventBus().send(ClientAdresses.GET_CLIENT_BY_BUSINESS_ID, businessId,
                ar -> replyDefaultGetJsonObject(ctx, action, ar));
    }

    private void getAllExchangesByClient(RoutingContext ctx) {
        String action = "appel du webservice getAllExchangesByClient";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        String businessId = ctx.request().getParam("businessId");
        vertx.eventBus().send(ExchangeAdresses.VORTEX_EXCHANGE_GET_BY_OWNER, businessId,
                ar -> replyDefaultGetJsonObject(ctx, action, ar));
    }

    private void getAllQueuesByClient(RoutingContext ctx) {
        String action = "appel du webservice getAllQueuesByClient";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        String businessId = ctx.request().getParam("businessId");
        vertx.eventBus().send(QueueAdresses.LIST_QUEUES_BY_OWNER, businessId,
                ar -> replyDefaultGetJsonObject(ctx, action, ar));
    }

    private void deleteClient(RoutingContext ctx) {
        String action = "appel du webservice deleteClient";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        String businessId = ctx.request().getParam("businessId");
        ctx.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);
        JsonObject json = new JsonObject();
		json.put("businessId", businessId);
		String header = ctx.request().getHeader("Authorization");
		json.put("header", header);
		json.put("origine", Constants.TRACE_ORIGINE_CASSIS);
         vertx.eventBus().send(ClientAdresses.DELETE_CLIENT, json,
                ar -> replyDefaultDelete(ctx, "delete client", ar));
    }

    private void putClient(RoutingContext ctx) {
        String action = "appel du webservice putClient";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        JsonObject jsonClient = ctx.getBodyAsJson();
		String header = ctx.request().getHeader("Authorization");
		jsonClient.put("header", header);
		jsonClient.put("origine", Constants.TRACE_ORIGINE_CASSIS);
        vertx.eventBus().send(ClientAdresses.UPDATE_CLIENT, jsonClient,
                ar -> this.replyToClient(ar, ctx, 200, "putClient"));
    }

    private void replyToClient(AsyncResult<Message<Object>> asyncResult, RoutingContext ctx, int succesCode,
            String operation) {
        ctx.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);
        if (asyncResult.succeeded()) {
            try {
                Message<Object> result = asyncResult.result();
                if (result.body() instanceof JsonObject) {
                    JsonObject body = (JsonObject) result.body();
                    ctx.response().setStatusCode(succesCode).end(body.encode());
                } else {
                    JsonArray body = (JsonArray) result.body();
                    ctx.response().setStatusCode(succesCode).end(body.encode());
                }
            } catch (Exception e) {
                sendError500(ctx, operation, e.getMessage());
            }
        } else {
            try {
                ReplyException cause = (ReplyException) asyncResult.cause();
                sendError(ctx, cause.failureCode(), operation, cause.getMessage());
            } catch (Exception e) {
                sendError500(ctx, operation, e.getMessage());
            }
        }
    }

}
