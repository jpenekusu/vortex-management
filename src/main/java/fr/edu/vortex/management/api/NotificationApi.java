//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.List;

import fr.edu.echanges.nat.vortex.common.adresses.notification.NotificationAdresses;
import fr.edu.vortex.management.Constants;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class NotificationApi extends AbstractApi {

    private static final Logger logger = LoggerFactory.getLogger(NotificationApi.class);

    private static final String URI_NOTIFICATION = "/notifications";
    private static final String CORRELATION_ID = "/:correlation_id";

    Vertx vertx;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;

        Router router = Router.router(vertx);

        router.get(URI_NOTIFICATION + CORRELATION_ID).handler(this::getNotificationByCorrelationId);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "Route Flux :" + route.getPath());
        }

        return router;

    }

    private void getNotificationByCorrelationId(RoutingContext ctx) {
        String correlationId = ctx.request().getParam("correlation_id");
        String action = "getNotificationByCorrelationId(RoutingContext ctx)";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

        vertx.eventBus().send(NotificationAdresses.GET_NOTIFICATION_BY_CORRELATION_ID, correlationId, ar -> {
            this.replyDefaultGetJsonArray(ctx, action, ar);
        });

    }

}
