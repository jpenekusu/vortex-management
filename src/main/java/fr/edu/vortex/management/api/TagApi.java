package fr.edu.vortex.management.api;

import fr.edu.vortex.management.Constants;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class TagApi extends AbstractApi {

	
	public static final String ADRESS_GET_TAG = "vortex.core.tag.get";

	public static final String URI_TAGS = "/tags";
	
	private static final Logger logger = LoggerFactory.getLogger(TagApi.class);

	public static final String KEY_JSON_NOM = "nom";

	Vertx vertx;

	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;

		Router router = Router.router(vertx);

		router.get(URI_TAGS).handler(this::listerTags);

		return router;

	}

	
	private void listerTags(RoutingContext ctx) {
		String action = "listerTags(RoutingContext ctx)";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

		JsonObject jsonCritere = new JsonObject();
		jsonCritere.put(KEY_JSON_NOM, ctx.request().getParam(KEY_JSON_NOM));
		
		vertx.eventBus().send(ADRESS_GET_TAG, jsonCritere, ar -> {
			if (ar.succeeded()) {
				this.replyDefaultGetJsonArray(ctx, "listerTags", ar);
			} else {
				ReplyException cause = (ReplyException) ar.cause();
				sendError(ctx, cause.failureCode(), cause.getMessage());
			}
		});
	}

}
