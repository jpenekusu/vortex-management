//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.time.Instant;
import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.metrics.PeriodicCount;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.AsyncMap;
import io.vertx.core.shareddata.Counter;
import io.vertx.core.shareddata.Lock;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class MetricsApi extends AbstractApi {

    private static final Logger logger = LoggerFactory.getLogger(MetricsApi.class);
    private static final String METRICS_NAME = "metrics";
    private static final String LATEST_PERIODIC_COUNT = "latestPeriodicCount";

    private static final String URI_METRICS = "/metrics";

    Vertx vertx;

    private SharedData sharedData;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;
        Router router = Router.router(vertx);
        sharedData = vertx.sharedData();
        this.gererMetriques();

        router.get(URI_METRICS + "/gateway/messages/periodic_count").handler(this::getMessagesPeriodicCount);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "Route MetricsApi :" + route.getPath());
        }

        return router;

    }

    private void getMessagesPeriodicCount(RoutingContext ctx) {
        try {

            sharedData.<String, JsonObject>getAsyncMap(METRICS_NAME, res -> {
                if (res.succeeded()) {
                    AsyncMap<String, JsonObject> map = res.result();
                    map.get(LATEST_PERIODIC_COUNT, resGet -> {
                        if (res.succeeded()) {
                            JsonObject latestPeriodicCount = resGet.result();
                            send200WithJsonObject(ctx, latestPeriodicCount);
                        } else {
                            String message = "Problème d'accès à l'objet latestPeriodicCount de la sharedData";
                            logger.warn(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, message);
                            sendError500(ctx, message);
                        }
                    });

                } else {
                    String message = "Problème d'accès aux metrics de la sharedData";
                    logger.warn(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, message);
                    sendError500(ctx, message);
                }
            });

        } catch (Exception e) {
            sendError500(ctx, e.getMessage());
        }
    }

    /*
     * Génération des metriques toutes les 10 secondes
     */
    public void gererMetriques() {

        // Le traitement s'execute toutes les 10 secondes
        vertx.setPeriodic(10000, arPeriodic -> {

            // On récupère un lock pour être sûr qu'une seule instance du cluster exécute le
            // code
            sharedData.getLock("LockNbGatewayMessages", arLock -> {
                if (arLock.succeeded()) {
                    Lock lock = arLock.result();

                    // On relache le lock juste avant les 10 secondes pour être sûr que la même
                    // instance le récupère.
                    vertx.setTimer(9999, tid -> lock.release());

                    // On récupère le compteur comptenant l'ensemble des messages passés dans la
                    // passerelle
                    sharedData.getCounter("nbGatewayMessages", getCounterRes -> {
                        if (getCounterRes.succeeded()) {
                            Counter counter = getCounterRes.result();
                            counter.get(getCount -> {
                                Long nbGatewayMessages = getCount.result();
                                long timestamp = System.currentTimeMillis();
                                logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID,
                                        "nombre de messages arrivés dans la passerelle = " + nbGatewayMessages);

                                if (nbGatewayMessages != null) {
                                    // Si des messages sont arrivés dans la passerelle, on récupére dans le tableau
                                    // des metriques le dernier objet latestPeriodicCount sauvegardé
                                    sharedData.<String, JsonObject>getAsyncMap(METRICS_NAME, res -> {
                                        if (res.succeeded()) {
                                            AsyncMap<String, JsonObject> map = res.result();
                                            map.get(LATEST_PERIODIC_COUNT, resGet -> {
                                                if (res.succeeded()) {
                                                    JsonObject json = resGet.result();
                                                    PeriodicCount latestPeriodicCount;

                                                    // Via l'objet récupéré, on récupère l'ancien nombre de message
                                                    // passé dans la passerelle et on génére un nouvel objet avec les
                                                    // dernières valeurs
                                                    Integer lastNbGatewayMessages = 0;
                                                    if (json == null) {
                                                        latestPeriodicCount = new PeriodicCount(0, 0,
                                                                Instant.now().getEpochSecond());
                                                    } else {
                                                        lastNbGatewayMessages = json
                                                                .getInteger(PeriodicCount.KEY_JSON_TOTAL_MESSAGES);
                                                    }

                                                    int nbGatewayMessagesInt = (int) Math.round(nbGatewayMessages);
                                                    int latestDiff = nbGatewayMessagesInt - lastNbGatewayMessages;
                                                    latestPeriodicCount = new PeriodicCount(nbGatewayMessagesInt,
                                                            latestDiff, timestamp);

                                                    map.put(LATEST_PERIODIC_COUNT, latestPeriodicCount.toJsonObject(),
                                                            putAr -> {
                                                            });

                                                }
                                            });
                                        }
                                    });

                                }
                            });

                        } else {
                            logger.warn(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID,
                                    "Problème d'accès au compteur nbGatewayMessages");
                        }
                    });

                }

            });

        });

    }

}
