//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.List;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.MainVerticle;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.utilisateur.pojo.NomenclatureTypeRapport;
import fr.edu.vortex.management.utilisateur.pojo.Rapport;
import fr.edu.vortex.management.utilisateur.verticle.RapportVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;



public class RapportApi extends AbstractApi {

	private static final String ACTION_LISTER_RAPPORT = "listerRapport(RoutingContext ctx)";
	private static final String ACTION_GET_RAPPORT = "getRapport(RoutingContext ctx)";

	private static final Logger logger = LoggerFactory.getLogger(RapportApi.class);

	public static final String URI_RAPPORT = "/rapports";
    private static final String RAPPORT_ID = "/:rapportId";
	
	public static final String URI_LIST_TYPES_RAPPORT = "/rapports_types/";
	private static final String ACTION_GET_ALL_TYPE_RAPPORT = "Lister les types de rapports";

	Vertx vertx;

	public Router getRouterApi(Vertx vertx) {
		this.vertx = vertx;

		Router router = Router.router(vertx);

		router.post(URI_RAPPORT).handler(BodyHandler.create());
		router.get(URI_RAPPORT).handler(this::listerRapport);
		
		router.get(URI_LIST_TYPES_RAPPORT).handler(this::listerTypesRapport);

        router.get(URI_RAPPORT + RAPPORT_ID).handler(this::getRapport);

		return router;

	}
	
	private void listerTypesRapport(RoutingContext ctx) {
	    	ctx.response().putHeader(HttpHeaders.CONTENT_TYPE.toString(), MainVerticle.CONTENT_TYPE_JSON);

		JsonArray response = new JsonArray();
		List<String> listeValeurs = NomenclatureTypeRapport.getListeValeurs();
		for (Iterator<String> iterator = listeValeurs.iterator(); iterator.hasNext();) {
			response.add(iterator.next());
		}

		ctx.response().setStatusCode(200).end(response.encode());

		logger.debug(
				Constants.LOG_MESSAGE,
				Constants.LOG_NO_CORRELATION_ID,
				ACTION_GET_ALL_TYPE_RAPPORT);
	}


	private void getRapport(RoutingContext ctx) {
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_GET_RAPPORT);
		
        String rapportId = ctx.request().getParam("rapportId");
		
		try {
			JsonObject json = new JsonObject();
			json.put("rapportId", Integer.valueOf(rapportId));
	        vertx.eventBus().send(RapportVerticle.GET_RAPPORT, json,
	                ar -> replyDefaultGetJsonObject(ctx, ACTION_GET_RAPPORT, ar));
        } catch (NumberFormatException e) {
            sendError400(ctx, ERR_ID_NOT_INTEGER);
        }
	}
	
	private void listerRapport(RoutingContext ctx) {
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, ACTION_LISTER_RAPPORT);
		
		JsonObject jsonCritere = new JsonObject();
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE));
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, ctx.request().getParam(FluxVerticle.KEY_JSON_PAGE_LIMIT));
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT));
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT_ORDER, ctx.request().getParam(FluxVerticle.KEY_JSON_SORT_ORDER));
		
		jsonCritere.put(Rapport.KEY_JSON_DATE_RAPPORT, ctx.request().getParam(Rapport.KEY_JSON_DATE_RAPPORT));
		jsonCritere.put(Rapport.KEY_JSON_LOGIN, ctx.request().getParam(Rapport.KEY_JSON_LOGIN));
		jsonCritere.put(Rapport.KEY_JSON_NOM, ctx.request().getParam(Rapport.KEY_JSON_NOM));
		jsonCritere.put(Rapport.KEY_JSON_PRENOM, ctx.request().getParam(Rapport.KEY_JSON_PRENOM));
		jsonCritere.put(Rapport.KEY_JSON_TYPE_RAPPORT, ctx.request().getParam(Rapport.KEY_JSON_TYPE_RAPPORT));
		
		vertx.eventBus().send(RapportVerticle.LISTER_RAPPORT, jsonCritere,ar ->{ 
		if (ar.succeeded()) {
			String count = ar.result().headers().get("count");
			if (count != null) {
				send206(ctx, count, (JsonArray) ar.result().body());
			} else {
				this.replyDefaultGetJsonArray(ctx, ACTION_LISTER_RAPPORT, ar);
			}
		} else {
			ReplyException cause = (ReplyException) ar.cause();
			sendError(ctx, cause.failureCode(), cause.getMessage());
		}		
		});
	}
	
	
	protected void send206(RoutingContext ctx, String count, JsonArray body) {
		ctx.response().setStatusCode(HttpURLConnection.HTTP_PARTIAL)
		.putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_CHARSET_UTF_8)
		.putHeader("x-total-count", count).end(body.encode());
	}

}
