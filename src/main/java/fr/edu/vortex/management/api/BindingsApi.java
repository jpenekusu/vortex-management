//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.List;

import fr.edu.echanges.nat.vortex.common.adresses.exchange.BindingAdresses;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.flux.FluxVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class BindingsApi extends AbstractApi {

//    private static final String FILTER_ID_CLIENT = "id_client";
//
//    private static final String FILTER_EXCHANGE_NAME = "exchange_name";
//
//    private static final String FILTER_QUEUE_NAME = "queue_name";

    private static final Logger logger = LoggerFactory.getLogger(BindingsApi.class);

    public static final String URI_BINDINGS = "/bindings/";
    
    private static final String ID = ":id";
    
    public static final String VORTEX_BINDING_UPDATE = "vortex.core.binding.update";
    public static final String VORTEX_BINDING_GET = "vortex.core.binding.get";
    
    public static final String FILTRE_PRODUCTEUR = "emetteur";
    public static final String FILTRE_DEPOT = "exchange";
    public static final String FILTRE_ROUTE = "pattern_binding";
    public static final String FILTRE_DESTINATAIRE = "destinataire";
    public static final String FILTRE_ZR = "queue";
   

    Vertx vertx;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;
        Router router = Router.router(vertx);

        router.get(URI_BINDINGS).handler(this::getBindings);

        router.delete(URI_BINDINGS + ":identifiant").handler(this::deleteBindings);

        router.post(URI_BINDINGS).handler(BodyHandler.create());
        router.post(URI_BINDINGS).handler(this::postBindings);

        router.put(URI_BINDINGS + ID).handler(BodyHandler.create());
        router.put(URI_BINDINGS + ID).handler(this::putBindings);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "route Bindings:" + route.getPath());
        }

        return router;
    }

    private void putBindings(RoutingContext context) {
    	String action = "put bindings";
    	JsonObject newBinding = new JsonObject();
    	try {
    		newBinding = context.getBodyAsJson();
    	} catch (Exception e) {
    		sendError400(context, "Body manquant ou mal formaté");
    	}
    	if (!newBinding.isEmpty()) {
    		if ((newBinding.getString("pattern_binding") == null) || ("".equals(newBinding.getString("pattern_binding")))) {
    			sendError400(context, "L'attribut 'pattern_binding' est absent ou contient une chaîne vide");
    		} else {
    			String header = context.request().getHeader("Authorization");
    			newBinding.put("header", header);
    			newBinding.put("origine", Constants.TRACE_ORIGINE_CASSIS);
    			vertx.eventBus().send(VORTEX_BINDING_UPDATE, newBinding,
    					replyHandler -> replyDefaultPost(context, action, replyHandler));
    		}
    	}
    }

    private void postBindings(RoutingContext context) {
        String action = "Créer une route";
        JsonObject newBinding = new JsonObject();
        try {
            newBinding = context.getBodyAsJson();
        } catch (Exception e) {
            sendError400(context, "Body manquant ou mal formaté");
        }
        if (!newBinding.isEmpty()) {
            checkBinding(context, action, newBinding);
        }

    }

    private void checkBinding(RoutingContext context, String action, JsonObject newBinding) {
        if ((newBinding.getString("pattern_binding") == null) || ("".equals(newBinding.getString("pattern_binding")))) {
            sendError400(context, "L'attribut 'pattern_binding' est absent ou contient une chaîne vide");
        } else {
            if (newBinding.getJsonObject("exchange") == null) {
                sendError400(context,
                        "La route doit être composée d'un objet 'exchange' ayant les attributs 'id', 'name' et 'type' renseignés");
            } else {
                checkExchange(context, action, newBinding);
                if (newBinding.getJsonObject("queue") == null) {
                    sendError400(context,
                            "La route doit être composée d'un objet 'queue' ayant les attributs 'id' et 'name' renseignés");
                } else {
                    checkQueue(context, action, newBinding);
        			String header = context.request().getHeader("Authorization");
        			newBinding.put("header", header);
        			newBinding.put("origine", Constants.TRACE_ORIGINE_CASSIS);
                    vertx.eventBus().send(BindingAdresses.VORTEX_BINDING_CREATE, newBinding,
                            replyHandler -> replyDefaultPost(context, action, replyHandler));
                }

            }

        }
    }

    private void checkExchange(RoutingContext context, String action, JsonObject newBinding) {
        if (newBinding.getJsonObject("exchange").getInteger("id") == null) {
            sendError400(context, "L'objet 'exchange' doit posséder un attribut 'id'");
        } else {
            if ((newBinding.getJsonObject("exchange").getString("name") == null)
                    || ("".equals(newBinding.getJsonObject("exchange").getString("name")))) {
                sendError400(context, "L'objet 'exchange' doit posséder un attribut 'name' non vide");
            } else {
                if (newBinding.getJsonObject("exchange").getString("type") == null) {
                    sendError400(context, "L'objet 'exchange' doit posséder un attribut 'type' non vide");
                }
            }

        }
    }

    private void checkQueue(RoutingContext context, String action, JsonObject newBinding) {
        if (newBinding.getJsonObject("queue").getInteger("id") == null) {
            sendError400(context, "L'objet 'queue' doit posséder un attribut 'id'");
        } else {
            if ((newBinding.getJsonObject("queue").getString("name") == null)
                    || ("".equals(newBinding.getJsonObject("queue").getString("name")))) {
                sendError400(context, "L'objet 'exchange' doit posséder un attribut 'name' non vide");
            }
        }
    }

    /**
     * Permet de router selon le paramètre passé dans la requête
     * 
     * @param context
     */
    private void getBindings(RoutingContext context) {

    	JsonObject jsonCritere = new JsonObject();
    	jsonCritere.put(FluxVerticle.KEY_JSON_PAGE, context.request().getParam(FluxVerticle.KEY_JSON_PAGE));
    	jsonCritere.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, context.request().getParam(FluxVerticle.KEY_JSON_PAGE_LIMIT));
    	jsonCritere.put(FluxVerticle.KEY_JSON_SORT, context.request().getParam(FluxVerticle.KEY_JSON_SORT));
    	jsonCritere.put(FluxVerticle.KEY_JSON_SORT_ORDER, context.request().getParam(FluxVerticle.KEY_JSON_SORT_ORDER));

    	jsonCritere.put(FILTRE_PRODUCTEUR, context.request().getParam(FILTRE_PRODUCTEUR));
    	jsonCritere.put(FILTRE_DEPOT, context.request().getParam(FILTRE_DEPOT));
    	jsonCritere.put(FILTRE_ROUTE, context.request().getParam(FILTRE_ROUTE));
    	jsonCritere.put(FILTRE_DESTINATAIRE, context.request().getParam(FILTRE_DESTINATAIRE));
    	jsonCritere.put(FILTRE_ZR, context.request().getParam(FILTRE_ZR));

    	vertx.eventBus().send(VORTEX_BINDING_GET, jsonCritere, ar ->{
    		if (ar.succeeded()) {
    			String count = ar.result().headers().get("count");
    			if (count != null) {
    				send206(context, count, (JsonArray) ar.result().body());
    			} else {
    				this.replyDefaultGetJsonArray(context, "getBindings", ar);
    			}
    		} else {
    			ReplyException cause = (ReplyException) ar.cause();
    			sendError(context, cause.failureCode(), cause.getMessage());
    		}
    	});
    }

//    /**
//     * Recherche des bindings avec des criteres (filtre)
//     * 
//     * @param context
//     */
//    private void getBindingsWithCriteria(RoutingContext context) {
//        String queueName = context.request().getParam(FILTER_QUEUE_NAME);
//        String exchangeName = context.request().getParam(FILTER_EXCHANGE_NAME);
//        String idClient = context.request().getParam(FILTER_ID_CLIENT);
//        List<Future> futures = new ArrayList<>();
//        if (queueName != null) {
//            Future<JsonArray> byQueue = callServiceCoreGetBindings(BindingAdresses.VORTEX_BINDING_GET_BY_QUEUE,
//                    queueName);
//            futures.add(byQueue);
//        }
//        if (exchangeName != null) {
//            Future<JsonArray> byExchanges = callServiceCoreGetBindings(BindingAdresses.VORTEX_BINDING_GET_BY_EXCHANGE,
//                    exchangeName);
//            futures.add(byExchanges);
//        }
//        if (idClient != null) {
//            Future<JsonArray> byClients = callServiceCoreGetBindings(BindingAdresses.VORTEX_BINDING_GET_BY_CLIENT,
//                    idClient);
//            futures.add(byClients);
//        }
//
//        CompositeFuture.all(futures).compose(this::mergeData)
//                .setHandler(arMergeData -> sendResponse(arMergeData, context));
//    }
//
//    /**
//     * Envoi au client(celui qui a fait la requete http) la reponse avec dans le
//     * body un jsonArray.
//     * 
//     * @param handler
//     * @param context
//     */
//    private void sendResponse(AsyncResult<JsonArray> handler, RoutingContext context) {
//        if (handler.succeeded()) {
//            JsonArray body = handler.result();
//            if (body.isEmpty()) {
//                send204(context);
//            } else {
//                send200WithJsonArray(context, body);
//            }
//        } else {
//            ReplyException cause = (ReplyException) handler.cause();
//            sendError(context, cause);
//        }
//    }
//
//    /**
//     * Récupére les retour de l'eventbus merge les jsonArray en un seul.
//     * 
//     * @param handler
//     * @return
//     */
//    private Future<JsonArray> mergeData(AsyncResult<CompositeFuture> handler) {
//        Future<JsonArray> future = Future.future();
//        if (handler.succeeded()) {
//            try {
//                JsonArray body = new JsonArray();
//                handler.result().list().stream().map(o -> (JsonArray) o).forEach(json -> {
//                    if (!json.isEmpty()) {
//                        json.stream().map(elt -> (JsonObject) elt).forEach(b -> {
//                            if (!body.contains(b)) {
//                                body.add(b);
//                            }
//                        });
//                    }
//                });
//                future.complete(body);
//            } catch (Exception e) {
//                future.fail(e.getMessage());
//            }
//        } else {
//            future.fail(handler.cause());
//        }
//        return future;
//    }
//
//    private Future<JsonArray> callServiceCoreGetBindings(String address, String filter) {
//        Future<JsonArray> future = Future.future();
//        vertx.eventBus().send(address, filter, replyHandler -> {
//            if (replyHandler.succeeded()) {
//                try {
//                    JsonArray jsonArray = (JsonArray) replyHandler.result().body();
//                    future.complete(jsonArray);
//                } catch (Exception e) {
//                    future.fail(e.getCause());
//                }
//            } else {
//                future.fail(replyHandler.cause());
//            }
//        });
//        return future;
//    }
//
//    private void getAllBindings(RoutingContext context) {
//        String action = "Lister les routes";
//        logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
//
//        action = "appel du webservice getAllBindings";
//        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
//
//        vertx.eventBus().send(BindingAdresses.VORTEX_BINDING_GET_ALL, null,
//                replyHandler -> replyDefaultGetJsonArray(context, "get Bindings", replyHandler));
//    }

    private void deleteBindings(RoutingContext context) {
        String action = "appel du webservice deleteBindings";
        logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);

        String identifiant = context.request().getParam("identifiant");
        if (null == identifiant) {
            sendError400(context, "Identifiant manquant");
        } else {
            if (!identifiant.matches("0|[1-9]\\d*")) {
                sendError404(context,
                        "L'identifiant doit être soit un entier qui ne commence pas par le chiffre '0', soit '0'");
            } else {
                JsonObject json = new JsonObject();
        		json.put("identifiant", identifiant);
        		String header = context.request().getHeader("Authorization");
        		json.put("header", header);
        		json.put("origine", Constants.TRACE_ORIGINE_CASSIS);
                vertx.eventBus().send(BindingAdresses.VORTEX_BINDING_DELETE, json,
                        replyHandler -> replyDefaultDelete(context, "delete Bindings", replyHandler));
            }
        }
    }
    
}
