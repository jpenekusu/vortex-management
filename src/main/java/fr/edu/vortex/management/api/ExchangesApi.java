//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.util.List;

import fr.edu.echanges.nat.vortex.common.adresses.exchange.BindingAdresses;
import fr.edu.echanges.nat.vortex.common.adresses.exchange.ExchangeAdresses;
import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.flux.FluxVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class ExchangesApi extends AbstractApi {

    private static final Logger logger = LoggerFactory.getLogger(ExchangesApi.class);

    public static final String URI_EXCHANGES = "/exchanges/";
    public static final String URI_EXCHANGE_TYPE = "/exchanges_types/";
    public static final String URI_BINDINGS = "/bindings";

    public static final String KEY_JSON_SYSTEME = "systeme";

    public static final String VORTEX_EXCHANGE_UPDATE = "vortex.core.exchange.update";
    public static final String LIST_DEPOT_FILTRE = "vortex.core.exchange.listExchangeFiltre";

    public static final String KEY_JSON_NAME = "name";
    public static final String KEY_JSON_TYPE = "type";
    public static final String KEY_JSON_CLIENT = "partenaire";

    Vertx vertx;

    public Router getRouterApi(Vertx vertx) {
        this.vertx = vertx;
        Router router = Router.router(vertx);

        router.get(URI_EXCHANGE_TYPE).handler(this::getAllExchangeTypes);

        router.get(URI_EXCHANGES).handler(this::getAllExchanges);
        router.get(URI_EXCHANGES + ":name").handler(this::getExchangesByName);

        router.get(URI_EXCHANGES + ":name" + URI_BINDINGS).handler(this::getAllBindingsByExchange);

        router.delete(URI_EXCHANGES + ":name").handler(this::deleteExchanges);

        router.post(URI_EXCHANGES).handler(BodyHandler.create());
        router.post(URI_EXCHANGES).handler(this::postExchanges);

        router.put(URI_EXCHANGES + ":id").handler(BodyHandler.create());
        router.put(URI_EXCHANGES + ":id").handler(this::putExchanges);

        List<Route> routes = router.getRoutes();
        for (Route route : routes) {
            logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "route Exchanges:" + route.getPath());
        }

        return router;
    }

    private void postExchanges(RoutingContext context) {
        if (context.getBody().length() == 0) {
            sendError400(context, "postExchanges : no body.");
        } else {
            try {
                JsonObject newExchange = context.getBodyAsJson();
                logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "Posting exchange");
                if (newExchange == null) {
                    sendError400(context, "postExchanges : no exchange.");
                } else if ((newExchange.getString("type") == null) || ("".equals(newExchange.getString("type")))) {
                    sendError400(context, "postExchanges : no exchange type.");
                } else if ((newExchange.getString("name") == null) || ("".equals(newExchange.getString("name")))) {
                    sendError400(context, "postExchanges : no exchange name.");
                } else if ((!newExchange.containsKey("partenaire"))
                        || (newExchange.getJsonObject("partenaire").getInteger("id") == null)) {
                    sendError400(context, "postExchanges : no partenaire id.");
                } else {
                    // On dématérialise la simple cote "'" afin que la requête d'insertion en base
                    // se passe bien
                    String exchangeName = newExchange.getString("name");
                    newExchange.put("name", exchangeName.replaceAll("'", "''"));

            		String header = context.request().getHeader("Authorization");
            		newExchange.put("header", header);
            		newExchange.put("origine", Constants.TRACE_ORIGINE_CASSIS);
                    vertx.eventBus().send(ExchangeAdresses.VORTEX_EXCHANGE_CREATE, newExchange,
                            replyHandler -> replyDefaultPost(context, "create Exchange", replyHandler));
                }

            } catch (Exception ex) {
                sendError400(context, "postExchanges : La structure de l'objet JSON est incorrecte");
            }

        }
    }

    private void putExchanges(RoutingContext context) {
        if (context.getBody().length() == 0) {
            sendError400(context, "putExchanges : no body.");
        } else {
            try {
                JsonObject newExchange = context.getBodyAsJson();
                logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "Posting exchange");
                if (newExchange == null) {
                    sendError400(context, "putExchanges : no exchange.");
                } else if ((newExchange.getString("type") == null) || ("".equals(newExchange.getString("type")))) {
                    sendError400(context, "putExchanges : no exchange type.");
                } else if ((newExchange.getString("name") == null) || ("".equals(newExchange.getString("name")))) {
                    sendError400(context, "putExchanges : no exchange name.");
                } else if ((!newExchange.containsKey("partenaire"))
                        || (newExchange.getJsonObject("partenaire").getInteger("id") == null)) {
                    sendError400(context, "putExchanges : no partenaire id.");
                } else {
                    // On dématérialise la simple cote "'" afin que la requête d'insertion en base
                    // se passe bien
                    String exchangeName = newExchange.getString("name");
                    newExchange.put("name", exchangeName.replaceAll("'", "''"));

            		String header = context.request().getHeader("Authorization");
            		newExchange.put("header", header);
            		newExchange.put("origine", Constants.TRACE_ORIGINE_CASSIS);

            		vertx.eventBus().send(VORTEX_EXCHANGE_UPDATE, newExchange,
                            replyHandler -> replyDefaultPost(context, "update Exchange", replyHandler));
                }

            } catch (Exception ex) {
                sendError400(context, "putExchanges : La structure de l'objet JSON est incorrecte");
            }

        }
    }

    private void getAllExchangeTypes(RoutingContext context) {
        String action = "Liste de tous les types de dépôt";
        logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
        vertx.eventBus().send(ExchangeAdresses.VORTEX_EXCHANGE_TYPE_LIST, null,
                replyHandler -> replyDefaultGetJsonArray(context, "Obtenir tous les types de dépôt", replyHandler));
    }

    private void getAllExchanges(RoutingContext context) {
        String action = "Liste de tous les types de dépôt";
        logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
       
      	JsonObject filters = new JsonObject();
       	filters.put(FluxVerticle.KEY_JSON_PAGE, context.request().getParam(FluxVerticle.KEY_JSON_PAGE));
       	filters.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, context.request().getParam(FluxVerticle.KEY_JSON_PAGE_LIMIT));
       	filters.put(FluxVerticle.KEY_JSON_SORT, context.request().getParam(FluxVerticle.KEY_JSON_SORT));
       	filters.put(FluxVerticle.KEY_JSON_SORT_ORDER, context.request().getParam(FluxVerticle.KEY_JSON_SORT_ORDER));

       	filters.put(KEY_JSON_CLIENT, context.request().getParam(KEY_JSON_CLIENT));
       	filters.put(KEY_JSON_TYPE, context.request().getParam(KEY_JSON_TYPE));
       	filters.put(KEY_JSON_NAME, context.request().getParam(KEY_JSON_NAME));
        
        String systemeParam = context.request().getParam(KEY_JSON_SYSTEME);
        if (context.request().getParam(KEY_JSON_SYSTEME) != null) {
            if (systemeParam.toUpperCase().equals("TRUE") || systemeParam.toUpperCase().equals("FALSE")) {
                filters.put(KEY_JSON_SYSTEME,
                        Boolean.valueOf(systemeParam));
            } else {
                sendError(context, 400, action, "Paramètre système mal formaté");
                return;
            }
        }
        
        vertx.eventBus().send(LIST_DEPOT_FILTRE, filters, ar ->{
    		if (ar.succeeded()) {
    			String count = ar.result().headers().get("count");
    			if (count != null) {
    				send206(context, count, (JsonArray) ar.result().body());
    			} else {
    				this.replyDefaultGetJsonArray(context, "getAllExchanges", ar);
    			}
    		} else {
    			ReplyException cause = (ReplyException) ar.cause();
    			sendError(context, cause.failureCode(), cause.getMessage());
    		}
    	});       
    }

    private void getExchangesByName(RoutingContext context) {

        String name = context.request().getParam("name");
        logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "Obtenir le dépôt :" + name);
        if (null == name) {
            sendError500(context, "Obtenir dépôt par nom : name incorrect.");
        } else {
            vertx.eventBus().send(ExchangeAdresses.VORTEX_EXCHANGE_GET_BY_NAME, name,
                    replyHandler -> replyDefaultGetJsonObject(context, "Obtenir un dépôt par nom", replyHandler));
        }
    }

    private void getAllBindingsByExchange(RoutingContext context) {

        String name = context.request().getParam("name");
        logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, "Obtenir route par dépôt :" + name);
        if (null == name) {
            sendError500(context, "Obtenir route par dépôt : name incorrect.");
        } else {
            vertx.eventBus().send(BindingAdresses.VORTEX_BINDING_GET_BY_EXCHANGE, name,
                    replyHandler -> replyDefaultGetJsonArray(context, "Obtenir route par dépôt", replyHandler));
        }
    }

    private void deleteExchanges(RoutingContext context) {
        String name = context.request().getParam("name");
        if (null == name) {
            sendError500(context, "name manquant");
        } else {
            JsonObject json = new JsonObject();
    		json.put("name", name);
    		String header = context.request().getHeader("Authorization");
    		json.put("header", header);
    		json.put("origine", Constants.TRACE_ORIGINE_CASSIS);
            vertx.eventBus().send(ExchangeAdresses.VORTEX_EXCHANGE_DELETE, json,
                    replyHandler -> replyDefaultDelete(context, "Suppression dépôt", replyHandler));
        }
    }
    
}
