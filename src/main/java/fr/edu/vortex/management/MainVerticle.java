//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigRenderOptions;

import fr.edu.echanges.nat.vortex.common.codec.VortexEventCodec;
import fr.edu.echanges.nat.vortex.common.configuration.ConfigHelper;
import fr.edu.echanges.nat.vortex.common.event.VortexEvent;
import fr.edu.vortex.management.api.ActionsApi;
import fr.edu.vortex.management.api.BindingsApi;
import fr.edu.vortex.management.api.BuisnessRouteApi;
import fr.edu.vortex.management.api.ClientsApi;
import fr.edu.vortex.management.api.ConfigurationApi;
import fr.edu.vortex.management.api.DashboardApi;
import fr.edu.vortex.management.api.ExchangesApi;
import fr.edu.vortex.management.api.FiltreUtilisateurApi;
import fr.edu.vortex.management.api.FluxApi;
import fr.edu.vortex.management.api.HealthApi;
import fr.edu.vortex.management.api.HistoFluxApi;
import fr.edu.vortex.management.api.MetricsApi;
import fr.edu.vortex.management.api.ModeleMailApi;
import fr.edu.vortex.management.api.NotificationApi;
import fr.edu.vortex.management.api.ParametreApi;
import fr.edu.vortex.management.api.NomenclatureTraceAPI;
import fr.edu.vortex.management.api.PermissionApi;
import fr.edu.vortex.management.api.QueuesApi;
import fr.edu.vortex.management.api.RacvisionApi;
import fr.edu.vortex.management.api.RapportApi;
import fr.edu.vortex.management.api.RoleApi;
import fr.edu.vortex.management.api.TagApi;
import fr.edu.vortex.management.api.TraceApi;
import fr.edu.vortex.management.api.TypesQueuesApi;
import fr.edu.vortex.management.api.UtilisateurApi;
import fr.edu.vortex.management.auth.AuthManager;
import fr.edu.vortex.management.auth.AuthentificationHandler;
import fr.edu.vortex.management.dashboard.DashboardVerticle;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.flux.HistoFluxVerticle;
import fr.edu.vortex.management.health.HealthDatabaseVerticle;
import fr.edu.vortex.management.health.HealthMangementVerticle;
import fr.edu.vortex.management.ldap.LdapVerticle;
import fr.edu.vortex.management.mail.ModeleMailVerticle;
import fr.edu.vortex.management.notification.NotificationVerticle;
import fr.edu.vortex.management.parametre.ParametreVerticle;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.FluxDAO;
import fr.edu.vortex.management.persistence.dao.HistoFluxDAO;
import fr.edu.vortex.management.persistence.dao.NotificationDAO;
import fr.edu.vortex.management.persistence.dao.ParametreDAO;
import fr.edu.vortex.management.persistence.dao.impl.FluxDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.HistoFluxDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.NotificationDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.ParametreDAOImpl;
import fr.edu.vortex.management.persistence.dao.impl.ParametreDAOImpl;
import fr.edu.vortex.management.utilisateur.verticle.FiltreUtilisateurVerticle;
import fr.edu.vortex.management.utilisateur.verticle.PermissionVerticle;
import fr.edu.vortex.management.utilisateur.verticle.RapportVerticle;
import fr.edu.vortex.management.utilisateur.verticle.RoleVerticle;
import fr.edu.vortex.management.utilisateur.verticle.TokenVerticle;
import fr.edu.vortex.management.utilisateur.verticle.TraceVerticle;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

public class MainVerticle extends AbstractVerticle {

	private static final String WEB_ROOT = "/dist/vortex-management-webapp";
	public static final String CONTENT_TYPE_JSON = "application/json; charset=utf-8";
	public static final String API_VERSION_1_0 = "v1.0/";

	public static final FluxDAO fluxDAO = new FluxDAOImpl();
	public static final HistoFluxDAO histoFluxDAO = new HistoFluxDAOImpl();
	public static final NotificationDAO notificationDAO = new NotificationDAOImpl();
	public static final ParametreDAO parametreDAO = new ParametreDAOImpl();

	private int port;

	private final Logger logger = LoggerFactory.getLogger(MainVerticle.class);

	@Override
	public void start(Future<Void> startFuture) throws Exception {

		try {
			fluxDAO.init(vertx);
			histoFluxDAO.init(vertx);
			notificationDAO.init(vertx);
			parametreDAO.init(vertx);
			enregistrerCodecs();

			Router router = initializeRouter();

			Future<Config> fConfig = ConfigHelper.Instance(vertx)
					.addStore(ConfigHelper.Type.FILE, ConfigHelper.Format.HOCON, "conf/default.conf")
					.addOptionalStore(ConfigHelper.Type.FILE, ConfigHelper.Format.HOCON, System.getProperty("conf"))
					.getConfig();

			fConfig.compose(config -> {
				String host = config.getString("vortex-management.http.host");
				port = config.getInt("vortex-management.http.port");

				HealthMangementVerticle healthVerticle = new HealthMangementVerticle();
				vertx.deployVerticle(healthVerticle);

				JsonObject cfg = new JsonObject(config.root().render(ConfigRenderOptions.concise()));
				HealthDatabaseVerticle healthDbVerticle = new HealthDatabaseVerticle();
				vertx.deployVerticle(healthDbVerticle, new DeploymentOptions().setConfig(cfg));

				FluxVerticle fluxVerticle = new FluxVerticle();
				vertx.deployVerticle(fluxVerticle);

				NotificationVerticle notifVerticle = new NotificationVerticle();
				vertx.deployVerticle(notifVerticle);

				PermissionVerticle permissionVerticle = new PermissionVerticle();
				vertx.deployVerticle(permissionVerticle);

                RoleVerticle roleVerticle = new RoleVerticle();
                vertx.deployVerticle(roleVerticle);
                
                TraceVerticle traceVerticle = new TraceVerticle();
                vertx.deployVerticle(traceVerticle);
                
                RapportVerticle rapportVerticle = new RapportVerticle();
                vertx.deployVerticle(rapportVerticle);

				Config dbConfig = config.getConfig("vortex-management.database");
				JsonObject configJson = new JsonObject(dbConfig.root().render(ConfigRenderOptions.concise()));

				Config LdapConfig = config.getConfig("vortex-management.ldap");
				JsonObject configLDAP = new JsonObject(LdapConfig.root().render(ConfigRenderOptions.concise()));

				UtilisateurVerticle utilisateurVerticle = new UtilisateurVerticle();
				vertx.deployVerticle(utilisateurVerticle);

				FiltreUtilisateurVerticle filtreUtilisateurVerticle = new FiltreUtilisateurVerticle();
				vertx.deployVerticle(filtreUtilisateurVerticle);

                DashboardVerticle dashboardVerticle = new DashboardVerticle();
                vertx.deployVerticle(dashboardVerticle);               
                
                LdapVerticle ldapVerticle = new LdapVerticle();
                vertx.deployVerticle(ldapVerticle, new DeploymentOptions().setConfig(configLDAP));
                
                ParametreVerticle parametreVerticle = new ParametreVerticle();                
                vertx.deployVerticle(parametreVerticle, new DeploymentOptions().setConfig(configLDAP));
                
                HistoFluxVerticle histoFluxVerticle = new HistoFluxVerticle();
                vertx.deployVerticle(histoFluxVerticle);

                ModeleMailVerticle modeleMailVerticle = new ModeleMailVerticle();                
                vertx.deployVerticle(modeleMailVerticle);

				DatabaseVerticle dataBaseVerticle = new DatabaseVerticle();
				vertx.deployVerticle(dataBaseVerticle, new DeploymentOptions().setConfig(configJson), handler -> {
					if (handler.failed()) {
						String action = "Déploiement du verticle";
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, handler.cause());
					}
				});

                Config authConfig = null;
                try {
                    authConfig = config.getConfig("vortex-management.auth");
                }
                catch(ConfigException ce){
                    logger.warn("Problème avec la clé de configuration \"vortex-management.auth\" : " + ce.getMessage());
                }
                AuthManager.create(vertx,authConfig, authMgrRes -> {
                    if (authMgrRes.failed()) {
                        logger.error(authMgrRes.cause().getMessage());
                    }
                    handleLogin(router, authMgrRes.result(), configLDAP);
                    TokenVerticle tokenVerticle = new TokenVerticle(vertx, authMgrRes.result());
                    vertx.deployVerticle(tokenVerticle);
               });
                
                

				vertx.createHttpServer().requestHandler(router::accept).listen(port, host, ar -> {
					if (ar.succeeded()) {
						String action = "serveur (management) http demarre sur le port " + port;
						logger.info(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
						startFuture.complete();
					} else {
						String action = "Erreur lors du démarrage du serveur (management) http";
						logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, ar.cause());
						startFuture.fail(ar.cause());
					}
				});
			}, startFuture);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void handleLogin(Router router, AuthManager authMgr, JsonObject configLdap) {
		AuthentificationHandler loginHandler = new AuthentificationHandler(authMgr.getProvider(), vertx);

		router.post("/" + API_VERSION_1_0 + "authlogin").handler(BodyHandler.create());
		router.post("/" + API_VERSION_1_0 + "authlogin").handler(loginHandler);

		router.get("/.well-known/jwks.json").handler(ar -> {
			ar.response().end(authMgr.getJwks().encodePrettily());
		});
		router.get(AuthManager.AUTH_PUBKEY_BASEURL + "/:kid").handler(ar -> {
			ar.response().end(authMgr.getPublicKeyAsPEM(ar.request().getParam("kid")));
		});

    }
    
    private Router initializeRouter() {
        Router router = Router.router(vertx);

		router.route()
				.handler(CorsHandler.create("*").allowedMethod(HttpMethod.GET).allowedMethod(HttpMethod.POST)
						.allowedMethod(HttpMethod.PUT).allowedMethod(HttpMethod.DELETE).allowedHeader("*, content-type")
						.exposedHeader("Content-Range").exposedHeader("x-total-count").exposedHeader("Authorization"));

		MetricsApi metricsApi = new MetricsApi();
		router.mountSubRouter("/" + API_VERSION_1_0, metricsApi.getRouterApi(vertx));

		ClientsApi clientsApi = new ClientsApi();
		router.mountSubRouter("/" + API_VERSION_1_0, clientsApi.getRouterApi(vertx));

		QueuesApi queuesApi = new QueuesApi();
		router.mountSubRouter("/" + API_VERSION_1_0, queuesApi.getRouterApi(vertx));

		TypesQueuesApi typeQueueApi = new TypesQueuesApi();
		router.mountSubRouter("/" + API_VERSION_1_0, typeQueueApi.getRouterApi(vertx));

		HealthApi healthApi = new HealthApi();
		router.mountSubRouter("/" + API_VERSION_1_0, healthApi.getRouterApi(vertx));

		RacvisionApi racvisionApi = new RacvisionApi();
		router.mountSubRouter("/", racvisionApi.getRouterApi(vertx));

		ExchangesApi exchangesApi = new ExchangesApi();
		router.mountSubRouter("/" + API_VERSION_1_0, exchangesApi.getRouterApi(vertx));

		BindingsApi bindingsApi = new BindingsApi();
		router.mountSubRouter("/" + API_VERSION_1_0, bindingsApi.getRouterApi(vertx));

		BuisnessRouteApi buisnessRouteApi = new BuisnessRouteApi();
		router.mountSubRouter("/" + API_VERSION_1_0, buisnessRouteApi.getRouterApi(vertx));

		ActionsApi actionApi = new ActionsApi();
		router.mountSubRouter("/" + API_VERSION_1_0, actionApi.getRouterApi(vertx));

		FluxApi fluxApi = new FluxApi();
		router.mountSubRouter("/" + API_VERSION_1_0, fluxApi.getRouterApi(vertx));

		NotificationApi notifApi = new NotificationApi();
		router.mountSubRouter("/" + API_VERSION_1_0, notifApi.getRouterApi(vertx));

		PermissionApi permissionApi = new PermissionApi();
		router.mountSubRouter("/" + API_VERSION_1_0, permissionApi.getRouterApi(vertx));

		RoleApi roleApi = new RoleApi();
		router.mountSubRouter("/" + API_VERSION_1_0, roleApi.getRouterApi(vertx));

        UtilisateurApi utilisateurApi = new UtilisateurApi();
        router.mountSubRouter("/" + API_VERSION_1_0, utilisateurApi.getRouterApi(vertx));
        
        TraceApi traceApi = new TraceApi();
        router.mountSubRouter("/" + API_VERSION_1_0, traceApi.getRouterApi(vertx));
        
        RapportApi rapportApi = new RapportApi();
        router.mountSubRouter("/" + API_VERSION_1_0, rapportApi.getRouterApi(vertx));

		ConfigurationApi exportApi = new ConfigurationApi();
		router.mountSubRouter("/" + API_VERSION_1_0, exportApi.getRouterApi(vertx));

        DashboardApi dashboardApi = new DashboardApi();
        router.mountSubRouter("/" + API_VERSION_1_0, dashboardApi.getRouterApi(vertx));

        NomenclatureTraceAPI nomenclatureTraceAPI = new NomenclatureTraceAPI();
        router.mountSubRouter("/" + API_VERSION_1_0, nomenclatureTraceAPI.getRouterApi(vertx));
        
        ParametreApi parametreApi = new ParametreApi();
        router.mountSubRouter("/" + API_VERSION_1_0, parametreApi.getRouterApi(vertx));
            

        FiltreUtilisateurApi filtreUtilisateurApi = new FiltreUtilisateurApi();
        router.mountSubRouter("/" + API_VERSION_1_0, filtreUtilisateurApi.getRouterApi(vertx));
        
        HistoFluxApi histoFluxApi = new HistoFluxApi();
        router.mountSubRouter("/" + API_VERSION_1_0, histoFluxApi.getRouterApi(vertx));

        TagApi tagApi = new TagApi();
        router.mountSubRouter("/" + API_VERSION_1_0, tagApi.getRouterApi(vertx));

        ModeleMailApi modeleMailApi = new ModeleMailApi();
        router.mountSubRouter("/" + API_VERSION_1_0, modeleMailApi.getRouterApi(vertx));

        try {
            HebergementWebApp hebergementWebApp = new HebergementWebApp();
            Router routerWebApp = hebergementWebApp.getRouterWebApp(vertx, WEB_ROOT);
            router.mountSubRouter("/", routerWebApp);
        } catch (Exception e) {
            String action = "impossible d'heberger la webApp ";
            String errorMessage = e.getMessage();
            logger.error(Constants.LOG_ERROR, Constants.LOG_NO_CORRELATION_ID, action, errorMessage);
        }

		return router;
	}

	/**
	 * Enregistre les codecs pour passer facilement des object java dans l event bus
	 */
	private void enregistrerCodecs() {
		String action = "Appel à la méthode enregistrerCodecs()";
		logger.debug(Constants.LOG_MESSAGE, Constants.LOG_NO_CORRELATION_ID, action);
		VortexEventCodec eventCodec = new VortexEventCodec();
		vertx.eventBus().registerDefaultCodec(VortexEvent.class, eventCodec);
	}
}
