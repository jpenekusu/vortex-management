//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.metrics;

import java.time.Instant;
import java.util.UUID;

import io.vertx.core.json.JsonObject;

public class PeriodicCount {

    public static final String KEY_JSON_TOTAL_MESSAGES = "total_messages";

    private String uid;

    private Integer nb = 0;

    private String fin;

    private Integer interval = 10;

    private String intervalUnit = "SECONDES";

    private Integer totalMessages = 0;

    public PeriodicCount(Integer totalMessages, Integer nbMessages, long timestampSnapshot) {
        this.uid = UUID.randomUUID().toString();
        this.fin = Instant.ofEpochMilli(timestampSnapshot).toString();
        this.totalMessages = totalMessages;
        this.nb = nbMessages;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getNb() {
        return nb;
    }

    public void setNb(Integer nb) {
        this.nb = nb;
    }

    public String getDebut() {
        return fin;
    }

    public void setDebut(String debut) {
        this.fin = debut;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public String getIntervalUnit() {
        return intervalUnit;
    }

    public void setIntervalUnit(String intervalUnit) {
        this.intervalUnit = intervalUnit;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonToEncode = simplePropsToJson();
        return jsonToEncode;
    }

    public String toJson() {
        return this.toJsonObject().encode();
    }

    private JsonObject simplePropsToJson() {
        JsonObject jsonToEncode = new JsonObject();

        jsonToEncode.put("id", this.uid);
        jsonToEncode.put("nb", this.nb);
        jsonToEncode.put("fin", this.fin);
        jsonToEncode.put("interval", this.interval);
        jsonToEncode.put("interval_unit", this.intervalUnit);
        jsonToEncode.put(KEY_JSON_TOTAL_MESSAGES, this.totalMessages);

        return jsonToEncode;
    }

    public int getTotalMessages() {
        return this.totalMessages;
    }

    public void setTotalMessages(Integer totalMessages) {
        this.totalMessages = totalMessages;
    }

}
