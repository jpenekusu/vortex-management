//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.dashboard;

import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.edu.vortex.management.Constants;
import fr.edu.vortex.management.api.DashboardApi;
import fr.edu.vortex.management.flux.Flux;
import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.persistence.dao.FluxDAO;
import fr.edu.vortex.management.persistence.dao.impl.FluxDAOImpl;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class RepartitionHandler implements Handler<Message<Object>> {

	private static final String MESSAGE_LIST_TRACES = "liste de traces récupérée";

	private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_TECH";

	private static final String DEMANDE_ENTRANTE = "Demande de la répartition des messages";

	private static final String ERR_LISTER_TRACES = "une erreur est survenue lors du calcul de la répartition des messages";

	private static final Logger logger = LoggerFactory.getLogger(RepartitionHandler.class);

	Vertx vertx;

	FluxDAO fluxDAO;

	public static RepartitionHandler create(Vertx vertx) {
		return new RepartitionHandler(vertx);
	}

	private RepartitionHandler(Vertx vertx) {
		this.vertx = vertx;
		fluxDAO = new FluxDAOImpl();
		fluxDAO.init(vertx);
	}

	@Override
	public void handle(Message<Object> event) {
		logger.trace(LOG_MESSAGE, LOG_NO_CORRELATION_ID, DEMANDE_ENTRANTE);

		JsonObject json = (JsonObject) event.body();
		String partenaire = json.getString(DashboardApi.KEY_JSON_PARTENAIRE);
		String depot = json.getString(Flux.KEY_DB_EXCHANGE_NAME);
		String zoneReception = json.getString(Flux.KEY_DB_CONSUMER_QUEUE_NAME);
		String startDate = json.getString(FluxVerticle.KEY_JSON_START_DATE_FILTER);
		String endDate = json.getString(FluxVerticle.KEY_JSON_END_DATE_FILTER);

		Map<String, Object> filters = new HashMap<>();
		filters.put(DashboardApi.KEY_JSON_PARTENAIRE, partenaire);
		filters.put(Flux.KEY_DB_EXCHANGE_NAME, depot);
		filters.put(Flux.KEY_DB_CONSUMER_QUEUE_NAME, zoneReception);		
		filters.put(FluxVerticle.KEY_JSON_START_DATE_FILTER, startDate);
		filters.put(FluxVerticle.KEY_JSON_END_DATE_FILTER, endDate);

		fluxDAO.getRepartition(filters)
		.setHandler(ar -> {
			if (ar.succeeded()) {
				List<JsonObject> result = ar.result();

				logger.trace(
						Constants.LOG_MESSAGE_WITH_DATA,
						LOG_NO_CORRELATION_ID,
						MESSAGE_LIST_TRACES,
						result);

				JsonArray response = new JsonArray();
				for (Iterator<JsonObject> iterator = result.iterator(); iterator.hasNext();) {
					JsonObject jsonObject = iterator.next();
					response.add(jsonObject);
				}

				event.reply(response);
			} else {
				logger.error(Constants.LOG_ERROR,
						LOG_NO_CORRELATION_ID,
						ERR_LISTER_TRACES,
						ERR_MANAGEMENT_TECH,
						ar.cause());
				event.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERR_LISTER_TRACES);
			}
		});

	}

}
