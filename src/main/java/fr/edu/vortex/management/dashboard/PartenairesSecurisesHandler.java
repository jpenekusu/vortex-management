//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.dashboard;

import static fr.edu.vortex.management.Constants.LOG_MESSAGE;
import static fr.edu.vortex.management.Constants.LOG_NO_CORRELATION_ID;

import java.net.HttpURLConnection;

import fr.edu.echanges.nat.vortex.common.adresses.client.ClientAdresses;
import fr.edu.vortex.management.Constants;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class PartenairesSecurisesHandler implements Handler<Message<Object>> {


	private static final String ERR_MANAGEMENT_TECH = "ERR_MANAGEMENT_TECH";

	private static final String DEMANDE_ENTRANTE = "Demande de récupération des partenaires sécurisés";

	private static final String ERR_LISTER_PARTENAIRES_SECURISES = "une erreur est survenue lors de la récupération des partenaires sécurisés";

	private static final Logger logger = LoggerFactory.getLogger(PartenairesSecurisesHandler.class);

	Vertx vertx;

	public static PartenairesSecurisesHandler create(Vertx vertx) {
		return new PartenairesSecurisesHandler(vertx);
	}

	private PartenairesSecurisesHandler(Vertx vertx) {
		this.vertx = vertx;
	}

	@Override
	public void handle(Message<Object> event) {
		logger.trace(LOG_MESSAGE, LOG_NO_CORRELATION_ID, DEMANDE_ENTRANTE);

		JsonObject filters = null;
		vertx.eventBus().send(ClientAdresses.LIST__CLIENTS, filters, ar -> {
			if (ar.succeeded()) {
				try {
					Message<Object> result = ar.result();
					
					JsonArray response = new JsonArray();
					
					JsonArray array = (JsonArray) result.body();
					for (int i = 0; i < array.size(); i++) {
					    JsonObject jsonClient = array.getJsonObject(i);
					    if (jsonClient.getBoolean("securise")) {
					    	response.add(jsonClient);
					    }
					}
					event.reply(response);
				} catch (Exception e) {
					event.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERR_LISTER_PARTENAIRES_SECURISES);
				}
			} else {
				try {
					logger.error(Constants.LOG_ERROR,
							LOG_NO_CORRELATION_ID,
							ERR_LISTER_PARTENAIRES_SECURISES,
							ERR_MANAGEMENT_TECH,
							ar.cause());
					event.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERR_LISTER_PARTENAIRES_SECURISES);
				} catch (Exception e) {
					logger.error(Constants.LOG_ERROR,
							LOG_NO_CORRELATION_ID,
							ERR_LISTER_PARTENAIRES_SECURISES,
							ERR_MANAGEMENT_TECH,
							ar.cause());
					event.fail(HttpURLConnection.HTTP_INTERNAL_ERROR, ERR_LISTER_PARTENAIRES_SECURISES);
				}
			}

		});


	}

}
