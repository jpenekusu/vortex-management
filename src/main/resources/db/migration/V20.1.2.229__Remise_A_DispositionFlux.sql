set search_path to 'management';


-- trigger utilisé lors de la suppression d''un flux
CREATE OR REPLACE FUNCTION update_flux_unique() RETURNS TRIGGER AS 
'
-- CURSEUR UTILISE POUR SAVOIR S IL EXISTE ENCORE UN FLUX DANS LA TABLE FLUX
DECLARE flux_already_exist_cursor REFCURSOR;  
DECLARE flux_already_exist_record RECORD;  
DECLARE flux_already_exist_query VARCHAR = ''SELECT * from management.flux where correlation_id = $1''; 

-- CURSEUR UTILISE POUR RECUPERER LE FLUX AVEC LE PLUS GRAND STATUS_ORDER DANS LA TABLE FLUX AVEC UN CONSOMMATEUR RENSEIGNE    
DECLARE get_max_flux_with_consumer_cursor REFCURSOR;  
DECLARE get_max_flux_with_consumer_record RECORD;
DECLARE get_max_flux_with_consumer_query VARCHAR = ''
	SELECT * FROM management.flux 
	WHERE correlation_id=$1 
	AND consumer_correlation_id = $2 
	AND status_order =  (
                        SELECT max(status_order) 
                        FROM management.flux
                        WHERE correlation_id=$1
                        AND consumer_correlation_id = $2  
                        )'';
                        
-- CURSEUR UTILISE POUR RECUPERER LE FLUX AVEC LE PLUS GRAND STATUS_ORDER DANS LA TABLE FLUX SANS CONSOMMATEUR RENSEIGNE        
DECLARE get_max_flux_without_consumer_cursor REFCURSOR;  
DECLARE get_max_flux_without_consumer_record RECORD;
DECLARE get_max_flux_without_consumer_query VARCHAR = ''
	SELECT * FROM management.flux 
	WHERE correlation_id=$1 
	AND consumer_correlation_id is null 
	AND status_order =  (
                        SELECT max(status_order) 
                        FROM management.flux
                        WHERE correlation_id=$1
                        AND consumer_correlation_id is null
                        )
    AND NOT EXISTS (
    				SELECT * FROM management.flux 
    				WHERE correlation_id=$1 
    				AND flux.consumer_correlation_id is not null
    				)'';                        
                        
DECLARE trouve BOOLEAN = false;             

BEGIN  
        -- DANS TOUS LES CAS ON SUPPRIME L ENREGISTREMENT DANS LA TABLE FLUX_UNIQUE QUI A LE MEME STATUT QUE CELUI SUPPRIME DANS LA TABLE FLUX (S IL EXISTE)
        DELETE FROM management.flux_unique where correlation_id=old.correlation_id and (consumer_correlation_id = old.consumer_correlation_id or consumer_correlation_id is null) and status_order=old.status_order;
        
        -- SI UN FLUX UNIQUE A ETE SUPPRIME, ON REGARDE S IL FAUT EN AJOUTER UN AUTRE A LA PLACE
        IF FOUND THEN
              	        
			-- ON VERIFIE QU IL EXISTE ENCORE AU MOINS UN ENREGISTREMENT DANS LA TABLE FLUX avec le même correlation_id 
			OPEN flux_already_exist_cursor FOR EXECUTE flux_already_exist_query USING new.correlation_id;
			FETCH flux_already_exist_cursor into flux_already_exist_record ;
			
			-- S IL Y A ENCORE AU MOINS UN ENREGISTREMENT DANS LA TABLE FLUX
			IF FOUND THEN
				-- ON RECUPERE LE FLUX POSSEDANT LES MEME correlation_id et consumer_correlation_id AVEC LE PLUS GRAND STATUT ET ON L INSERE DANS LA TABLE FLUX_UNIQUE
				OPEN get_max_flux_with_consumer_cursor FOR EXECUTE get_max_flux_with_consumer_query USING new.correlation_id, new.consumer_correlation_id;
					LOOP
							FETCH get_max_flux_with_consumer_cursor into get_max_flux_with_consumer_record;
							EXIT WHEN NOT FOUND;
							trouve = true;
							INSERT INTO management.flux_unique
								(
									correlation_id, 
									status, 
									status_order, 
									date, 
									producer, 
									exchange_name, 
									routing_key,
									consumer_correlation_id, 
									consumer_business_id, 
									consumer_queue_name, 
									file_name, 
									file_size, 
									file_url, 
									error_message, 
									stacktrace, 
									error_code
								)
								VALUES
								(
									get_max_flux_with_consumer_record.correlation_id, 
									get_max_flux_with_consumer_record.status, 
									get_max_flux_with_consumer_record.status_order, 
									get_max_flux_with_consumer_record.date, 
									get_max_flux_with_consumer_record.producer, 
									get_max_flux_with_consumer_record.exchange_name, 
									get_max_flux_with_consumer_record.routing_key,
									get_max_flux_with_consumer_record.consumer_correlation_id, 
									get_max_flux_with_consumer_record.consumer_business_id, 
									get_max_flux_with_consumer_record.consumer_queue_name, 
									get_max_flux_with_consumer_record.file_name, 
									get_max_flux_with_consumer_record.file_size, 
									get_max_flux_with_consumer_record.file_url, 
									get_max_flux_with_consumer_record.error_message, 
									get_max_flux_with_consumer_record.stacktrace, 
									get_max_flux_with_consumer_record.error_code
								);
					END LOOP;
				CLOSE get_max_flux_with_consumer_cursor;
				
				--  SI IL N Y A PAS DE FLUX AVEC LES MEME correlation_id et consumer_correlation_id
				IF NOT TROUVE THEN	
					-- ON RECUPERE LE FLUX AVEC LE MEME correlation_id ET SANS consumer_correlation_id AVEC LE PLUS GRAND STATUT ET ON L INSERE DANS LA TABLE FLUX_UNIQUE
					-- (S IL N EXISTE PAS UN FLUX AVEC LE MEME correlation_id ET AVEC UN AUTRE consumer_correlation_id)
					OPEN get_max_flux_without_consumer_cursor FOR EXECUTE get_max_flux_without_consumer_query USING new.correlation_id;
						LOOP
								FETCH get_max_flux_without_consumer_cursor into get_max_flux_without_consumer_record;
								EXIT WHEN NOT FOUND;
								trouve = true;
								INSERT INTO management.flux_unique
									(
										correlation_id, 
										status, 
										status_order, 
										date, 
										producer, 
										exchange_name, 
										routing_key,
										consumer_correlation_id, 
										consumer_business_id, 
										consumer_queue_name, 
										file_name, 
										file_size, 
										file_url, 
										error_message, 
										stacktrace, 
										error_code
									)
									VALUES
									(
										get_max_flux_without_consumer_record.correlation_id, 
										get_max_flux_without_consumer_record.status, 
										get_max_flux_without_consumer_record.status_order, 
										get_max_flux_without_consumer_record.date, 
										get_max_flux_without_consumer_record.producer, 
										get_max_flux_without_consumer_record.exchange_name, 
										get_max_flux_without_consumer_record.routing_key,
										get_max_flux_without_consumer_record.consumer_correlation_id, 
										get_max_flux_without_consumer_record.consumer_business_id, 
										get_max_flux_without_consumer_record.consumer_queue_name, 
										get_max_flux_without_consumer_record.file_name, 
										get_max_flux_without_consumer_record.file_size, 
										get_max_flux_without_consumer_record.file_url, 
										get_max_flux_without_consumer_record.error_message, 
										get_max_flux_without_consumer_record.stacktrace, 
										get_max_flux_without_consumer_record.error_code
									);
						END LOOP;		
					CLOSE get_max_flux_without_consumer_cursor;
				END IF; 
				CLOSE flux_already_exist_cursor;			
        	END IF;
		END IF;
		
        RETURN NEW;         
END;'
 LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_update_flux_unique ON management.flux;
CREATE TRIGGER trg_update_flux_unique AFTER UPDATE ON management.flux
FOR EACH ROW EXECUTE procedure update_flux_unique();