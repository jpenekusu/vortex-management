set search_path to 'management';

INSERT INTO permission (code, libelle, scope, ressource, contexte, action) VALUES ('EC_PARAM05', 'Accès à l''import-export', 'cassis', 'parametrage-import-export', '*', 'visualisation');

INSERT INTO role_permission (SELECT 1, code FROM permission WHERE  code = 'EC_PARAM05');
INSERT INTO role_permission (SELECT 3, code FROM permission WHERE  code = 'EC_PARAM05');
