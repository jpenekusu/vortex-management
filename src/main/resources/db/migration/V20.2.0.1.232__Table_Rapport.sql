set search_path to 'management';


CREATE TABLE IF NOT EXISTS rapport
(
        id BIGSERIAL NOT NULL,	
		date_rapport TIMESTAMP WITH TIME ZONE NOT NULL,
		login VARCHAR(250) NOT NULL,
		nom VARCHAR(250) NOT NULL,
		prenom VARCHAR(250) NOT NULL,
		type_rapport VARCHAR(250) NOT NULL,
		contenu JSON NOT NULL,    
		CONSTRAINT pk_rapport PRIMARY KEY(id)
);

INSERT INTO permission (code, libelle, scope, ressource, contexte) VALUES ('EC_AUDIT02', 'Audit des rapports', 'cassis', 'audit-rapports', '*');

INSERT INTO role_permission (role_id, permission_code, actions) values (1, 'EC_AUDIT02', '*');
INSERT INTO role_permission (role_id, permission_code, actions) values (3, 'EC_AUDIT02', '*');