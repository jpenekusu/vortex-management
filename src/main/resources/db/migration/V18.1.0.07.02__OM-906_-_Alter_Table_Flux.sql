set search_path to 'management';
ALTER TABLE
    flux ALTER COLUMN exchange_name TYPE CHARACTER VARYING(36);
ALTER TABLE
    flux ALTER COLUMN consumer_business_id TYPE CHARACTER VARYING(36);
ALTER TABLE
    flux ALTER COLUMN consumer_queue_name TYPE CHARACTER VARYING(36);
ALTER TABLE
    flux ALTER COLUMN correlation_id TYPE CHARACTER VARYING(110);
ALTER TABLE
    flux ALTER COLUMN consumer_correlation_id TYPE CHARACTER VARYING(110);