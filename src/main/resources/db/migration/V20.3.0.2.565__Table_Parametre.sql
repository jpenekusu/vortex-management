set search_path to 'management';

CREATE TABLE IF NOT EXISTS parametre
(
		code VARCHAR(50) NOT NULL,
		valeur VARCHAR(50) NOT NULL,
		CONSTRAINT pk_parametre PRIMARY KEY(code)
);

INSERT INTO parametre (code, valeur) values ('AUTO_ENREGISTREMENT', '1');