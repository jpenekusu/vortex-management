set search_path to 'management';

DROP INDEX idx_flux_consumer_queue_name;

ALTER TABLE flux ADD COLUMN error_code CHARACTER VARYING(50);