set search_path to 'management';



CREATE TABLE IF NOT EXISTS trace
(
        id BIGSERIAL NOT NULL,	
		date_trace TIMESTAMP WITH TIME ZONE NOT NULL,
		login VARCHAR(250) NOT NULL,
		nom VARCHAR(250) NULL,
        prenom VARCHAR(250) NULL,
        origine VARCHAR(250) NOT NULL,
		event VARCHAR(250) NOT NULL,
		id_element VARCHAR(110) NOT NULL,
		type_element VARCHAR(110) NOT NULL,
		contenu JSON NOT NULL,    
		CONSTRAINT pk_trace PRIMARY KEY(id)
);
