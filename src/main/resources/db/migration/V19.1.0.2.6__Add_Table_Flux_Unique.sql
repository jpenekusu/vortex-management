set search_path to 'management';

CREATE TABLE IF NOT EXISTS flux_unique (
        id BIGSERIAL NOT NULL,
        correlation_id VARCHAR(110) NOT NULL,
        status VARCHAR(50) NOT NULL,
        status_order SMALLINT NOT NULL,
        date TIMESTAMP WITH TIME ZONE NOT NULL,
        producer VARCHAR(30) NOT NULL,
        exchange_name VARCHAR(36),
        routing_key VARCHAR(50),
        consumer_correlation_id VARCHAR(110),
        consumer_business_id VARCHAR(36),
        consumer_queue_name VARCHAR(36),
        file_name VARCHAR(1000),
        file_size BIGINT,
        file_url VARCHAR(1000),
        error_message VARCHAR(1000),
        stacktrace TEXT,
        error_code CHARACTER VARYING(50),        
        CONSTRAINT pk_flux_unique PRIMARY KEY(id)
);

CREATE INDEX idx_flux_unique_correlation_id ON flux_unique (correlation_id);
CREATE INDEX idx_flux_unique_consumer_correlation_id ON flux_unique (consumer_correlation_id);

-- trigger utilisé lors de l''insertion d''un nouveau flux
CREATE OR REPLACE FUNCTION insert_flux_unique() RETURNS TRIGGER AS 
'
-- CURSEUR UTILISE POUR SAVOIR S IL EXISTE ENCORE UN FLUX DANS LA TABLE FLUX_UNIQUE
DECLARE flux_already_exist_cursor CURSOR FOR Select * FROM flux_unique WHERE correlation_id=new.correlation_id;  
DECLARE flux_already_exist_record RECORD;  

-- CURSEUR UTILISE POUR SAVOIR S IL FAUT SUPPRIMER UN FLUX DANS LA TABLE UNIQUE
DECLARE flux_to_delete_cursor CURSOR FOR Select * FROM flux_unique WHERE correlation_id=new.correlation_id and status_order<new.status_order;  
DECLARE flux_to_delete_record RECORD; 


BEGIN          
        -- ON VERIFIE QU IL EXISTE UN ENREGISTREMENT DANS LA TABLE FLUX_UNIQUE
        OPEN flux_already_exist_cursor;
        FETCH flux_already_exist_cursor INTO flux_already_exist_record;

        -- S IL N Y A PLUS D ENREGISTREMENT DANS LA TABLE FLUX UNIQUE, ON INSERT LE MEME QUE CELUI INSERE DANS LA TABLE FLUX      
        IF NOT FOUND THEN              
            INSERT INTO flux_unique
                (
                    correlation_id, 
                    status, 
                    status_order, 
                    date, 
                    producer, 
                    exchange_name, 
                    routing_key,
                    consumer_correlation_id, 
                    consumer_business_id, 
                    consumer_queue_name, 
                    file_name, 
                    file_size, 
                    file_url, 
                    error_message, 
                    stacktrace, 
                    error_code
                )
                VALUES
                (
                    new.correlation_id, 
                    new.status, 
                    new.status_order, 
                    new.date, 
                    new.producer, 
                    new.exchange_name, 
                    new.routing_key,
                    new.consumer_correlation_id, 
                    new.consumer_business_id, 
                    new.consumer_queue_name, 
                    new.file_name, 
                    new.file_size, 
                    new.file_url, 
                    new.error_message, 
                    new.stacktrace, 
                    new.error_code
                ); 
        -- SI IL Y A UN ENREGISTREMENT DANS LA TABLE FLUX_UNIQUE
        ELSE
            
            -- ON REGARDE S IL FAUT LE SUPPRIMER 
            OPEN flux_to_delete_cursor;
            FETCH flux_to_delete_cursor INTO flux_to_delete_record;
                        
            -- SI LE NOUVEAU STATUT_ORDER EST PLUS GRAND QUE CELUI PRESENT DANS FLUX_UNIQUE ON MET A JOUR L ENREGISTREMENT PRESENT DANS FLUX_UNIQUE AVEC LES DONNEES DE CELUI INSERE DANS LA TABLE FLUX
            IF FOUND THEN     
                                    
                UPDATE flux_unique SET                    
                        correlation_id = new.correlation_id, 
                        status = new.status, 
                        status_order = new.status_order,
                        date = new.date, 
                        producer = new.producer,  
                        exchange_name = new.exchange_name, 
                        routing_key = new.routing_key,
                        consumer_correlation_id = new.consumer_correlation_id,  
                        consumer_business_id = new.consumer_business_id,  
                        consumer_queue_name = new.consumer_queue_name, 
                        file_name = new.file_name,  
                        file_size = new.file_size,  
                        file_url = new.file_url,  
                        error_message = new.error_message,  
                        stacktrace = new.stacktrace, 
                        error_code = new.error_code                        
                    WHERE correlation_id=new.correlation_id and status_order<new.status_order;   
                                    
            END IF;
            CLOSE flux_to_delete_cursor; 
        END IF;
        CLOSE flux_already_exist_cursor ;
        RETURN NEW;         
END;'
 LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_insert_flux_unique ON flux;
CREATE TRIGGER trg_insert_flux_unique AFTER INSERT ON flux
FOR EACH ROW EXECUTE procedure insert_flux_unique();


-- trigger utilisé lors de la suppression d''un flux
CREATE OR REPLACE FUNCTION delete_flux_unique() RETURNS TRIGGER AS 
'
-- CURSEUR UTILISE POUR SAVOIR S IL EXISTE ENCORE UN FLUX DANS LA TABLE FLUX
DECLARE flux_already_exist_cursor REFCURSOR;  
DECLARE flux_already_exist_record RECORD;  
DECLARE flux_already_exist_query VARCHAR = ''SELECT * from flux where correlation_id = $1 ''; 

-- CURSEUR UTILISE POUR SAVOIR S IL EXISTE ENCORE UN FLUX DANS LA TABLE FLUX_UNIQUE
DECLARE flux_unique_already_exist_cursor REFCURSOR;  
DECLARE flux_unique_already_exist_record RECORD;  
DECLARE flux_unique_already_exist_query VARCHAR = ''SELECT * from flux_unique where correlation_id = $1 ''; 

-- CURSEUR UTILISE POUR RECUPERER LE FLUX AVEC LE PLUS GRAND STATUS_ORDER DANS LA TABLE FLUX     
DECLARE get_max_flux_cursor REFCURSOR;  
DECLARE get_max_flux_record RECORD;
DECLARE get_max_flux_query VARCHAR = ''select * from flux where correlation_id=$1 AND status_order =  (
                        select max(status_order) 
                        from flux
                        where correlation_id=$1
                        )'';            

BEGIN  
        -- DANS TOUS LES CAS ON SUPPRIME L ENREGISTREMENT DANS LA TABLE FLUX_UNIQUE QUI A LE MEME STATUT QUE CELUI SUPPRIME DANS LA TABLE FLUX (S IL EXISTE)
        DELETE FROM flux_unique where correlation_id=old.correlation_id and status_order=old.status_order;
        
        -- ON VERIFIE QU IL EXISTE ENCORE UN ENREGISTREMENT DANS LA TABLE FLUX_UNIQUE
        OPEN flux_unique_already_exist_cursor FOR EXECUTE flux_unique_already_exist_query USING old.correlation_id;
        FETCH flux_unique_already_exist_cursor into flux_unique_already_exist_record ;
        
        -- S IL N Y A PLUS D ENREGISTREMENT DANS LA TABLE FLUX_UNIQUE
        IF NOT FOUND THEN
        
            -- ON VERIFIE QU IL EXISTE ENCORE AU MOINS UN ENREGISTREMENT DANS LA TABLE FLUX
            OPEN flux_already_exist_cursor FOR EXECUTE flux_already_exist_query USING old.correlation_id;
            FETCH flux_already_exist_cursor into flux_already_exist_record ;
            
            -- S IL Y A ENCORE AU MOINS UN ENREGISTREMENT DANS LA TABLE FLUX, ON RECUPERE LE FLUX AVEC LE PLUS GRAND STATUT ET ON L INSERE DANS LA TABLE FLUX_UNIQUE
            IF FOUND THEN
    
                OPEN get_max_flux_cursor FOR EXECUTE get_max_flux_query USING old.correlation_id;
                FETCH get_max_flux_cursor into get_max_flux_record ;
                INSERT INTO flux_unique
                    (
                        correlation_id, 
                        status, 
                        status_order, 
                        date, 
                        producer, 
                        exchange_name, 
                        routing_key,
                        consumer_correlation_id, 
                        consumer_business_id, 
                        consumer_queue_name, 
                        file_name, 
                        file_size, 
                        file_url, 
                        error_message, 
                        stacktrace, 
                        error_code
                    )
                    VALUES
                    (
                        get_max_flux_record.correlation_id, 
                        get_max_flux_record.status, 
                        get_max_flux_record.status_order, 
                        get_max_flux_record.date, 
                        get_max_flux_record.producer, 
                        get_max_flux_record.exchange_name, 
                        get_max_flux_record.routing_key,
                        get_max_flux_record.consumer_correlation_id, 
                        get_max_flux_record.consumer_business_id, 
                        get_max_flux_record.consumer_queue_name, 
                        get_max_flux_record.file_name, 
                        get_max_flux_record.file_size, 
                        get_max_flux_record.file_url, 
                        get_max_flux_record.error_message, 
                        get_max_flux_record.stacktrace, 
                        get_max_flux_record.error_code
                    );
                CLOSE get_max_flux_cursor;
            END IF;
            CLOSE flux_already_exist_cursor;
        END IF; 
        CLOSE flux_unique_already_exist_cursor;                    
        RETURN NEW;         
END;'
 LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_flux_unique ON flux;
CREATE TRIGGER trg_delete_flux_unique AFTER DELETE ON flux
FOR EACH ROW EXECUTE procedure delete_flux_unique();