set search_path to 'management';

--DROP TABLE IF EXISTS flux;

CREATE TABLE IF NOT EXISTS flux (
        id BIGSERIAL NOT NULL,
        correlation_id VARCHAR(100) NOT NULL,
        status VARCHAR(50) NOT NULL,
        status_order SMALLINT NOT NULL,
        date TIMESTAMP WITH TIME ZONE NOT NULL,
        producer VARCHAR(30) NOT NULL,
        exchange_name VARCHAR(250),
        routing_key VARCHAR(50),
        consumer_correlation_id VARCHAR(100),
        consumer_business_id VARCHAR(30),
        consumer_queue_name VARCHAR(250),
        file_name VARCHAR(1000),
        file_size BIGINT,
        file_url VARCHAR(1000),
        error_message VARCHAR(1000),
        stacktrace TEXT,
        CONSTRAINT pk_flux PRIMARY KEY(id)
);

CREATE INDEX idx_flux_correlation_id ON flux (correlation_id);
CREATE INDEX idx_flux_consumer_correlation_id ON flux (consumer_correlation_id);
CREATE INDEX idx_flux_consumer_queue_name ON flux (consumer_queue_name);
    