set search_path to 'management';

INSERT INTO permission (code, libelle, scope, ressource, contexte) VALUES ('EC_ADMIN03', 'Administration des modèles de mail', 'cassis', 'administration-modeles-mail', '*');

INSERT INTO role_permission (role_id, permission_code, actions) VALUES (1, 'EC_ADMIN03', '*');