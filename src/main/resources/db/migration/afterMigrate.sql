-- Script sql de grant qui sera passé à chaque commande migrate de flyway

-- Droit de type T (Pole BDD Toulouse)
GRANT USAGE ON SCHEMA management TO tmanag;
GRANT SELECT, INSERT, UPDATE, DELETE, TRIGGER ON ALL TABLES IN SCHEMA management TO tmanag;
GRANT SELECT, UPDATE, USAGE ON ALL SEQUENCES IN SCHEMA management TO tmanag;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA management TO tmanag;


-- Droit de type C (Pole BDD Toulouse)
GRANT USAGE ON SCHEMA management TO cmanag;
GRANT SELECT ON ALL TABLES IN SCHEMA management TO cmanag;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA management TO cmanag;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA management TO cmanag;

GRANT USAGE ON SCHEMA management TO dmanag;
GRANT SELECT ON ALL TABLES IN SCHEMA management TO dmanag;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA management TO dmanag;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA management TO dmanag;

GRANT USAGE ON SCHEMA management TO racpg;
GRANT SELECT ON ALL TABLES IN SCHEMA management TO racpg;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA management TO racpg;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA management TO racpg;
