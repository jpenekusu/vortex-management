set search_path to 'management';

CREATE TABLE IF NOT EXISTS histo_flux
(
        id BIGSERIAL NOT NULL,	
		date TIMESTAMP WITH TIME ZONE NOT NULL,
		correlation_id VARCHAR(110) NOT NULL,
		consumer_correlation_id VARCHAR(110) NULL,
		consumer_queue_name VARCHAR(36) NULL,
        status VARCHAR(50) NOT NULL,
        status_order SMALLINT NOT NULL,		
		action VARCHAR(100) NOT NULL,    
		CONSTRAINT pk_histo_flux PRIMARY KEY(id)
);

UPDATE trace SET event ='PURGE' where event in ('MISE_EN_OBSOLESCENCE', 'PURGE_MESSAGES');