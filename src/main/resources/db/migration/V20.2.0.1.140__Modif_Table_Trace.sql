set search_path to 'management';

ALTER TABLE trace ADD COLUMN id_entite VARCHAR(110);

ALTER TABLE trace ALTER COLUMN id_element DROP NOT NULL;
ALTER TABLE trace ALTER COLUMN contenu DROP NOT NULL;
ALTER TABLE trace ALTER COLUMN id_entite DROP NOT NULL;
