set search_path to 'management';

INSERT INTO permission (code, libelle, scope, ressource, contexte) VALUES ('WIDGET_ETATS_MESSAGES', 'Widget états des messages', 'cassis', 'widget-etats-messages', '*');

INSERT INTO role_permission (role_id, permission_code, actions) values (1, 'WIDGET_ETATS_MESSAGES', '*');
INSERT INTO role_permission (role_id, permission_code, actions) values (3, 'WIDGET_ETATS_MESSAGES', '*');
