set search_path to 'management';

CREATE TABLE IF NOT EXISTS filtre
(
        id BIGSERIAL NOT NULL,	
        utilisateur_id BIGSERIAL NOT NULL,	
		nom VARCHAR(100) NOT NULL,
		description VARCHAR(250),
		type_filtre VARCHAR(250) NOT NULL,
		contenu JSON NOT NULL,    
		CONSTRAINT pk_filtre PRIMARY KEY(id)
);