set search_path to 'management';

CREATE TABLE IF NOT EXISTS notification (
        id BIGSERIAL NOT NULL,
        correlation_id VARCHAR(110) NOT NULL,
        type VARCHAR(100) NOT NULL,
        etat SMALLINT NOT NULL default 0,
        etat_emission SMALLINT NOT NULL default 0,
        CONSTRAINT pk_notification PRIMARY KEY(id),
        CONSTRAINT unique_notification UNIQUE(correlation_id, type)
);

CREATE INDEX idx_notification_correlation_id ON flux (correlation_id);
