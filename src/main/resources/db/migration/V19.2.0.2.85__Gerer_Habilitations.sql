set search_path to 'management';

ALTER TABLE permission DROP COLUMN action;

ALTER TABLE role_permission ADD COLUMN actions VARCHAR(5);

UPDATE permission SET libelle ='Supervision des messages' WHERE code = 'EC_SUP01';
UPDATE permission SET libelle ='Supervision de la passerelle' WHERE code = 'EC_SUP02';
UPDATE permission SET libelle ='Supervision des zones de réception' WHERE code = 'EC_SUP03';
UPDATE permission SET libelle ='Supervision des messages d''une zone de réception' WHERE code = 'EC_SUP04';
UPDATE permission SET libelle ='Paramétrage des partenaires' WHERE code = 'EC_PARAM01';
UPDATE permission SET libelle ='Paramétrage des routes' WHERE code = 'EC_PARAM02';
UPDATE permission SET libelle ='Paramétrage des zones de réception' WHERE code = 'EC_PARAM03';
UPDATE permission SET libelle ='Paramétrage des dépôts' WHERE code = 'EC_PARAM04';
UPDATE permission SET libelle ='Monitoring des microservices' WHERE code = 'EC_MONI01';
UPDATE permission SET libelle ='Import-export de configuration' WHERE code = 'EC_PARAM05';
UPDATE permission SET libelle ='Administration des utilisateurs' WHERE code = 'EC_ADMIN01';
UPDATE permission SET libelle ='Administration des rôles' WHERE code = 'EC_ADMIN02';

UPDATE role_permission SET actions ='*';