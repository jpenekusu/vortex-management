set search_path to 'management';

CREATE TABLE IF NOT EXISTS modele_mail
(
        id BIGSERIAL NOT NULL,	
		nom VARCHAR(100) NOT NULL,
		email VARCHAR(250) NOT NULL,
		destinataire VARCHAR(50) NOT NULL,
		contenu JSON NOT NULL,    
        systeme BOOLEAN NOT NULL default false,
		CONSTRAINT pk_modele_mail PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS modele_mail_permission
(
        modele_mail_id BIGSERIAL NOT NULL,	
		permission_code VARCHAR(50) NOT NULL,
		CONSTRAINT pk_modele_mail_permission PRIMARY KEY (modele_mail_id, permission_code),
		CONSTRAINT fk_permission_modele_mail_id FOREIGN KEY (modele_mail_id) REFERENCES modele_mail (id),
        CONSTRAINT fk_modele_mail_permission_code FOREIGN KEY (permission_code) REFERENCES permission (code)
);