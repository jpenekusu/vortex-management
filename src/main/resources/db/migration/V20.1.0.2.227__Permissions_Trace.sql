set search_path to 'management';

INSERT INTO permission (code, libelle, scope, ressource, contexte) VALUES ('EC_AUDIT01', 'Audit des actions utilisateurs', 'cassis', 'audit-actions', '*');

INSERT INTO role_permission (role_id, permission_code, actions) values (1, 'EC_AUDIT01', '*');
INSERT INTO role_permission (role_id, permission_code, actions) values (3, 'EC_AUDIT01', '*');