set search_path to 'management';

-- trigger utilisé lors de l''insertion d''un nouveau flux
CREATE OR REPLACE FUNCTION insert_flux_unique() RETURNS TRIGGER AS 
'
-- CURSEUR UTILISE POUR SAVOIR S IL EXISTE UN FLUX DANS LA TABLE FLUX_UNIQUE
DECLARE correlation_id_already_exist_cursor CURSOR FOR Select * FROM management.flux_unique WHERE correlation_id=new.correlation_id;  
DECLARE consumer_correlation_id_already_exist_cursor CURSOR FOR Select * FROM management.flux_unique WHERE correlation_id=new.correlation_id and consumer_correlation_id = new.consumer_correlation_id;  
DECLARE flux_already_exist_record RECORD;  

BEGIN         
 
        -- ON VERIFIE SI LE NOUVEAU FLUX A UN CONSUMER_CORRELATION_ID ET QU IL EXISTE UN ENREGISTREMENT DANS LA TABLE FLUX_UNIQUE
        -- 2 CAS : 
        -- 		1 CAS POUR LES FLUX SANS CONSUMER_CORRELATION_ID (JUSQU AU STATUT ROUTAGE)
        IF new.consumer_correlation_id IS NULL THEN
                OPEN correlation_id_already_exist_cursor;
                FETCH correlation_id_already_exist_cursor INTO flux_already_exist_record;
                CLOSE correlation_id_already_exist_cursor;

        -- 		1 CAS POUR LES FLUX AVEC CONSUMER_CORRELATION_ID (A PARTIR DU STATUT A_ENVOYER)                
        ELSE
                -- SI LE NOUVEAU FLUX A SAUVEGARDER A UN CONSUMER_CORRELATION_ID, ON PEUT SUPPRIMER LE FLUX LIE QUI N EN POSSEDENT PAS
                DELETE FROM management.flux_unique where correlation_id=new.correlation_id and consumer_correlation_id is null and status_order<new.status_order; 
                
                OPEN consumer_correlation_id_already_exist_cursor;
                FETCH consumer_correlation_id_already_exist_cursor INTO flux_already_exist_record;
                CLOSE consumer_correlation_id_already_exist_cursor;               
                
        END IF;

        -- S IL N Y A PAS D ENREGISTREMENT DANS LA TABLE FLUX UNIQUE, ON INSERT LE MEME QUE CELUI INSERE DANS LA TABLE FLUX      
        IF NOT FOUND THEN              
                INSERT INTO management.flux_unique
                (
                    correlation_id, 
                    status, 
                    status_order, 
                    date, 
                    producer, 
                    exchange_name, 
                    routing_key,
                    consumer_correlation_id, 
                    consumer_business_id, 
                    consumer_queue_name, 
                    file_name, 
                    file_size, 
                    file_url, 
                    error_message, 
                    stacktrace, 
                    error_code
                )
                VALUES
                (
                    new.correlation_id, 
                    new.status, 
                    new.status_order, 
                    new.date, 
                    new.producer, 
                    new.exchange_name, 
                    new.routing_key,
                    new.consumer_correlation_id, 
                    new.consumer_business_id, 
                    new.consumer_queue_name, 
                    new.file_name, 
                    new.file_size, 
                    new.file_url, 
                    new.error_message, 
                    new.stacktrace, 
                    new.error_code
                ); 
                
        -- SI IL Y A UN ENREGISTREMENT DANS LA TABLE FLUX_UNIQUE
        ELSE
            
            -- SI LE NOUVEAU STATUT_ORDER EST PLUS GRAND QUE CELUI PRESENT DANS FLUX_UNIQUE ON MET A JOUR L ENREGISTREMENT PRESENT DANS FLUX_UNIQUE AVEC LES DONNEES DE CELUI INSERE DANS LA TABLE FLUX
	        -- 2 CAS : 
	        -- 		1 CAS POUR LES FLUX SANS CONSUMER_CORRELATION_ID (JUSQU AU STATUT ROUTAGE)            
                IF new.consumer_correlation_id IS NULL THEN
                                    
                        UPDATE management.flux_unique SET                    
                                correlation_id = new.correlation_id, 
                                status = new.status, 
                                status_order = new.status_order,
                                date = new.date, 
                                producer = new.producer,  
                                exchange_name = new.exchange_name, 
                                routing_key = new.routing_key,
                                consumer_correlation_id = new.consumer_correlation_id,  
                                consumer_business_id = new.consumer_business_id,  
                                consumer_queue_name = new.consumer_queue_name, 
                                file_name = new.file_name,  
                                file_size = new.file_size,  
                                file_url = new.file_url,  
                                error_message = new.error_message,  
                                stacktrace = new.stacktrace, 
                                error_code = new.error_code                        
                            WHERE correlation_id=new.correlation_id and status_order<new.status_order;   
           
                
                ELSE
            -- 		1 CAS POUR LES FLUX AVEC CONSUMER_CORRELATION_ID (A PARTIR DU STATUT A_ENVOYER)      
                        UPDATE management.flux_unique SET                    
                                correlation_id = new.correlation_id, 
                                status = new.status, 
                                status_order = new.status_order,
                                date = new.date, 
                                producer = new.producer,  
                                exchange_name = new.exchange_name, 
                                routing_key = new.routing_key,
                                consumer_correlation_id = new.consumer_correlation_id,  
                                consumer_business_id = new.consumer_business_id,  
                                consumer_queue_name = new.consumer_queue_name, 
                                file_name = new.file_name,  
                                file_size = new.file_size,  
                                file_url = new.file_url,  
                                error_message = new.error_message,  
                                stacktrace = new.stacktrace, 
                                error_code = new.error_code                        
                            WHERE correlation_id=new.correlation_id and consumer_correlation_id = new.consumer_correlation_id and status_order<new.status_order;                   
                END IF;
        END IF;
        RETURN NEW;         
END;'
 LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_insert_flux_unique ON management.flux;
CREATE TRIGGER trg_insert_flux_unique AFTER INSERT ON management.flux
FOR EACH ROW EXECUTE procedure insert_flux_unique();



-- trigger utilisé lors de la suppression d''un flux
CREATE OR REPLACE FUNCTION delete_flux_unique() RETURNS TRIGGER AS 
'
-- CURSEUR UTILISE POUR SAVOIR S IL EXISTE ENCORE UN FLUX DANS LA TABLE FLUX
DECLARE flux_already_exist_cursor REFCURSOR;  
DECLARE flux_already_exist_record RECORD;  
DECLARE flux_already_exist_query VARCHAR = ''SELECT * from management.flux where correlation_id = $1''; 

-- CURSEUR UTILISE POUR RECUPERER LE FLUX AVEC LE PLUS GRAND STATUS_ORDER DANS LA TABLE FLUX AVEC UN CONSOMMATEUR RENSEIGNE    
DECLARE get_max_flux_with_consumer_cursor REFCURSOR;  
DECLARE get_max_flux_with_consumer_record RECORD;
DECLARE get_max_flux_with_consumer_query VARCHAR = ''
	SELECT * FROM management.flux 
	WHERE correlation_id=$1 
	AND consumer_correlation_id = $2 
	AND status_order =  (
                        SELECT max(status_order) 
                        FROM management.flux
                        WHERE correlation_id=$1
                        AND consumer_correlation_id = $2  
                        )'';
                        
-- CURSEUR UTILISE POUR RECUPERER LE FLUX AVEC LE PLUS GRAND STATUS_ORDER DANS LA TABLE FLUX SANS CONSOMMATEUR RENSEIGNE        
DECLARE get_max_flux_without_consumer_cursor REFCURSOR;  
DECLARE get_max_flux_without_consumer_record RECORD;
DECLARE get_max_flux_without_consumer_query VARCHAR = ''
	SELECT * FROM management.flux 
	WHERE correlation_id=$1 
	AND consumer_correlation_id is null 
	AND status_order =  (
                        SELECT max(status_order) 
                        FROM management.flux
                        WHERE correlation_id=$1
                        AND consumer_correlation_id is null
                        )
    AND NOT EXISTS (
    				SELECT * FROM management.flux 
    				WHERE correlation_id=$1 
    				AND flux.consumer_correlation_id is not null
    				)'';                        
                        
DECLARE trouve BOOLEAN = false;             

BEGIN  
        -- DANS TOUS LES CAS ON SUPPRIME L ENREGISTREMENT DANS LA TABLE FLUX_UNIQUE QUI A LE MEME STATUT QUE CELUI SUPPRIME DANS LA TABLE FLUX (S IL EXISTE)
        DELETE FROM management.flux_unique where correlation_id=old.correlation_id and (consumer_correlation_id = old.consumer_correlation_id or consumer_correlation_id is null) and status_order=old.status_order;
        
        -- SI UN FLUX UNIQUE A ETE SUPPRIME, ON REGARDE S IL FAUT EN AJOUTER UN AUTRE A LA PLACE
        IF FOUND THEN
              	        
			-- ON VERIFIE QU IL EXISTE ENCORE AU MOINS UN ENREGISTREMENT DANS LA TABLE FLUX avec le même correlation_id 
			OPEN flux_already_exist_cursor FOR EXECUTE flux_already_exist_query USING old.correlation_id;
			FETCH flux_already_exist_cursor into flux_already_exist_record ;
			
			-- S IL Y A ENCORE AU MOINS UN ENREGISTREMENT DANS LA TABLE FLUX
			IF FOUND THEN
				-- ON RECUPERE LE FLUX POSSEDANT LES MEME correlation_id et consumer_correlation_id AVEC LE PLUS GRAND STATUT ET ON L INSERE DANS LA TABLE FLUX_UNIQUE
				OPEN get_max_flux_with_consumer_cursor FOR EXECUTE get_max_flux_with_consumer_query USING old.correlation_id, old.consumer_correlation_id;
					LOOP
							FETCH get_max_flux_with_consumer_cursor into get_max_flux_with_consumer_record;
							EXIT WHEN NOT FOUND;
							trouve = true;
							INSERT INTO management.flux_unique
								(
									correlation_id, 
									status, 
									status_order, 
									date, 
									producer, 
									exchange_name, 
									routing_key,
									consumer_correlation_id, 
									consumer_business_id, 
									consumer_queue_name, 
									file_name, 
									file_size, 
									file_url, 
									error_message, 
									stacktrace, 
									error_code
								)
								VALUES
								(
									get_max_flux_with_consumer_record.correlation_id, 
									get_max_flux_with_consumer_record.status, 
									get_max_flux_with_consumer_record.status_order, 
									get_max_flux_with_consumer_record.date, 
									get_max_flux_with_consumer_record.producer, 
									get_max_flux_with_consumer_record.exchange_name, 
									get_max_flux_with_consumer_record.routing_key,
									get_max_flux_with_consumer_record.consumer_correlation_id, 
									get_max_flux_with_consumer_record.consumer_business_id, 
									get_max_flux_with_consumer_record.consumer_queue_name, 
									get_max_flux_with_consumer_record.file_name, 
									get_max_flux_with_consumer_record.file_size, 
									get_max_flux_with_consumer_record.file_url, 
									get_max_flux_with_consumer_record.error_message, 
									get_max_flux_with_consumer_record.stacktrace, 
									get_max_flux_with_consumer_record.error_code
								);
					END LOOP;
				CLOSE get_max_flux_with_consumer_cursor;
				
				--  SI IL N Y A PAS DE FLUX AVEC LES MEME correlation_id et consumer_correlation_id
				IF NOT TROUVE THEN	
					-- ON RECUPERE LE FLUX AVEC LE MEME correlation_id ET SANS consumer_correlation_id AVEC LE PLUS GRAND STATUT ET ON L INSERE DANS LA TABLE FLUX_UNIQUE
					-- (S IL N EXISTE PAS UN FLUX AVEC LE MEME correlation_id ET AVEC UN AUTRE consumer_correlation_id)
					OPEN get_max_flux_without_consumer_cursor FOR EXECUTE get_max_flux_without_consumer_query USING old.correlation_id;
						LOOP
								FETCH get_max_flux_without_consumer_cursor into get_max_flux_without_consumer_record;
								EXIT WHEN NOT FOUND;
								trouve = true;
								INSERT INTO management.flux_unique
									(
										correlation_id, 
										status, 
										status_order, 
										date, 
										producer, 
										exchange_name, 
										routing_key,
										consumer_correlation_id, 
										consumer_business_id, 
										consumer_queue_name, 
										file_name, 
										file_size, 
										file_url, 
										error_message, 
										stacktrace, 
										error_code
									)
									VALUES
									(
										get_max_flux_without_consumer_record.correlation_id, 
										get_max_flux_without_consumer_record.status, 
										get_max_flux_without_consumer_record.status_order, 
										get_max_flux_without_consumer_record.date, 
										get_max_flux_without_consumer_record.producer, 
										get_max_flux_without_consumer_record.exchange_name, 
										get_max_flux_without_consumer_record.routing_key,
										get_max_flux_without_consumer_record.consumer_correlation_id, 
										get_max_flux_without_consumer_record.consumer_business_id, 
										get_max_flux_without_consumer_record.consumer_queue_name, 
										get_max_flux_without_consumer_record.file_name, 
										get_max_flux_without_consumer_record.file_size, 
										get_max_flux_without_consumer_record.file_url, 
										get_max_flux_without_consumer_record.error_message, 
										get_max_flux_without_consumer_record.stacktrace, 
										get_max_flux_without_consumer_record.error_code
									);
						END LOOP;		
					CLOSE get_max_flux_without_consumer_cursor;
				END IF; 
				CLOSE flux_already_exist_cursor;			
        	END IF;
		END IF;
		
        RETURN NEW;         
END;'
 LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_flux_unique ON management.flux;
CREATE TRIGGER trg_delete_flux_unique AFTER DELETE ON management.flux
FOR EACH ROW EXECUTE procedure delete_flux_unique();