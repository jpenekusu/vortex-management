set search_path to 'management';

INSERT INTO permission (code, libelle, scope, ressource, contexte) VALUES ('WIDGET_EXPIRATIONS_CERTIFICATS', 'Widget expiration des certificats', 'cassis', 'widget-expirations-certificats', '*');

INSERT INTO role_permission (role_id, permission_code, actions) values (1, 'WIDGET_EXPIRATIONS_CERTIFICATS', '*');
INSERT INTO role_permission (role_id, permission_code, actions) values (3, 'WIDGET_EXPIRATIONS_CERTIFICATS', '*');



