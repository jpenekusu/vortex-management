//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.flux;

import java.io.IOException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.edu.vortex.management.dao.MockFluxDAO;
import fr.edu.vortex.management.persistence.dao.FluxDAO;
import io.vertx.core.Vertx;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
@Ignore
public class FluxVerticleTest {

    Vertx vertx;

    @Before
    public void setUp(TestContext context) throws IOException {
        vertx = Vertx.vertx();

        FluxVerticle verticle = new FluxVerticle();
        FluxDAO fluxDao = new MockFluxDAO();
        verticle.setFluxDAO(fluxDao);
        vertx.deployVerticle(verticle, context.asyncAssertSuccess());
    }

    @Test
    public void listFluxTest(TestContext ctx) {
        Async async = ctx.async();
        vertx.eventBus().send(FluxAdresses.LIST_FLUX, "0-1", result -> {
            ctx.assertTrue(result.succeeded());
            async.complete();
        });

    }

    @Test
    public void getFluxByCorrelationIdTest(TestContext ctx) {
        Async async = ctx.async();
        vertx.eventBus().send(FluxAdresses.GET_FLUX_BY_CORRELATION_ID, "123456", result -> {
            ctx.assertTrue(result.succeeded());
            async.complete();
        });
    }

}
