//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.dao;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import fr.edu.vortex.management.flux.Flux;
import fr.edu.vortex.management.flux.StatusEnum;
import fr.edu.vortex.management.flux.StatusHistory;
import fr.edu.vortex.management.persistence.dao.FluxDAO;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

/**
 * Une implementation NOP( No operation) du service de persistence.
 *
 */
public class MockFluxDAO implements FluxDAO {

    static Flux flux = new Flux("987654", StatusEnum.IMMATRICULE, Instant.now(), "ANA001");
    static Flux flux2 = new Flux("456789", StatusEnum.RECU, Instant.now(), "ANA001", "ANATOSEM", "SEM", null, null,
            null, "TATA.txt", new Long(1000), "/nfs.tata.txt");

    static List<Flux> listflux = Arrays.asList(flux, flux2);

    static List<StatusHistory> listStatusHistoryFlux = Arrays.asList(
            new StatusHistory(StatusEnum.IMMATRICULE, new Date()), new StatusHistory(StatusEnum.RECU, new Date()));

    public Future<Void> fut;

    @Override
    public void init(Vertx vertx) {
        // TODO Auto-generated method stub
    }

    @Override
    public Future<List<Flux>> loadFlux(int borneDebut, int borneFin, Map<String, Object> parameters, String sort,
            String sortOrder) {
        Future<List<Flux>> future = Future.future();
        for (Flux flux : listflux) {
            System.out.println(flux.toJsonObject().encodePrettily());
        }
        future.complete(listflux);
        return future;
    }

    @Override
    public Future<List<Flux>> getFluxByCorrelationId(String correlationId) {
        Future<List<Flux>> future = Future.future();

        List<Flux> listRetour = new ArrayList<>();
        for (Flux flux : listflux) {
            if (flux.getCorrelationId().equals(correlationId)) {
                listRetour.add(flux);
                System.out.println(flux.toJsonObject());
            }
        }
        future.complete(listRetour);
        return future;
    }

    @Override
    public Future<Integer> countFlux(Map<String, Object> parameters) {
        Future<Integer> future = Future.future();
        System.out.println(listflux.size());
        future.complete(listflux.size());
        return future;
    }

    @Override
    public Future<Flux> save(Flux flux) {
        Future<Flux> future = Future.future();
        System.out.println(flux.toJsonObject().encodePrettily());
        future.complete(flux);
        return future;
    }

    @Override
    public Future<List<StatusHistory>> loadStatusHistoryFluxByCorrelationId(String correlationId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Future<List<StatusHistory>> loadAllStatusHistoryFluxByCorrelationsId(String correlationId,
            String consumerCorrelationId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Future<Flux> getFluxByConsumerCorrelationId(String consumerCorrelationId) {
        Future<Flux> future = Future.future();
        flux.setStatusHistoryList(listStatusHistoryFlux);
        System.out.println(flux);
        future.complete(flux);
        return future;
    }

    @Override
    public Future<List<Flux>> getFluxByCorrelationIdAndConsumerCorrelationId(String correlationId,
            String consumerCorrelationId) {
        Future<List<Flux>> future = Future.future();
        List<Flux> listRetour = new ArrayList<>();
        flux.setStatusHistoryList(listStatusHistoryFlux);
        listRetour.add(flux);
        System.out.println(flux);
        future.complete(listRetour);

        return future;
    }

    @Override
    public Future<Void> delete(String correlationId) {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public Future<List<JsonObject>> getRepartition(Map<String, Object> likeFilters) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Future<List<Flux>> getFluxByCorrelationIdAndStatus(String correlationId, String status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Future<Void> deleteByCorrelationIdAndStatus(String correlationId, StatusEnum status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Future<Void> deleteByConsumerCorrelationIdAndStatusGreaterThanOrEqual(String consumerCorrelationId,
			StatusEnum status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Future<List<String>> loadFluxLot(Map<String, Object> likeFilters) {
		// TODO Auto-generated method stub
		return null;
	}

}
