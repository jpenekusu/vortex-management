//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence;

import java.util.Optional;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import fr.edu.vortex.management.parametre.Parametre;
import fr.edu.vortex.management.persistence.dao.ParametreDAO;
import fr.edu.vortex.management.persistence.dao.impl.ParametreDAOImpl;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class ParametreDAOTest {

	static Vertx vertx;
	static ParametreDAO parametreDAO;

	@BeforeClass
	public static void before(TestContext context) {
		vertx = Vertx.vertx();
		
		Config config = ConfigFactory.load();

		JsonObject conf = new JsonObject().put("url", config.getString("base.url"))
				.put("user", config.getString("base.user")).put("password", config.getString("base.password"))
				.put("driver_class", config.getString("base.driver_class"));

		DeploymentOptions options = new DeploymentOptions().setConfig(conf);
		vertx.deployVerticle(DatabaseVerticle.class.getName(), options, context.asyncAssertSuccess());

		parametreDAO = new ParametreDAOImpl();
		parametreDAO.init(vertx);
	}

	@Test
	public void testGetParametreByCode(TestContext ctx) throws InterruptedException {

		Async async = ctx.async();

		parametreDAO.getParametreByCode("AUTO_ENREGISTREMENT").setHandler(res -> {
			if (res.failed()) {
				res.cause().printStackTrace();
				ctx.assertTrue(false);
				async.complete();
			} else {
				Optional<Parametre> parametre = (Optional<Parametre>) res.result();
				if(parametre.isPresent()) {
					System.out.println(parametre.get().toJsonObject().encodePrettily());
					ctx.assertTrue(true);
				}
				else {
					ctx.assertTrue(false);					
				}
				async.complete();
			}
		});

	}

}
