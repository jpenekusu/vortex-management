//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import fr.edu.vortex.management.flux.Flux;
import fr.edu.vortex.management.flux.StatusEnum;
import fr.edu.vortex.management.flux.StatusHistory;
import fr.edu.vortex.management.persistence.dao.FluxDAO;
import fr.edu.vortex.management.persistence.dao.impl.FluxDAOImpl;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class FluxDAOTest {

	static Vertx vertx;
	static FluxDAO fluxDAO;
	static Flux flux = new Flux("987654", StatusEnum.IMMATRICULE, Instant.now(), "ANA001");
	static Flux flux2 = new Flux("456789", StatusEnum.RECU, Instant.now(), "ANA001", "ANATOSEM", "SEM", null, null,
			null, "TATA.txt", new Long(1000), "/nfs.tata.txt");
	static Flux flux3 = new Flux("123456", StatusEnum.A_ENVOYER, Instant.now(), "ANA001", "ANATOSEM", "SEM",
			"123456-SEM001", "SEM", "SEM001", "TATA.txt", new Long(1000), "/nfs.tata.txt");

	@BeforeClass
	public static void before(TestContext context) {
		vertx = Vertx.vertx();

		Config config = ConfigFactory.load();

		JsonObject conf = new JsonObject().put("url", config.getString("base.url"))
				.put("user", config.getString("base.user")).put("password", config.getString("base.password"))
				.put("driver_class", config.getString("base.driver_class"));

		DeploymentOptions options = new DeploymentOptions().setConfig(conf);
		vertx.deployVerticle(DatabaseVerticle.class.getName(), options, context.asyncAssertSuccess());

		fluxDAO = new FluxDAOImpl();
		fluxDAO.init(vertx);
	}

	@Test
	public void testSaveFlux(TestContext ctx) throws InterruptedException {
		Async async = ctx.async();

		Future<Flux> futureJsonObj = fluxDAO.save(flux);
		futureJsonObj.setHandler(res -> {
			if (res.failed()) {
				res.cause().printStackTrace();
				ctx.assertTrue(res.succeeded());
				async.complete();
			} else {
				System.out.println(res.result().toJsonObject().encodePrettily());
				ctx.assertTrue(res.succeeded());
				async.complete();
			}
		});
	}

	@Test
	public void testLoadAllFlux(TestContext ctx) throws InterruptedException {

		Async async = ctx.async();
		Map<String, String> filters = new HashMap<>();
		filters.put("status", "A_ENVOYER");
		filters.put("routing_key", "SAX001");

		fluxDAO.save(flux2).setHandler(arSave -> {
			if (arSave.failed()) {
				arSave.cause().printStackTrace();
				ctx.assertTrue(false);
				async.complete();
			} else {
				fluxDAO.loadFlux(0, 10, null, null, "ASC").setHandler(arloadFlux -> {
					if (arloadFlux.failed()) {
						arloadFlux.cause().printStackTrace();
						ctx.assertTrue(false);
						async.complete();
					} else {
						ArrayList<Flux> allFlux = (ArrayList<Flux>) arloadFlux.result();
						if (allFlux.size() > 0) {
							ctx.assertTrue(true);
						} else {
							ctx.assertTrue(false);
						}
						async.complete();
					}
				});
			}
		});

	}

	@Test
	public void testLoadAllFluxByQueueName(TestContext ctx) throws InterruptedException {

		Async async = ctx.async();
		Map<String, Object> filters = new HashMap<>();
		filters.put("consumer_queue_name", "SEM001");

		Future<List<Flux>> futureJsonObj = fluxDAO.loadFlux(0, 10, filters, "correlation_id", "ASC");
		futureJsonObj.setHandler(res -> {
			if (res.failed()) {
				res.cause().printStackTrace();
				ctx.assertTrue(res.succeeded());
				async.complete();
			} else {
				ArrayList<Flux> allFlux = (ArrayList<Flux>) res.result();

				for (Flux unFlux : allFlux) {
					System.out.println(unFlux.toJsonObject().encodePrettily());
				}

				ctx.assertTrue(res.succeeded());
				async.complete();
			}
		});

	}

	@Test
	public void testGetFluxByCorrelationId(TestContext ctx) throws InterruptedException {

		Async async = ctx.async();

		Future<List<Flux>> futureJsonObj = fluxDAO.getFluxByCorrelationId("456789");
		futureJsonObj.setHandler(res -> {
			if (res.failed()) {
				res.cause().printStackTrace();
				ctx.assertTrue(res.succeeded());
				async.complete();
			} else {
				ArrayList<Flux> allFlux = (ArrayList<Flux>) res.result();

				for (Flux unFlux : allFlux) {
					System.out.println(unFlux.toJsonObject().encodePrettily());
				}

				ctx.assertTrue(res.succeeded());
				async.complete();
			}
		});

	}

	@Test
	public void testGetFluxByConsumerCorrelationId(TestContext ctx) throws InterruptedException {
		Async async = ctx.async();

		fluxDAO.save(flux3).setHandler(arSave -> {
			if (arSave.failed()) {
				arSave.cause().printStackTrace();
				ctx.assertTrue(false);
				async.complete();
			} else {
				fluxDAO.getFluxByConsumerCorrelationId("123456-SEM001").setHandler(arGetFlux -> {
					if (arGetFlux.failed()) {
						arGetFlux.cause().printStackTrace();
						ctx.assertTrue(false);
						async.complete();
					} else {
						Flux flux = (Flux) arGetFlux.result();
						if(flux!=null) {
							System.out.println(flux.toJsonObject());
							ctx.assertTrue(true);
						}
						else {
							ctx.assertTrue(false);
						}
						async.complete();
					}
				});
			}
		});

	}

	@Test
	public void testCountAllFlux(TestContext ctx) throws InterruptedException {

		Async async = ctx.async();

		Map<String, Object> filters = new HashMap<>();

		Future<Integer> futureJsonObj = fluxDAO.countFlux(filters);
		futureJsonObj.setHandler(res -> {
			if (res.failed()) {
				res.cause().printStackTrace();
				ctx.assertTrue(res.succeeded());
				async.complete();
			} else {
				Integer nbFlux = (Integer) res.result();

				System.out.println("NbFlux = " + nbFlux);

				ctx.assertTrue(res.succeeded());
				async.complete();
			}
		});
	}

	@Test
	public void testCountFlux(TestContext ctx) throws InterruptedException {

		Async async = ctx.async();
		Map<String, Object> filters = new HashMap<>();
		filters.put("consumer_queue_name", "SEM001");

		Future<Integer> futureJsonObj = fluxDAO.countFlux(filters);
		futureJsonObj.setHandler(res -> {
			if (res.failed()) {
				res.cause().printStackTrace();
				ctx.assertTrue(res.succeeded());
				async.complete();
			} else {
				Integer nbFlux = (Integer) res.result();

				System.out.println("NbFlux = " + nbFlux);

				ctx.assertTrue(res.succeeded());
				async.complete();
			}
		});
	}

	@Test
	public void testLoadStatusHistoryFluxByCorrelationId(TestContext ctx) throws InterruptedException {

		Async async = ctx.async();

		Future<List<StatusHistory>> futureJsonObj = fluxDAO.loadStatusHistoryFluxByCorrelationId("456789");
		futureJsonObj.setHandler(res -> {
			if (res.failed()) {
				res.cause().printStackTrace();
				ctx.assertTrue(res.succeeded());
				async.complete();
			} else {
				List<StatusHistory> allStatusHistory = (ArrayList<StatusHistory>) res.result();

				for (StatusHistory statusHistory : allStatusHistory) {
					System.out.println(statusHistory.toJsonObject().encodePrettily());
				}

				ctx.assertTrue(res.succeeded());
				async.complete();
			}
		});

	}

	@Test
	public void loadAllStatusHistoryFluxByCorrelationsId(TestContext ctx) throws InterruptedException {

		Async async = ctx.async();

		Future<List<StatusHistory>> futureJsonObj = fluxDAO.loadAllStatusHistoryFluxByCorrelationsId("456789",
				"456789-SEM001");
		futureJsonObj.setHandler(res -> {
			if (res.failed()) {
				res.cause().printStackTrace();
				ctx.assertTrue(res.succeeded());
				async.complete();
			} else {
				List<StatusHistory> allStatusHistory = (ArrayList<StatusHistory>) res.result();

				for (StatusHistory statusHistory : allStatusHistory) {
					System.out.println(statusHistory.toJsonObject().encodePrettily());
				}

				ctx.assertTrue(res.succeeded());
				async.complete();
			}
		});

	}

}
