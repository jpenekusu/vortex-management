//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.persistence;

import java.time.Instant;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import fr.edu.vortex.management.flux.ActionEnum;
import fr.edu.vortex.management.flux.HistoFlux;
import fr.edu.vortex.management.flux.StatusEnum;
import fr.edu.vortex.management.persistence.dao.HistoFluxDAO;
import fr.edu.vortex.management.persistence.dao.impl.HistoFluxDAOImpl;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class HistoFluxDAOTest {

	static Vertx vertx;
	static HistoFluxDAO histoFluxDAO;
	static HistoFlux histoFlux = new HistoFlux(Instant.now(), "CHO001-ba447be8-d8e9-479b-a21b-440f8ffb1525",
			"CHO001-ba447be8-d8e9-479b-a21b-440f8ffb1525-IMG001", "IMG001", StatusEnum.IMMATRICULE, ActionEnum.SYSTEME);

	@BeforeClass
	public static void before(TestContext context) {
		vertx = Vertx.vertx();

		Config config = ConfigFactory.load();

		JsonObject conf = new JsonObject().put("url", config.getString("base.url"))
				.put("user", config.getString("base.user")).put("password", config.getString("base.password"))
				.put("driver_class", config.getString("base.driver_class"));

		DeploymentOptions options = new DeploymentOptions().setConfig(conf);
		vertx.deployVerticle(DatabaseVerticle.class.getName(), options, context.asyncAssertSuccess());

		histoFluxDAO = new HistoFluxDAOImpl();
		histoFluxDAO.init(vertx);
	}

	@Test
	public void testSaveHistoFlux(TestContext ctx) throws InterruptedException {
		Async async = ctx.async();

		Future<HistoFlux> futureJsonObj = histoFluxDAO.save(histoFlux);
		futureJsonObj.setHandler(res -> {
			if (res.failed()) {
				res.cause().printStackTrace();
				ctx.assertTrue(res.succeeded());
				async.complete();
			} else {
				System.out.println(res.result().toJsonObject().encodePrettily());
				ctx.assertTrue(res.succeeded());
				async.complete();
			}
		});
	}

	@Test
	public void testGetHistoFluxByCorrelationIdAndConsumerCorrelationId(TestContext ctx) throws InterruptedException {
		Async async = ctx.async();

		histoFluxDAO.save(histoFlux).setHandler(arSave -> {
			if (arSave.failed()) {
				arSave.cause().printStackTrace();
				ctx.assertTrue(false);
				async.complete();
			} else {
				histoFluxDAO.getHistoFluxByCorrelationIdAndConsumerCorrelationId(
						"CHO001-ba447be8-d8e9-479b-a21b-440f8ffb1525",
						"CHO001-ba447be8-d8e9-479b-a21b-440f8ffb1525-IMG001").setHandler(arGetHisto -> {
							if (arGetHisto.failed()) {
								arGetHisto.cause().printStackTrace();
								ctx.assertTrue(false);
								async.complete();
							} else {
								ArrayList<HistoFlux> histoFluxList = (ArrayList<HistoFlux>) arGetHisto.result();
								if (histoFlux != null) {
									for (HistoFlux histo : histoFluxList) {
										System.out.println(histo.toJsonObject().encodePrettily());
									}
									ctx.assertTrue(true);
								} else {
									ctx.assertTrue(false);
								}
								async.complete();
							}
						});
			}
		});
	}

}
