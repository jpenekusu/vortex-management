package fr.edu.vortex.management.filtre;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.edu.vortex.management.utilisateur.pojo.Filtre;
import fr.edu.vortex.management.utilisateur.verticle.FiltreUtilisateurVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class FiltreUtilisateurTest {

	Vertx vertx;

	@Before
	public void setUp(TestContext context) throws IOException {
		vertx = Vertx.vertx();

		FiltreUtilisateurVerticle verticle = new FiltreUtilisateurVerticle();
		vertx.deployVerticle(verticle, context.asyncAssertSuccess());
	}

	@Test
	public void creerFiltreRGTest(TestContext ctx) {
		Async async = ctx.async();
		
		
		vertx.eventBus().send(FiltreUtilisateurVerticle.CREER_FILTRE_UTILISATEUR, null, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.MAL_FORMATE);
			ctx.assertFalse(result.succeeded());
		});
		
		JsonObject filtre = new JsonObject();
		//nom pas renseigné
		vertx.eventBus().send(FiltreUtilisateurVerticle.CREER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_NOM_FILTRE);
			ctx.assertFalse(result.succeeded());
		});
		
		//nom invalide
		filtre.clear();
		filtre.put(Filtre.KEY_JSON_NOM, true);
		vertx.eventBus().send(FiltreUtilisateurVerticle.CREER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_NOM_FILTRE_INVALIDE);
			ctx.assertFalse(result.succeeded());
		});
	
		//identifiant utilisateur non renseigné
		filtre.clear();
		filtre.put(Filtre.KEY_JSON_NOM, "test filtre");
		vertx.eventBus().send(FiltreUtilisateurVerticle.CREER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_UTILISATEUR_FILTRE);
			ctx.assertFalse(result.succeeded());
		});

		//identifiant utilisateur invalide
		filtre.clear();
		filtre.put(Filtre.KEY_JSON_NOM, "test filtre");
		filtre.put(Filtre.KEY_JSON_UTILISATEUR_ID, "test filtre");
		vertx.eventBus().send(FiltreUtilisateurVerticle.CREER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_UTILISATEUR_FILTRE_INVALIDE);
			ctx.assertFalse(result.succeeded());
		});		
		
		//type filtre non renseigné
		filtre.clear();
		filtre.put(Filtre.KEY_JSON_NOM, "test filtre");
		filtre.put(Filtre.KEY_JSON_UTILISATEUR_ID, 12);
		vertx.eventBus().send(FiltreUtilisateurVerticle.CREER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_TYPE_FILTRE);
			ctx.assertFalse(result.succeeded());
		});

		//type filtre invalide
		filtre.clear();
		filtre.put(Filtre.KEY_JSON_NOM, "test filtre");
		filtre.put(Filtre.KEY_JSON_UTILISATEUR_ID, 12);
		filtre.put(Filtre.KEY_JSON_TYPE_FILTRE, 12);
		vertx.eventBus().send(FiltreUtilisateurVerticle.CREER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_TYPE_FILTRE_INVALIDE);
			ctx.assertFalse(result.succeeded());
		});			

		//criteres vides
		filtre.clear();
		filtre.put(Filtre.KEY_JSON_NOM, "test filtre");
		filtre.put(Filtre.KEY_JSON_UTILISATEUR_ID, 12);
		filtre.put(Filtre.KEY_JSON_TYPE_FILTRE, "supervision-flux");
		vertx.eventBus().send(FiltreUtilisateurVerticle.CREER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_UTILISATEUR_CRITERES);
			ctx.assertFalse(result.succeeded());
		});
		
		//description invalide
		filtre.clear();
		filtre.put(Filtre.KEY_JSON_NOM, "test filtre");
		filtre.put(Filtre.KEY_JSON_UTILISATEUR_ID, 12);
		filtre.put(Filtre.KEY_JSON_TYPE_FILTRE, "supervision-flux");
		filtre.put(Filtre.KEY_JSON_CONTENU, new JsonObject());
		filtre.put(Filtre.KEY_JSON_DESCRIPTION, true);
		vertx.eventBus().send(FiltreUtilisateurVerticle.CREER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_DESCRIPTION_FILTRE_INVALIDE);
			ctx.assertFalse(result.succeeded());
			async.complete();
		});
	}
	
	@Test
	public void modifierFiltreRGTest(TestContext ctx) {
		Async async = ctx.async();
		
		
		vertx.eventBus().send(FiltreUtilisateurVerticle.MODIFIER_FILTRE_UTILISATEUR, null, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.MAL_FORMATE);
			ctx.assertFalse(result.succeeded());
		});
		
		JsonObject filtre = new JsonObject();
		//nom pas renseigné
		vertx.eventBus().send(FiltreUtilisateurVerticle.MODIFIER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_NOM_FILTRE);
			ctx.assertFalse(result.succeeded());
		});
		
		//nom invalide
		filtre.clear();
		filtre.put(Filtre.KEY_JSON_NOM, true);
		vertx.eventBus().send(FiltreUtilisateurVerticle.MODIFIER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_NOM_FILTRE_INVALIDE);
			ctx.assertFalse(result.succeeded());
		});
			
		//description invalide
		filtre.clear();
		filtre.put(Filtre.KEY_JSON_NOM, "test filtre");
		filtre.put(Filtre.KEY_JSON_UTILISATEUR_ID, 12);
		filtre.put(Filtre.KEY_JSON_TYPE_FILTRE, "supervision-flux");
		filtre.put(Filtre.KEY_JSON_CONTENU, new JsonObject());
		filtre.put(Filtre.KEY_JSON_DESCRIPTION, true);
		vertx.eventBus().send(FiltreUtilisateurVerticle.MODIFIER_FILTRE_UTILISATEUR, filtre, result -> {
			ctx.assertEquals(result.cause().getMessage(),FiltreUtilisateurVerticle.ERR_DESCRIPTION_FILTRE_INVALIDE);
			ctx.assertFalse(result.succeeded());
			async.complete();
		});
	}
}
