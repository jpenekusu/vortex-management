//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.notification;

import java.io.IOException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.edu.echanges.nat.vortex.common.adresses.notification.NotificationAdresses;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.persistence.dao.NotificationDAO;
import fr.edu.vortex.management.persistence.dao.impl.NotificationDAOImpl;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
@Ignore
public class NotificationVerticleTest {

    Vertx vertx;

    @Before
    public void setUp(TestContext context) throws IOException {
        vertx = Vertx.vertx();

        JsonObject conf = new JsonObject()
                .put("url", "jdbc:postgresql://localhost:5432/omogen?currentSchema=management").put("user", "postgres")
                .put("password", "postgres").put("driver_class", "org.postgresql.Driver");

        DeploymentOptions options = new DeploymentOptions().setConfig(conf);
        // vertx.deployVerticle(DatabaseVerticle.class.getName(), options,
        // context.asyncAssertSuccess());

        DatabaseVerticle dbVerticle = new DatabaseVerticle();
        vertx.deployVerticle(dbVerticle, options, context.asyncAssertSuccess());

        NotificationVerticle verticle = new NotificationVerticle();
        NotificationDAO notifDao = new NotificationDAOImpl();
        verticle.setNotificationDAO(notifDao);
        vertx.deployVerticle(verticle, context.asyncAssertSuccess());
    }

    @Test
    public void acquittementNotifTest(TestContext ctx) {
        Async async = ctx.async();

        vertx.eventBus().send(NotificationAdresses.ACQUITTEMENT_MESSAGE_OK, "123456", result -> {
            Notification notif = (Notification) result.result();
            System.out.println(notif.toJsonObject().encodePrettily());
            ctx.assertTrue(result.succeeded());
            async.complete();
        });

    }

}
