//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.io.IOException;

import fr.edu.vortex.management.utils.Utils;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.web.Router;

public abstract class AbstractTestApi {
    
    protected int port;
    protected HttpClient client;

    protected void setUp(Vertx vertx, TestContext context, Router subRouteApi) throws IOException {

        port = Utils.getPort();

        client = Utils.createSimpleHttpClient(vertx, port);

        Router router = Router.router(vertx);
        router.mountSubRouter("/", subRouteApi);
        vertx.createHttpServer().requestHandler(router::accept).listen(port, context.asyncAssertSuccess());
    }
    
    /**
     * Creation d'une requete (HttpClientRequest) avec un timeout et qui met le test en échec et l'arrete 
     * quand la requête échoue.
     * @param method
     * @param requestURI
     * @param context
     * @param async
     * @return
     */
    protected HttpClientRequest createClientRequest(HttpMethod method, String requestURI, TestContext context, Async async) {
        HttpClientRequest request = client.request(method, requestURI);
        request.setTimeout(5000);
        request.exceptionHandler(handler -> {
            handler.printStackTrace();
            context.fail();
            async.complete();
        });
        return request;
    }
    
    
    /**
     * Vérifie que le header http content-type existe
     * Vérifie que le header http content-type est équal à AbstractApi.APPLICATION_JSON_CHARSET_UTF_8
     * Vérifie le code http
     * Vérifie que le body récupéré est un JsonObject
     * @param reponse la reponse a analyse
     * @param expectedStatusCode le code http attendu
     * @param context le contexte du test
     * @param async
     */
    protected void assertCodeAndBodyJson(HttpClientResponse reponse, int expectedStatusCode,TestContext context, Async async) {
        int statusCode = reponse.statusCode();
        String contentType = reponse.getHeader(HttpHeaders.CONTENT_TYPE);
        context.assertNotNull(contentType);
        context.assertEquals(AbstractApi.APPLICATION_JSON_CHARSET_UTF_8, contentType);
        context.assertEquals(expectedStatusCode, statusCode);
        reponse.bodyHandler(bodyHandler -> {
            try {
                new JsonObject(bodyHandler);
            } catch (Exception e) {
                context.fail("le body n'est pas un JsonObject");
            } finally {
                async.complete();
            }
        });
    }
    
    /**
     * vérifie le code http et verifie que le retour est un JsonArray
     * @param reponse la reponse a analyse
     * @param expectedStatusCode le code http attendu
     * @param context le contexte du test
     * @param async
     */
    protected void assertCodeAndBodyJsonArray(HttpClientResponse reponse, int expectedStatusCode,TestContext context, Async async) {
        int statusCode = reponse.statusCode();
        String contentType = reponse.getHeader(HttpHeaders.CONTENT_TYPE);
        context.assertNotNull(contentType);
        context.assertEquals(AbstractApi.APPLICATION_JSON_CHARSET_UTF_8, contentType);
        context.assertEquals(expectedStatusCode, statusCode);
        reponse.bodyHandler(bodyHandler -> {
            try {
                new JsonArray(bodyHandler);
            } catch (Exception e) {
                context.fail("le body n'est pas un JsonArray");
            } finally {
                async.complete();
            }
        });
    }

    /**
     * Verifie la reponse, vérifie le code http
     * @param reponse la reponse a analyse
     * @param expectedStatusCode le code http attendu
     * @param context le contexte du test
     * @param async
     */
    protected void assertCode(HttpClientResponse reponse, int expectedStatusCode,TestContext context, Async async) {
        int statusCode = reponse.statusCode();
        String contentType = reponse.getHeader(HttpHeaders.CONTENT_TYPE);
        context.assertNotNull(contentType);
        context.assertEquals(AbstractApi.APPLICATION_JSON_CHARSET_UTF_8, contentType);
        context.assertEquals(expectedStatusCode, statusCode);
        async.complete();
    }

}
