//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.edu.vortex.management.utilisateur.verticle.RoleVerticle;
import fr.edu.vortex.management.utils.Utils;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;

@RunWith(VertxUnitRunner.class)
public class RoleApiTest extends AbstractWebClientTestApi {

    Vertx vertx;

    @Before
    public void setUp(TestContext context) throws IOException {
        System.setProperty("hazelcast.logging.type", "sl4j");
        System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");

        VertxOptions opt = new VertxOptions();
        opt.setBlockedThreadCheckInterval(600000);

        vertx = Vertx.vertx(opt);

        RoleApi api = new RoleApi();
        super.setUp(vertx, context, api.getRouterApi(vertx));
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void listerRoleTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus()
                .consumer(RoleVerticle.LISTER_ROLE)
                .handler(Utils::replyJsonArrayTailleDeuxEventBus);

        WebClient.create(vertx)
                .get(port, localhost, RoleApi.URI_ROLES)
                .send(response -> assertCodeAndBodyJsonArray(response, 200, context, async));

    }

    @Test
    public void deleteTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus()
                .consumer(RoleVerticle.SUPPRIMER_ROLE)
                .handler(Utils::replyNullEventBus);

        WebClient.create(vertx)
                .delete(port, localhost, RoleApi.URI_ROLES + "/123")
                .send(ar -> {
                    if (ar.failed()) {
                        ar.cause().printStackTrace();
                        context.fail(ar.cause().getMessage());
                    } else {
                        HttpResponse<Buffer> reponse = ar.result();
                        context.assertEquals(204, reponse.statusCode());
                        context.assertTrue(reponse.headers().contains(HttpHeaders.CONTENT_TYPE));

                        String contentType = reponse.headers().get(HttpHeaders.CONTENT_TYPE);
                        context.assertEquals("application/json; charset=utf-8", contentType);

                        async.complete();
                    }
                });

    }

    @Test
    public void deleteNotIntegerTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus()
                .consumer(RoleVerticle.SUPPRIMER_ROLE)
                .handler(Utils::replyNullEventBus);

        WebClient.create(vertx)
                .delete(port, localhost, RoleApi.URI_ROLES + "/ceci-n-est-pas-un-entier")
                .send(ar -> {
                    if (ar.failed()) {
                        ar.cause().printStackTrace();
                        context.fail(ar.cause().getMessage());
                    } else {
                        context.assertEquals(400, ar.result().statusCode());
                        async.complete();
                    }
                });
    }

    @Test
    public void createOkTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus()
                .consumer(RoleVerticle.CREER_ROLE)
                .handler(Utils::replyJsonObjectEventBus);

        JsonObject input = new JsonObject()
                .put("key", "data");

        WebClient.create(vertx)
                .post(port, localhost, RoleApi.URI_ROLES)
                .sendBuffer(input.toBuffer(), response -> assertCode(response, 201, context, async));

    }

    @Test
    public void createMauvaisBodyEntrantTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus()
                .consumer(RoleVerticle.CREER_ROLE)
                .handler(Utils::replyJsonObjectEventBus);

        Buffer buffer = Buffer.buffer("ceci n'est pas un json");

        WebClient.create(vertx)
                .post(port, localhost, RoleApi.URI_ROLES)
                .sendBuffer(buffer, response -> assertCode(response, 400, context, async));

    }
}
