package fr.edu.vortex.management.api;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import fr.edu.echanges.nat.vortex.common.adresses.client.ClientAdresses;
import fr.edu.vortex.management.mail.ModeleMail;
import fr.edu.vortex.management.mail.ModeleMailVerticle;
import fr.edu.vortex.management.persistence.DatabaseVerticle;
import fr.edu.vortex.management.utils.Utils;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class ModeleMailApiTest extends AbstractTestApi {

    Vertx vertx;
    
    @Before
    public void setUp(TestContext context) throws IOException {
        VertxOptions opt = new VertxOptions();
        opt.setBlockedThreadCheckInterval(600000);
        vertx = Vertx.vertx(opt);
        
        ModeleMailApi api = new ModeleMailApi();
        
		ModeleMailVerticle verticle = new ModeleMailVerticle();
		vertx.deployVerticle(verticle, context.asyncAssertSuccess());
	
        vertx.eventBus().consumer(ModeleMailVerticle.LISTER_MODELE_MAIL, Utils::replyJsonArrayTailleUnEventBus);

        super.setUp(vertx, context, api.getRouterApi(vertx));

    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }
    
    @Test
    public void putTest(TestContext context) {
        Async async = context.async();
                
        JsonObject corps = new JsonObject();
        corps.put("sujet","sujet du mail");
        corps.put("corps","corps du mail plusieurs lignes");
        
        JsonObject fakeModele = new JsonObject().put(ModeleMail.KEY_JSON_NOM, "modele test").put(ModeleMail.KEY_JSON_SYSTEME, false)
        		.put(ModeleMail.KEY_JSON_CONTENU, corps);
        
        HttpClientRequest request = createClientRequest(HttpMethod.PUT, ModeleMailApi.URI+"/id",context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 400, context, async));
        request.end(fakeModele.encode());
    }
    
    
    @Test
    public void getAllModelesTest(TestContext context) {
        Async async = context.async();
        HttpClientRequest request = createClientRequest(HttpMethod.GET, ModeleMailApi.URI,context, async);
        request.handler(reponse -> assertCodeAndBodyJsonArray(reponse, 200, context, async));
        request.end();
    }
    
    @Test
    public void postTestRG1(TestContext context) {
        Async async = context.async();
        
        JsonObject corps = new JsonObject();
        corps.put("sujet","sujet du mail");
        corps.put("corps","corps du mail plusieurs lignes");
        
        JsonObject modele = new JsonObject();
        modele.put(ModeleMail.KEY_JSON_NOM, "modele test");
        
        HttpClientRequest request = createClientRequest(HttpMethod.POST, ModeleMailApi.URI,context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 400, context, async));
        request.end(modele.encode());
    }
        
    @Test
    public void postTestRG2(TestContext context) {
        Async async = context.async();
        
        JsonObject corps = new JsonObject();
        corps.put("sujet","sujet du mail");
        corps.put("corps","corps du mail plusieurs lignes");
        
        JsonObject modele = new JsonObject();
        modele.put(ModeleMail.KEY_JSON_NOM, "modele test");
        modele.put(ModeleMail.KEY_JSON_EMAIL, "rio@yahoo.fr");
       
        HttpClientRequest request = createClientRequest(HttpMethod.POST, ModeleMailApi.URI,context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 400, context, async));
        request.end(modele.encode());       
    }
    
    @Test
    public void postTestRG3(TestContext context) {
        Async async = context.async();
        
        JsonObject corps = new JsonObject();
        corps.put("sujet","sujet du mail");
        corps.put("corps","corps du mail plusieurs lignes");
        
        JsonObject modele = new JsonObject();
        modele.put(ModeleMail.KEY_JSON_NOM, "modele test");
        modele.put(ModeleMail.KEY_JSON_EMAIL, "rio@yahoo.fr");
        modele.put(ModeleMail.KEY_JSON_DESTINATAIRE, "PARTENAIRE");
        
        HttpClientRequest request = createClientRequest(HttpMethod.POST, ModeleMailApi.URI,context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 400, context, async));
        request.end(modele.encode());
    }
    
    @Test
    public void postTestRG4(TestContext context) {
        Async async = context.async();
        
        JsonObject corps = new JsonObject();
        corps.put("sujet","sujet du mail");
        corps.put("corps","corps du mail plusieurs lignes");
        
        JsonObject modele = new JsonObject();
        modele.put(ModeleMail.KEY_JSON_NOM, "modele test");
        modele.put(ModeleMail.KEY_JSON_EMAIL, "rio@yahoo.fr");
        modele.put(ModeleMail.KEY_JSON_DESTINATAIRE, "PARTENAIRE");
        modele.put(ModeleMail.KEY_JSON_SYSTEME, false);

        HttpClientRequest request = createClientRequest(HttpMethod.POST, ModeleMailApi.URI,context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 400, context, async));
        request.end(modele.encode());
   }
    
    @Test
    public void postTestRG5(TestContext context) {
        Async async = context.async();
        
        JsonObject corps = new JsonObject();
        corps.put("sujet","sujet du mail");
        corps.put("corps","corps du mail plusieurs lignes");
        
        JsonObject modele = new JsonObject();
        modele.put(ModeleMail.KEY_JSON_NOM, "modele test");
        modele.put(ModeleMail.KEY_JSON_EMAIL, "rio@yahoo.fr");
        modele.put(ModeleMail.KEY_JSON_DESTINATAIRE, "PARTENAIRE");
        modele.put(ModeleMail.KEY_JSON_SYSTEME, false);
        modele.put(ModeleMail.KEY_JSON_CONTENU, corps);

        HttpClientRequest request = createClientRequest(HttpMethod.POST, ModeleMailApi.URI,context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 400, context, async));
        request.end(modele.encode());
    }
    
    @Test
    public void deleteTest(TestContext context) {
        Async async = context.async();
                
        HttpClientRequest request = createClientRequest(HttpMethod.DELETE, ModeleMailApi.URI+"/id",context, async);
        request.handler(reponse -> assertCode(reponse, 400, context, async));
        request.end();
    }

    
}
