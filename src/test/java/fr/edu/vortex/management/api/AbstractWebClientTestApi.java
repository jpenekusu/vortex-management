//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.io.IOException;

import fr.edu.vortex.management.utils.Utils;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.client.HttpResponse;

public abstract class AbstractWebClientTestApi {

    protected int port;
    protected static String localhost = "localhost";

    protected void setUp(Vertx vertx, TestContext context, Router subRouteApi) throws IOException {

        port = Utils.getPort();

        Router router = Router.router(vertx);
        router.mountSubRouter("/", subRouteApi);

        vertx.createHttpServer()
                .requestHandler(router)
                .listen(port, context.asyncAssertSuccess());
    }

    protected void assertCodeAndBodyJsonArray(AsyncResult<HttpResponse<Buffer>> asynResult, int expectedStatusCode,
            TestContext context,
            Async async) {
        if (asynResult.failed()) {
            context.fail();
        } else {
            HttpResponse<Buffer> response = asynResult.result();
            int statusCode = response.statusCode();
            String contentType = response.headers().get(HttpHeaders.CONTENT_TYPE);
            context.assertNotNull(contentType);
            context.assertEquals(AbstractApi.APPLICATION_JSON_CHARSET_UTF_8, contentType);
            context.assertEquals(expectedStatusCode, statusCode);

            try {
                new JsonArray(response.body());
            } catch (Exception e) {
                context.fail("le body n'est pas un JsonArray");
            } finally {
                async.complete();
            }
        }
    }

    protected void assertIsInternalServerError(AsyncResult<HttpResponse<Buffer>> asynResult,
            TestContext context,
            Async async) {

        if (asynResult.failed()) {
            context.fail();
        } else {
            HttpResponse<Buffer> response = asynResult.result();
            int statusCode = response.statusCode();
            String contentType = response.headers().get(HttpHeaders.CONTENT_TYPE);
            context.assertNotNull(contentType);
            context.assertEquals(AbstractApi.APPLICATION_JSON_CHARSET_UTF_8, contentType);
            context.assertEquals(500, statusCode);
            try {
                new JsonObject(response.body());
            } catch (Exception e) {
                context.fail("le body n'est pas un json");
            } finally {
                async.complete();
            }
        }

    }

    protected void assertCode(AsyncResult<HttpResponse<Buffer>> asyncResult, int expectedStatusCode,
            TestContext context,
            Async async) {

        if (asyncResult.failed()) {
            context.fail();
        } else {
            HttpResponse<Buffer> reponse = asyncResult.result();
            int statusCode = reponse.statusCode();
            context.assertEquals(expectedStatusCode, statusCode);
            async.complete();
        }
    }

}
