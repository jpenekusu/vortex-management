package fr.edu.vortex.management.api;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.edu.vortex.management.utilisateur.verticle.TokenVerticle;
import fr.edu.vortex.management.utils.Utils;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class ConfigurationApiTest extends AbstractTestApi {
    
    Vertx vertx;
    
    @Before
    public void setUp(TestContext context) throws IOException {
        VertxOptions opt = new VertxOptions();
        opt.setBlockedThreadCheckInterval(600000);
        vertx = Vertx.vertx(opt);
        
        ConfigurationApi api = new ConfigurationApi();
        super.setUp(vertx, context, api.getRouterApi(vertx));
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }
    
    @Test
    public void testError401(TestContext context) {
        Async async = context.async();
        
        vertx.eventBus().consumer(TokenVerticle.TOKEN_UTILISATEUR, Utils::replyFail400EventBus);
  
        HttpClientRequest request = createClientRequest(HttpMethod.POST, ConfigurationApi.URI,context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, HttpURLConnection.HTTP_UNAUTHORIZED, context, async));
        request.end();

    }
}
