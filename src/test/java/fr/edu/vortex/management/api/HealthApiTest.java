//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.io.IOException;
import java.net.ServerSocket;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.edu.echanges.nat.vortex.common.adresses.core.health.CoreHealthAdresses;
import fr.edu.echanges.nat.vortex.common.adresses.gateway.health.GatewayHealthAdresses;
import fr.edu.echanges.nat.vortex.common.adresses.management.health.ManagementHealthAdresses;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.Router;

@RunWith(VertxUnitRunner.class)
public class HealthApiTest {

    private static final String HEALTH_URI = "/healths";
    Vertx vertx;
    private int port;

    @Before
    public void setUp(TestContext context) throws IOException {
        vertx = Vertx.vertx();

        // prend un port libre
        ServerSocket socket = new ServerSocket(0);
        port = socket.getLocalPort();
        socket.close();

        Router router = Router.router(vertx);
        HealthApi healthApi = new HealthApi();
        router.mountSubRouter("/", healthApi.getRouterApi(vertx));
        vertx.createHttpServer().requestHandler(router::accept).listen(port, context.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void allServicesDownTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus().consumer(GatewayHealthAdresses.GET_HEALTH).handler(handler -> {
            handler.fail(500, "");
        });
        vertx.eventBus().consumer(CoreHealthAdresses.GET_HEALTH).handler(handler -> {
            handler.fail(500, "");
        });
        vertx.eventBus().consumer(ManagementHealthAdresses.GET_HEALTH).handler(handler -> {
            handler.fail(500, "");
        });

        // creation d un client http et envoi d une requete
        HttpClientOptions options = new HttpClientOptions().setDefaultPort(port).setLogActivity(true);
        HttpClient client = vertx.createHttpClient(options);
        HttpClientRequest request = client.request(HttpMethod.GET, HEALTH_URI);

        request.handler(reponse -> {
            int statusCode = reponse.statusCode();
            context.assertEquals(200, statusCode);
            reponse.bodyHandler(bodyHandler -> {
                try {
                    new JsonArray(bodyHandler);
                } catch (Exception e) {
                    context.fail(e.getMessage());
                } finally {
                    async.complete();
                }
            });
        });
        client.close();
        request.end();
    }

    @Test
    public void allServicesUpTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus().consumer(GatewayHealthAdresses.GET_HEALTH).handler(handler -> {
            String json = "{\"outcome\":\"UP\", \"checks\":[{\"id\":\"main\",\"data\":{\"info\":\"fake\"}}]}";
            JsonObject message = new JsonObject(json);
            handler.reply(message);
        });
        vertx.eventBus().consumer(CoreHealthAdresses.GET_HEALTH).handler(handler -> {
            String json = "{\"outcome\":\"UP\", \"checks\":[{\"id\":\"main\",\"data\":{\"info\":\"fake\"}}]}";
            JsonObject message = new JsonObject(json);
            handler.reply(message);
        });
        vertx.eventBus().consumer(ManagementHealthAdresses.GET_HEALTH).handler(handler -> {
            String json = "{\"outcome\":\"UP\", \"checks\":[{\"id\":\"main\",\"data\":{\"info\":\"fake\"}}]}";
            JsonObject message = new JsonObject(json);
            handler.reply(message);
        });

        // creation d un client http et envoi d une requete
        HttpClientOptions options = new HttpClientOptions().setDefaultPort(port).setLogActivity(true);
        HttpClient client = vertx.createHttpClient(options);
        HttpClientRequest request = client.request(HttpMethod.GET, HEALTH_URI);

        request.handler(reponse -> {
            int statusCode = reponse.statusCode();
            context.assertEquals(200, statusCode);
            reponse.bodyHandler(bodyHandler -> {
                try {
                    new JsonArray(bodyHandler);
                } catch (Exception e) {
                    context.fail(e.getMessage());
                } finally {
                    async.complete();
                }
            });
        });
        client.close();
        request.end();
    }

    @Test
    public void dirtyJsonTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus().consumer(GatewayHealthAdresses.GET_HEALTH).handler(handler -> {
            String json = "{\"not\":\"good\"}";
            JsonObject message = new JsonObject(json);
            handler.reply(message);
        });
        vertx.eventBus().consumer(CoreHealthAdresses.GET_HEALTH).handler(handler -> {
            String json = "{\"outcome\":\"UP\",\"not\":\"good\"}";
            JsonObject message = new JsonObject(json);
            handler.reply(message);
        });
        vertx.eventBus().consumer(ManagementHealthAdresses.GET_HEALTH).handler(handler -> {
            String json = "{\"outcome\":\"UP\", \"checks\":[]}";
            JsonObject message = new JsonObject(json);
            handler.reply(message);
        });

        // creation d un client http et envoi d une requete
        HttpClientOptions options = new HttpClientOptions().setDefaultPort(port).setLogActivity(true);
        HttpClient client = vertx.createHttpClient(options);
        HttpClientRequest request = client.request(HttpMethod.GET, HEALTH_URI);

        request.handler(reponse -> {
            int statusCode = reponse.statusCode();
            context.assertEquals(200, statusCode);
            reponse.bodyHandler(bodyHandler -> {
                try {
                    new JsonArray(bodyHandler);
                } catch (Exception e) {
                    context.fail(e.getMessage());
                } finally {
                    async.complete();
                }
            });
        });
        client.close();
        request.end();
    }

}
