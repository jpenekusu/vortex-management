//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.edu.echanges.nat.vortex.common.adresses.exchange.BindingAdresses;
import fr.edu.vortex.management.utils.Utils;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class BindingsApiTest extends AbstractTestApi {

    Vertx vertx;

    @Before
    public void setUp(TestContext context) throws IOException {
        vertx = Vertx.vertx();

        VertxOptions opt = new VertxOptions();
        opt.setBlockedThreadCheckInterval(600000);
        vertx = Vertx.vertx(opt);

        BindingsApi api = new BindingsApi();
        super.setUp(vertx, context, api.getRouterApi(vertx));
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void getAllBindingsTest(TestContext context) {
        Async async = context.async();

        // mock(bouchon) vortex-core
        vertx.eventBus().consumer(BindingsApi.VORTEX_BINDING_GET).handler(Utils::replyJsonArrayTailleUnEventBus);

//        // mock(bouchon) vortex-core
//        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_ALL)
//                .handler(Utils::replyJsonArrayTailleUnEventBus);

        HttpClientRequest request = createClientRequest(HttpMethod.GET, BindingsApi.URI_BINDINGS, context, async);
        request.handler(reponse -> assertCodeAndBodyJsonArray(reponse, 200, context, async));
        request.end();
    }

    @Test
    public void deleteBindingsTest(TestContext context) {
        Async async = context.async();

        // mock(bouchon) vortex-core
        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_DELETE).handler(Utils::replyOkEventBus);

        HttpClientRequest request = createClientRequest(HttpMethod.DELETE, BindingsApi.URI_BINDINGS + "/22", context,
                async);
        request.handler(reponse -> assertCode(reponse, 204, context, async));
        request.end();
    }

    @Test
    public void postBindingsTest(TestContext context) {
        Async async = context.async();

        JsonObject bodyRequest = new JsonObject();
        bodyRequest.put("pattern_binding", "mock");
        bodyRequest.put("type", "mock");
        JsonObject exchange = new JsonObject();
        exchange.put("id", 1);
        exchange.put("name", "valeur");
        exchange.put("type", "valeur");
        JsonObject queue = new JsonObject();
        queue.put("id", 1);
        queue.put("name", "valeur");
        bodyRequest.put("exchange", exchange);
        bodyRequest.put("queue", queue);

        // mock(bouchon) vortex-core
        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_CREATE).handler(Utils::replyJsonObjectEventBus);

        HttpClientRequest request = createClientRequest(HttpMethod.POST, BindingsApi.URI_BINDINGS, context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 201, context, async));
        request.end(bodyRequest.encode());
    }

    @Test
    public void postBindingsNoBodyTest(TestContext context) {
        Async async = context.async();

        // mock(bouchon) vortex-core
        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_CREATE).handler(Utils::replyJsonObjectEventBus);

        HttpClientRequest request = createClientRequest(HttpMethod.POST, BindingsApi.URI_BINDINGS, context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 400, context, async));
        request.end();
    }

    @Test
    public void getBy3CriteriaTest(TestContext context) {
        Async async = context.async();

        // mock(bouchon) vortex-core
        vertx.eventBus().consumer(BindingsApi.VORTEX_BINDING_GET).handler(this::firstDataSets);

//        // mock(bouchon) vortex-core
//        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_BY_QUEUE).handler(this::firstDataSets);
//
//        // mock(bouchon) vortex-core
//        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_BY_EXCHANGE).handler(this::firstDataSets);
//
//        // mock(bouchon) vortex-core
//        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_BY_CLIENT).handler(this::secondDataSets);

        String requestURI = BindingsApi.URI_BINDINGS + "?queue=test&exchange=test&pattern_binding=test";
        HttpClientRequest request = createClientRequest(HttpMethod.GET, requestURI, context, async);
        request.handler(reponse -> assertCodeAndBodyJsonArray(reponse, 200, context, async));
        request.end();
    }

    @Test
    public void getByOneCosumerNotExistTest(TestContext context) {
        Async async = context.async();

        // mock(bouchon) vortex-core
        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_BY_QUEUE).handler(this::firstDataSets);

        // mock(bouchon) vortex-core
        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_BY_EXCHANGE).handler(this::firstDataSets);

        String requestURI = BindingsApi.URI_BINDINGS + "?queue_name=test&exchange_name=test&id_client=test";
        HttpClientRequest request = createClientRequest(HttpMethod.GET, requestURI, context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 500, context, async));
        request.end();
    }

    @Test
    public void getBy2CriteriaTest(TestContext context) {
        Async async = context.async();

        // mock(bouchon) vortex-core
        vertx.eventBus().consumer(BindingsApi.VORTEX_BINDING_GET).handler(this::firstDataSets);

//        // mock(bouchon) vortex-core
//        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_BY_QUEUE).handler(this::firstDataSets);
//
//        // mock(bouchon) vortex-core
//        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_BY_EXCHANGE).handler(this::firstDataSets);

        String requestURI = BindingsApi.URI_BINDINGS + "?queue=test&exchange=test";
        HttpClientRequest request = createClientRequest(HttpMethod.GET, requestURI, context, async);
        request.handler(reponse -> assertCodeAndBodyJsonArray(reponse, 200, context, async));
        request.end();
    }

    @Test
    public void getByWithFailTest(TestContext context) {
        Async async = context.async();

        // mock(bouchon) vortex-core
        vertx.eventBus().consumer(BindingsApi.VORTEX_BINDING_GET).handler(ar -> ar.fail(321, "test"));

//        // mock(bouchon) vortex-core
//        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_BY_QUEUE).handler(this::firstDataSets);
//
//        // mock(bouchon) vortex-core
//        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_BY_EXCHANGE).handler(this::firstDataSets);
//
//        // mock(bouchon) vortex-core
//        vertx.eventBus().consumer(BindingAdresses.VORTEX_BINDING_GET_BY_CLIENT).handler(ar -> ar.fail(321, "test"));

        String requestURI = BindingsApi.URI_BINDINGS + "?queue_name=test&exchange_name=test&id_client=test";
        HttpClientRequest request = createClientRequest(HttpMethod.GET, requestURI, context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 321, context, async));
        request.end();
    }

    private void firstDataSets(Message<Object> ar) {
        JsonArray array = new JsonArray();
        JsonObject first = createJsonBinding(1, "q1", "pat", "", "ex1");
        JsonObject second = createJsonBinding(2, "q2", "pat", "", "ex2");
        array.add(first).add(second);
        ar.reply(array);
    }

    private void secondDataSets(Message<Object> ar) {
        JsonArray array = new JsonArray();
        JsonObject first = createJsonBinding(3, "queue3", "pat", "", "ex1");
        JsonObject second = createJsonBinding(4, "queue4", "pat", "", "ex2");
        array.add(first).add(second);
        ar.reply(array);
    }

    private JsonObject createJsonBinding(int id, String queue, String pattern, String args, String exchange) {
        String KEY_JSON_EXCHANGE_NAME = "exchange_name";
        String KEY_JSON_BINDING_ARGS = "binding_args";
        String KEY_JSON_PATTERN_BINDING = "pattern_binding";
        String KEY_JSON_QUEUE_NAME = "queue_name";
        String KEY_JSON_ID = "id";
        JsonObject first = new JsonObject();
        first.put(KEY_JSON_ID, id);
        first.put(KEY_JSON_QUEUE_NAME, queue);
        first.put(KEY_JSON_PATTERN_BINDING, pattern);
        first.put(KEY_JSON_BINDING_ARGS, args);
        first.put(KEY_JSON_EXCHANGE_NAME, exchange);
        return first;
    }

}
