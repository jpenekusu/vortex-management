//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.transform.Source;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.xmlunit.builder.Input;
import org.xmlunit.validation.Languages;
import org.xmlunit.validation.ValidationResult;
import org.xmlunit.validation.Validator;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class RacvisionApiTest extends AbstractTestApi {

    private static final String RACVISION_URI = "/racvision";
    private Vertx vertx;
    // private HttpClient client;
    // private int port;

    @Before
    public void setUp(TestContext context) throws IOException {
        vertx = Vertx.vertx();

        RacvisionApi api = new RacvisionApi();
        super.setUp(vertx, context, api.getRouterApi(vertx));

        // vertx.deployVerticle(
        // MainVerticle.class.getName(),
        // // new DeploymentOptions().setConfig(new JsonObject().put("http.port",
        // port)),
        // context.asyncAssertSuccess()
        // );
        //
        // client = vertx.createHttpClient(
        // new HttpClientOptions().setDefaultPort(port).setLogActivity(true)
        // );
    }

    @After
    public void tearDown(TestContext context) {
        client.close();
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void ValidateXml(TestContext context) {
        final Async async = context.async();

        client.getNow("localhost", RACVISION_URI, response -> {
            context.assertEquals(200, response.statusCode());
            context.assertEquals("application/xml", response.getHeader(HttpHeaders.CONTENT_TYPE));

            response.bodyHandler(body -> {
                Validator validator = Validator.forLanguage(Languages.XML_DTD_NS_URI);
                try {
                    Source dtd = Input.from(getClass().getResourceAsStream("/apptest-1.8.dtd")).build();
                    dtd.setSystemId(getClass().getResource("/apptest-1.8.dtd").toURI().toString());
                    validator.setSchemaSource(dtd);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                ValidationResult result = validator.validateInstance(Input.from(body.toString()).build());
                context.assertTrue(result.isValid());

                async.complete();
            });
        });
    }

}