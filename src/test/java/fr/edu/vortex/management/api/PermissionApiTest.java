//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.edu.vortex.management.utilisateur.verticle.PermissionVerticle;
import fr.edu.vortex.management.utils.Utils;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClient;

@RunWith(VertxUnitRunner.class)
public class PermissionApiTest extends AbstractWebClientTestApi {

    Vertx vertx;

    @Before
    public void setUp(TestContext context) throws IOException {
        System.setProperty("hazelcast.logging.type", "sl4j");
        System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");

        VertxOptions opt = new VertxOptions();
        opt.setBlockedThreadCheckInterval(600000);

        vertx = Vertx.vertx(opt);

        PermissionApi api = new PermissionApi();
        super.setUp(vertx, context, api.getRouterApi(vertx));
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void getDeuxPermissionsTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus()
                .consumer(PermissionVerticle.LISTER_PERMISSION)
                .handler(Utils::replyJsonArrayTailleDeuxEventBus);

        WebClient.create(vertx)
                .get(port, localhost, PermissionApi.URI_PERMISSIONS)
                .send(response -> assertCodeAndBodyJsonArray(response, 200, context, async));

    }

    @Test
    public void getEmptyJsonArrayTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus()
                .consumer(PermissionVerticle.LISTER_PERMISSION)
                .handler(Utils::replyJsonArrayEmptyEventBus);

        WebClient.create(vertx)
                .get(port, localhost, PermissionApi.URI_PERMISSIONS)
                .send(response -> assertCode(response, 204, context, async));

    }

    @Test
    public void getErrorTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus()
                .consumer(PermissionVerticle.LISTER_PERMISSION)
                .handler(Utils::replyFailEventBus);

        WebClient.create(vertx)
                .get(port, localhost, PermissionApi.URI_PERMISSIONS)
                .send(response -> assertCode(response, 666, context, async));

    }

    @Test
    public void verticleDownTest(TestContext context) {
        Async async = context.async();

        WebClient.create(vertx)
                .get(port, localhost, PermissionApi.URI_PERMISSIONS)
                .send(response -> assertIsInternalServerError(response, context, async));

    }

}
