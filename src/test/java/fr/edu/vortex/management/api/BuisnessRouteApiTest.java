//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.edu.vortex.management.utils.Utils;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class BuisnessRouteApiTest extends AbstractTestApi {

    Vertx vertx;
    
    @Before
    public void setUp(TestContext context) throws IOException {
        vertx = Vertx.vertx();   
        
        VertxOptions opt = new VertxOptions();
        opt.setBlockedThreadCheckInterval(600000);
        vertx = Vertx.vertx(opt);
        
        BuisnessRouteApi api = new BuisnessRouteApi();
        super.setUp(vertx, context, api.getRouterApi(vertx));   
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void getAllDataTest(TestContext context) {
        Async async = context.async();
        
        //mock(bouchon) vortex-core
        vertx.eventBus()
            .consumer(BuisnessRouteApi.ADRESS_GET_ALL_ROUTES)
            .handler(Utils::replyJsonArrayTailleUnEventBus);
        
        HttpClientRequest request = createClientRequest(HttpMethod.GET, BuisnessRouteApi.URI, context, async);
        request.handler(reponse -> assertCodeAndBodyJsonArray(reponse, 200, context, async));
        request.end();
    }
    
    @Test
    public void getAllNoDataTest(TestContext context) {
        Async async = context.async();
        
        //mock(bouchon) vortex-core
        vertx.eventBus()
            .consumer(BuisnessRouteApi.ADRESS_GET_ALL_ROUTES)
            .handler(Utils::replyJsonArrayEmptyEventBus);
        
        HttpClientRequest request = createClientRequest(HttpMethod.GET, BuisnessRouteApi.URI, context, async);
        request.handler(reponse -> assertCode(reponse, 204, context, async));
        request.end();
    }
    
    @Test
    public void getAllErrorTest(TestContext context) {
        Async async = context.async();
        
        //mock(bouchon) vortex-core
        vertx.eventBus()
            .consumer(BuisnessRouteApi.ADRESS_GET_ALL_ROUTES)
            .handler(Utils::replyFailEventBus);
        
        HttpClientRequest request = createClientRequest(HttpMethod.GET, BuisnessRouteApi.URI, context, async);
        request.handler(reponse -> assertCodeAndBodyJson(reponse, 666, context, async));
        request.end();
    }

}
