//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.api;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.edu.echanges.nat.vortex.common.adresses.exchange.ExchangeAdresses;
import fr.edu.vortex.management.utils.Utils;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class ExchangesApiTest extends AbstractTestApi {

	private Vertx vertx;

	@Before
	public void setUp(TestContext context) throws IOException {
		VertxOptions opt = new VertxOptions();
		opt.setBlockedThreadCheckInterval(600000);
		vertx = Vertx.vertx(opt);

		ExchangesApi api = new ExchangesApi();
		super.setUp(vertx, context, api.getRouterApi(vertx));
	}

	@After
	public void tearDown(TestContext context) {
		vertx.close(context.asyncAssertSuccess());
	}

	@Test
	public void getAllExchangeTypesTest(TestContext context) {
		Async async = context.async();

		// mock(bouchon) vortex-core
		vertx.eventBus().consumer(ExchangeAdresses.VORTEX_EXCHANGE_TYPE_LIST)
				.handler(Utils::replyJsonArrayTailleDeuxEventBus);

		HttpClientRequest request = createClientRequest(HttpMethod.GET, ExchangesApi.URI_EXCHANGE_TYPE, context, async);
		request.handler(reponse -> assertCodeAndBodyJsonArray(reponse, 200, context, async));
		request.end();
	}

	@Test
	public void getAllExchangesTest(TestContext context) {
		Async async = context.async();

		// mock(bouchon) vortex-core
		vertx.eventBus().consumer(ExchangesApi.LIST_DEPOT_FILTRE)
				.handler(Utils::replyJsonArrayTailleDeuxEventBus);

		HttpClientRequest request = createClientRequest(HttpMethod.GET, ExchangesApi.URI_EXCHANGES, context, async);
		request.handler(reponse -> assertCodeAndBodyJsonArray(reponse, 200, context, async));
		request.end();
	}

	@Test
	public void getExchangesByNameTest(TestContext context) {
		Async async = context.async();

		// mock(bouchon) vortex-core
		vertx.eventBus().consumer(ExchangeAdresses.VORTEX_EXCHANGE_GET_BY_NAME).handler(Utils::replyJsonObjectEventBus);

		HttpClientRequest request = createClientRequest(HttpMethod.GET, ExchangesApi.URI_EXCHANGES + "/name", context,
				async);
		request.handler(reponse -> assertCodeAndBodyJson(reponse, 200, context, async));
		request.end();
	}

	@Test
	public void getExchangesByNameNotExistTest(TestContext context) {
		Async async = context.async();

		// mock(bouchon) vortex-core
		vertx.eventBus().consumer(ExchangeAdresses.VORTEX_EXCHANGE_GET_BY_NAME).handler(Utils::replyFail204EventBus);

		HttpClientRequest request = createClientRequest(HttpMethod.GET, ExchangesApi.URI_EXCHANGES + "/name", context,
				async);
		request.handler(reponse -> assertCode(reponse, 204, context, async));
		request.end();
	}

	@Test
	public void deleteExchangesTest(TestContext context) {
		Async async = context.async();

		// mock(bouchon) vortex-core
		vertx.eventBus().consumer(ExchangeAdresses.VORTEX_EXCHANGE_DELETE).handler(Utils::replyOkEventBus);

		HttpClientRequest request = createClientRequest(HttpMethod.DELETE, ExchangesApi.URI_EXCHANGES + "/name",
				context, async);
		request.handler(reponse -> assertCode(reponse, 204, context, async));
		request.end();
	}

	@Test
	public void postExchangesTest(TestContext context) {
		Async async = context.async();

		JsonObject bodyRequest = new JsonObject();
		bodyRequest.put("name", "mock");
		bodyRequest.put("type", "mock");
		JsonObject partenaire = new JsonObject();
		partenaire.put("id", 1);
		bodyRequest.put("partenaire", partenaire);

		vertx.eventBus().consumer(ExchangeAdresses.VORTEX_EXCHANGE_CREATE).handler(Utils::replyJsonObjectEventBus);

		HttpClientRequest request = createClientRequest(HttpMethod.POST, ExchangesApi.URI_EXCHANGES, context, async);
		request.handler(reponse -> assertCodeAndBodyJson(reponse, 201, context, async));
		request.end(bodyRequest.encode());
	}

}
