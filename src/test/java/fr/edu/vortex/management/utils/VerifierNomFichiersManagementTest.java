//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utils;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class VerifierNomFichiersManagementTest {

	@Test
	public void verifierNomScriptSQL(TestContext context) {
		Path migrationDBDirectory = Paths.get("src","main","resources","db","migration");
		File directory = migrationDBDirectory.toFile();
		File[] listFiles = directory.listFiles();
		List<String> listeErreur = new ArrayList<String>();
		List<String> nomsCallbackAutorises = new ArrayList<String>();
		nomsCallbackAutorises.add("afterMigrate");
		boolean auMoinsUnFichierInvalide = false;
		for (int i = 0; i < listFiles.length; i++) {
			String nomFichier = listFiles[i].getName();

			boolean nomCallback = false;
			//liste de noms	autorisés
			for (Iterator<String> iterator = nomsCallbackAutorises.iterator(); iterator.hasNext();) {
				String nomAuth = iterator.next();
				if (nomFichier.equals(nomAuth + ".sql")) {
					nomCallback = true;
				}				
			}
			//regex
			boolean matches = nomFichier.matches("^(?:V[0-9][0-9\\.]+|R|U[0-9])__[0-9a-zA-Z\\-_]*.sql");			
			if (!nomCallback && !matches) {
				String erreur = "Le nom du fichier : '" + nomFichier + "' est invalide";
				listeErreur.add(erreur);
				auMoinsUnFichierInvalide = true;
			}			
		}
		if (auMoinsUnFichierInvalide) {
			context.fail(listeErreur.toString());
		}

	}

}
