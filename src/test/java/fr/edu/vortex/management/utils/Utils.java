//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utils;

import java.io.IOException;
import java.net.ServerSocket;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class Utils {
    
    public static boolean isSnakeCase(String value) {
        String snakeCasePattern = "([a-z]|_)+";
        return value.matches(snakeCasePattern);
    }
    
    public static int getPort() throws IOException {
     // prend un port libre
        ServerSocket socket = new ServerSocket(0);
        int port = socket.getLocalPort();    
        socket.close();
        return port;
    }
    
    public static HttpClient createSimpleHttpClient(Vertx vertx, int port) {
     // creation d un client http et envoi d une requete
        HttpClientOptions options = new HttpClientOptions().setDefaultPort(port).setLogActivity(true);
        HttpClient client = vertx.createHttpClient(options);
        return client;
    }
    
    /**
     * handler de message de l'event bus qui retourne un String "OK"
     * @param message
     */
    public static void replyOkEventBus(Message<Object> message) {
        message.reply("OK");
    }
    
    /**
     * handler de message de l'event bus qui retourne un JsonArray vide
     * @param message
     */
    public static void replyJsonArrayEmptyEventBus(Message<Object> message) {
        JsonArray array = new JsonArray();
        message.reply(array);
    }
    
    /**
     * handler de message de l'event bus qui retourne un JsonArray contenant 2 éléments
     * ["first","second"]
     * @param message
     */
    public static void replyJsonArrayTailleDeuxEventBus(Message<Object> message) {
        JsonArray array = new JsonArray();
        array.add("first").add("second");
        message.reply(array);
    }
    
    /**
     * handler de message de l'event bus qui retourne un JsonArray contenant 1 éléments
     * ["first"]
     * @param message
     */
    public static void replyJsonArrayTailleUnEventBus(Message<Object> message) {
        JsonArray array = new JsonArray();
        array.add("first");
        message.reply(array);
    }
    
    /**
     * handler de message de l'event bus qui retourne un JsonObject
     * @param message
     */
    public static void replyJsonObjectEventBus(Message<Object> message) {
        JsonObject rep = new JsonObject();
        rep.put("un", "valeur");
        rep.put("nom", "valeur");
        message.reply(rep);
    }
    
    /**
     * handler de message de l'event bus qui retourne un JsonObject vide
     * @param message
     */
    public static void replyEmptyJsonObjectEventBus(Message<Object> message) {
        JsonObject rep = new JsonObject();
        message.reply(rep);
    }
    
    /**
     * handler de message de l'event bus qui retourne null
     * @param message
     */
    public static void replyNullEventBus(Message<Object> message) {
        message.reply(null);
    }
    
    /**
     * message fail avec code erreur 666
     */
    public static void replyFailEventBus(Message<Object> message) {
        message.fail(666, "erreur");
    }
    
    /**
    * message fail avec code erreur 204
    */
   public static void replyFail204EventBus(Message<Object> message) {
       message.fail(204,"message");
   }

   /**
   * message fail avec code erreur 400
   */
  public static void replyFail400EventBus(Message<Object> message) {
      message.fail(400,"message");
  }
}
