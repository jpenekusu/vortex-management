//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.persistence.dao.UtilisateurDAO;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class ListerUtilisateurHandlerTest {

    Vertx vertx;

    @Mock
    UtilisateurDAO utilisateurDao;

    @InjectMocks
    ListerUtilisateurHandler handlesUnderTest;

    @BeforeClass
    public static void before() {
        System.setProperty("hazelcast.logging.type", "sl4j");
        System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
    }

    @Before
    public void setUp(TestContext context) throws Exception {

        vertx = Vertx.vertx();

        handlesUnderTest = ListerUtilisateurHandler.create(vertx);
        MockitoAnnotations.initMocks(this);

        vertx.eventBus().consumer("test").handler(handlesUnderTest);

    }

    @After
    public void tearDown(TestContext context) throws Exception {
        vertx.close();
    }

    @Test
    public void listPasUtilisateurTest(TestContext context) {
        Async async = context.async();

        List<Utilisateur> emptyList = new ArrayList<Utilisateur>();

        when(utilisateurDao.listerUtilisateurs(any(Integer.class), any(Integer.class), any(JsonObject.class), any(String.class), any(String.class)))
                .thenReturn(Future.succeededFuture(emptyList));

        when(utilisateurDao.countUtilisateurs(any(JsonObject.class)))
        .thenReturn(Future.succeededFuture(0));

		JsonObject jsonCritere = new JsonObject();
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE, "1");
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, "2");
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT, "login");
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT_ORDER, "ASC");

        vertx.eventBus().<JsonArray>send("test", jsonCritere, ar -> {
            if (ar.succeeded()) {
                JsonArray body = ar.result().body();
                context.assertEquals(0, body.size());
                async.complete();
            } else {
                context.fail();
            }
        });
    }

    @Test
    public void listSansParamsUtilisateurTest(TestContext context) {
        Async async = context.async();

        List<Utilisateur> deuxUtilisateurs = new ArrayList<Utilisateur>();

        Utilisateur uti1 = new Utilisateur();
        uti1.setId(0);

        Utilisateur uti2 = new Utilisateur();
        uti2.setId(0);

        deuxUtilisateurs.add(uti1);
        deuxUtilisateurs.add(uti2);

        when(utilisateurDao.countUtilisateurs(any(JsonObject.class)))
        .thenReturn(Future.succeededFuture(2));

        when(utilisateurDao.listerUtilisateurs(any(Integer.class), any(Integer.class), any(JsonObject.class), any(String.class), any(String.class)))
                .thenReturn(Future.succeededFuture(deuxUtilisateurs));


		JsonObject jsonCritere = new JsonObject();
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE, "1");
		jsonCritere.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, "2");
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT, "login");
		jsonCritere.put(FluxVerticle.KEY_JSON_SORT_ORDER, "ASC");

        vertx.eventBus().<JsonArray>send("test", jsonCritere, ar -> {
            if (ar.succeeded()) {
                JsonArray body = ar.result().body();
                context.assertEquals(2, body.size());
                async.complete();
            } else {
                context.fail();
            }
        });
    }

    @Test
    public void erreurDaoTest(TestContext context) {
        Async async = context.async();

        when(utilisateurDao.listerUtilisateurs(any(Integer.class), any(Integer.class), any(JsonObject.class), any(String.class), any(String.class)))
                .thenReturn(Future.failedFuture("drame de test"));

        vertx.eventBus().<JsonArray>send("test", null, ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {
                async.complete();
            }
        });
    }

}
