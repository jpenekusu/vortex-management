//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.pojo;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import io.vertx.core.json.JsonObject;

public class UtilisateurTest {

    @Test
    public void testConstructeurVide() {
        Utilisateur utilisateur = new Utilisateur();

        assertThat(utilisateur, isA(Utilisateur.class));
        assertThat(utilisateur.getLogin(), is(nullValue()));
        assertThat(utilisateur.getRole(), is(nullValue()));

        utilisateur.setLogin("login");
        assertThat(utilisateur.getLogin(), is("login"));

    }

    @Test
    public void testConstructeurParams() {

        List<Permission> perms = new ArrayList<>();
        perms.add(new Permission("code", "libelle", "scope", "ressource", "contexte", "actions"));
        perms.add(new Permission("code", "libelle", "scope", "ressource", "contexte", "actions"));

        Role role = new Role(2, "lili bertine", false, perms);

        Utilisateur utilisateur = new Utilisateur(123, "michel", "andre", "mandre", "pass", "sel", 12,
                "mandre@gemail.com",
                "ldap",
                false, role);

        assertThat(utilisateur, isA(Utilisateur.class));
        assertThat(utilisateur.getPrenom(), is("andre"));
        assertThat(utilisateur.getRole().getPermissions().size(), is(2));

    }

    @Test
    public void testToJson() {

        List<Permission> perms = new ArrayList<>();
        perms.add(new Permission("code", "libelle", "scope", "ressource", "contexte", "actions"));
        perms.add(new Permission("code", "libelle", "scope", "ressource", "contexte", "actions"));
        perms.add(new Permission("code", "libelle", "scope", "ressource", "contexte", "actions"));

        Role role = new Role(2, "lili bertine", false, perms);

        Utilisateur utilisateur = new Utilisateur(123, "michel", "andre", "mandre", "pass", "sel", 12,
                "mandre@gemail.com",
                "ldap",
                false, role);

        JsonObject json = utilisateur.toJson();

        assertThat(json.getBoolean("systeme"), is(false));
        assertThat(json.getJsonObject("role"), isA(JsonObject.class));
        assertThat(json.getString("hash"), is("pass"));
        assertThat(json.getJsonObject("role").getJsonArray("permissions").size(), is(3));

    }

    @Test
    public void testFromJson() {

        JsonObject json = new JsonObject(
                "{\"id\":123,\"nom\":\"michel\",\"prenom\":\"andre\",\"login\":\"mandre\",\"hash\":\"pass\",\"sel\":\"sel\",\"email\":\"mandre@gemail.com\",\"type_authentification\":\"ldap\",\"systeme\":false,\"role\":{\"id\":2,\"libelle\":\"lili bertine\",\"systeme\":false,\"permissions\":[{\"code\":\"code\",\"libelle\":\"libelle\",\"scope\":\"scope\",\"ressource\":\"ressource\",\"contexte\":\"contexte\",\"actions\":\"actions\"},{\"code\":\"code\",\"libelle\":\"libelle\",\"scope\":\"scope\",\"ressource\":\"ressource\",\"contexte\":\"contexte\",\"actions\":\"actions\"},{\"code\":\"code\",\"libelle\":\"libelle\",\"scope\":\"scope\",\"ressource\":\"ressource\",\"contexte\":\"contexte\",\"actions\":\"actions\"}]}}");

        Utilisateur fromJson = Utilisateur.fromJson(json);
        assertThat(fromJson.getTypeAuthentification(), is("ldap"));
        assertThat(fromJson.getRole().getPermissions().size(), is(3));
        assertThat(fromJson.getRole().getPermissions().get(1).getActions(), is("actions"));

    }

}
