//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.edu.vortex.management.flux.FluxVerticle;
import fr.edu.vortex.management.persistence.dao.RoleDAO;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class ListerRoleHandlerTest {

    Vertx vertx;

    @Mock
    RoleDAO roleDao;

    @InjectMocks
    ListerRoleHandler listerRoleHandler;

    @BeforeClass
    public static void before() {
        System.setProperty("hazelcast.logging.type", "sl4j");
        System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
    }

    @Before
    public void setUp(TestContext context) throws Exception {

        vertx = Vertx.vertx();

        listerRoleHandler = ListerRoleHandler.create(vertx);
        Async async = context.async();
        MockitoAnnotations.initMocks(this);

        vertx.eventBus().consumer("ListerRoleHandlerTest.test").handler(listerRoleHandler);

        async.complete();

    }

    @After
    public void tearDown(TestContext context) throws Exception {
        vertx.close();
    }

    @Test
    public void listDeuxRolesTest(TestContext context) {
        Async async = context.async();

        List<Role> roles = new ArrayList<Role>();
        roles.add(new Role(1, "libelle 1", false, null));
        roles.add(new Role(2, "libelle 2", false, null));

        JsonObject json = new JsonObject();
        json.put(Role.KEY_JSON_SYSTEME,false);
        when(roleDao.listerRoles(any(Integer.class), any(Integer.class), any(JsonObject.class), any(String.class), any(String.class)))
                .thenReturn(Future.succeededFuture(roles)); 
 
        when(roleDao.countRoles(any(JsonObject.class)))
        .thenReturn(Future.succeededFuture(2)); 

        json.put(FluxVerticle.KEY_JSON_PAGE, "1");
        json.put(FluxVerticle.KEY_JSON_PAGE_LIMIT, "2");
        json.put(FluxVerticle.KEY_JSON_SORT, Role.KEY_JSON_LIBELLE);
        json.put(FluxVerticle.KEY_JSON_SORT_ORDER, "ASC");

        vertx.eventBus().<JsonArray>send("ListerRoleHandlerTest.test", json, ar -> {
            if (ar.succeeded()) {
                JsonArray body = ar.result().body();
                assertEquals(2, body.size());
                async.complete();
            } else {
                context.fail();
            }
        });

    }

    @Test
    public void ErrorTest(TestContext context) {
        Async async = context.async();

        JsonObject json = new JsonObject();
        json.put(Role.KEY_JSON_SYSTEME,true);
        when(roleDao.listerRoles(any(Integer.class), any(Integer.class), any(JsonObject.class), any(String.class), any(String.class)))
                .thenReturn(Future.failedFuture("erreur de test")); 
 
        when(roleDao.countRoles(json))
        .thenReturn(Future.succeededFuture(0)); 
       
        vertx.eventBus().<JsonArray>send("ListerRoleHandlerTest.test", null, ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {
                async.complete();
            }
        });

    }

}
