//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.edu.vortex.management.persistence.dao.PermissionDAO;
import fr.edu.vortex.management.persistence.dao.RoleDAO;
import fr.edu.vortex.management.persistence.dao.RolePermissionDAO;
import fr.edu.vortex.management.utilisateur.pojo.Permission;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class CreerRoleHandlerTest {

    private static final String ADRESSE = "CreerRoleHandler.test";

    Vertx vertx;

    @Mock
    RoleDAO roleDao;

    @Mock
    RolePermissionDAO rolePermDao;

    @Mock
    PermissionDAO permDao;

    @InjectMocks
    CreerRoleHandler creerRoleHandler;

    @BeforeClass
    public static void before() {
        System.setProperty("hazelcast.logging.type", "sl4j");
        System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
    }

    @Before
    public void setUp(TestContext context) throws Exception {

        vertx = Vertx.vertx();

        creerRoleHandler = CreerRoleHandler.create(vertx);
        MockitoAnnotations.initMocks(this);

        vertx.eventBus().<JsonObject>consumer(ADRESSE).handler(creerRoleHandler);
    }

    @After
    public void tearDown(TestContext context) throws Exception {
        vertx.close();
    }

    @Test
    public void creerAvecPermissions(TestContext context) {
        // Async async = context.async();

        Permission perm1 = new Permission();
        perm1.setCode("code de la permission 1");
        perm1.setActions("action 1");

        Permission perm2 = new Permission();
        perm2.setCode("code de la permission 2");
        perm2.setActions("action 2");

        List<Permission> perms = Arrays.asList(perm1, perm2);

        Role role = new Role();
        role.setLibelle("le role de test a creer");
        role.setPermissions(perms);

        when(roleDao.chercherRoleParLibelle(any(String.class)))
                .thenReturn(Future.succeededFuture(new ArrayList<>()));

        when(roleDao.creer(any(Role.class)))
                .thenReturn(Future.succeededFuture(role));

        when(rolePermDao.create(anyInt(), anyString(), anyString()))
                .thenReturn(Future.succeededFuture());

        when(permDao.lister())
                .thenReturn(Future.succeededFuture(perms));

        vertx.eventBus().send(ADRESSE, role.toJson(), context.asyncAssertSuccess());

    }

    @Test
    public void creerSansPermissions(TestContext context) {
        Async async = context.async();

        Role role = new Role();
        role.setLibelle("le role de test a creer");

        when(roleDao.chercherRoleParLibelle(any(String.class)))
                .thenReturn(Future.succeededFuture(new ArrayList<>()));

        when(roleDao.creer(any(Role.class)))
                .thenReturn(Future.succeededFuture(role));

        when(rolePermDao.create(anyInt(), anyString(), anyString()))
                .thenReturn(Future.succeededFuture());

        Permission perm1 = new Permission();
        perm1.setCode("code de la permission 1");
        List<Permission> perms = Arrays.asList(perm1);
        when(permDao.lister())
                .thenReturn(Future.succeededFuture(perms));

        vertx.eventBus().send(ADRESSE, role.toJson(), ar -> {
            if (ar.succeeded()) {
                async.complete();
            } else {
                ar.cause().printStackTrace();
                context.fail();
            }
        });
    }

    @Test
    public void creerRoleMalFormate(TestContext context) {
        Async async = context.async();

        Role role = new Role();
        role.setLibelle("le role de test a creer");

        when(roleDao.chercherRoleParLibelle(any(String.class)))
                .thenReturn(Future.succeededFuture(new ArrayList<>()));

        when(roleDao.creer(any(Role.class)))
                .thenReturn(Future.succeededFuture(role));

        when(rolePermDao.create(anyInt(), anyString(), anyString()))
                .thenReturn(Future.succeededFuture());

        JsonObject malFormateJson = role.toJson().put("permissions", "ceci n est pas un jsonArray");

        vertx.eventBus().send(ADRESSE, malFormateJson, ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {

                ReplyException cause = (ReplyException) ar.cause();
                context.assertEquals(400, cause.failureCode());
                async.complete();

            }
        });
    }

    @Test
    public void creerRoleLibelleExisteDeja(TestContext context) {
        Async async = context.async();

        Role role = new Role();
        role.setLibelle("le role avec un libelle qui existe deja");

        ArrayList<Role> roleDejaExistant = new ArrayList<>();
        roleDejaExistant.add(role);

        when(roleDao.chercherRoleParLibelle(any(String.class)))
                .thenReturn(Future.succeededFuture(roleDejaExistant));

        when(roleDao.creer(any(Role.class)))
                .thenReturn(Future.succeededFuture(role));

        when(rolePermDao.create(anyInt(), anyString(), anyString()))
                .thenReturn(Future.succeededFuture());

        vertx.eventBus().send(ADRESSE, role.toJson(), ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {

                ReplyException cause = (ReplyException) ar.cause();
                context.assertEquals(409, cause.failureCode());
                async.complete();

            }
        });
    }

    @Test
    public void creerRolePermissionSansCode(TestContext context) {
        Async async = context.async();

        Permission perm1 = new Permission();
        perm1.setLibelle("libelle de la permission 1");

        List<Permission> perms = Arrays.asList(perm1);

        Role role = new Role();
        role.setLibelle("le role de test a creer");
        role.setPermissions(perms);

        when(roleDao.chercherRoleParLibelle(any(String.class)))
                .thenReturn(Future.succeededFuture(new ArrayList<>()));

        when(roleDao.creer(any(Role.class)))
                .thenReturn(Future.succeededFuture(role));

        when(rolePermDao.create(anyInt(), anyString(), anyString()))
                .thenReturn(Future.succeededFuture());

        vertx.eventBus().send(ADRESSE, role.toJson(), ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {
                ReplyException cause = (ReplyException) ar.cause();
                context.assertEquals(400, cause.failureCode());
                context.assertEquals(
                        "demande de création d'un rôle avec des permissions mal formatée",
                        cause.getMessage());
                async.complete();
            }
        });
    }

}
