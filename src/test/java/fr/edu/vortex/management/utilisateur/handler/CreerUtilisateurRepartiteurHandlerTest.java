//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.edu.vortex.management.persistence.dao.UtilisateurDAO;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.RoleVerticle;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class CreerUtilisateurRepartiteurHandlerTest {

    private static final String ADRESSE = "CreerUtilisateurRepartiteurHandler.test";

    Vertx vertx;

    CreerUtilisateurRepartiteurHandler handlerUnderTest;

    @Mock
    UtilisateurDAO utilisateurDao;

    private Utilisateur dummyUtilisateur = new Utilisateur(
            1,
            "test",
            "test",
            "test",
            "test",
            "test",
            12,
            "test@test.fr",
            Utilisateur.AUTHENTIFICATION_LOCALE,
            false,
            null);

    @BeforeClass
    public static void before() {
        System.setProperty("hazelcast.logging.type", "sl4j");
        System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
    }

    @Before
    public void setUp(TestContext context) throws Exception {

        vertx = Vertx.vertx();

        vertx.eventBus().consumer(ADRESSE, CreerUtilisateurRepartiteurHandler.create(vertx));
        
    	MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown(TestContext context) throws Exception {
        vertx.close();
    }

    @Test
    public void inputNotAJson(TestContext context) {
        Async async = context.async();
        String badInput = "ceci n est pas un json";
        vertx.eventBus().send(ADRESSE, badInput, ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {
                ReplyException cause = (ReplyException) ar.cause();
                context.assertEquals(400, cause.failureCode());
                async.complete();
            }
        });

    }

    @Test
    public void dispatchLocalTest(TestContext context) {
        Async async = context.async();

        dummyUtilisateur.setTypeAuthentification(Utilisateur.AUTHENTIFICATION_LOCALE);

        vertx.eventBus().consumer(UtilisateurVerticle.CREER_UTILISATEUR_LOCALE, event -> {
            event.reply(dummyUtilisateur.toJson());
        });

        vertx.eventBus().send(ADRESSE, dummyUtilisateur.toJson(), ar -> {
            if (ar.succeeded()) {
                async.complete();
            } else {
                ar.cause().printStackTrace();
                context.fail();
            }
        });
    }

    // @Test
    public void dispatchLdapTest(TestContext context) {
        Async async = context.async();

        dummyUtilisateur.setTypeAuthentification(Utilisateur.AUTHENTIFICATION_LDAP);

        vertx.eventBus().consumer(UtilisateurVerticle.CREER_UTILISATEUR_LDAP, event -> {
            event.reply(dummyUtilisateur.toJson());
        });

        vertx.eventBus().send(ADRESSE, dummyUtilisateur.toJson(), ar -> {
            if (ar.succeeded()) {
                async.complete();
            } else {
                ar.cause().printStackTrace();
                context.fail();
            }
        });
    }

    @Test
    public void creationNOKRGPrenomTest(TestContext context) {
    	Async async = context.async();

    	vertx.eventBus().consumer(UtilisateurVerticle.LISTER_UTILISATEUR, event -> {
    		event.reply(new JsonArray());
    	});

    	vertx.eventBus().<JsonObject>consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, event -> {
    		event.reply("OK");
    	});

    	when(utilisateurDao.creer(any(Utilisateur.class)))
    	.thenReturn(Future.succeededFuture(dummyUtilisateur));

    	dummyUtilisateur.setMotDePasse("q4//{{}][[}}//\\/-V\\h4n=-+-zqjkA");
    	dummyUtilisateur.setEmail("bob@yahoo.fr");
    	dummyUtilisateur.setNom("D'Abbo ville");
    	dummyUtilisateur.setPrenom(" Gérard");
    	JsonObject input = dummyUtilisateur.toJson();

    	vertx.eventBus().send(ADRESSE, input, ar -> {
    		if (ar.succeeded()) {
    			context.fail();
    		} else {
    			context.assertEquals(String.format(UtilisateurHandler.ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_PRENOM), ar.cause().getMessage());
       			async.complete();
    		}
    	});
    }
    
    @Test
    public void creationNOKRGNomTest(TestContext context) {
    	Async async = context.async();

    	vertx.eventBus().consumer(UtilisateurVerticle.LISTER_UTILISATEUR, event -> {
    		event.reply(new JsonArray());
    	});

    	vertx.eventBus().<JsonObject>consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, event -> {
    		event.reply("OK");
    	});

    	when(utilisateurDao.creer(any(Utilisateur.class)))
    	.thenReturn(Future.succeededFuture(dummyUtilisateur));

    	dummyUtilisateur.setMotDePasse("q4//{{}][[}}//\\/-V\\h4n=-+-zqjkA");
    	dummyUtilisateur.setEmail("bob@yahoo.fr");
    	dummyUtilisateur.setNom("D'Abbo v$ille");
    	dummyUtilisateur.setPrenom("Gérard");
    	JsonObject input = dummyUtilisateur.toJson();

    	vertx.eventBus().send(ADRESSE, input, ar -> {
    		if (ar.succeeded()) {
    			context.fail();
    		} else {
    			context.assertEquals(String.format(UtilisateurHandler.ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_NOM), ar.cause().getMessage());
       			async.complete();
    		}
    	});
    }
    
    @Test
    public void creationNOKRGLoginTest(TestContext context) {
    	Async async = context.async();

    	vertx.eventBus().consumer(UtilisateurVerticle.LISTER_UTILISATEUR, event -> {
    		event.reply(new JsonArray());
    	});

    	vertx.eventBus().<JsonObject>consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, event -> {
    		event.reply("OK");
    	});

    	when(utilisateurDao.creer(any(Utilisateur.class)))
    	.thenReturn(Future.succeededFuture(dummyUtilisateur));

    	dummyUtilisateur.setMotDePasse("q4//{{}][[}}//\\/-V\\h4n=-+-zqjkA");
    	dummyUtilisateur.setEmail("bob@yahoo.fr");
    	dummyUtilisateur.setNom("D'Abbo ville");
    	dummyUtilisateur.setPrenom("Jean-Pascal");
    	dummyUtilisateur.setLogin("gérardA");
    	JsonObject input = dummyUtilisateur.toJson();

    	vertx.eventBus().send(ADRESSE, input, ar -> {
    		if (ar.succeeded()) {
    			context.fail();
    		} else {
    			context.assertEquals(String.format(UtilisateurHandler.ERR_CHAMPS_UTILISATEUR_INVALIDE, Utilisateur.KEY_JSON_LOGIN), ar.cause().getMessage());
       			async.complete();
    		}
    	});
    }  
}
