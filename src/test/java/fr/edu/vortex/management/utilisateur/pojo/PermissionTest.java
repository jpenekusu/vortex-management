//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.pojo;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import io.vertx.core.json.JsonObject;

public class PermissionTest {

    @Test
    public void testConstructeurVide() {
        Permission perm = new Permission();

        assertThat(perm, isA(Permission.class));
        assertThat(perm.getCode(), is(nullValue()));
        assertThat(perm.getLibelle(), is(nullValue()));

        perm.setLibelle("mon libelle");
        assertThat(perm.getLibelle(), is("mon libelle"));

    }

    @Test
    public void testConstructeurParams() {

        Permission perm = new Permission("code", "libelle", "scope", "ressource", "contexte", "actions");

        assertThat(perm, isA(Permission.class));
        assertThat(perm.getCode(), is("code"));
        assertThat(perm.getLibelle(), is("libelle"));
        assertThat(perm.getScope(), is("scope"));
        assertThat(perm.getRessource(), is("ressource"));
        assertThat(perm.getContexte(), is("contexte"));
        assertThat(perm.getActions(), is("actions"));

    }

    @Test
    public void testToJsonFull() {
        Permission perm = new Permission(
                "le-code",
                "voici un libelle",
                "scopito",
                "manon des ressource",
                "cahier de contexte",
                "moteur - actions");

        JsonObject json = perm.toJson();
        assertThat(json, isA(JsonObject.class));

        assertThat(json.getString("code"), is("le-code"));
        assertThat(json.getString("actions"), is("moteur - actions"));
        assertThat(json.getString("ressource"), is("manon des ressource"));

    }

    @Test
    public void testToJsonPartiel() {
        Permission perm = new Permission(
                "le-code",
                "voici un libelle",
                null,
                "manon des ressource",
                "cahier de contexte",
                "moteur - actions");

        JsonObject json = perm.toJson();
        System.out.println(json.encode());

        assertThat(json, isA(JsonObject.class));

        assertThat(json.getString("code"), is("le-code"));
        assertThat(json.getString("actions"), is("moteur - actions"));
        assertThat(json.containsKey("scope"), is(false));

    }

    @Test
    public void testFromJson() {

        JsonObject json = new JsonObject(
                "{\"code\":\"le-code\",\"libelle\":\"voici un libelle\",\"ressource\":\"manon des ressource\",\"contexte\":\"cahier de contexte\",\"actions\":\"moteur - actions\"}");

        Permission fromJson = Permission.fromJson(json);
        assertThat(fromJson.getCode(), is("le-code"));
        assertThat(fromJson.getActions(), is("moteur - actions"));
        assertThat(fromJson.getScope(), is(nullValue()));
    }

}
