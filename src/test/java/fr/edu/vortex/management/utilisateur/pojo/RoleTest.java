//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.pojo;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import io.vertx.core.json.JsonObject;

public class RoleTest {

    @Test
    public void testConstructeurVide() {
        Role role = new Role();

        assertThat(role, isA(Role.class));
        assertThat(role.getLibelle(), is(nullValue()));
        assertThat(role.getPermissions(), is(nullValue()));

        role.setLibelle("mon libelle");
        assertThat(role.getLibelle(), is("mon libelle"));

    }

    @Test
    public void testConstructeurParams() {

        List<Permission> perms = new ArrayList<>();
        perms.add(new Permission("code", "libelle", "scope", "ressource", "contexte", "action"));
        perms.add(new Permission("code", "libelle", "scope", "ressource", "contexte", "action"));

        Role role = new Role(2, "lili bertine", false, perms);

        assertThat(role, isA(Role.class));
        assertThat(role.getLibelle(), is("lili bertine"));
        assertThat(role.getPermissions().size(), is(2));

    }

    @Test
    public void testToJsonFull() {
        List<Permission> perms = new ArrayList<>();
        perms.add(new Permission("code", "libelle", "scope", "ressource", "contexte", "action"));
        perms.add(new Permission("code", "libelle", "scope", "ressource", "contexte", "action"));

        Role role = new Role(2, "lili bertine", false, perms);

        JsonObject json = role.toJson();
        assertThat(json, isA(JsonObject.class));

        assertThat(json.getInteger("id"), is(2));
        assertThat(json.getBoolean("systeme"), is(false));
        assertThat(json.getJsonArray("permissions").size(), is(2));

    }

    @Test
    public void testToJsonPartiel() {

        Role role = new Role(2, "lili bertine", false, null);

        JsonObject json = role.toJson();
        assertThat(json, isA(JsonObject.class));
        assertThat(json.containsKey("permissions"), is(false));

        List<Permission> perms = new ArrayList<>();
        role.setPermissions(perms);

        json = role.toJson();
        assertThat(json, isA(JsonObject.class));
        assertThat(json.containsKey("permissions"), is(false));

    }

    @Test
    public void testFromJsonSansPermissions() {

        JsonObject json = new JsonObject(
                "{\"id\":2,\"libelle\":\"lili bertine\",\"systeme\":false,\"permissions\":[]}");

        Role fromJson = Role.fromJson(json);
        assertThat(fromJson.getId(), is(2));
        assertThat(fromJson.getPermissions(), isA(List.class));
        assertThat(fromJson.getPermissions().size(), is(0));
    }

    @Test
    public void testFromJsonAvecPermissions() {

        JsonObject json = new JsonObject(
                "{\"id\":2,\"libelle\":\"lili bertine\",\"systeme\":false,\"permissions\":[{\"code\":\"code\",\"libelle\":\"libelle\",\"scope\":\"scope\",\"ressource\":\"ressource\",\"contexte\":\"contexte\",\"actions\":\"actions\"},{\"code\":\"code\",\"libelle\":\"libelle\",\"scope\":\"scope\",\"ressource\":\"ressource\",\"contexte\":\"contexte\",\"actions\":\"actions\"}]}");

        Role fromJson = Role.fromJson(json);
        assertThat(fromJson.getId(), is(2));
        assertThat(fromJson.getPermissions(), isA(List.class));
        assertThat(fromJson.getPermissions().size(), is(2));
    }

}
