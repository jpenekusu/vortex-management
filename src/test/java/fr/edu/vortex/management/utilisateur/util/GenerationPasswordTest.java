//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class GenerationPasswordTest {
    

    @Test
    public void testGenerationTailleMotdePasseCorrect() {
        String generationMotDePasse = GenerationPassword.generationMotDePasse();
        assertTrue(generationMotDePasse.length()==12);
    }
    
    
    @Test
    public void testGenerationTailleMotdePasseIncorrect() {
        String generationMotDePasse = "abcd2D)etyopio";
        boolean validationMotDePasse = GenerationPassword.validationMotDePasse(generationMotDePasse);
        assertFalse(validationMotDePasse);

    }
    
    
    @Test
    public void testGenerationMotdePasseNombreCaractere0() {
        boolean validationMotDePasse = GenerationPassword.validationMotDePasse("");
        assertFalse(validationMotDePasse);
    }
    

    @Test
    public void testGenerationMotdePasseAphanumerique() {
	 boolean validationMotDePasse = GenerationPassword.validationMotDePasse("aagtyuEry142");
	 assertFalse(validationMotDePasse);	
    }
    
    @Test
    public void testGenerationMotdePasseCaractereSansMajuscules() {
	 boolean validationMotDePasse = GenerationPassword.validationMotDePasse("atg6èçàlp45lp");
	 assertFalse(validationMotDePasse);	
    }
    
    @Test
    public void testGenerationMotdePasseCorrect1() {
	 boolean validationMotDePasse = GenerationPassword.validationMotDePasse("AAAAAAAAA1*p");
	 assertTrue(validationMotDePasse);	
    }
    
    @Test
    public void testGenerationMotdePasseCorrect2() {
	 boolean validationMotDePasse = GenerationPassword.validationMotDePasse("ù*$=)à[()Ab1");
	 assertTrue(validationMotDePasse);	
    }
    
    @Test
    public void testGenerationMotdePasseCorrect3() {
	 boolean validationMotDePasse = GenerationPassword.validationMotDePasse("abcdefghiJ!4");
	 assertTrue(validationMotDePasse);	
    }

}
