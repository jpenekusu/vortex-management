//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.edu.vortex.management.persistence.dao.RoleDAO;
import fr.edu.vortex.management.persistence.dao.RolePermissionDAO;
import fr.edu.vortex.management.utilisateur.pojo.Role;
import fr.edu.vortex.management.utilisateur.verticle.RoleVerticle;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class SupprimerRoleHandlerTest {

    private static final String ADRESSE = "ListerRoleHandlerTest.test";

    Vertx vertx;

    @Mock
    RoleDAO roleDao;

    @Mock
    RolePermissionDAO rolePermDao;

    @InjectMocks
    SupprimerRoleHandler suppRoleHandler;

    @BeforeClass
    public static void before() {
        System.setProperty("hazelcast.logging.type", "sl4j");
        System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
    }

    @Before
    public void setUp(TestContext context) throws Exception {

        vertx = Vertx.vertx();

        suppRoleHandler = SupprimerRoleHandler.create(vertx);
        MockitoAnnotations.initMocks(this);

        vertx.eventBus().<JsonObject>consumer(ADRESSE).handler(suppRoleHandler);
    }

    @After
    public void tearDown(TestContext context) throws Exception {
        vertx.close();
    }

    @Test
    public void inputNotAJsonTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus().consumer(UtilisateurVerticle.LISTER_UTILISATEUR, arMock -> {
            arMock.fail(888, "ce test ne doit dans apeller cette adresse.");
        });

        String input = "ceci n'est pas un json";

        vertx.eventBus().<JsonArray>send(ADRESSE, input, ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {
                ReplyException cause = (ReplyException) ar.cause();
                assertEquals(400, cause.failureCode());
                async.complete();
            }
        });
    }

    @Test
    public void inputMauvaisJsonTest(TestContext context) {
        Async async = context.async();

        JsonObject input = new JsonObject().put("not-a-id", 123);

        vertx.eventBus().<JsonArray>send(ADRESSE, input, ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {
                ReplyException cause = (ReplyException) ar.cause();
                assertEquals(400, cause.failureCode());
                async.complete();
            }
        });
    }

    @Test
    public void roleNotExistTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus().consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, arMock -> {
            arMock.fail(404, "role n'existe pas");
        });

        JsonObject input = new JsonObject().put("id", 123);

        vertx.eventBus().<JsonArray>send(ADRESSE, input, ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {
                ReplyException cause = (ReplyException) ar.cause();
                assertEquals(404, cause.failureCode());
                async.complete();
            }
        });
    }

    @Test
    public void roleSystemeTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus().consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, arMock -> {
            Role role = new Role(2, "role de test", true, null);
            arMock.reply(role.toJson());
        });

        JsonObject input = new JsonObject().put("id", 123);

        vertx.eventBus().<JsonArray>send(ADRESSE, input, ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {
                ReplyException cause = (ReplyException) ar.cause();
                assertEquals(403, cause.failureCode());
                async.complete();
            }
        });
    }

    @Test
    public void roleLieAUtilisateurTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus().consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, arMockChercherRole -> {
            Role role = new Role(2, "role de test", false, null);
            arMockChercherRole.reply(role.toJson());
        });

        vertx.eventBus().consumer(UtilisateurVerticle.LISTER_UTILISATEUR, arMockListUtilisateur -> {
            arMockListUtilisateur.reply(new JsonArray().add("user1"));
        });

        JsonObject input = new JsonObject().put("id", 123);

        vertx.eventBus().<JsonArray>send(ADRESSE, input, ar -> {
            if (ar.succeeded()) {
                context.fail();
            } else {
                ReplyException cause = (ReplyException) ar.cause();
                assertEquals(409, cause.failureCode());
                async.complete();
            }
        });
    }

    @Test
    public void casPassantTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus().consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, arMockChercherRole -> {
            Role role = new Role(2, "role de test", false, null);
            arMockChercherRole.reply(role.toJson());
        });

        vertx.eventBus().consumer(UtilisateurVerticle.LISTER_UTILISATEUR, arMockListUtilisateur -> {
            arMockListUtilisateur.reply(new JsonArray());
        });

        when(roleDao.delete(Mockito.anyInt()))
                .thenReturn(Future.succeededFuture(1));

        when(rolePermDao.delete(Mockito.anyInt()))
                .thenReturn(Future.succeededFuture(1));

        JsonObject input = new JsonObject().put("id", 123);

        vertx.eventBus().<JsonArray>send(ADRESSE, input, ar -> {
            if (ar.succeeded()) {
                async.complete();
            } else {
                context.fail();
            }
        });
    }

    @Test
    public void casThrowErrorTest(TestContext context) {
        Async async = context.async();

        vertx.eventBus().consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, arMockChercherRole -> {
            Role role = new Role(2, "role de test", false, null);
            arMockChercherRole.reply(role.toJson());
        });

        vertx.eventBus().consumer(UtilisateurVerticle.LISTER_UTILISATEUR, arMockListUtilisateur -> {
            arMockListUtilisateur.reply(new JsonArray());
        });

        when(roleDao.delete(Mockito.anyInt()))
                .thenReturn(Future.succeededFuture(1));

        when(rolePermDao.delete(Mockito.anyInt()))
                .thenReturn(Future.failedFuture(new Exception("ceci est une errreur")));

        JsonObject input = new JsonObject().put("id", 123);

        vertx.eventBus().<JsonArray>send(ADRESSE, input, ar -> {
            if (ar.succeeded()) {
                ReplyException cause = (ReplyException) ar.cause();
                context.assertEquals(500, cause.failureCode());
                context.fail();
            } else {
                async.complete();
            }
        });
    }

}
