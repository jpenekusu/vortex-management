//***********************************************************************/
//    This file is part of vortex-management.
//
//    vortex-management is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    vortex-management is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with vortex-management.  If not, see <https://www.gnu.org/licenses/>
//
//***********************************************************************/

package fr.edu.vortex.management.utilisateur.handler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.edu.vortex.management.persistence.dao.UtilisateurDAO;
import fr.edu.vortex.management.utilisateur.pojo.Utilisateur;
import fr.edu.vortex.management.utilisateur.verticle.RoleVerticle;
import fr.edu.vortex.management.utilisateur.verticle.UtilisateurVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class ModifierUtilisateurLocalHandlerTest {

	private static final String ADRESSE = "ModifierUtilisateurLocalHandler.test";

	Vertx vertx;

	private Utilisateur dummyUtilisateur;

	@Mock
	UtilisateurDAO utilisateurDao;

	@InjectMocks
	ModifierUtilisateurLocalHandler handlerUnderTest;

	@BeforeClass
	public static void before() {
		System.setProperty("hazelcast.logging.type", "sl4j");
		System.setProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
	}

	@Before
	public void setUp(TestContext context) throws Exception {

		dummyUtilisateur = new Utilisateur(
				1,
				"test",
				"test",
				"test",
				"test",
				"test",
				12,
				"test",
				Utilisateur.AUTHENTIFICATION_LOCALE,
				false,
				null);

		vertx = Vertx.vertx();

		handlerUnderTest = ModifierUtilisateurLocalHandler.create(vertx);

		MockitoAnnotations.initMocks(this);

		vertx.eventBus().consumer(ADRESSE, handlerUnderTest);
	}

	@After
	public void tearDown(TestContext context) throws Exception {
		vertx.close();
	}

	@Test
	public void inputNotAGoodJson(TestContext context) {
		Async async = context.async();

		JsonObject badInput = new JsonObject()
				.put("cle", "bidon");

		vertx.eventBus().send(ADRESSE, badInput, ar -> {
			if (ar.succeeded()) {
				context.fail();
			} else {
				ReplyException cause = (ReplyException) ar.cause();
				context.assertEquals(400, cause.failureCode());
				async.complete();
			}
		});
	}

	@Test
	public void inputJsonNotGoodRole(TestContext context) {
		Async async = context.async();

		JsonObject badInput = dummyUtilisateur.toJson();
		badInput.put("role", "pas une representation d'un role");

		vertx.eventBus().send(ADRESSE, badInput, ar -> {
			if (ar.succeeded()) {
				context.fail();
			} else {
				ReplyException cause = (ReplyException) ar.cause();
				context.assertEquals(400, cause.failureCode());
				async.complete();
			}
		});
	}

	@Test
	public void modificationOkTest(TestContext context) {
		Async async = context.async();

		vertx.eventBus().consumer(UtilisateurVerticle.LISTER_UTILISATEUR, event -> {
			event.reply(new JsonArray());
		});

		vertx.eventBus().<JsonObject>consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, event -> {
			event.reply("OK");
		});

		when(utilisateurDao.modifierLOCAL(any(Utilisateur.class)))
		.thenReturn(Future.succeededFuture(dummyUtilisateur));

		dummyUtilisateur.setMotDePasse("motDepassedetest32$");
		JsonObject input = dummyUtilisateur.toJson();

		vertx.eventBus().send(ADRESSE, input, ar -> {
			if (ar.succeeded()) {
				JsonObject body = (JsonObject) ar.result().body();
				System.out.println(body.encode());

				async.complete();
			} else {
				ar.cause().printStackTrace();
				context.fail();
			}
		});
	}

	@Test
	public void modificationNOkEmailTest(TestContext context) {
		Async async = context.async();

		vertx.eventBus().consumer(UtilisateurVerticle.LISTER_UTILISATEUR, event -> {
			event.reply(new JsonArray());
		});

		vertx.eventBus().<JsonObject>consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, event -> {
			event.reply("OK");
		});

		when(utilisateurDao.modifierLOCAL(any(Utilisateur.class)))
		.thenReturn(Future.succeededFuture(dummyUtilisateur));

		dummyUtilisateur.setMotDePasse("motDepassedetest32$");
    	dummyUtilisateur.setEmail("");
		JsonObject input = dummyUtilisateur.toJson();

		vertx.eventBus().send(ADRESSE, input, ar -> {
    		if (ar.succeeded()) {
    			context.fail();
    		} else {
    			context.assertEquals(UtilisateurHandler.EMAIL_MANQUANT, ar.cause().getMessage());
       			async.complete();
    		}
		});
	}
	
	@Test
	public void modificationLDAPNOkEmailTest(TestContext context) {
		Async async = context.async();

		vertx.eventBus().consumer(UtilisateurVerticle.LISTER_UTILISATEUR, event -> {
			event.reply(new JsonArray());
		});

		vertx.eventBus().<JsonObject>consumer(RoleVerticle.CHERCHER_ROLE_PAR_ID, event -> {
			event.reply("OK");
		});

		when(utilisateurDao.modifierLOCAL(any(Utilisateur.class)))
		.thenReturn(Future.succeededFuture(dummyUtilisateur));

		dummyUtilisateur.setMotDePasse("motDepassedetest32$");
    	dummyUtilisateur.setEmail("");
    	dummyUtilisateur.setTypeAuthentification(Utilisateur.AUTHENTIFICATION_LDAP);
		JsonObject input = dummyUtilisateur.toJson();

		vertx.eventBus().send(ADRESSE, input, ar -> {
    		if (ar.succeeded()) {
    			context.fail();
    		} else {
    			context.assertEquals(UtilisateurHandler.EMAIL_MANQUANT, ar.cause().getMessage());
       			async.complete();
    		}
		});
	}
}
