FROM java:8
LABEL maintainer "Raphaël Ricordeau"

COPY docker/conf/cluster.xml /opt/cluster.xml
COPY docker/conf/vortex.conf /opt/vortex.conf
COPY docker/conf/logback.xml /opt/logback.xml
COPY target/vortex-management-*.jar /opt/vortex-management.jar
# retrait du dist, il sera porté dans sa propre image docker (avec un tomcat ou autre)
#COPY target/dist/ /opt/dist/

ARG cluster_port
ARG cluster_group
ARG management_port


EXPOSE 8081
CMD ["java",\
    "-Dhazelcast.logging.type=slf4j",\
    "-Dlogback.configurationFile=/opt/logback.xml",\
    "-Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory",\
    "-Djava.util.logging.config.file=/usr/java/default/jre/lib/logging.properties",\
    "-Dvertx.hazelcast.config=/opt/cluster.xml",\
    "-Dfile.encoding=UTF-8",\
    "-Dconf=/opt/vortex.conf",\
    "-Xms512m",\
    "-Xmx2048m",\
    "-jar","/opt/vortex-management.jar",\
    "run","fr.edu.vortex.management.MainVerticle",\
    "--cluster"]
