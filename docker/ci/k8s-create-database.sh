#!/bin/bash

if ! psql -h 172.31.21.185 -p 30629 -U postgres -lqt | cut -d\| -f 1 | grep -qw ${1}; then
    createdb -h 172.31.21.185 -p 30629 -U postgres -O pgiomg ${1}

    psql -h 172.31.21.185 -p 30629 -U postgres -d ${1}  -c "DROP SCHEMA public;"
fi
