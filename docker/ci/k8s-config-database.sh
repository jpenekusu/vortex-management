#!/bin/bash

# XXX: Pbm sur déploiement en parallèle ?
sed -i 's@postgres-develop@172.17.0.8@' docker/conf/vortex.conf
sed -i 's@30629@5432@' docker/conf/vortex.conf
sed -i "s@/omg-po?@/${1}?@" docker/conf/vortex.conf
sed -i 's@currentSchema=core@currentSchema=management@' docker/conf/vortex.conf
sed -i 's@user: "pgiomg"@"username": "pgiomg"@' docker/conf/vortex.conf
sed -i 's@password: ""@password: ""@' docker/conf/vortex.conf

#zip target/vortex-core-1.0.0-SNAPSHOT-fat.jar docker/conf/vortex.conf
