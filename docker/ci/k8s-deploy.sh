#!/bin/bash

unset http_proxy
unset HTTP_PROXY
unset https_proxy
unset HTTPS_PROXY

docker login -u liste.ing-dpn@ac-dijon.fr -p Dijon21/! container-registry.oracle.com
docker build --build-arg cluster_port=${2} --build-arg cluster_group=${1} --build-arg management_port=${3} --pull --rm --tag kuberegistry:5001/${1}vortex-management:latest .
docker push kuberegistry:5001/${1}vortex-management:latest

/usr/local/bin/kubectl --insecure-skip-tls-verify set image deployment/${1}vortex-management ${1}vortex-management=kuberegistry:5001/${1}vortex-management:latest
