FROM boxfuse/flyway:5.2.4
LABEL maintainer="Intégration DIJON"

COPY target/classes/db/migration/V* /flyway/sql/
COPY src/test/resources/db/migration/* /flyway/sql/